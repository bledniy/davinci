<?php declare(strict_types=1);

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPayments extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'user_payments';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }

   public function up(): void
   {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $table->id();
            $this->builder
				->belongsToUser()
				->unsignedInt('payed')
				->unsignedInt('need_pay')
				->unsignedInt('billing_id')
				->createBoolean('is_payed', false)
				->createNullableString('reason')
            ;
            $table->timestamps();
        });
    }

    public function down(): void
	{
        Schema::dropIfExists($this->table);
    }
}
