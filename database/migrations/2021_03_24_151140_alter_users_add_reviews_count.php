<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Builders\Migration\MigrationBuilder;

class AlterUsersAddReviewsCount extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

    public function __construct()
    {
      $this->builder = app(MigrationBuilder::class);
    }


    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $this->builder->setTable($table);
			$table->unsignedInteger('reviews_count')->default(0);
			$table->unsignedInteger('customer_reviews_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
