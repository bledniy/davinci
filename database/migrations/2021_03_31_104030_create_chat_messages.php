<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatMessages extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $table = 'chat_messages';

	private $foreignKey = 'chat_id';

	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}


	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);
			$table->id();

			$table->unsignedBigInteger($this->foreignKey);
			$this->builder->addForeign($this->foreignKey,'chats');
			$this->builder
				->createBoolean('is_watched', false)
				->createType()
				->createNullableString('message', 3000)
				->belongsToUser('user_id', true)
			;
			$table->timestamps();
		});

	}


	public function down()
	{
		Schema::dropIfExists($this->table);
	}
}
