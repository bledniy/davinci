<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPremiumTable extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'user_premiums';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }

   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $table->id();
            $table->unsignedBigInteger('premium_id');
            $this->builder->belongsToUser();
			$table->dateTime('expires_at')->nullable();
            $table->timestamps();
        });

    }


    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
