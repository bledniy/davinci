<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiaTable extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $table = 'premiums';


	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}

	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);
			$table->id();
			$this->builder->createType()->createName();
			$table->unsignedInteger('price')->default(0);
			$table->timestamps();
		});

	}


	public function down()
	{
		Schema::dropIfExists($this->table);
	}
}
