<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotificationConfigs extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $table = 'user_notification_configs';

	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}


	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);
			$this->builder->belongsToUser();
			$table->unsignedBigInteger('notification_config_id');
			$table->primary(['user_id', 'notification_config_id']);
		});

	}


	public function down()
	{
		Schema::dropIfExists($this->table);
	}
}
