<?php declare(strict_types=1);

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCitiesAddUrl extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}


	public function up()
	{
		Schema::table('cities', function (Blueprint $table) {
			$this->builder->setTable($table);
			$this->builder->createUniqueUrl();
			//
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cities', function (Blueprint $table) {
			$table->dropColumn('url');
		});
	}
}
