<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerformerCategories extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'performer_categories';


   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }

   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $this->builder
                ->belongsToUser()
                ->belongsToCategory()
            ;
            $table->primary(['user_id', 'category_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
