<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$this->builder->setTable($table);
			$table->bigIncrements('id');
			$table->string('email')->unique()->nullable();
			$table->string('phone')->unique()->nullable();
			$table->timestamp('email_verified_at')->nullable();
			$table->timestamp('phone_verified_at')->nullable();
			$table->timestamp('authenticated_at')->nullable();
			$table->dateTime('last_seen_at')->nullable();
			$table->string('password');
			$this->builder->createActive();
			$table->ipAddress('last_login_ip')->nullable();
			$table->string('user_agent')->nullable();
			$table->rememberToken();
			$this->addPersonColumns($table);
			$table->char('slug')->nullable()->index();

			$table->integer('balance')->default(0);
			$table->index(['city_id', 'active', 'rating', 'last_seen_at']);

			$table->timestamps();
		});
	}

	private function addPersonColumns(Blueprint $table)
	{
		$this->builder
			->createNullableChar('language_id')
			->createNullableChar('gender')
			->createNullableChar('name')
			->createNullableChar('surname')
			->createNullableChar('patronymic')
			->getSelf($table->dateTime('date_birth')->nullable())
			->createNullableString('avatar', 1000)
			->createNullableChar('phone_second')
			->createNullableText('about')
			->createNullableChar('type')
			->createNullableString('profession')
			->getSelf($table->unsignedBigInteger('city_id')->nullable())
			->createFloat('rating')
			//
			->createNullableText('customer_about')
			->createFloat('customer_rating')
		;
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
