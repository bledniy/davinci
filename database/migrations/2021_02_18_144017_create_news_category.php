<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsCategory extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'news_categories';

   private $foreignKey = 'news_category_id';

   private $tableLang = 'news_category_langs';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }

   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->id();
            $this->builder->createSort();
            $table->timestamps();
        });


        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->builder->setTable($table);
            $table->unsignedBigInteger($this->foreignKey);

            $this->builder->createName()->createLanguageKey();
            $table->foreign($this->foreignKey)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
