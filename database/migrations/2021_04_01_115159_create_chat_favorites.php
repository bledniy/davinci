<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatFavorites extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'chat_favorites';

   private $foreignKey = 'chat_id';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }
   	
   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);
			$table->unsignedBigInteger($this->foreignKey);
			$this->builder->addForeign($this->foreignKey,'chats');
            $this->builder
				->belongsToUser()
            ;
        });
    }


    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
