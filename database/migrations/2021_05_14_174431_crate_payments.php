<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CratePayments extends Migration
{

	private $table = 'payments';
	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $model = \App\Models\Payment::class;
	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}

	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);

			$table->id();
			$table->nullableMorphs('paymentable');
			$this->builder
				->createNullableChar('payment_status')
				->createNullableString('payment_link')
				->createIntPrice()
				->unsignedInt('payed')
				->createNullableChar('destination')
			;
			$table->boolean('is_payed')->default(false);
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
