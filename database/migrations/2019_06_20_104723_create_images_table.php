<?php

	use App\Builders\Migration\MigrationBuilder;
	use App\Traits\Migrations\MigrationCreateFieldTypes;
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateImagesTable extends Migration
	{

		use MigrationCreateFieldTypes;

		public function __construct()
		{
			$this->builder = app(MigrationBuilder::class);
		}

		public function up()
		{
			Schema::create('images', function (Blueprint $table) {
				$this->builder->setTable($table);
				$table->bigIncrements('id');
				$table->nullableMorphs('imageable');
				//
				$this->builder
					->createActive()
					->createName()
					->createImage()
					->createSort()
					->createNullableChar('type')
				;
				$table->timestamps();
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('images');
		}
	}
