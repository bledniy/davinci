<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'orders';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }


   public function up()
    {
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);

			$table->id();
			$this->builder
				->belongsToCategory('category_id', true)
				->belongsToUser('performer_id', true)
				->belongsToUser('customer_id', true)
				->createTitle(1000)
				->createNullableString('description', 3000)
				->unsignedBigInt('price')
				->unsignedBigInt('views')
				->unsignedBigInt('city_id')
				->createActive()
				->createUrl(true, 'slug')
				->createBoolean('can_online', false)
				->smallInt('bids_count')
				->createNullableDateTime('date_completion')
				->createBoolean('is_closed', false)
				->getSelf($table->dateTime('closes_at')->nullable())
				->getSelf($table->dateTime('published_at')->nullable()->comment('for display date publish, because publishing can diff with date create'))
			;
			$table->tinyInteger('available_edits')->default(3)->unsigned();
			$table->index(['city_id', 'category_id', 'is_closed', 'published_at']);
			$table->timestamps();
		});
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
