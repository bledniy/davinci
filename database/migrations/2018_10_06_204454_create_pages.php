<?php

	use App\Builders\Migration\MigrationBuilder;
	use App\Traits\Migrations\MigrationCreateFieldTypes;
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreatePages extends Migration
	{

		/**
		 * @var MigrationBuilder
		 */
		private $builder;

		protected $table = 'pages';
		protected $foreignKey = 'page_id';
		protected $tableLang = 'page_langs';

		public function __construct()
		{
			$this->builder = app(MigrationBuilder::class);
		}
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create($this->table, function (Blueprint $table) {
				$this->builder->setTable($table);
				$table->id();
				$this->builder
					->createSort()
					->createUrl()
					->createActive()
					->createImage()
					->createImage('sub_image')
					->createNullableChar('page_type')
					->createNullableText('options')
				;
				$table->timestamps();

				$table->index('id');
			});
			//
			Schema::create($this->tableLang, function (Blueprint $table) {
				$this->builder->setTable($table);
				$table->unsignedBigInteger($this->foreignKey)->unsigned();
				$this->builder
					->addForeign($this->foreignKey, $this->table)
					->createLanguageKey()
					->createName()
					->createTitle()
					->createDescription()
					->createExcerpt()
					->createNullableChar('sub_title')
					->createNullableText('sub_description')
				;

			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists($this->tableLang);
			Schema::dropIfExists($this->table);
		}
	}
