<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Builders\Migration\MigrationBuilder;

class AlterVerificationsAddVerifiedAt extends Migration
{

	public function up()
	{
		Schema::table('verifications', function (Blueprint $table) {
			$table->dateTime('verified_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('verifications', function (Blueprint $table) {
			$table->dropColumn('verified_at');
		});
	}
}
