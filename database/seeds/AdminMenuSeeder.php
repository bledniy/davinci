<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Admin\AdminMenu;
use App\Repositories\Admin\AdminMenuRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class AdminMenuSeeder extends AbstractSeeder
{
	private $adminMenuRepository;

	private $menusByUrl;

	public function __construct(AdminMenuRepository $adminMenuRepository)
	{
		$this->adminMenuRepository = $adminMenuRepository;
		$this->menusByUrl = $this->adminMenuRepository->all()->keyBy('url');
		$this->reguard();
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$menus = [
			$this->getBuilder()->setName(__('modules.settings.title'))->setIconFont('<i class="material-icons">settings</i>')->setUrl('/settings')->setGateRule('view_settings')->build(),
			$this->getBuilder()->setName('Локализация')->setIconFont('<i class="fa fa-language" aria-hidden="true"></i>')->setUrl('/translate')->setGateRule('view_translate')->setChildrens([
				$this->getBuilder()->setName(__('modules.notification_config.title'))->setUrl('/notifications/config')->build(),
			])->build(),
			$this->getBuilder()->setName('SEO')->setIconFont('<i class="fa fa-google-plus" aria-hidden="true"></i>')->setUrl('/meta')->setGateRule('view_meta')->setChildrens([
				$this->getBuilder('/redirects')->setName('Redirects')->setIconFont('<i class="fa fa-arrow-right" aria-hidden="true"></i>')->setGateRule('view_redirects')->build(),
				$this->getBuilder('/sitemap')->setName('Sitemap')->setIconFont('<i class="fa fa-sitemap" aria-hidden="true"></i>')->build(),
				$this->getBuilder('/robots')->setName('Robots.txt')->setIconFont('<i class="fa fa-android" aria-hidden="true"></i>')->setGateRule('view_robots')->build(),
			])->build(),
			$this->getBuilder('/users')->setName('Пользователи')->setIconFont('<i class="fa fa-user" aria-hidden="true"></i>')->setGateRule('view_users')->build(),
			$this->getBuilder('roles')->setName('Роли')->setIconFont('<i class="fa fa-users" aria-hidden="true"></i>')->setGateRule('view_roles')->build(),
			$this->getBuilder('/admin-menus')->setName('Админ меню')->setIconFont('<i class="fa fa-bars" aria-hidden="true"></i>')->setGateRule('view_admin-menus')->build(),
//			$this->getBuilder('/menu')->setName('Меню')->setIconFont('<i class="fa fa-bars" aria-hidden="true"></i>')->setGateRule('view_menu')->build(),
			$this->getBuilder('/logs')->setName('Logs')->setIconFont('<i class="fa fa-history" aria-hidden="true"></i>')->setGateRule('view_logs')->setSort(20)->build(),
			$this->getBuilder('/cities')->setName(__('modules.cities.title_plural'))->setGateRule('view_cities')->build(),
			$this->getBuilder()->setName(__('modules.news.title_plural'))->setUrl('/news')->setGateRule('view_news')->setChildrens([
				$this->getBuilder()->setName(__('modules.news-categories.title'))->setUrl('/news/categories')->setGateRule('view_news')->build(),
			])->build(),
			$this->getBuilder()->setName(__('modules.feedback.title_plural'))->setIconFont('<i class="fa fa-commenting" aria-hidden="true"></i>')->setUrl('/feedback')->setGateRule('view_feedback')
				->setChildrens([
					$this->getBuilder()->setName('Ошибки')->setIconFont('<i class="fa fa-bug" aria-hidden="true"></i>')->setUrl('/feedback/report')->setGateRule('view_feedback')->build(),
				])->build(),
			$this->getBuilder()->setName('F.A.Q.')->setUrl('/faq')->setGateRule('view_faq')->build(),
			$this->getBuilder()->setName('Страницы')->setUrl('/pages')->setGateRule('view_pages')->setIconFont('<i class="fa fa-columns" aria-hidden="true"></i>')->setChildrens([
				$this->getBuilder()->setName('О нас')->setUrl('/pages/1/edit')->setGateRule('view_pages')->setChildrens([
					$this->getBuilder()->setName('Команда')->setUrl('/team')->setGateRule('view_team')->build(),
				])->build(),
				$this->getBuilder()->setName('Оферта')->setUrl('/pages/3/edit')->setGateRule('view_pages')->build(),
				$this->getBuilder()->setName('Политика конфиденциальности')->setUrl('/pages/4/edit')->setGateRule('view_pages')->build(),
				$this->getBuilder()->setName('Оплата и возврат')->setUrl('/pages/6/edit')->setGateRule('view_pages')->build(),

				$this->getBuilder()->setName('Для исполнителя')->setUrl('/pages/5/edit')->setGateRule('view_pages')->setChildrens([
					$this->getBuilder()->setName('Как это работает')->setUrl('/how-performer')->setGateRule('view_how-performer')->build(),
					$this->getBuilder()->setName('Вам понадобится')->setUrl('/need')->setGateRule('view_need')->build(),
					$this->getBuilder()->setName('Услуги')->setUrl('/performer-services')->setGateRule('view_performer-services')->build(),
				])->build(),
				$this->getBuilder()->setName('Главная')->setUrl('/pages/2/edit')->setGateRule('view_pages')->setChildrens([
					$this->getBuilder()->setName('Нижние блоки')->setUrl('/main-bottom')->setGateRule('view_main-bottom')->build(),
					$this->getBuilder()->setName('Как это работает')->setUrl('/how')->setGateRule('view_how')->build(),
					$this->getBuilder()->setName('Услуги')->setUrl('/main-services')->setGateRule('view_main-services')->build(),
				])->build(),
			])->build(),

			$this->getBuilder()->setName('Кабинет пользователя')->setUrl('#')->setGateRule('')->setChildrens([
				$this->getBuilder()->setName(__('modules.certifications.menu_title'))->setUrl('/certifications')->setContentProvider('')->setGateRule('view_certifications')->build(),
			])->build(),
			$this->getBuilder('reviews')->setName('Отзывы')->setGateRule('view_reviews')->setIconFont('<i class="fa fa-comments" aria-hidden="true"></i>')->build(),
			$this->getBuilder('premiums')->setName(__('modules.premiums.title'))->setGateRule('view_premiums')->setIconFont('<i class="fa fa-diamond" aria-hidden="true"></i>')->build(),
			$this->getBuilder('orders')->setName(__('modules.orders.title'))->setGateRule('view_orders')->setIconFont('')->build(),

		];
		$this->loop($menus);

		Artisan::call('cache:clear');
	}

	private function loop(array $menus, ?AdminMenu $parentMenu = null)
	{
		foreach ($menus as $menu) {
			$menuModel = $this->createMenu($menu, $parentMenu);
			if ($menuModel && Arr::get($menu, 'childrens')) {
				$this->loop(\Arr::wrap(Arr::get($menu, 'childrens')), $menuModel);
			}
		}
	}

	private function createMenu(array $menu, ?AdminMenu $parentMenu = null): ?AdminMenu
	{
		if ($this->isMenuExistsByUrl($menu['url'] ?? '')) {
			return $this->getMenuExistsByUrl($menu['url'] ?? '');
		}
		$menu = array_merge(['active' => 1], $menu);
		if ($parentMenu) {
			$menu['parent_id'] = $parentMenu->getKey();
		}

		return tap($this->adminMenuRepository->create($menu), function ($adminMenu) {
			$this->onCreated($adminMenu);
		});
	}

	private function onCreated(AdminMenu $adminMenu)
	{

	}

	private function isMenuExistsByUrl(?string $url)
	{
		return $this->menusByUrl->offsetExists($url);
	}

	private function getMenuExistsByUrl(?string $url)
	{
		return $this->menusByUrl->get($url);
	}

	private function getBuilder($url = '')
	{
		$builder = new class implements \ArrayAccess {
			private $active = true;

			private $url = '';

			private $gateRule = '';

			private $sort = 0;

			private $name = '';

			private $iconFont = '';

			private $contentProvider = '';

			private $childrens = [];

			public function setName(string $_): self
			{
				$this->name = $_;
				return $this;
			}

			/**
			 * @param string $url
			 */
			public function setUrl(string $url): self
			{
				$this->url = $url;
				return $this;
			}

			/**
			 * @param string $_
			 * @return
			 */
			public function setGateRule(string $_): self
			{
				$this->gateRule = $_;
				return $this;
			}

			public function setSort(int $_): self
			{
				$this->sort = $_;
				return $this;
			}

			/**
			 * @param string $_
			 */
			public function setIconFont(string $_): self
			{
				$this->iconFont = $_;
				return $this;
			}

			/**
			 * @param bool $active
			 */
			public function setActive(bool $active): self
			{
				$this->active = $active;
				return $this;
			}

			/**
			 * @param array $_
			 */
			public function setChildrens(array $_): self
			{
				$this->childrens = $_;
				return $this;
			}

			public function build()
			{
				$menu = [
					'active'           => (int)$this->active,
					'url'              => $this->url,
					'gate_rule'        => $this->gateRule,
					'name'             => $this->name,
					'sort'             => $this->sort,
					'icon_font'        => $this->iconFont,
					'content_provider' => $this->contentProvider,
				];
				if ($this->childrens) {
					$menu['childrens'] = $this->childrens;
				}
				return $menu;
			}

			public function offsetExists($offset)
			{
				return property_exists($this, $offset);
			}

			public function offsetGet($offset)
			{
				return $this->offsetExists($offset) ? $this->{$offset} : null;
			}

			public function offsetSet($offset, $value)
			{
				$method = Str::camel('set' . ucfirst($offset));
				if (method_exists($this, $method)) {
					$this->$method($value);
				}
			}

			public function offsetUnset($offset)
			{
				if ($this->offsetExists($offset)) {
					$this->{$offset} = null;
				}
			}

			/**
			 * @param string $contentProvider
			 * @return
			 */
			public function setContentProvider(string $contentProvider): self
			{
				$this->contentProvider = $contentProvider;
				return $this;
			}

		};
		return $builder->setUrl($url);
	}
}
