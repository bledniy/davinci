<?php

namespace Database\Seeders;

use App\Repositories\PremiumRepository;

class PremiumSeeder extends AbstractSeeder
{
	/**
	 * @var PremiumRepository
	 */
	private $premiumRepository;

	public function __construct(PremiumRepository $premiumRepository)
	{
		$this->premiumRepository = $premiumRepository;
	}

	public function run()
	{
		$premiums = [
			['price' => 200, 'type' => null],
		];
		foreach ($premiums as $premium) {
			if ($this->premiumRepository->isExistsByType($premium['type'] ?? null)) {
				continue;
			}
			$this->premiumRepository->create($premium);
		}
	}
}
