<?php
	namespace Database\Seeders;

use App\Repositories\Admin\LanguageRepository;
use Illuminate\Database\Seeder;

class LanguageTableSeeder extends AbstractSeeder
{

	/**
	 * @var LanguageRepository
	 */
	private $languageRepository;

	public function __construct(LanguageRepository $languageRepository)
	{
		$this->languageRepository = $languageRepository;
	}

	public function run()
	{
		$languages = [
			[
				'name'    => 'Українська',
				'key'     => 'uk',
				'active'  => 1,
				'default' => 1,
			],
			[
				'name'    => 'Русский',
				'key'     => 'ru',
				'active'  => 1,
				'default' => 0,
			],
			[
				'name'    => 'English',
				'key'     => 'en',
				'active'  => 1,
				'default' => 0,
			],
		];
		foreach ($languages as $language) {
			$this->languageRepository->create($language);
		}

	}
}
