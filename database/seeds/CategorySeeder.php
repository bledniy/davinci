<?php

namespace Database\Seeders;

use App\Models\Category\Category;
use App\Models\Category\CategoryLang;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var CategoryRepository
	 */
	private $repository;

	public function __construct(\Faker\Generator $factory, CategoryRepository $repository)
	{
		$this->factory = $factory;
		$this->repository = $repository;
	}

	public function run()
	{
		foreach ($this->getCategories() as $item) {
			if ($this->repository->joinLang()->where('name', $item)->count()) {
			    continue;
			}
			$this->repository->create(['name' => $item]);
		}
	}

	private function getCategories()
	{
		return [
			'Юрист',
			'Адвокат',
			'Нотариус',
		];
	}
}
