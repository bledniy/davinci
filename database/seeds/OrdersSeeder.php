<?php

namespace Database\Seeders;

use App\Models\Category\Category;
use App\Models\User;
use App\Repositories\CategoryRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
	 */
	private $users;
	/**
	 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
	 */
	private $categories;

	public function __construct(
		\Faker\Generator $factory,
		OrderRepository $orderRepository,
		UserRepository $userRepository,
		CategoryRepository $categoryRepository
	)
	{
		$this->factory = $factory;
		$this->orderRepository = $orderRepository;
		$this->users = $userRepository->all()->keyBy('id');
		$this->categories = $categoryRepository->all()->keyBy('id');
	}

	public function run()
	{
		return false ;

		$batch = [];
		foreach (range(1, env('DEV_SEED') ? 100000 : 10) as $i) {
			$performerId = $this->randomUser()->getKey();
			$customerId = $this->randomUser()->getKey();
			$categoryId = $this->randomCategory()->getKey();
			$batch[] = $order = [
				'performer_id' => $performerId,
				'customer_id'  => $customerId,
				'category_id'  => $categoryId,
				'description'  => '....',
				'title'        => $this->factory->jobTitle,
				'price'        => $i,
			];
		}
		foreach (array_chunk($batch, 10000) as $chunk) {
			$this->orderRepository->insert($chunk);
		}
	}


	private function randomUser(): User
	{
		static $count;
		if (null === $count) {
			$count = $this->users->count();
		}
		return $this->users->get(random_int(1, $count));
	}

	private function randomCategory(): Category
	{
		static $count;
		if (null === $count) {
			$count = $this->categories->count();
		}
		return $this->categories->get(random_int(1, $count));
	}
}
