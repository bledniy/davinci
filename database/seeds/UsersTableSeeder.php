<?php

namespace Database\Seeders;

use App\Models\City\City;
use App\Models\Temporary;
use App\Platform\Contract\UserTypeContract;
use App\Repositories\CityRepository;
use App\Repositories\UserRepository;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends AbstractSeeder
{
	/**
	 * @var UserRepository
	 */
	private $repository;
	/**
	 * @var \Faker\Generator
	 */
	protected $factory;

	private $cities;

	public function __construct(UserRepository $repository, \Faker\Generator $factory, CityRepository $cityRepository)
	{
		$this->repository = $repository;
		$this->factory = $factory;
		$this->cities = $cityRepository->all()->keyBy('id');
	}

	public function run()
	{
		try{
			$this->required();
		} catch (\Throwable $e){
		}

		$users = [];
		$now = now();
		$range = range(1, env('DEV_SEED') ? 2000 : 100);
		foreach ($range as $item) {
			$user = $this->default();
			$user['last_seen_at'] = $now;
			$users[] = $user;
			if ($item % 1000 === 0) {
				dump($item);
			}
		}
		foreach (array_chunk($users, 2000) as $num => $chunk) {
			try {
				$this->repository->insert($chunk);
				dump(sprintf('Users chunk #%s', $num));
			} catch (\Throwable $e) {
				dump(sprintf('FAIL Users chunk #%s', $num), Str::limit($e->getMessage(), 100));
			}
		}
	}

	private function required()
	{
		try {
			/** @var  $performer User */
			$performer = $this->repository->updateOrCreate(array_merge($this->default(), [
				'email' => 'performer@qq.com',
				'phone' => '+380999999997',
				'type'  => UserTypeContract::TYPE_PERFORMER,
			]), []);
			$performer->tokens()->create([
				'name'      => $performer->name,
				'token'     => hash('sha256', 'Z2IY8r4InPuxsEpAspelLRERzwMkfA4JnRuuF1KA'),
				'abilities' => ['*'],
			])
			;
			Temporary::create(['key' => 'BchfMlOQskgjKWsCiRGgHMFpXHVO0O0dPR9Gilq840pIStqqJPlso31Om9o446SY']);
			$customer = $this->repository->updateOrCreate(array_merge($this->default(), [
				'email' => 'customer@qq.com',
				'phone' => '+380999999998',
			]), []);

			$customer->tokens()->create([
				'name'      => $customer->name,
				'token'     => hash('sha256', '123'),
				'abilities' => ['*'],
			])
			;
		} catch (\Throwable $e) {
			app(\App\Helpers\Debug\LoggerHelper::class)->error($e);
		}
	}

	private function default()
	{
		[$name, $surname, $patronymic] = explode(' ', $this->factory->name());
		return [
			'name'       => $name,
			'surname'    => $surname,
			'patronymic' => $patronymic,
			'email'      => Str::random(8) . $this->factory->email,
			'phone'      => $this->generatePhone(),
			'password'   => $this->password(),
			'rating'     => $this->factory->randomFloat(2, 1, 5),
			'about'      => $this->factory->text(),
			'city_id'    => $this->randomCity()->getKey(),
			'avatar'     => $this->image(),
			'type'       => UserTypeContract::TYPE_CUSTOMER,
			'slug'       => Str::slug(Str::random(6) . '-'. implode('-', [$name, $surname, $patronymic])),
		];
	}

	private function password()
	{
		static $pass;
		if (null === $pass) {
			$pass = Hash::make('qweqweqwe');
		}
		return $pass;
	}

	protected $phones = [];

	protected function generatePhone()
	{
		$phone = random_int(10000000, 1000000000);
		if (isset($this->phones['phone'])) {
			return $this->generatePhone();
		}
		$this->phones[$phone] = $phone;
		return $phone;
	}

	protected function randomCity(): City
	{
		static $count;
		if (null === $count) {
			$count = $this->cities->count();
		}
		return $this->cities->get(random_int(1, $count));
	}

}
