<?php

namespace Database\Seeders;

use App\Repositories\NewsCategoryRepository;

class NewsCategorySeeder extends AbstractSeeder
{

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var NewsCategoryRepository
	 */
	private $repository;

	public function __construct(\Faker\Generator $factory, NewsCategoryRepository $repository)
	{
		$this->factory = $factory;
		$this->repository = $repository;
	}


	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$cat = [
			['name' => 'Юристы'],
			['name' => 'Адвокаты'],
			['name' => 'Нотариус'],
		];
		foreach ($cat as $item) {
			$this->repository->create($item);
		}
	}
}
