<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\City\City;
use App\Repositories\CityRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CitySeeder extends Seeder
{

	private $cityRepository;

	public function __construct(CityRepository $cityRepository)
	{
		$this->cityRepository = $cityRepository;
	}

	public function run(): void
	{
		foreach ($this->getFormattedCities() as $formattedCity) {
			$this->cityRepository->create($formattedCity);
		}
	}

	protected function getFormattedCities(): array
	{
		$now = now();
		$cb = static function ($city) use ($now) {
			return ['name' => $city, 'created_at' => $now, 'updated_at' => $now, 'url' => Str::slug($city)];
		};
		return array_map($cb, $this->getCities());
	}

	protected function getCities(): array
	{
		return [
			'Александрия',
			'Белая Церковь',
			'Белгород-Днестровский',
			'Бердичев',
			'Бердянск',
			'Березань',
			'Борисполь',
			'Боярка',
			'Бровары',
			'Буча',
			'Васильков',
			'Винница',
			'Вишнёвое',
			'Вышгород',
			'Днепр (Днепропетровск)',
			'Дрогобыч',
			'Житомир',
			'Запорожье',
			'Ивано-Франковск',
			'Измаил',
			'Ирпень',
			'Каменец-Подольский',
			'Каменское (Днепродзержинск)',
			'Киев',
			'Ковель',
			'Конотоп',
			'Краматорск',
			'Кременчуг',
			'Кривой Рог',
			'Кропивницкий (Кировоград)',
			'Лисичанск',
			'Лозовая',
			'Луцк',
			'Львов',
			'Мариуполь',
			'Мелитополь',
			'Мукачево',
			'Нежин',
			'Николаев',
			'Никополь',
			'Новомосковск',
			'Обухов',
			'Одесса',
			'Павлоград',
			'Первомайск',
			'Переяслав-Хмельницкий',
			'Полтава',
			'Прилуки',
			'Ржищев',
			'Ровно',
			'Северодонецк',
			'Славутич',
			'Славянск',
			'Смела',
			'Сумы',
			'Тернополь',
			'Ужгород',
			'Украинка',
			'Умань',
			'Фастов',
			'Харьков',
			'Херсон',
			'Хмельницкий',
			'Червоноград',
			'Черкассы',
			'Чернигов',
			'Черновцы',
			'Черноморск (Ильичевск)',
			'Шостка',
			'Энергодар',
			'Яготин',
		];
	}
}
