<?php

namespace Database\Seeders;

use App\Models\Translate\Translate;
use App\Repositories\TranslateRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TranslateTableSeeder extends Seeder
{
	private $translates;
	/**
	 * @var TranslateRepository
	 */
	private $repository;

	public function __construct(TranslateRepository $repository)
	{
		$this->translates = $repository->all()->keyBy('key');
		$this->repository = $repository;
	}

	public function run()
	{
		$groups = [
			'contacts' => [
				$this->getBuilder('urface.name')->setValue('ФОП Вербецька Т.М.')->build(),
				$this->getBuilder('urface.data')->setValue('ЄДРПОУ 2643219105')->build(),
			],
			'notifications' => [
				$this->getBuilder('readed')->setValue('Уведомления прочитаны')->build(),
				$this->getBuilder('chat.unread-message')->setValue('Новое сообщение в чате %chat% от %username%')->build(),
				$this->getBuilder('news.new')->setValue('Новая запись в блоге')->build(),
				$this->getBuilder('deals.selected-as-performer')->setValue('Вы выбраны исполнителем')->build(),
				$this->getBuilder('deals.selected-as-performer.message')->setValue('Свяжитесь с заказчиком для обсуждения деталей проекта')->build(),
				$this->getBuilder('deals.review.from-customer')->setValue('Заказчик оставил о Вас отзыв')->build(),
				$this->getBuilder('deals.review.from-customer.opposite')->setValue('Закройте сделку чтобы увидеть отзыв заказчика')->build(),
				$this->getBuilder('deals.closed')->setValue('Проект выполнен')->build(),
				$this->getBuilder('deals.bids.new')->setValue('Новые ставки к вашей сделке')->build(),
				$this->getBuilder('deals.bids.new.message')->setValue('Новых ставок: %count%')->build(),
				$this->getBuilder('deals.new')->setValue('Новые сделки')->build(),
				$this->getBuilder('deals.new.message')->setValue('Новых сделок: %count%')->build(),
				$this->getBuilder('pro-account.expiring')->setValue('У вас заканчивается pro аккаунт через')->build(),
				$this->getBuilder('pro-account.expired')->setValue('У вас закончился pro аккаунт')->build(),
				$this->getBuilder('pro-account.expiring.message')->setValue('Продлите подписку, чтобы искать новые проекты')->build(),
			],
			'chat'   => [
				$this->getBuilder('favorites.detached')->setValue('Чат удален из избранного')->build(),
				$this->getBuilder('favorites.attached')->setValue('Чат добавлен в избранное')->build(),
				$this->getBuilder('deal.on-select-performer')->setValue('Я выбрал вас исполнителем для своего проекта')->build(),
			],
			'global' => [
				['key' => 'show-more', 'value' => 'Показать еще'],
				['key' => 'back', 'value' => 'назад'],
				['key' => 'yes', 'value' => 'Да'],
				['key' => 'no', 'value' => 'Нет'],
				['key' => 'download', 'value' => 'Скачать'],
				['key' => '404', 'value' => 'Страница не найдена'],
				['key' => '404-description', 'value' => 'Страница не найдена, попробуйте другой адресс', 'type' => 'textarea'],
				'work-hours' => 'График работы:',
			],

			'forms' => [
				'fio'      => 'ФИО',
				'name'     => 'Имя',
				'phone'    => 'Номер телефона',
				'email'    => 'E-mail',
				'message'  => 'Комментарий',
				'send'     => 'Отправить заявку',
				'region'   => 'Область',
				'district' => 'Район',
				'city'     => 'Город',
				'password' => 'Пароль',
			],
			'users' => [
				$this->getBuilder('email')->setValue('E-mail')->build(),
				$this->getBuilder('fio')->setValue('ФИО')->build(),
			],
			'pages' => [
				'about'    => 'О нас',
				'contacts' => 'Контакты',
			],


			'feedback'      => [
				'throttle-message'                 => 'Отправлять заявку можно не чаще 1 раза в минуту.',
				'send-success'                     => 'Заявка успешно отправлена',
				'send-failed'                      => 'Заявка не была отправлена, произошла ошибка на сервере, пожалуйста попробуйте позже, или позвоните нам по одному из номеров телефона',
				'validation.files-total-size.fail' => 'Общий размер загружаемых файлов не должен превышать 50 Мб',
				'name'                             => 'Имя',
				'email'                            => 'Почта',
				'report_short'                     => 'Вкратце об ошибке',
				'report_text'                      => 'Подробно об ошибке',
			],
			'uploads'       => [
				$this->getBuilder('danger')->setValue('Файлы с расширением %extension% запрещены к загрузке (Файл %file%)')->setVariables(['%file%', '%extension%'])->build(),
			],
			'reviews'       => [
				'title' => 'Отзывы',
			],
			'how-it-works'  => [
				'title'       => 'Как это работает?',
				'description' => 'За создание и поиск работы мы не берём комиссию с заказчика, вы можете попробовать абсолютно бесплатно',
			],
			'for-performer' => [
				'how-it-works.title'       => 'Как начать сотрудничество?',
				'how-it-works.description' => '',
				'need.title'               => 'Вам понадобится',
			],
			'team'          => [
				'title' => 'Наша команда',
			],
			'faq'           => [
				'title' => 'Вопросы и ответы',
			],
			'registration'  => [
				$this->getBuilder('do.registration')->setValue('Зарегистрироваться')->build(),
				$this->getBuilder('success')->setValue('Вы успешно зарегистрированы')->build(),
				$this->getBuilder('fail')->setValue('При регистрации произошла ошибка')->build(),
				$this->getBuilder('step.success')->setValue('Данные сохранены')->build(),
				$this->getBuilder('social.user-found')->setValue('Данные соцсети получены')->build(),
			],
			'auth'          => [
				$this->getBuilder('title')->setValue('Авторизация')->build(),
				$this->getBuilder('success')->setValue('Вы успешно авторизированы')->build(),
				$this->getBuilder('description')->setValue('Введите номер телефона и пароль, который вы вводили при регистрации.')->build(),
				$this->getBuilder('login.enter')->setValue('Войти')->build(),
				$this->getBuilder('recover.forget')->setValue('Забыли пароль?')->build(),
				$this->getBuilder('social.auth.fail')->setValue('Ошибка авторизации соцсети %social%, пожалуйста попробуйте другую соцсеть, или пройдите регистрацию')->build(),
				$this->getBuilder('social.auth.user-not-found')->setValue('Нет пользователя с этой почтой. Хотите зарегистрироваться под ней?')->build(),
				$this->getBuilder('logout.fail')->setValue('Вы не авторизированы')->build(),
				$this->getBuilder('logout.success')->setValue('Вы успешно вышли из аккаунта')->build(),
			],
			'validation'    => [
				$this->getBuilder('login.not-exists')->setSubGroup('login')->setValue('Email не найден')->build(),
				$this->getBuilder('login.success')->setSubGroup('login')->setValue('Email введен правильно')->build(),
				$this->getBuilder('login.credentials.wrong')->setSubGroup('login')->setValue('Не правильный пароль')->build(),
			],
			'cabinet'       => [
				$this->getBuilder('certification.upload.success')->setSubGroup('certification')->setValue('Сертификат загружен')->build(),
				$this->getBuilder('certification.upload.unavailable')->setSubGroup('certification')->setValue('Загрузка сертификата не доступна, ваша заявка уже рассмотрена')->build(),
				$this->getBuilder('certification.upload.confirm')->setSubGroup('certification')->setValue('Подтвердить сертификацию')->build(),
				$this->getBuilder('services.owner.fail')->setSubGroup('skills')->setValue('Данная услуга вам не принадлежит')->build(),
				$this->getBuilder('services.delete.success')->setSubGroup('skills')->setValue('Услуга удалена')->build(),
				$this->getBuilder('services.update.success')->setSubGroup('skills')->setValue('Услуга успешно обновлена')->build(),
				$this->getBuilder('services.create.success')->setSubGroup('skills')->setValue('Услуга успешно создана')->build(),
				$this->getBuilder('diploma.delete.success')->setSubGroup('diploma')->setValue('Сертификат/диплом удален')->build(),
				$this->getBuilder('diploma.update.success')->setSubGroup('diploma')->setValue('Сертификат/диплом успешно обновлен')->build(),
				$this->getBuilder('diploma.create.success')->setSubGroup('diploma')->setValue('Сертификат/диплом успешно добавлен')->build(),
				$this->getBuilder('password.current-invalid')->setValue('Текущий пароль аккаунта введен не верно')->build(),
				$this->getBuilder('password.credentials-updated')->setValue('Пароль аккаунта успешно изменен')->build(),
				$this->getBuilder('phone.changed')->setValue('Номер телефона изменен')->build(),
				$this->getBuilder('phone.change.fail')->setValue('Сначала подтвердите код из смс')->build(),
				$this->getBuilder('email.changed')->setValue('E-mail изменен')->build(),
				$this->getBuilder('email.change.fail')->setValue('Сначала подтвердите код из письма')->build(),
			],

			'performers'   => [
				$this->getBuilder('services')->setValue('Услуги')->build(),
				$this->getBuilder('diplomas')->setValue('Сертификаты и дипломы')->build(),
			],
			'verification' => [
				$this->getBuilder('code')->setValue('Код подтверждения')->build(),
				$this->getBuilder('code-resend-limit')->setValue('Повторная отправка кода доступна раз в минуту')->build(),
				$this->getBuilder('code-resend')->setValue('Отправить повторно')->build(),
				$this->getBuilder('code-resend-after')->setValue('Отправить код повторно через')->build(),
				$this->getBuilder('code-non-equals')->setValue('Не правильный код верификации')->build(),
				$this->getBuilder('sms.code-send')->setValue('На номер %phone% мы отправили 4х значный код, введите его ниже')->setVariables(['phone' => 'Номер телефона'])->build(),
				$this->getBuilder('sms.code-resend-sended')->setValue('Код был повторно отправлен на номер %phone%')->setVariables(['phone' => 'Номер телефона'])->build(),
				$this->getBuilder('sms.code-from')->setValue('Код из смс')->build(),
				$this->getBuilder('email.code-send')->setValue('На почту %email% мы отправили 4х значный код, введите его ниже')->build(),
				$this->getBuilder('email.code-resend-sended')->setValue('Код был повторно отправлен на почту %email%')->build(),
				$this->getBuilder('email.code-from')->setValue('Код из почты')->build(),
			],
			'messages'     => [
				'cabinet.personal.success' => 'Данные успешно обновлены',
			],
			'payments'     => [
				$this->getBuilder('premium.description')->setValue('Покупка премиум аккаунта')->build(),
			],
			'order'        => [
				$this->getBuilder('list')->setValue('Сделки')->build(),
				$this->getBuilder('list.onpage')->setValue('Сделок на странице')->build(),
				$this->getBuilder('show')->setValue('Просмотр сделки')->build(),
				$this->getBuilder('create')->setValue('Создание сделки')->build(),
				$this->getBuilder('create.success')->setValue('Сделка успешно создана')->build(),
				$this->getBuilder('create.fail')->setValue('Ошибка при создании сделки')->build(),
				$this->getBuilder('create.need-auth')->setValue('Ваша сделка будет опубликована после регистрации(или авторизации если вы уже зарегистрированы)')->build(),
				$this->getBuilder('update.fail')->setValue('Ошибка при редактировании сделки')->build(),
				$this->getBuilder('update.fail.deal-is-close')->setValue('Сделка закрыта')->build(),
				$this->getBuilder('update.fail.limit-edit-exceeded')->setValue('Вы исчерпали лимит редактирования')->build(),
				$this->getBuilder('update.fail.has-selected-performer')->setValue('Нельзя редактировать сделку после выбора исполнителя')->build(),
				$this->getBuilder('update.success')->setValue('Сделка успешно отредактирована')->build(),
				$this->getBuilder('files.delete.success')->setValue('Файл %name% удален')->build(),

				$this->getBuilder('more-deals')->setValue('Больше заказов')->build(),
				$this->getBuilder('more-details')->setValue('Подробнее')->build(),
				$this->getBuilder('unauth.expired')->setValue('Заказ не найден, пожалуйста создайте его заново')->build(),
				$this->getBuilder('need-verification')->setValue('Пожалуйста сначала подтвердите свои навыки в личном кабинете')->build(),
				$this->getBuilder('bids.add.fail.add-to-own')->setValue('Нельзя оставить заявку к своей сделке')->build(),
				$this->getBuilder('bids.add.fail.deal-is-closed')->setValue('Сделка закрыта')->build(),
				$this->getBuilder('bids.add.fail.performer-is-selected')->setValue('Исполнитель уже выбран')->build(),
				$this->getBuilder('bids.add.fail.already-have')->setValue('Можно оставлять только одну заявку к сделке')->build(),
				$this->getBuilder('bids.add.fail.premium-account')->setValue('Чтобы добавить заявку, активируйте премиум аккаунт')->build(),
				$this->getBuilder('bids.add.fail.confirm-qualification')->setValue('Чтобы добавить заявку, подтвердите квалификацию в личном кабинете')->build(),
				$this->getBuilder('bids.add.success')->setValue('Заявка успешно добавлена')->build(),
				$this->getBuilder('bids.edit.unavailable')->setValue('Редактирование ставки не доступно')->build(),
				$this->getBuilder('bids.edit.success')->setValue('Заявка успешно обновлена')->build(),
				$this->getBuilder('bids.cancel.unavailable')->setValue('Нельзя отозвать ставку')->build(),
				$this->getBuilder('bids.cancel.success')->setValue('Вы отозвали ставку')->build(),
				$this->getBuilder('performer.select.success')->setValue('Исполнителем выбран %name%')->build(),
				$this->getBuilder('performer.select.fail')->setValue('Нельзя выбрать исполнителя')->build(),
				//
				$this->getBuilder('close.fail')->setValue('Нельзя закрыть сделку')->build(),
				$this->getBuilder('close.success')->setValue('Сделка успешно закрыта')->build(),
				//
				$this->getBuilder('status.open')->setValue('Открытая сделка')->build(),
				$this->getBuilder('status.active')->setValue('Активная сделка')->build(),
				$this->getBuilder('status.closed')->setValue('Сделка закрыта')->build(),
				//
				$this->getBuilder('reviews.success')->setValue('Ваш отзыв успешно добавлен')->setSubGroup('reviews')->build(),
				$this->getBuilder('reviews.fail.duplicate')->setValue('Вы уже оставили отзыв к сделке')->setSubGroup('reviews')->build(),
				$this->getBuilder('reviews.fail.performer.not-owner')->setValue('Вы не можете оставить отзыв где вы не выбраны исполнителем')->setSubGroup('reviews')->build(),
				$this->getBuilder('reviews.fail.customer.not-owner')->setValue('Отзыв может оставить только автор сделки')->setSubGroup('reviews')->build(),
				$this->getBuilder('reviews.fail.deal-not-closed')->setValue('Добавление отзыва будет доступно после закрытия сделки')->setSubGroup('reviews')->build(),
				$this->getBuilder('reviews.fail.date-expired')->setValue('Период добавления отзыва истек')->setSubGroup('reviews')->build(),
			],
		];
		foreach ($groups as $groupName => $group) {
			foreach ($group as $key => $item) {
				if (!is_array($item)) {
					$item = ['key' => $key, 'value' => $item];
				}
				$item['group'] = $groupName;
				$item['key'] = $this->addPrefixGroupToKey($item['key'], $groupName);
				if ($this->translateExists($item['key'])) {
					continue;
				}
				$translate = $this->repository->create($item);
				$this->addTranslate($translate->getAttribute('key'), $translate);
			}
		}
	}

	private function getTranslateByKey(string $key): ?Translate
	{
		return \Arr::get($this->translates, $key);
	}

	private function translateExists(string $key): bool
	{
		return (bool)$this->getTranslateByKey($key);
	}

	private function addPrefixGroupToKey(string $key, string $prefix)
	{
		return implode('.', [$prefix, $key]);
	}

	private function getBuilder(?string $key = null)
	{
		$builder = new class implements \ArrayAccess {
			private $key = '';

			private $value = '';

			private $type = 'text';

			private $displayName = '';

			private $subGroup = '';

			/** @var array */
			private $variables = [];

			public function setKey(string $key): self
			{
				$this->key = $key;
				return $this;
			}

			public function setTypeText(): self
			{
				$this->type = Translate::TYPE_TEXT;
				return $this;
			}

			public function setTypeTextarea(): self
			{
				$this->type = Translate::TYPE_TEXTAREA;
				return $this;
			}

			public function setTypeEditor(): self
			{
				$this->type = Translate::TYPE_EDITOR;
				return $this;
			}

			public function setValue(?string $value = null): self
			{
				$this->value = $value;
				return $this;
			}

			public function setDisplayName(string $displayName): self
			{
				$this->displayName = $displayName;
				return $this;
			}

			/**
			 * @param array $variables
			 */
			public function setVariables(array $variables): self
			{
				$this->variables = $variables;
				return $this;
			}

			public function build()
			{
				return [
					'key'          => $this->key,
					'value'        => $this->value,
					'type'         => $this->type,
					'display_name' => $this->displayName,
					'variables'    => $this->variables,
					'sub_group'    => $this->subGroup,
				];
			}

			public function offsetGet($offset)
			{
				return $this->offsetExists($offset) ? $this->{$offset} : null;
			}

			public function offsetExists($offset)
			{
				return property_exists($this, $offset);
			}

			public function offsetSet($offset, $value)
			{
				$method = Str::camel('set' . ucfirst($offset));
				if (method_exists($this, $method)) {
					$this->$method($value);
				}
			}

			public function offsetUnset($offset)
			{
				if ($this->offsetExists($offset)) {
					$this->{$offset} = null;
				}
			}

			/**
			 * @param string $subGroup
			 */
			public function setSubGroup(string $subGroup): self
			{
				$this->subGroup = $subGroup;
				return $this;
			}

		};
		if ($key) {
			$builder->setKey($key);
		}
		return $builder;
	}

	private function addTranslate(string $key, Translate $translate)
	{
		$this->translates->offsetSet($key, $translate);
	}
}
