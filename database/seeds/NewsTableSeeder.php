<?php

namespace Database\Seeders;

use App\Events\Admin\NewsChangedEvent;
use App\Helpers\Debug\LoggerHelper;
use App\Models\News\NewsCategory;
use App\Repositories\NewsCategoryRepository;
use App\Repositories\NewsRepository;
use Faker\Generator;
use Illuminate\Support\Str;

class NewsTableSeeder extends AbstractSeeder
{
	protected $categories;
	/**
	 * @var NewsRepository
	 */
	private $repository;
	/**
	 * @var Generator
	 */
	private $factory;
	/**
	 * @var NewsCategoryRepository
	 */
	private $categoryRepository;

	public function __construct(NewsRepository $repository, Generator $factory, NewsCategoryRepository $categoryRepository)
	{
		$this->repository = $repository;
		$this->factory = $factory;
		$this->categoryRepository = $categoryRepository;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$news = $this->getNews();
		$this->command->getOutput()->progressStart(count($news));
		foreach ($news as $item) {
			$category = $this->getRandomCategory();
			try{
				$this->repository->createRelated($item, $category);
			} catch (\Throwable $e){
			    app(LoggerHelper::class)->error($e);
			}
			$this->command->getOutput()->progressAdvance();
		}

		$this->command->getOutput()->progressFinish();
		event(new NewsChangedEvent());
	}


	/**
	 * @return array
	 */
	private function getNews()
	{
		$news = [];
		foreach (range(1, 15) as $item) {
			$name = $this->factory->jobTitle;
			$news[] = [
				'published_at' => $this->factory->date('Y-m-d H:i:s'),
				'name'         => $name,
				'title'        => $this->factory->city,
				'description'  => $this->factory->text,
				'excerpt'      => $this->factory->text(160),
				'image'        => $this->image($this->factory->imageUrl()),
				'url'          => Str::slug($name . Str::random(2)),
			];
		}
		return $news;
	}

	protected function getRandomCategory(): NewsCategory
	{
		if (null === $this->categories) {
			$this->categories = $this->categoryRepository->all()->keyBy('id');
		}
		static $count;
		if (null === $count) {
			$count = $this->categories->count();
		}
		$rand = random_int(1, $count);
		return $this->categories->get($rand);
	}
}
