<?php
	namespace Database\Seeders;
use App\Repositories\RedirectRepository;
use Illuminate\Database\Seeder;
use Prettus\Validator\Exceptions\ValidatorException;

class RedirectsTableSeeder extends Seeder
{
	private $model = \App\Models\Redirect::class;
	/**
	 * @var RedirectRepository
	 */
	private $repository;

	public function __construct(RedirectRepository $repository)
	{
		$this->repository = $repository;
	}

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 * @throws ValidatorException
	 */
	public function run()
	{
		foreach ($this->getRedirects() as $from => $to) {
			$array = [
				'from' => $from,
				'to'   => $to,
				'code' => \Symfony\Component\HttpFoundation\Response::HTTP_MOVED_PERMANENTLY,
			];
			$this->repository->create($array);
		}
	}

	private function getRedirects(): array
	{
		return [

		];
	}
}