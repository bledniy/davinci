<?php
	namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(LanguageTableSeeder::class);
		$this->call(AdminsTableSeeder::class);
		$this->call(CitySeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(RolesAndPermissionsSeeder::class);
        $this->call(AdminMenuSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(TranslateTableSeeder::class);
//        $this->call(PageSeeder::class);
        $this->call(MenuGroupSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(MetaTableSeeder::class);
        $this->call(RedirectsTableSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(OrdersSeeder::class);
        $this->call(ReviewsSeeder::class);
        $this->call(NewsCategorySeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(HowSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(NotificationConfigTypesSeeder::class);
        $this->call(PremiumSeeder::class);
    }
}
