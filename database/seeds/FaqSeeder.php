<?php

namespace Database\Seeders;

use App\Models\Faq\FaqTypes;
use App\Repositories\FaqRepository;

class FaqSeeder extends AbstractSeeder
{

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var FaqRepository
	 */
	private $repository;

	public function __construct(\Faker\Generator $factory, FaqRepository $repository)
	{
		$this->factory = $factory;
		$this->repository = $repository;
	}


	public function run()
	{
		foreach (range(1, 10) as $item) {
			$this->repository->create([
				'type'     => ($item % 2 === 0) ? FaqTypes::PERFORMER : FaqTypes::DEFAULT,
				'question' => $this->factory->name,
				'answer'   => $this->factory->jobTitle,
			]);
		}
	}
}
