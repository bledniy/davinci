<?php

namespace Database\Seeders;

use App\Repositories\ContentRepository;

class HowSeeder extends AbstractSeeder
{

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var ContentRepository
	 */
	private $contentRepository;

	public function __construct(\Faker\Generator $factory, ContentRepository $contentRepository)
	{
		$this->factory = $factory;
		$this->contentRepository = $contentRepository;
	}

	public function run()
	{
		$hows = [
			['name' => 'Закройте заказ', 'excerpt' => 'Опишите, что и когда нужно сделать', 'type' => 'how',],
			['name' => 'Проведите работу', 'excerpt' => 'Опишите, что и когда нужно сделать', 'type' => 'how',],
			['name' => 'Выберите исполнителя', 'excerpt' => 'Опишите, что и когда нужно сделать', 'type' => 'how',],
			['name' => 'Создайте заказ', 'excerpt' => 'Опишите, что и когда нужно сделать', 'type' => 'how',],

			['name' => 'Начните работать', 'excerpt' => 'Опишите, что и когда нужно сделать', 'type' => 'how-performer',],
			['name' => 'Получите подтверждение', 'excerpt' => 'Опишите, что и когда нужно сделать', 'type' => 'how-performer',],
			['name' => 'Прикрепите документы', 'excerpt' => 'Опишите, что и когда нужно сделать', 'type' => 'how-performer',],
			['name' => 'Зарегистрируйтесь', 'excerpt' => 'Опишите, что и когда нужно сделать', 'type' => 'how-performer',],

			['name' => 'John Doe', 'type' => 'team',],
			['name' => 'John Mike', 'type' => 'team',],
			['name' => 'Mike Doe', 'type' => 'team',],

			['name' => 'Сертификация', 'type' => 'need',],
			['name' => 'Образование', 'type' => 'need',],
			['name' => 'Опыт', 'type' => 'need',],

			['name' => 'Нотариус', 'type' => 'main-services', 'content_type' => 'notarius', 'title' => 'Подобрать нотариуса'],
			['name' => 'Юристы', 'type' => 'main-services', 'content_type' => 'jurist', 'title' => 'Подобрать юриста'],
			['name' => 'Адвокаты', 'type' => 'main-services', 'content_type' => 'lawyer', 'title' => 'Подобрать адвоката'],

			['name' => 'Поддержка', 'type' => 'performer-services'],
			['name' => 'Система отзывов', 'type' => 'performer-services'],
			['name' => 'Поиск клиентов', 'type' => 'performer-services'],

			['name' => 'Поиск работы', 'type' => 'main-bottom', 'content_type' => 'performer'],
			['name' => 'Поиск исполнителя', 'type' => 'main-bottom', 'content_type' => 'customer'],

			['name' => $this->factory->name, 'type' => 'reviews', 'description' => 'Сервис помог найти мне нотариуса в короткий срок. Мне пришло множество вариантов и я выбрала под свой бюджет и в удобном месте'],
			['name' => $this->factory->name, 'type' => 'reviews', 'description' => 'Сервис помог найти мне нотариуса в короткий срок. Мне пришло множество вариантов и я выбрала под свой бюджет и в удобном месте'],
			['name' => $this->factory->name, 'type' => 'reviews', 'description' => 'Сервис помог найти мне нотариуса в короткий срок. Мне пришло множество вариантов и я выбрала под свой бюджет и в удобном месте'],
			['name' => $this->factory->name, 'type' => 'reviews', 'description' => 'Сервис помог найти мне нотариуса в короткий срок. Мне пришло множество вариантов и я выбрала под свой бюджет и в удобном месте'],
			['name' => $this->factory->name, 'type' => 'reviews', 'description' => 'Сервис помог найти мне нотариуса в короткий срок. Мне пришло множество вариантов и я выбрала под свой бюджет и в удобном месте'],
		];

		foreach ($hows as $how) {
			$how['description'] = $how['description'] ?? $this->factory->realText();
			$how['image'] = $this->image($this->factory->imageUrl());
			if ($this->contentRepository->joinLang()->where('type', $how['type'])->where('name', $how['name'])->count()) {
				continue;
			}
			$this->contentRepository->create($how);
		}
	}
}
