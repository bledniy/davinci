<?php

namespace Database\Seeders;

use App\Platform\Contract\UserTypeContract;
use App\Platform\User\Notification\NotificationConfigTypes;
use App\Repositories\NotificationConfigRepository;

class NotificationConfigTypesSeeder extends AbstractSeeder
{

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var NotificationConfigRepository
	 */
	private $repository;

	public function __construct(\Faker\Generator $factory, NotificationConfigRepository $repository)
	{
		$this->factory = $factory;
		$this->repository = $repository;
	}


	public function run()
	{
		$this->repository->truncate();
		$types = [
			'EMAIL'   => [
				'name'     => 'Уведомления на почту',
				'code'     => NotificationConfigTypes::GROUP_MAIL,
				'children' => [
					['code' => NotificationConfigTypes::MAIL_PERSONAL_MESSAGES, 'name' => 'Новые личные сообщения'],
					['code' => NotificationConfigTypes::MAIL_NEW_ORDERS, 'name' => 'О новых заказах (раз в час)', 'type' => UserTypeContract::TYPE_PERFORMER,],
					['code' => NotificationConfigTypes::MAIL_NEW_ORDER_BIDS, 'name' => 'О новых заявках', 'type' => UserTypeContract::TYPE_CUSTOMER,],
					['code' => NotificationConfigTypes::MAIL_NEW_NEWS, 'name' => 'Новые публикации в блоге'],
				],
			],
			'BROWSER' => [
				'name'     => 'Уведомления в браузере',
				'code'     => NotificationConfigTypes::GROUP_BROWSER,
				'children' => [
					['code' => NotificationConfigTypes::BROWSER_PERSONAL_MESSAGES, 'name' => 'Новые личные сообщения'],
					['code' => NotificationConfigTypes::BROWSER_NEW_ORDERS, 'name' => 'О новых заказах (раз в час)', 'type' => UserTypeContract::TYPE_PERFORMER,],
					['code' => NotificationConfigTypes::BROWSER_NEW_ORDER_BIDS, 'name' => 'О новых заявках', 'type' => UserTypeContract::TYPE_CUSTOMER,],
					['code' => NotificationConfigTypes::BROWSER_NEW_NEWS, 'name' => 'Новые публикации в блоге'],
				],
			],
		];

		foreach ($types as $group => $typeGroup) {
			$typeGroup['group'] = $group;
			$typeParent = $this->repository->create($typeGroup);
			if (!$typeParent) {
			    continue;
			}
			foreach ($typeGroup['children'] as $notificationType) {
				$notificationType['group'] = $group;
				$this->repository->createParent($notificationType, $typeParent);
			}
		}
	}
}
