<?php

namespace Tests\Feature;

use App\Helpers\ResponseHelper;
use App\Test\Traits\UsersActingTrait;
use Tests\TestCase;

class PremiumAccountTest extends TestCase
{
	use UsersActingTrait;

	public function __construct(?string $name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);
		$this->withExceptionHandling();
	}

	public function testApplyFreePremium()
	{
		$this->switchToPerformer();
		if ($this->getPerformer()->getPremiumAccountService()->canApplyFreeProAccount()) {
			$response = $this->postJson(route('premium.free.apply'));
			$response
				->assertCreated()
				->assertJsonStructure(['data' => ['expires_at']])
			;
		} else {
			$response = $this->postJson(route('premium.free.apply'));
			$response->assertJsonFragment([ResponseHelper::STATUS_KEY => ResponseHelper::ERROR_KEY]);
		}

		$this->getPerformer()->getPremiumAccountService()->getProAccount()->delete();
	}

}
