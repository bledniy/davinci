<?php

namespace Tests\Feature;

use App\Events\Order\OrderBeforePublished;
use App\Events\Order\OrderCreatedEvent;
use App\Events\Order\OrderPublished;
use App\Models\Category\Category;
use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Order\Services\OrderCreator;
use App\Platform\Order\Services\TestOrderCreateContainerFakeDataFiller;
use App\Platform\ProAccount\UserProAccountActivationContainer;
use App\Platform\ProAccount\UserProAccountActivator;
use App\Repositories\PremiumRepository;
use App\Test\Traits\UsersActingTrait;
use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class OrderTest extends TestCase
{
	use UsersActingTrait;
	use WithFaker;

	public function __construct(?string $name = null, array $data = [], $dataName = '')
	{
		parent::__construct($name, $data, $dataName);
		$this->withExceptionHandling();
		$this->faker = Factory::create();
	}

	public function testCreateUpdateOrder()
	{
		$responseCreate = $this->createOrderResponse();
		$this->switchToCustomer();
		$responseCreate->assertCreated();

		$orderId = $responseCreate->json('order.id');

//		$file = UploadedFile::fake()->image('avatar.jpg');

		$responseUpdate = $this->postJson(route('deals.update', $orderId), [
			'category_id' => $responseCreate->json('order.category.id'),
			'city_id' => $responseCreate->json('order.city.id'),
			'title' => $responseCreate->json('order.title'),
			'description' => $responseCreate->json('order.description') . Str::random(),
			'price' => $responseCreate->json('order.price') + 10,
			'date_completion' => getDateCarbon($responseCreate->json('order.date_completion'))->addDay(),
			'can_online' => false,
			'uploads' => [
//				$file,
			],
			'_method' => 'PUT',
		]);
		$responseUpdate->assertOk();

		$this->switchToPerformer();
		$responseBidAdd = $this->postJson(route('deals.bids.create', $orderId), $this->makeFakeBid());
	}

	public function testCloseOrderWithoutPerformer()
	{
		$responseCreate = $this->createOrderResponse();
		$this->switchToCustomer();

		$orderId = $responseCreate->json('order.id');
		$responseCloseFake = $this->postJson(route('deals.close', PHP_INT_MAX));
		$responseCloseFake->assertNotFound();

		$responseClose = $this->postJson(route('deals.close', $orderId));
		$responseClose->assertOk();
	}

	private function makePerformerPossibleSendBids(User $performer)
	{
		$userPremium = new User\UserPremium();
		$userPremium->user()->associate($performer);
		$this->app->make(UserProAccountActivator::class)
			->extendDateExpirationProAccount(
			app(PremiumRepository::class)->getDefaultPremium(),
			$userPremium,
				$this->app->make(UserProAccountActivationContainer::class)->setDays(1)
		);
		if ($performer->getPremiumAccountService()->canApplyFreeProAccount()) {
			$this->postJson(route('premium.free.apply'));
		}

		$performer->performerCategories()->sync(Category::query()->pluck('id')->toArray());
	}

	public function testCloseOrderWithSelectedPerformer()
	{
		$this->makePerformerPossibleSendBids($this->getPerformer());

		$responseCreate = $this->createOrderResponse();
		$this->switchToCustomer();
		$orderId = $responseCreate->json('order.id');

		//BID
		$this->switchToPerformer();

		$responseBidAdd = $this->postJson(route('deals.bids.create', $orderId), $this->makeFakeBid());
		$responseBidAdd->assertCreated();

		$responseBidEdit = $this->putJson(route('deals.bids.update', ['deal' => $orderId, 'bid' => $responseBidAdd->json('bid.id')]), $this->makeFakeBid());
		$responseBidEdit->assertOk();

		$this->switchToCustomer();
		$responseSelect = $this->postJson(route('deals.select-performer', ['deal' => $orderId, 'performerId' => $responseBidAdd->json('bid.performer.id')]));
		$responseSelect->assertOk()
			->assertJsonFragment(['status' => 'success'])
		;

		$responseClose = $this->postJson(route('deals.close', $orderId));
		$responseClose->assertOk()
			->assertJsonFragment(['status' => 'success'])
		;

		$responseReview = $this->postJson(route('deals.review.customer', $orderId), $this->makeFakeReview());
		$responseReview->assertOk()
			->assertJsonFragment(['status' => 'success'])
		;
		//
		$this->switchToPerformer();
		$responseReview = $this->postJson(route('deals.review.performer', $orderId), $this->makeFakeReview());
		$responseReview->assertOk()
			->assertJsonFragment(['status' => 'success'])
		;
	}

	protected function createOrderResponse($replace = [])
	{
		$this->switchToCustomer();
		$data = $this->app->make(TestOrderCreateContainerFakeDataFiller::class)->fill()->toArray();
		$data = array_merge($data, $replace);

		return $this->postJson(route('deals.create'), $data);
	}


	protected function makeFakeReview()
	{
		return [
			'comment' => $this->faker->text,
			'adequate_rating' => $this->faker->numberBetween(1, 5),
			'legibility_rating' => $this->faker->numberBetween(1, 5),
			'sociability_rating' => $this->faker->numberBetween(1, 5),
			'conflict_rating' => $this->faker->numberBetween(1, 5),
			'recommend_rating' => $this->faker->numberBetween(1, 5),
			'deadline' => (string)now()->addWeek(),
		];
	}

	protected function makeFakeBid()
	{
		return [
			'text' => $this->faker->text,
			'bid' => $this->faker->numberBetween(50, 2000),
			'deadline' => (string)now()->addDays($this->faker->numberBetween(3, 9)),
		];
	}

}
