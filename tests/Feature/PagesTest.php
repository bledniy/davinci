<?php

namespace Tests\Feature;

use App\Models\Order\Order;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PagesTest extends TestCase
{
	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testBasicTest()
	{
		$this->withoutExceptionHandling();
		$urls = [
			'/', '/api/cities',
		];
		$api = [
			'pages/about',
			'pages/offer',
			'pages/privacy',
			'pages/main',
			'pages/for-performer',
			'categories',
			'news/categories',
			'news',
			'faq',
			'reviews',
			'how-it-works',
			'for-performer/how-it-works',
			'for-performer/need',
			'teams',
			'deals',
		];
		$api = arrayEachPrepend($api, 'api/');

		$urls = array_merge($urls, $api);
		foreach ($urls as $url) {
			try {
				$response = $this->get($url);
				$response->assertStatus(200);
			} catch (\Throwable $e) {
				dump($url);
				throw $e;
			}
		}
	}

	public function testDealPageShow()
	{
		$user = User::first();
		$deal = Order::first();
		Sanctum::actingAs($user, ['*']);
		$response = $this
			->get('/api/deals/' . $deal->getContainer()->getSlug())
		;
		$response->assertOk();
	}
}
