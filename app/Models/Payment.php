<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\MorphTo;

class Payment extends Model
{
	protected $guarded = ['id'];

	protected $casts = [
		'is_payed' => 'bool',
		'payed'    => 'integer',
	];

	public function paymentable(): MorphTo
	{
		return $this->morphTo();
	}

	public function setPrice(string $price): self
	{
		$this->setAttribute('price', $price);
		return $this;
	}

	public function setPayed(int $payed): self
	{
		$this->setAttribute('payed', $payed);
		return $this;
	}

	public function setPaymentLink(string $link): self
	{
		$this->setAttribute('payment_link', $link);
		return $this;
	}

	public function setPaymentStatus(string $value): self
	{
		$this->setAttribute('payment_status', $value);
		return $this;
	}

	public function setSuccessPayment(): self
	{
		$this->setAttribute('is_payed', true);
		return $this;
	}

	public function setFailPayment()
	{
		$this->setAttribute('is_payed', false);
	}

	public function getPrice(): ?string
	{
		return $this->getAttribute('price');
	}

	public function getPayed(): int
	{
		return (int)$this->getAttribute('payed');
	}

	public function getPaymentStatus(): ?string
	{
		return $this->getAttribute('payment_status');
	}

	public function isPayed(): bool
	{
		return (bool)$this->getAttribute('is_payed');
	}

	public function setDestination(string $destination): self
	{
		$this->setAttribute('destination', $destination);
		return $this;
	}

	public function getDestination(): ?string
	{
		return $this->getAttribute('destination');
	}
}
