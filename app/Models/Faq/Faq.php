<?php

namespace App\Models\Faq;

use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Traits\Models\Localization\RedirectLangColumn;

class Faq extends Model implements HasLocalized
{
	use RedirectLangColumn;

	protected $hasOneLangArguments = [FaqLang::class];
	protected $langColumns = ['question', 'answer', 'language_id'];

	protected $casts = [
		'active' => 'boolean',
	];

	protected $guarded = ['id'];

	public function getQuestion(): string
	{
		return (string)$this->getAttribute('question');
	}

	public function getAnswer(): string
	{
		return (string)$this->getAttribute('answer');
	}


}
