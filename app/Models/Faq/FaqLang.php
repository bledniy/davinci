<?php

namespace App\Models\Faq;

use App\Models\ModelLang;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FaqLang extends ModelLang
{
	protected $primaryKey = ['faq_id', 'language_id'];

	public function faq(): BelongsTo
	{
		return $this->belongsTo(Faq::class);
	}

}
