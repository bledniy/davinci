<?php

namespace App\Models\Page;


use App\Contracts\HasLocalized;
use App\Enum\PageTypeEnum;
use App\Models\Content\Content;
use App\Models\Content\HasContentable;
use App\Models\Model;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\Localization\RedirectLangColumn;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Arr;
use InvalidArgumentException;
use TypeError;


class Page extends Model implements HasLocalized, HasContentable
{
	use ImageAttributeTrait;
	use RedirectLangColumn;

	protected $langColumns = [
		'name', 'title', 'description', 'excerpt', 'sub_title', 'sub_description',
	];

	protected $hasOneLangArguments = [PageLang::class];

	protected $guarded = [
	];

	public function getUrl()
	{
		$column = 'url';

		return Arr::get($this->attributes, $column);
	}

	public function content(): MorphMany
	{
		return $this->morphMany(Content::class, 'contentable');
	}

	public function getTitle()
	{
		return $this->title ?: $this->name;
	}

	public function getTypeEnum(): ?PageTypeEnum
	{
		try {
			return new PageTypeEnum($this->page_type);
		} catch (InvalidArgumentException | TypeError $e) {
			return null;
		}
	}

}
