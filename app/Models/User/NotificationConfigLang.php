<?php

namespace App\Models\User;

use App\Models\ModelLang;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NotificationConfigLang extends ModelLang
{
	protected $primaryKey = ['notification_config_id', 'language_id'];

	public function config(): BelongsTo
	{
		return $this->belongsTo(NotificationConfig::class);
	}
}
