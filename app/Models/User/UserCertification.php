<?php declare(strict_types=1);

namespace App\Models\User;

use App\Models\Model;
use App\Models\Staff\Setters\UserCertificationSetter;
use App\Platform\Cabinet\Certification\CertificationStatuses;
use App\Traits\Models\BelongsToCategory;
use App\Traits\Models\BelongsToUser;
use Illuminate\Support\Carbon;

class UserCertification extends Model
{
	use BelongsToUser, BelongsToCategory;

	protected $guarded = ['id'];

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
		$this->setAttribute('status', CertificationStatuses::getBaseStatus());
	}

	public function getStatus(): string
	{
		return (string)$this->status;
	}

	public function getDateCertification(): ?Carbon
	{
		if (null === $this->getAttribute('certified_at')) {
			return null;
		}

		return getDateCarbon($this->getAttribute('certified_at'));
	}

	public function getCertificationStatuses(): CertificationStatuses
	{
		return app(CertificationStatuses::class, ['certification' => $this,]);
	}

	public function getSetter(): UserCertificationSetter
	{
		return app(UserCertificationSetter::class, ['certification' => $this]);
	}

	public function isUnread(): bool
	{
		return $this->getCertificationStatuses()->isOnCheck();
	}

	public function getImagePath(): ?string
	{
		return $this->getAttribute('image');
	}

}