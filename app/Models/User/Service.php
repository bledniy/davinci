<?php

namespace App\Models\User;

use App\Models\Model;
use App\Traits\Models\BelongsToUser;

class Service extends Model
{
	use BelongsToUser;

	protected $table = 'user_services';

	protected $guarded = ['id'];
}
