<?php declare(strict_types=1);

namespace App\Models\User;

use App\Contracts\Models\ModelHasMorphPayment;
use App\Models\Model;
use App\Models\Payment;
use App\Models\Premium\Premium;
use App\Traits\Models\BelongsToUser;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

class UserPremium extends Model implements ModelHasMorphPayment
{
	use BelongsToUser;

	protected $table = 'user_premiums';

	protected $guarded = ['id'];

	public function getExpiresAt(): ?Carbon
	{
		return getDateCarbonIfValid($this->expires_at);
	}

	public function setExpiresAt(Carbon $carbon): self
	{
		$this->setAttribute('expires_at', $carbon);
		return $this;
	}

	public function premium(): BelongsTo
	{
		return $this->belongsTo(Premium::class);
	}

	public function setPremiumId($premiumId):self
	{
		$this->setAttribute($this->premium()->getForeignKeyName(), $premiumId);
		return $this;
	}

	public function payments()
	{
		return $this->morphMany(Payment::class, 'paymentable');
	}

}
