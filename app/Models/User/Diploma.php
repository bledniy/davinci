<?php

namespace App\Models\User;

use App\Models\Model;
use App\Traits\Models\BelongsToUser;

class Diploma extends Model
{
	protected $guarded = ['id'];

	use BelongsToUser;

}
