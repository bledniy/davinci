<?php

namespace App\Models\User;

use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Models\User;
use App\Traits\Models\Localization\RedirectLangColumn;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class NotificationConfig extends Model implements HasLocalized
{
	use RedirectLangColumn;

	protected $guarded = ['id'];

	protected $langColumns = [
		'name', 'language_id',
	];

	protected $hasOneLangArguments = [NotificationConfigLang::class];

	public function child()
	{
		return $this->hasMany(__CLASS__, 'parent_id');
	}

	public function user():BelongsToMany
	{
		return $this->belongsToMany(User::class);
	}

}
