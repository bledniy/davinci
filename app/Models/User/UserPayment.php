<?php declare(strict_types=1);

namespace App\Models\User;

use App\Enum\UserPaymentReasonEnum;
use App\Models\Model;
use App\Services\Payment\Portmone\PaymentResponseDataInterface;
use App\Traits\Models\BelongsToUser;

class UserPayment extends Model
{
	use BelongsToUser;

	protected $guarded = ['id'];
	protected $casts = [
		'is_payed' => 'bool',
		'payed' => 'integer',
	];

	public function setPayed(int $value): self
	{
		$this->setAttribute('payed', $value);

		return $this;
	}

	public function getPayed(): int
	{
		return $this->getAttribute('payed');
	}

	public function setNeedPay(int $value): self
	{
		$this->setAttribute('need_pay', $value);

		return $this;
	}

	public function getNeedPay(): int
	{
		return (int)$this->getAttribute('need_pay');
	}

	public function setBillingId(int $value): self
	{
		$this->setAttribute('billing_id', $value);

		return $this;
	}

	public function setReason(UserPaymentReasonEnum $enum): self
	{
		$this->setAttribute('reason', $enum);

		return $this;
	}

	public function hasReason():bool
	{
		return null !== $this->getAttribute('reason');
	}

	public function getReasonEnum(): UserPaymentReasonEnum
	{
		return new UserPaymentReasonEnum($this->getAttribute('reason'));
	}

	public function setIsPayed(bool $value): self
	{
		$this->setAttribute('is_payed', $value);

		return $this;
	}

	public function isPayed(): bool
	{
		return $this->getAttribute('is_payed');
	}


	public function fillFromPaymentResponseData(PaymentResponseDataInterface $data): self
	{
		$this->setBillingId($data->getBillingId())
			->setPayed($data->getPayedAmount())
			->setUpdatedAt($data->getPayDateTime())
		;

		return $this;
	}
}
