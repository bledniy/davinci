<?php declare(strict_types=1);

namespace App\Models\Feedback;

use App\Enum\FeedbackTypeEnum;
use App\Models\Model;

class Feedback extends Model
{
	protected $guarded = ['id'];

	protected $casts = [
		'data' => 'array',
		'files' => 'array',
	];

	public function setFiles(array $files): Feedback
	{
		$this->setAttribute('files', $files);

		return $this;
	}

	public function getPhoneDisplay(): string
	{
		return extractDigits($this->getAttribute('phone'));
	}

	public function getTypeEnum(): FeedbackTypeEnum
	{
		return new FeedbackTypeEnum($this->type);
	}

	public function getFiles()
	{
		return (array)$this->files;
	}
}
