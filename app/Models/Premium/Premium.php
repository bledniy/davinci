<?php

namespace App\Models\Premium;

use App\Models\Model;

class Premium extends Model
{

	protected $table = 'premiums';
	protected $guarded = ['id'];

	public function getPrice(): int
	{
		return (int)$this->price;
	}

	public function getType()
	{
		return $this->type;
	}
}
