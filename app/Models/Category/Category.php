<?php

namespace App\Models\Category;

use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Traits\Models\Localization\RedirectLangColumn;

class Category extends Model implements HasLocalized
{
	use RedirectLangColumn;

	protected $langColumns = ['name', 'language_id', 'description'];

	protected $hasOneLangArguments = [CategoryLang::class];

	protected $guarded = ['id'];

	public function getNameDisplay(): string
	{
		return (string)$this->getAttribute('name');
	}


}
