<?php

namespace App\Models\Category;

use App\Models\ModelLang;


class CategoryLang extends ModelLang
{

	protected $primaryKey = ['category_id', 'language_id'];

	protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
	}

}
