<?php declare(strict_types=1);

namespace App\Models\News;

use App\Contracts\HasLocalized;
use App\Contracts\Models\HasNextPrevAttributes;
use App\Models\Category\Category;
use App\Models\Model;
use App\Traits\Models\HasImages;
use App\Traits\Models\Localization\RedirectLangColumn;
use App\Traits\Models\PrevNextAttributes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class News extends Model implements  HasNextPrevAttributes, HasLocalized
{
	use HasImages;
	use PrevNextAttributes;
	use RedirectLangColumn;

	protected $langColumns = [
		'name', 'title', 'description', 'excerpt', 'language_id'
	];

	protected $hasOneLangArguments = [NewsLang::class, 'news_id'];

	protected $casts = [
		'video' => 'string',
	];

	protected $guarded = ['id'];

	public function category(): BelongsTo
	{
		return $this->belongsTo(NewsCategory::class, 'news_category_id');
	}

	public static function boot()
	{
		parent::boot();
	}

	public function getSlug()
	{
		return $this->url;
	}

	public function prevNew(): HasOne
	{
		return $this->hasOne(__CLASS__, 'prev_id')->with('lang');
	}

	public function nextNew(): HasOne
	{
		return $this->hasOne(__CLASS__, 'next_id')->with('lang');
	}

	public function getExcept()
	{
		return $this->getAttribute('excerpt');
	}

	public function getDescription()
	{
		return $this->getAttribute('description');
	}

	public function hasVideo(): bool
	{
		return (bool)$this->video;
	}

	public function getNext(): ?self
	{
		return $this->nextNew;
	}

	public function getPublishedAt()
	{
		return getDateCarbon($this->published_at);
	}

}
