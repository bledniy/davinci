<?php

namespace App\Models\News;

use App\Models\ModelLang;

/**
 * App\Models\News\NewsLang
 *
 * @property int $news_id
 * @property string|null $name
 * @property int $language_id
 * @property string|null $description
 * @property string|null $except
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\News\News $news
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\NewsLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\NewsLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\NewsLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\NewsLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\NewsLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\NewsLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\NewsLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News\NewsLang whereNewsId($value)
 * @mixin \Eloquent
 */
class NewsLang extends ModelLang
{
	protected $casts = [
//		'video' => 'array'
	];

	protected $primaryKey = ['news_id', 'language_id'];

	protected $guarded = [];

    public function news()
    {
        return $this->belongsTo(News::class);
	}

}
