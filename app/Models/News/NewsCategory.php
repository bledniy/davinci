<?php declare(strict_types=1);

namespace App\Models\News;

use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Traits\Models\Localization\RedirectLangColumn;
use Illuminate\Database\Eloquent\Relations\HasMany;

class NewsCategory extends Model implements HasLocalized
{
	use RedirectLangColumn;

	protected $langColumns = ['name', 'language_id'];

	protected $hasOneLangArguments = [NewsCategoryLang::class, 'news_category_id'];

	protected $guarded = ['id'];

	public function news(): HasMany
	{
		return $this->hasMany(News::class);
	}

	public function toArray()
	{
		if ($this->relationLoaded('lang')) {
			return array_merge(parent::toArray(), $this->lang->toArray());
		}
		return parent::toArray();
	}


}
