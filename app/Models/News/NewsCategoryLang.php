<?php

namespace App\Models\News;

use App\Models\ModelLang;


class NewsCategoryLang extends ModelLang
{
	protected $casts = [
//		'video' => 'array'
	];

	protected $primaryKey = ['news_category_id', 'language_id'];

	protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(NewsCategory::class);
	}

}
