<?php

namespace App\Models\Chat;

use App\Models\Model;
use App\Platform\Contract\Chat\ChatMessageTypesContract;
use App\Platform\Order\Model\ChatMessageSetter;
use App\Traits\Models\BelongsToChat;
use App\Traits\Models\BelongsToUser;

class ChatMessage extends Model
{
	use BelongsToChat;
	use BelongsToUser;

	protected $perPage = 50;
	protected $guarded = ['id'];
	/**
	 * @var ChatMessageSetter
	 */
	private $setter;

	/**
	 * @return array|string
	 */
	public function getMessage()
	{
		if ($this->getType() === ChatMessageTypesContract::TYPE_FILE) {
		    try{
				return json_decode($this->getAttribute('message'), true, 512, JSON_THROW_ON_ERROR);
		    } catch (\Throwable $e){
		    }
		}
		return (string)$this->getAttribute('message');
	}

	public function getType(): string
	{
		return (string)$this->getAttribute('type');
	}

	public function isWatched():bool
	{
		return (bool)$this->is_watched;
	}

	public function getSetter(): ChatMessageSetter
	{
		if (null === $this->setter) {
			$this->setter = app(ChatMessageSetter::class, ['entity' => $this]);
		}
		return $this->setter;
	}
}
