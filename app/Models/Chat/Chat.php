<?php

namespace App\Models\Chat;

use App\Models\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Chat extends Model
{

	protected $guarded = ['id'];

	public function chatable(): MorphTo
	{
		return $this->morphTo('chatable');
	}

	public function users()
	{
		return $this->belongsToMany(User::class, 'chat_users');
	}

	public function messages()
	{
		return $this->hasMany(ChatMessage::class);
	}

	public function unreadMessages()
	{
		return $this->hasMany(ChatMessage::class)->where('is_watched', 0);
	}

	public function getType()
	{
		return $this->type;
	}

	public function favorites()
	{
		return $this->belongsToMany(User::class, 'chat_favorites');
	}

	public function setType($type):self
	{
		$this->setAttribute('type', $type);
		return $this;
	}
}
