<?php

namespace App\Models\Chat;

use App\Models\Model;
use App\Models\User;
use App\Traits\Models\BelongsToChat;
use App\Traits\Models\BelongsToUser;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class ChatFavorite extends Model
{
	use BelongsToChat;
	use BelongsToUser;
}
