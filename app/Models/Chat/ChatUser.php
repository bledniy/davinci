<?php

namespace App\Models\Chat;

use App\Models\Model;
use App\Traits\Models\BelongsToChat;

class ChatUser extends Model
{
	use BelongsToChat;

	protected $guarded = ['id'];
}
