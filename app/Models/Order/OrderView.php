<?php

namespace App\Models\Order;

use App\Models\Category\Category;
use App\Models\City;
use App\Models\Model;
use App\Models\User;
use App\Platform\Order\Containers\ShowMainDataContainer;
use App\Platform\Order\Model\OrderSetter;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

class OrderView extends Model
{

	protected $guarded = [];

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

	public function order(): BelongsTo
	{
		return $this->belongsTo(Order::class);
	}

	public function getUser(): User
	{
		return $this->user;
	}

	public function getOrder(): Order
	{
		return $this->order;
	}

}
