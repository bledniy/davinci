<?php declare(strict_types=1);

namespace App\Models\Order;

use App\Contracts\Models\ModelHasChat;
use App\Models\City\City;
use App\Models\Model;
use App\Models\User;
use App\Platform\Order\DataContainers\OrderMainDataContainer;
use App\Platform\Order\Model\OrderSetter;
use App\Traits\Models\BelongsToCategory;
use App\Traits\Models\ModelHasChatTrait;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model implements ModelHasChat
{
	use BelongsToCategory;
	use ModelHasChatTrait;

	protected $guarded = ['id'];

	private $setter;
	private $container;

	public function getTitle()
	{
		return $this->title;
	}

	public function getSetter(): OrderSetter
	{
		if (null === $this->setter) {
			$this->setter = app(OrderSetter::class, ['order' => $this]);
		}

		return $this->setter;
	}

	public function city(): BelongsTo
	{
		return $this->belongsTo(City::class);
	}

	public function getCity(): City
	{
		return $this->city;
	}

	public function getPrice(): int
	{
		return (int)$this->price;
	}

	public function customer(): BelongsTo
	{
		return $this->belongsTo(User::class, 'customer_id');
	}

	public function getCustomer(): User
	{
		return $this->customer;
	}

	public function performer(): BelongsTo
	{
		return $this->belongsTo(User::class, 'performer_id');
	}

	public function getPerformer(): ?User
	{
		return $this->performer;
	}

	public function getContainer()
	{
		if (null === $this->container) {
			$this->container = app(OrderMainDataContainer::class, ['order' => $this]);
		}

		return $this->container;
	}

	public function uploads(): HasMany
	{
		return $this->hasMany(OrderUpload::class);
	}

	public function getChatName()
	{
		return $this->getContainer()->getTitle();
	}

	public function bids(): HasMany
	{
		return $this->hasMany(OrderBid::class);
	}
}
