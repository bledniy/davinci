<?php

namespace App\Models\Order;

use App\Models\Model;
use App\Models\User;
use App\Platform\Order\DataContainers\ReviewDisplayContainer;
use App\Platform\Order\Model\ReviewSetter;
use App\Traits\Models\BelongsToOrder;
use App\Traits\Models\HasImages;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Review extends Model
{
	private $container;
	private $setter;
	use HasImages, BelongsToOrder;

	protected $guarded = ['id'];

	public function reviewFrom(): BelongsTo
	{
		return $this->belongsTo(User::class, 'from_id');
	}

	public function reviewTo(): BelongsTo
	{
		return $this->belongsTo(User::class, 'to_id');
	}

	public function getReviewFrom(): User
	{
		return $this->reviewFrom;
	}

	public function getReviewTo(): User
	{
		return $this->reviewTo;
	}

	public function getOrder(): Order
	{
		return $this->order;
	}

	public function getRating(): float
	{
		return (float)$this->rating;
	}

	public function getComment()
	{
		return $this->comment;
	}

	public function getContainer()
	{
		if (null === $this->container) {
			$this->container = app(ReviewDisplayContainer::class, ['review' => $this]);
		}
		return $this->container;
	}

	public function getSetter()
	{
		if (null === $this->setter) {
			$this->setter = app(ReviewSetter::class, ['entity' => $this]);
		}
		return $this->setter;
	}

	public function getOrderKey()
	{
		return $this->getAttribute('order_id');
	}

	public function getReviewAbout()
	{
		return $this->getAttribute('about');
	}
}
