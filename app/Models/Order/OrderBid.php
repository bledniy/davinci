<?php

	namespace App\Models\Order;

	use App\Models\Model;
	use App\Models\User;
	use App\Platform\Order\DataContainers\OrderBidContainer;
	use App\Platform\Order\Model\OrderBidSetter;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	class OrderBid extends Model
	{
		private $container;

		protected $guarded = ['id'];

		protected $casts = [
			'days'        => 'integer',
			'modified_at' => 'datetime',
			'canceled_at' => 'datetime',
		];

		public function order(): BelongsTo
		{
			return $this->belongsTo(Order::class);
		}

		public function user(): BelongsTo
		{
			return $this->belongsTo(User::class);
		}

		public function getUser(): User
		{
			return $this->user;
		}

		public function getUserId()
		{
			return $this->getAttribute('user_id');
		}

		public function getContainer()
		{
			if (is_null($this->container)) {
				$this->container = app(OrderBidContainer::class, ['bid' => $this]);
			}
			return $this->container;
		}

		public function getSetter()
		{
			return app(OrderBidSetter::class, ['orderBid' => $this]);
		}
	}
