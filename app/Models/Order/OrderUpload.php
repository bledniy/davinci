<?php

namespace App\Models\Order;

use App\Models\Model;
use App\Traits\Models\BelongsToOrder;

class OrderUpload extends Model
{
	use BelongsToOrder;

	protected $guarded = ['id'];

	public function setPath($path): OrderUpload
	{
		$this->setAttribute('file', $path);
		return $this;
	}

	public function getPath()
	{
		return $this->getAttribute('file');
	}

	public function setOriginalName($path): OrderUpload
	{
		$this->setAttribute('original_name', $path);
		return $this;
	}

	public function getOriginalName()
	{
		return $this->getAttribute('original_name');
	}

}
