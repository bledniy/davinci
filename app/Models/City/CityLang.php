<?php declare(strict_types=1);

namespace App\Models\City;

use App\Models\ModelLang;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CityLang extends ModelLang
{
	protected $primaryKey = ['city_id', 'language_id'];

	public function city(): BelongsTo
	{
		return $this->belongsTo(City::class);
	}
}
