<?php declare(strict_types=1);

namespace App\Models\City;

use App\Contracts\HasLocalized;
use App\Models\Model;
use App\Traits\Models\Localization\RedirectLangColumn;

class City extends Model implements HasLocalized
{
	use RedirectLangColumn;

	protected $hasOneLangArguments = [CityLang::class, 'city_id'];

	protected $guarded = ['id'];

	protected $langColumns = ['name', 'language_id'];

	public function getName(): string
	{
		return (string)$this->name;
	}

	public function getUrl(): string
	{
		return (string)$this->getAttribute('url');
	}

}
