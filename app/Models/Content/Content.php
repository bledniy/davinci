<?php

	namespace App\Models\Content;


	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Traits\Models\ImageAttributeTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use Illuminate\Database\Eloquent\Relations\MorphTo;

	class Content extends Model implements HasLocalized
	{
		use RedirectLangColumn;
		use ImageAttributeTrait;

		protected $langColumns = ['name', 'title', 'excerpt', 'description', 'language_id'];

		protected $guarded = ['id'];

		protected $fillable = [
			'contentable_type',
			'contentable_id',
			'type',
			'image',
			'sort',
			'active',
		];

		protected $hasOneLangArguments = [ContentLang::class];

		public function contentable(): MorphTo
		{
			return $this->morphTo();
		}

	}
