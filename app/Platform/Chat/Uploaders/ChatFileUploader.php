<?php


namespace App\Platform\Chat\Uploaders;


use App\Uploaders\BaseFileUploader;

class ChatFileUploader extends BaseFileUploader
{
	protected $path = 'uploads/chats';
}