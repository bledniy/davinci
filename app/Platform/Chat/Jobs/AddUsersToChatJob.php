<?php


namespace App\Platform\Chat\Jobs;


use App\Contracts\Models\ModelHasChat;
use App\Models\Chat\Chat;
use App\Models\User;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Support\Arr;

class AddUsersToChatJob
{

	/**
	 * @var ChatRepository
	 */
	private $chatRepository;
	/**
	 * @var Chat
	 */
	private $chat;
	/**
	 * @var array |  User[]
	 */
	private $users;

	public function __construct(Chat $chat, array $users, ChatRepository $chatRepository)
	{
		$this->chatRepository = $chatRepository;
		$this->chat = $chat;
		$this->users = $users;
	}

	public static function make(Chat $chat, array $users): self
	{
		return app(__CLASS__, compact('chat', 'users'));
	}

	public function add(): void
	{
		$ids = Arr::pluck($this->users, 'id');
		$this->chatRepository->addUsersToChat($this->chat, $ids);
	}
}