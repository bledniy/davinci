<?php


namespace App\Platform\Chat\Jobs;


use App\Contracts\Models\ModelHasChat;
use App\Helpers\Debug\LoggerHelper;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Models\User;
use App\Platform\Cabinet\Display\UserLastActivity;
use App\Platform\Chat\Services\PresenceChatUsersService;
use App\Platform\Contract\Chat\ChatMessageTypesContract;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Support\Facades\Redis;

class ChatMessageCreatorJob
{
	private $messageMutators = [];
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;
	/**
	 * @var Chat
	 */
	private $chat;
	/**
	 * @var User
	 */
	private $userFrom;

	private $message;

	public function __construct(Chat $chat, User $userFrom, $message, ChatRepository $chatRepository)
	{
		$this->chatRepository = $chatRepository;
		$this->chat = $chat;
		$this->userFrom = $userFrom;
		$this->message = $message;
	}

	public static function make(Chat $chat, User $userFrom, $message = ''): self
	{
		return app(__CLASS__, compact('chat', 'userFrom', 'message'));
	}

	public function create(): ChatMessage
	{
		$currentUser = $this->userFrom;
		/** @var  $userTo User */
		$userTo = $this->chatRepository->getCompanion($this->chat, $currentUser);
		$setter = ($chatMessage = app(ChatMessage::class))->getSetter()
			->setChatId($this->chat->getKey())
			->setUserId($currentUser->getKey())
			->setMessage($this->message)
			->setIsWatched(false)
		;
		if ($userTo) {
			$hasActiveUsers = app(PresenceChatUsersService::class)->isChatNotEmpty($this->chat->getKey(), $currentUser);
			$isOnline = app(UserLastActivity::class, ['user' => $userTo])->isOnline();
			$setter->setIsWatched($hasActiveUsers && $isOnline);
		}

		if (!$this->chat->getUpdatedAt() || now()->diffInMinutes($this->chat->getUpdatedAt()) >= 1) {
			$this->chat->touch();
		}
		foreach ($this->messageMutators as $messageMutator) {
			$messageMutator($chatMessage);
		}
		$chatMessage->save();
		return $chatMessage;
	}

	public function addMessageMutator(callable $func): self
	{
		$this->messageMutators[] = $func;
		return $this;
	}

	/**
	 * @param mixed $message
	 */
	public function setMessage($message): self
	{
		$this->message = $message;
		return $this;
	}

	public function withType(string $type):self
	{
		$this->addMessageMutator(
			function (ChatMessage $chatMessage) use ($type) {
				$chatMessage->getSetter()->setType($type);
			}
		);
		return $this;
	}
}