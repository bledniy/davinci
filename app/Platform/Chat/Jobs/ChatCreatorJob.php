<?php


namespace App\Platform\Chat\Jobs;


use App\Contracts\Models\ModelHasChat;
use App\Models\Chat\Chat;
use App\Repositories\Chat\ChatRepository;

class ChatCreatorJob
{
	/**
	 * @var ModelHasChat
	 */
	private $model;
	/**
	 * @var array
	 */
	private $attributes;
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(ModelHasChat $model, ChatRepository $chatRepository, array $attributes = [])
	{
		$this->model = $model;
		$this->attributes = $attributes;
		$this->chatRepository = $chatRepository;
	}

	public static function make(ModelHasChat $model, array $attributes = []): self
	{
		return app(__CLASS__, compact('model', 'attributes'));
	}

	public function create(): Chat
	{
		$this->attributes = array_merge(['name' => $this->model->getChatName()], $this->attributes);
		return $this->chatRepository->createForRelated($this->model, $this->attributes);
	}

}