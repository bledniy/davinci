<?php


namespace App\Platform\Chat\Services;


use App\Models\User;

class UserPresenceOnDealChannel
{
	/**
	 * @var PresenceMembersService
	 */
	private $presenceChatUsersService;

	public function __construct(PresenceMembersService $presenceChatUsersService)
	{
		$this->presenceChatUsersService = $presenceChatUsersService;
	}

	public function isUserOnDealPage(User $user, $dealId): bool
	{
		return $this->presenceChatUsersService->setKey(sprintf('deals.%d.new-bid', (int)$dealId))->isChatHasUser($user);
	}
}