<?php

	namespace App\Platform\Chat\Services;


	use App\Helpers\Debug\LoggerHelper;
	use App\Models\User;
	use Illuminate\Support\Facades\Redis;

	class PresenceMembersService
	{
		protected $key = '';

		public function __construct($key = null)
		{
			if ($key) {
				$this->key = $key;
			}
		}

		protected function getChannelName(): string
		{
			return sprintf('presence-%s:members', (string)$this->key);
		}

		public function isChatHasUser(User $user): bool
		{
			return collect($this->getUsersInChat())->where('user_id', (string)$user->getKey())->count() > 0;
		}

		public function getUsersInChat(?User $exceptUser = null): array
		{
			try {
				$users = Redis::get($this->getChannelName());
				if (!isJson($users)) {
					return [];
				}
				return collect(json_decode($users, true, 512, JSON_THROW_ON_ERROR))
					->when($exceptUser,
						function ($collection, $exceptUser) {
							return $collection->where('user_id', '!=', $exceptUser->getKey());
						})
					->all()
					;
			} catch (\Throwable $e) {
				app(LoggerHelper::class)->error($e);
			}
			return [];
		}

		public function setKey($key)
		{
			$this->key = $key;
			return $this;
		}
	}