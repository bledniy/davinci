<?php


namespace App\Platform\Chat\Services;


use App\Helpers\Debug\LoggerHelper;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;

class PresenceChatUsersService
{
	/**
	 * @var PresenceMembersService
	 */
	private $chatMembersService;

	public function __construct(PresenceMembersService $chatMembersService)
	{
		$this->chatMembersService = $chatMembersService;
	}

	public function isChatNotEmpty($chatId, ?User $exceptUser = null): bool
	{
		return count($this->chatMembersService->setKey('chats.'.$chatId)->getUsersInChat($exceptUser)) > 0;
	}

}