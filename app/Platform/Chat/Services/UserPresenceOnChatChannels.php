<?php


namespace App\Platform\Chat\Services;


use App\Models\User;

class UserPresenceOnChatChannels
{
	/**
	 * @var PresenceMembersService
	 */
	private $presenceChatUsersService;

	public function __construct(PresenceMembersService $presenceChatUsersService)
	{
		$this->presenceChatUsersService = $presenceChatUsersService;
	}

	public function isUserNeedsNotify(User $user, $chatIdSubmittedMessage)
	{
		if ($this->presenceChatUsersService->setKey('chats.' . $chatIdSubmittedMessage)->isChatHasUser($user)) {
			return false;
		}
		return !$this->presenceChatUsersService->setKey('chats.user.' . $user->getKey())->isChatHasUser($user);
	}
}