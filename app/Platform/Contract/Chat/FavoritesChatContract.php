<?php


namespace App\Platform\Contract\Chat;


interface FavoritesChatContract
{
	public const SUCCESS_ATTACHED  = 1;
	public const SUCCESS_DETACHED  = 2;

}