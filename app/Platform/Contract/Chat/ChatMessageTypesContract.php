<?php


namespace App\Platform\Contract\Chat;


interface ChatMessageTypesContract
{
	public const TYPE_SYSTEM = 'system';
	public const TYPE_SYSTEM_PERFORMER = 'system_performer';
	public const TYPE_FILE = 'file';


}