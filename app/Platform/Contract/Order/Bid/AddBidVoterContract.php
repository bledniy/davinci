<?php


namespace App\Platform\Contract\Order\Bid;


use App\Models\Order\Order;
use App\Models\User;

interface AddBidVoterContract
{
	public function check(Order $order, ?User $checkableUser = null): bool;

	public function message(): string;
}