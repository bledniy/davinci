<?php


namespace App\Platform\Route;


interface PlatformRoutes
{
	public const CABINET_PERFORMER_PERSONAL = 'cabinet.personal';
	public const CABINET_PERFORMER_PORTFOLIO = 'cabinet.portfolio';
	public const CABINET_PERFORMER_PASSWORD = 'cabinet.password';
	public const CABINET_PERFORMER_REVIEWS = 'cabinet.reviews';
	public const CABINET_PERFORMER_DELETE_AVATAR = 'cabinet.avatar.delete';

	public const CABINET_CUSTOMER_PERSONAL = 'cabinet.customer.personal';
	public const CABINET_CUSTOMER_PASSWORD = 'cabinet.password';
	public const CABINET_CUSTOMER_REVIEWS = 'cabinet.customer.reviews';
	public const CABINET_CUSTOMER_DELETE_AVATAR = 'cabinet.customer.avatar.delete';

	public const CABINET_ORDERS = 'cabinet.orders';

	public const PERFORMERS_LIST = 'platform.performers.index';
	public const PERFORMERS_SHOW = 'platform.performers.show';

	public const CUSTOMERS_SHOW = 'platform.customers.show';


}