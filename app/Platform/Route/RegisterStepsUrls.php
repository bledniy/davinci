<?php


namespace App\Platform\Route;


interface RegisterStepsUrls
{
	public const STEP_CHOOSE_TYPE = 'register/type';
	public const STEP_DATA_CUSTOMER = 'register/customer/personal';
	public const STEP_DATA_PERFORMER = 'register/performer/personal';
	public const STEP_PERFORMER_CONFIRM_EMAIL = 'register/performer/email/confirm';
	public const STEP_PERFORMER_CONFIRM_EMAIL_RESEND = 'register/performer/email/resend';
	public const STEP_PERFORMER_CONFIRM_PHONE = 'register/performer/phone/confirm';
	public const STEP_PERFORMER_CONFIRM_PHONE_RESEND = 'register/performer/phone/resend';
}