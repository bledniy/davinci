<?php


namespace App\Platform\Model;


use App\Platform\Contract\UserTypeContract;

class UserFields
{
	public static function get($type, string $field): string
	{
		$types = [
			UserTypeContract::TYPE_CUSTOMER => [
				'about'           => 'customer_about',
				'rating'          => 'customer_rating',
			],
		];
		return $types[$type][$field] ?? $field;
	}

	public static function getCustomer($field): string
	{
		return self::get(UserTypeContract::TYPE_CUSTOMER, $field);
	}
}