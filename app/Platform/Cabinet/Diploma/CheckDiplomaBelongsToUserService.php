<?php


namespace App\Platform\Cabinet\Diploma;


use App\Models\User;
use App\Models\User\Diploma;

class CheckDiplomaBelongsToUserService
{
	/**
	 * @var Diploma
	 */
	private $diploma;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(Diploma $diploma, User $user)
	{
		$this->diploma = $diploma;
		$this->user = $user;
	}

	public static function make(Diploma $diploma, User $user): self
	{
		return app(self::class, compact('diploma', 'user'));
	}

	public function check()
	{
		return $this->user->getKey() === (int)$this->diploma->getBelongsKey('user');
	}
}