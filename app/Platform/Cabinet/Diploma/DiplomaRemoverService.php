<?php


namespace App\Platform\Cabinet\Diploma;


use App\Helpers\Media\ImageRemover;
use App\Models\User\Diploma;
use App\Repositories\PerformerDiplomaRepository;

class DiplomaRemoverService
{
	/**
	 * @var PerformerDiplomaRepository
	 */
	private $diplomaRepository;
	/**
	 * @var Diploma
	 */
	private $diploma;
	/**
	 * @var ImageRemover
	 */
	private $imageRemover;

	public function __construct(Diploma $portfolio, PerformerDiplomaRepository $diplomaRepository, ImageRemover $imageRemover) {
		$this->diplomaRepository = $diplomaRepository;
		$this->diploma = $portfolio;
		$this->imageRemover = $imageRemover;
	}

	public static function make(Diploma $portfolio)
	{
		return app(self::class, compact('portfolio'));
	}

	public function remove(): bool
	{
		if ($this->diplomaRepository->delete($this->diploma->getKey())) {
			$this->imageRemover->removeImage($this->diploma->image);
		    return true;
		}
		return false;
	}
}