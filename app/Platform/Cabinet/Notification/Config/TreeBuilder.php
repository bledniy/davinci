<?php


namespace App\Platform\Cabinet\Notification\Config;


use App\Models\User\NotificationConfig;
use Illuminate\Support\Collection;

class TreeBuilder
{
	public static function build(Collection $list)
	{

		$parents = $list->filter(function (NotificationConfig $config) {
			return null === $config->parent_id;
		});
		$parents->map(function (NotificationConfig $config) use ($list) {
			$config->setRelation('child', $list->where('parent_id', $config->getKey()));
		});
		return $parents;

	}
}