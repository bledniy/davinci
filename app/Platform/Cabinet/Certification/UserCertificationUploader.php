<?php


namespace App\Platform\Cabinet\Certification;


use App\Uploaders\BaseFileUploader;

class UserCertificationUploader extends BaseFileUploader
{
	protected $path = 'users/certifications';
}