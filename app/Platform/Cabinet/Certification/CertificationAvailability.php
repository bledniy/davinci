<?php declare(strict_types=1);

namespace App\Platform\Cabinet\Certification;

use App\Models\Category\Category;
use App\Models\User;
use App\Models\User\UserCertification;
use App\Repositories\UserCertificationRepository;
use function app;

class CertificationAvailability
{

	/**
	 * @var UserCertificationRepository
	 */
	private $certificationRepository;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user, UserCertificationRepository $certificationRepository)
	{
		$this->certificationRepository = $certificationRepository;
		$this->user = $user;
	}

	public static function make(User $user)
	{
		return app(self::class, ['user' => $user]);
	}

	public function isCategoryAvailable(Category $category): bool
	{
		/** @var  $certification null | UserCertification */
		$certification = $this->certificationRepository->findByUserCategory($this->user, $category);
		if (null === $certification) {
			return true;
		}

		return self::check($certification);
	}

	public static function check(UserCertification $certification): bool
	{
		if (null !== $certification->getDateCertification()) {
		    return false;
		}
		return $certification->getCertificationStatuses()->isRejected();
	}
}