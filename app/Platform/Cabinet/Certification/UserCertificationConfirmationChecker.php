<?php declare(strict_types=1);

namespace App\Platform\Cabinet\Certification;

use App\Models\Category\Category;
use App\Models\User;
use App\Platform\Cache\PlatformCacheKeysGenerator;
use Illuminate\Support\Facades\Cache;
use function app;

class UserCertificationConfirmationChecker
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public static function make(User $user)
	{
		return app(self::class, ['user' => $user]);
	}

	public function check(Category $category): bool
	{
		$cacheKey = PlatformCacheKeysGenerator::userCategoryCertification($this->user, $category);

		return Cache::get($cacheKey, function () use ($cacheKey, $category) {
			$exists = $this->user->performerCategories()->whereCategoryId($category->getKey())->exists();
			Cache::set($cacheKey, $exists);

			return $exists;
		});
	}
}