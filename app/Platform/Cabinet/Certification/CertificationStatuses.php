<?php declare(strict_types=1);

namespace App\Platform\Cabinet\Certification;

use App\Models\User\UserCertification;

class CertificationStatuses
{
	public const STATUS_CHECK = 'check';
	public const STATUS_SUCCESS = 'success';
	public const STATUS_REJECTED = 'rejected';
	/**
	 * @var UserCertification
	 */
	private $certification;

	public function __construct(UserCertification $certification)
	{
		$this->certification = $certification;
	}

	public function getStatusName(): ?string
	{
		$statuses = $this->getStatuses();
		return $statuses[$this->certification->getStatus()] ?? null;
	}

	public function getStatuses(): array
	{
		return [
			self::STATUS_CHECK    => 'Сертификат на проверке',
			self::STATUS_SUCCESS  => 'Сертификация подтверждена',
			self::STATUS_REJECTED => 'Сертификация отклонена',
		];
	}

	public static function getBaseStatus(): string
	{
		return self::STATUS_CHECK;
	}

	public function changeStatus($status): void
	{
		$setter = $this->certification->getSetter()->resetCertifiedAt()->setStatus($status);

		if ($status === self::STATUS_SUCCESS) {
			$setter->wasCertified();
		}
	}

	public function isSuccess(): bool
	{
		return $this->getCertification()->getStatus() === self::STATUS_SUCCESS;
	}

	public function isOnCheck(): bool
	{
		return $this->getCertification()->getStatus() === self::STATUS_CHECK;
	}

	public function isRejected(): bool
	{
		return $this->getCertification()->getStatus() === self::STATUS_REJECTED;
	}

	/**
	 * @return UserCertification
	 */
	public function getCertification(): UserCertification
	{
		return $this->certification;
	}
}