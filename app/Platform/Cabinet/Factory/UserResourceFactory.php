<?php

namespace App\Platform\Cabinet\Factory;

use App\Http\Resources\User\Platform\CustomerResource;
use App\Http\Resources\User\Platform\PerformerResource;
use App\Models\User;
use App\Platform\Contract\UserTypeContract;
use App\Platform\User\Type\UserTypeNotFound;
use Illuminate\Support\Collection;

class UserResourceFactory
{
    /**
     * @param User $user
     * @return CustomerResource|PerformerResource
     * @throws UserTypeNotFound
     */
    public static function make(User $user)
    {
        switch ($user->getType()) {
            case UserTypeContract::TYPE_CUSTOMER:
                return CustomerResource::make($user);
            case UserTypeContract::TYPE_PERFORMER:
                return PerformerResource::make($user);
            default:
                throw new UserTypeNotFound('Not found user type on create user resource');
        }
    }

    /**
     * @param array $users | User[] | Collection
     * @return CustomerResource[]|PerformerResource[]|array | Collection
     * @throws UserTypeNotFound
     */
    public static function collection($users = [])
    {
        $fn = function (User $user) {
            return self::make($user);
        };
        if ($users instanceof Collection) {
            return $users->map($fn);
        }
        return array_map($fn, $users);
    }
}