<?php declare(strict_types=1);

namespace App\Platform\Cabinet\Display;

use App\Models\User;
use App\Platform\Analytics\UserLastSeenAt;
use Illuminate\Support\Carbon;

class UserLastActivity
{
	/**
	 * @var UserLastSeenAt
	 */
	private $lastSeenAt;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user, UserLastSeenAt $lastSeenAt)
	{
		$this->lastSeenAt = $lastSeenAt;
		$this->user = $user;
	}

	public function isOnline(): bool
	{
		$lastSeen = $this->getLastSeenAt();
		if (!$lastSeen) {
			return false;
		}
		return now()->subMinutes(5) < $lastSeen;
	}

	public function getLastSeenAt(): ?Carbon
	{
		if (null === ($lastSeen = $this->lastSeenAt->getLastSeen((string)$this->user->getKey()))) {
			$lastSeen = $this->user->getDateLastSeen();
			$this->lastSeenAt->setLastSeen((string)$this->user->getKey(), $lastSeen);
		}

		return $lastSeen;
	}

}