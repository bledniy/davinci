<?php


namespace App\Platform\Cabinet\Display;


use App\Models\User;

class UserAvatarHelper
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function getPerformerAvatar()
	{
		$avatar = $this->user->getAvatar();
		if (!$avatar) {
			return null;
		}
		if (isExternalFile($avatar)) {
			return $avatar;
		}
		if (storageFileExists($avatar)) {
			return getApiImageUrl($avatar);
		}
		return null;
	}

	public function getCustomerAvatar()
	{
		return $this->getPerformerAvatar();
	}
}