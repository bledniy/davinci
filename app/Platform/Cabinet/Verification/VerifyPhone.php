<?php


namespace App\Platform\Cabinet\Verification;


use App\Helpers\Debug\LoggerHelper;
use App\Models\User;
use App\Services\SMS\SMSSenderService;
use App\Services\Verification\BaseVerificationService;

class VerifyPhone extends BaseVerificationService
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user)
	{
		parent::__construct();
		$this->user = $user;
	}

	protected function getTarget()
	{
		return $this->user->getPhone();
	}

	protected function getType()
	{
		return 'phone-verification';
	}

	public function notify()
	{
		$this->notifyViaSms();
	}
}