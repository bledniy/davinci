<?php

namespace App\Platform\Cabinet\Verification;

use App\Models\User;
use App\Services\Verification\BaseVerificationService;

class VerifyEmail extends BaseVerificationService
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user)
	{
		parent::__construct();
		$this->user = $user;
	}

	protected function getTarget(): string
	{
		return $this->user->getEmail();
	}

	protected function getType(): string
	{
		return 'email-verification';
	}

	public function notify() :void
	{
		$this->notifyViaMail($this->user);
	}
}