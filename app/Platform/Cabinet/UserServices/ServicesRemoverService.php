<?php


namespace App\Platform\Cabinet\UserServices;


use App\Models\User\Service;
use App\Repositories\PerformerServiceRepository;

class ServicesRemoverService
{
	/**
	 * @var PerformerServiceRepository
	 */
	private $serviceRepository;
	/**
	 * @var Service
	 */
	private $service;

	public function __construct(Service $service, PerformerServiceRepository $skillsRepository) {
		$this->serviceRepository = $skillsRepository;
		$this->service = $service;
	}

	public static function make(Service $service)
	{
		return app(self::class, compact('service'));
	}

	public function remove()
	{
		if ($this->serviceRepository->delete($this->service->getKey())) {
		    return true;
		}
		return false;
	}
}