<?php


namespace App\Platform\Cabinet\UserServices;


use App\Models\User;
use App\Models\User\Service;

class CheckServiceBelongsToUserService
{
	/**
	 * @var Service
	 */
	private $service;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(Service $service, User $user)
	{
		$this->service = $service;
		$this->user = $user;
	}

	public static function make(Service $service, User $user): self
	{
		return app(self::class, compact('service', 'user'));
	}

	public function check()
	{
		return $this->user->getKey() === (int)$this->service->getBelongsKey('user');
	}
}