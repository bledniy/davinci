<?php


namespace App\Platform\Cabinet\Contacts;


use App\Helpers\Debug\LoggerHelper;
use App\Models\User;
use App\Services\SMS\SMSSenderService;
use App\Services\Verification\BaseVerificationService;

class ChangePhone extends BaseVerificationService
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user)
	{
		parent::__construct();
		$this->user = $user;
	}

	protected function getTarget()
	{
		return $this->user->getPhone();
	}

	protected function getType()
	{
		return 'phone-change';
	}

	public function notify()
	{
		$this->notifyViaSms();
	}
}