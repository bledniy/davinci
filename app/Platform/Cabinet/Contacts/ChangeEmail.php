<?php declare(strict_types=1);

namespace App\Platform\Cabinet\Contacts;

use App\Helpers\Debug\LoggerHelper;
use App\Mail\Register\VerifyMail;
use App\Models\User;
use App\Services\Verification\BaseVerificationService;
use Illuminate\Support\Facades\Mail;

class ChangeEmail extends BaseVerificationService
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user)
	{
		parent::__construct();
		$this->user = $user;
	}

	protected function getTarget()
	{
		return $this->user->getEmail();
	}

	protected function getType()
	{
		return 'email-change';
	}

	public function notify()
	{
		$this->notifyViaMail($this->user);
	}

}