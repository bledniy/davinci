<?php


namespace App\Platform\Cabinet\Uploads;


use App\Helpers\Media\ImageSaver;
use Illuminate\Http\UploadedFile;

class PerformerDiplomaImagesUploader
{
	/**
	 * @var ImageSaver
	 */
	private $imageSaver;

	private $folderName = 'users/portfolio';

	public function __construct(ImageSaver $imageSaver)
	{
		$this->imageSaver = $imageSaver;
	}

	public function upload(UploadedFile $file): string
	{
		$this->imageSaver->setFolderName($this->folderName)
			->setThumbnailSizes(400, 400)
		;
		return $this->imageSaver->saveFromUploadedFile($file);
	}

	/**
	 * @param string $folderName
	 */
	public function setFolderName(string $folderName): void
	{
		$this->folderName = $folderName;
	}

}