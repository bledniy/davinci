<?php


namespace App\Platform\Cabinet\Uploads\Avatar;


use App\Helpers\Media\ImageSaver;
use App\Models\User;
use Illuminate\Http\UploadedFile;

class AvatarUploader
{
	/**
	 * @var UploadedFile
	 */
	private $file;
	/**
	 * @var ImageSaver
	 */
	private $imageSaver;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(UploadedFile $file, User $user, ImageSaver $imageSaver)
	{
		$this->file = $file;
		$this->imageSaver = $imageSaver;
		$this->user = $user;
	}

	public function uploadDefault(): string
	{
		$this->defaults()->imageSaver->setFolderName($this->user->getTable() . DIRECTORY_SEPARATOR . $this->user->getKey());
		return $this->imageSaver->saveFromUploadedFile($this->file);
	}

	protected function defaults(): self
	{
		$this->imageSaver
			->setWithThumbnail(false)
			->setResizeTo(600, 600)
		;
		return $this;
	}
}