<?php


namespace App\Platform\User;


class AuthenticatedUserType
{
	/**
	 * @var object | null
	 */
	private $settedBy;

	/**
	 * @var AuthenticatedUserTypeStorage
	 */
	private $authenticatedUserTypeStorage;

	public function __construct(AuthenticatedUserTypeStorage $authenticatedUserTypeStorage)
	{
		$this->authenticatedUserTypeStorage = $authenticatedUserTypeStorage;
	}

	/**
	 * @param string $userType
	 * @return AuthenticatedUserType
	 */
	public function setUserType(string $userType): AuthenticatedUserType
	{
		$this->authenticatedUserTypeStorage->storeUserType($userType);
		return $this;
	}

	/**
	 * @return ?string
	 */
	public function getUserType(): ?string
	{
		return $this->authenticatedUserTypeStorage->getStoredUserType();
	}

	/**
	 * @param mixed $settedBy
	 * @return AuthenticatedUserType
	 */
	public function setSettedBy($settedBy): AuthenticatedUserType
	{
		$this->settedBy = $settedBy;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSettedBy()
	{
		return $this->settedBy;
	}

}