<?php


namespace App\Platform\User;


use App\Models\User;

class UserLanguageExtractor
{
	/**
	 * @var User | null
	 */
	private $user;

	public function __construct(?User $user)
	{
		$this->user = $user;
	}

	public static function make(?User $user): self
	{
		return app(__CLASS__, compact('user'));
	}

	public function getLanguageId(): int
	{
		$default = (int)getDefaultLang(false)->getKey();
		if (!$this->user) {
			return $default;
		}
		return (int)$this->user->getLanguageId() ?: $default;
	}
}