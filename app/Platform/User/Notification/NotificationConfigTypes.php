<?php


namespace App\Platform\User\Notification;


interface NotificationConfigTypes
{
	public const GROUP_MAIL = 'GROUP_MAIL';
	public const GROUP_BROWSER = 'GROUP_BROWSER';

	public const MAIL_PERSONAL_MESSAGES = 'MAIL_PERSONAL_MESSAGES';
	public const MAIL_NEW_ORDERS = 'MAIL_NEW_ORDERS';
	public const MAIL_NEW_ORDER_BIDS = 'MAIL_NEW_ORDER_BIDS';
	public const MAIL_NEW_NEWS = 'MAIL_NEW_NEWS';
	//
	public const BROWSER_PERSONAL_MESSAGES = 'BROWSER_PERSONAL_MESSAGES';
	public const BROWSER_NEW_ORDERS = 'BROWSER_NEW_ORDERS';
	public const BROWSER_NEW_ORDER_BIDS = 'BROWSER_NEW_ORDER_BIDS';
	public const BROWSER_NEW_NEWS = 'BROWSER_NEW_NEWS';

}