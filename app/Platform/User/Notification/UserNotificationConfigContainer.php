<?php


	namespace App\Platform\User\Notification;


	use App\Models\User;
	use App\Repositories\NotificationConfigRepository;

	class UserNotificationConfigContainer
	{
		private $config;
		/**
		 * @var User
		 */
		private $user;
		/**
		 * @var NotificationConfigRepository
		 */
		private $notificationConfigRepository;

		public function __construct(User $user, NotificationConfigRepository $notificationConfigRepository)
		{
			$this->user = $user;
			$this->notificationConfigRepository = $notificationConfigRepository;
		}

		public static function make(User $user): self
		{
			return app(__CLASS__, compact('user'));
		}

		public function getUserNotifications()
		{
			if (null === $this->config) {
				$this->config = $this->notificationConfigRepository->allOfUser($this->user)->keyBy('code');
			}
			return $this->config;
		}

		public function isEnabled($key): bool
		{
			return null !== $this->getUserNotifications()->get($key)->user_id;
		}

		public function isEnabledMailChatMessage(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::MAIL_PERSONAL_MESSAGES);
		}

		public function isEnabledBrowserChatMessage(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::BROWSER_PERSONAL_MESSAGES);
		}

		public function isEnabledMailNews(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::MAIL_NEW_NEWS);
		}

		public function isEnabledBrowserNews(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::BROWSER_NEW_NEWS);
		}

		public function isEnabledMailNewBids(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::MAIL_NEW_ORDER_BIDS);
		}

		public function isEnabledBrowserNewBids(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::BROWSER_NEW_ORDER_BIDS);
		}

		public function isEnabledMailNewDeals(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::MAIL_NEW_ORDERS);
		}

		public function isEnabledBrowserNewDeals(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::MAIL_NEW_ORDERS);
		}

		public function isEnabledMailNewOrders(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::MAIL_NEW_ORDERS);
		}

		public function isEnabledBrowserNewOrders(): bool
		{
			return $this->isEnabled(NotificationConfigTypes::BROWSER_NEW_ORDERS);
		}

		public function isEnabledAnyFromGroupNewPost():bool
		{
			return $this->isEnabledBrowserNews() || $this->isEnabledMailNews();
		}

		public function isEnabledAnyFromGroupNewBids():bool
		{
			return $this->isEnabledBrowserNewBids() || $this->isEnabledMailNewBids();
		}
	}