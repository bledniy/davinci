<?php


namespace App\Platform\User\Notification;


class UserNotificationGroups
{
	public static function getTypesOfNewPost(): array
	{
		return [
			NotificationConfigTypes::BROWSER_NEW_NEWS,
			NotificationConfigTypes::MAIL_NEW_NEWS,
		];
	}
}