<?php declare(strict_types=1);

namespace App\Platform\Analytics;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class UserLastSeenAt
{
	private $key = 'platform.users.last-seen';

	private function getData($key)
	{
		return Cache::get($key);
	}

	private function setData($key, $value): self
	{
		Cache::set($key, $value, now()->addMinutes(5));

		return $this;
	}

	protected function getKey(string $key): string
	{
		return implode(':', [$this->key, $key]);
	}

	public function getLastSeen(string $userIdAsKey): ?Carbon
	{
		return $this->transform($this->getData($this->getKey($userIdAsKey)));
	}

	public function setLastSeen(string $userIdAsKey, ?Carbon $date = null): self
	{
		if (!$date) {
			return $this;
		}

		return $this->setData($this->getKey($userIdAsKey), $date->toDateTimeString());
	}

	private function transform($date)
	{
		return isDateValid($date) ? getDateCarbon($date) : null;
	}

}