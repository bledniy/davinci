<?php declare(strict_types=1);

namespace App\Platform\ProAccount;

interface UserProAccountActivationContainerInterface
{
	public function getDays(): int;

	public function getMonths(): int;

	public function getYears(): int;

	public function getHours(): int;
}