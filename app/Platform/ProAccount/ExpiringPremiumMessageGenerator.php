<?php


namespace App\Platform\ProAccount;


use App\Helpers\Date\DaysName\DaysNameHelper;
use App\Models\User;
use App\Models\User\UserPremium;

class ExpiringPremiumMessageGenerator
{
	private static $daysHelper;

	public static function expiresIn(UserPremium $userPremium, User $user): string
	{
		$langId = $user->language_id ?? null;
		$message = getTranslate('notifications.pro-account.expiring', null, $langId);
		$expiresDays = now()->diffInDays($userPremium->getExpiresAt());
		$message .= sprintf(' %d %s', $expiresDays, self::getDaysHelper()->getNameByDay($expiresDays));
		return $message;
	}

	public static function expired(UserPremium $userPremium): string
	{
		$langId = $user->language_id ?? null;
		return getTranslate('notifications.pro-account.expired', null, $langId);
	}

	protected static function getDaysHelper(): DaysNameHelper
	{
		if (null === self::$daysHelper) {
			self::$daysHelper = app(DaysNameHelper::class);
		}
		return self::$daysHelper;
	}
}