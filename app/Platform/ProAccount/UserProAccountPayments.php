<?php

namespace App\Platform\ProAccount;

use App\DataContainers\Platform\Payment\UserPaymentSearchContainer;
use App\Enum\UserPaymentReasonEnum;
use App\Models\User;
use App\Models\User\UserPayment;
use App\Repositories\UserPaymentRepositoryEloquent;
use Illuminate\Support\Collection;

class UserProAccountPayments
{

	private $paymentRepository;

	private $container;

	public function __construct(UserPaymentSearchContainer $container, UserPaymentRepositoryEloquent $paymentRepository)
	{
		$this->paymentRepository = $paymentRepository;
		$this->container = $container;
	}

	/**
	 * @return Collection | UserPayment[]
	 */
	public function getPayments(User $user): Collection
	{
		$this->container->setUser($user)->setIsPayed(true);

		return $this->paymentRepository->findMany($this->container);
	}

	public function addFreePayment(User $user): void
	{
		$payment = new UserPayment();
		$payment->user()->associate($user);
		$payment->setReason(new UserPaymentReasonEnum(UserPaymentReasonEnum::FREE_PREMIUM))
			->setIsPayed(true)
		;
		$payment->save();
	}

}