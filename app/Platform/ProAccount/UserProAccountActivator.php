<?php

namespace App\Platform\ProAccount;

use App\Models\Premium\Premium;
use App\Models\User\UserPremium;
use App\Repositories\UserPremiumRepository;

class UserProAccountActivator
{
	/**
	 * @var UserPremiumRepository
	 */
	private $userPremiumRepository;

	public function __construct(UserPremiumRepository $userPremiumRepository)
	{
		$this->userPremiumRepository = $userPremiumRepository;
	}

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function extendDateExpirationProAccount(Premium $premium, UserPremium $userPremium, UserProAccountActivationContainerInterface $container): UserPremium
	{
		$expiresAt = getDateCarbon($userPremium->getExpiresAt());
		if ($container->getYears()) {
			$expiresAt->addYears($container->getYears());
		}
		if ($container->getMonths()) {
			$expiresAt->addMonths($container->getMonths());
		}
		if ($container->getDays()) {
			$expiresAt->addDays($container->getDays());
		}
		if ($container->getHours()) {
			$expiresAt->addHours($container->getHours());
		}
		$userPremium->setExpiresAt($expiresAt)->setPremiumId($premium->getKey());
		try {
			$this->userPremiumRepository->update([], $userPremium);
		} catch (\Throwable $e) {
			logger($e);
		}
		return $userPremium;
	}
}