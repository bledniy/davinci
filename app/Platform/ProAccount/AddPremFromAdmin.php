<?php declare(strict_types=1);

namespace App\Platform\ProAccount;

use App\Helpers\Debug\LoggerHelper;
use App\Http\Resources\UserPremiumResource;
use App\Models\Premium\Premium;
use App\Models\User;

class AddPremFromAdmin
{

	/**
	 * @var UserProAccountActivator
	 */
	private $proAccountActivator;

	public function __construct(UserProAccountActivator $proAccountActivator)
	{
		$this->proAccountActivator = $proAccountActivator;
	}

	public function add(User $user, Premium $premium, UserProAccountActivationContainerInterface $accountActivationContainer)
	{
		$userPremiumService = $user->getPremiumAccountService();

		$userPremium = $userPremiumService->getProAccount();
		$accountActivationContainer->setMonths(1);
		try {
			$this->proAccountActivator->extendDateExpirationProAccount($premium, $userPremium, $accountActivationContainer);

			return UserPremiumResource::make($userPremium);
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
		}

		return null;
	}
}