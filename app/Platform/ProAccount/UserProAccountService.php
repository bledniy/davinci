<?php declare(strict_types=1);

namespace App\Platform\ProAccount;

use App\DataContainers\Platform\Payment\UserPaymentSearchContainer;
use App\Enum\UserPaymentReasonEnum;
use App\Models\User;
use App\Models\User\UserPremium;
use App\Repositories\PremiumRepository;
use App\Repositories\UserPaymentRepositoryEloquent;
use App\Repositories\UserPremiumRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class UserProAccountService
{
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var UserPremiumRepository
	 */
	private $userPremiumRepository;
	/**
	 * @var PremiumRepository
	 */
	private $premiumRepository;
	/**
	 * @var UserProAccountActivator
	 */
	private $userProAccountActivator;
	/**
	 * @var UserProAccountPayments
	 */
	private $proAccountPayments;
	/**
	 * @var UserPaymentRepositoryEloquent
	 */
	private $userPaymentRepository;

	public function __construct(
		User                    $user,
		UserPremiumRepository   $userPremiumRepository,
		UserPaymentRepositoryEloquent $userPaymentRepository,
		PremiumRepository       $premiumRepository,
		UserProAccountActivator $userProAccountActivator,
		UserProAccountPayments  $proAccountPayments
	)
	{
		$this->user = $user;
		$this->userPremiumRepository = $userPremiumRepository;
		$this->premiumRepository = $premiumRepository;
		$this->userProAccountActivator = $userProAccountActivator;
		$this->proAccountPayments = $proAccountPayments;
		$this->userPaymentRepository = $userPaymentRepository;
	}

	public function isActivePremium(): bool
	{
		if (!$this->getProAccount() || null === $this->getProAccount()->getExpiresAt()) {
			return false;
		}

		return $this->getProAccount()->getExpiresAt()->greaterThan(
			now()
		);
	}

	public function getProAccount(): UserPremium
	{
		$userPremium = $this->userPremiumRepository->findByUser($this->user);
		if (null === $userPremium) {
			$userPremium = new UserPremium();
			$userPremium->user()->associate($this->user);
		}

		return $userPremium;
	}

	public function canApplyFreeProAccount(): bool
	{
		$container = new UserPaymentSearchContainer();
		$container->setUser($this->user)->setIsPayed(true)
			->setReasons([new UserPaymentReasonEnum(UserPaymentReasonEnum::FREE_PREMIUM)]);
		return null === $this->userPaymentRepository->findOne($container);
	}

	public function activateFreeProAccount(): UserPremium
	{
		if (!$this->canApplyFreeProAccount()) {
			throw new BadRequestException('You already applied free premium account');
		}

		$premium = $this->premiumRepository->getDefaultPremium();
		$userPremium = $this->getProAccount();
		$userPremium->user()->associate($this->user);
		($proAccActivationContainer = new UserProAccountActivationContainer)->setMonths(1);
		$this->userProAccountActivator->extendDateExpirationProAccount($premium, $userPremium, $proAccActivationContainer);
		$this->proAccountPayments->addFreePayment($this->user);

		return $userPremium;
	}
}