<?php declare(strict_types=1);

namespace App\Platform\ProAccount;

final class UserProAccountActivationContainer implements UserProAccountActivationContainerInterface
{
	private $years = 0;
	private $months = 0;
	private $days = 0;
	private $hours = 0;

	public function getDays(): int
	{
		return $this->days;
	}

	public function setDays(int $days): self
	{
		$this->days = $days;

		return $this;
	}

	public function getMonths(): int
	{
		return $this->months;
	}

	public function setMonths(int $months): self
	{
		$this->months = $months;

		return $this;
	}

	public function getYears(): int
	{
		return $this->years;
	}

	public function setYears(int $years): self
	{
		$this->years = $years;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getHours(): int
	{
		return $this->hours;
	}

	public function setHours(int $hours): self
	{
		$this->hours = $hours;

		return $this;
	}
}