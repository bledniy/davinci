<?php


	namespace App\Platform\Order\Services;


	use App\Models\Category\Category;
	use App\Models\City\City;
	use App\Models\User;
	use App\Platform\Order\DataContainers\OrderCreateContainer;
	use Illuminate\Support\Str;

	class TestOrderCreateContainerFakeDataFiller
	{
		/**
		 * @var OrderCreateContainer
		 */
		private $container;
		/**
		 * @var \Faker\Generator
		 */
		private $factory;

		static $cachedCategoryId = false;
		static $cachedCityId = false;
		static $cachedCustomerId = false;

		public function __construct(OrderCreateContainer $container, \Faker\Generator $factory)
		{
			$this->container = $container;
			$this->factory = $factory;
		}

		public function fill()
		{
			return $this->container
				->setCategoryId($this->getCategoryId())
				->setCityId($this->getCityId())
				->setTitle($this->factory->jobTitle)
				->setDescription($this->factory->text)
				->setPrice(500)
				->setSlug(Str::random())
				->setDateCompletion(null)
				->setClosesAt(now()->addWeeks(2))
				->setCustomerId($this->getCustomerId())
				->setCanOnline(true)
				->setPublishedAt(now())
			;


		}

		protected function getCategoryId(): ?int
		{
			if (false === static::$cachedCategoryId) {
				static::$cachedCategoryId = (int)Category::first('id')->getKey();
			}
			return static::$cachedCategoryId;
		}

		protected function getCityId(): ?int
		{
			if (false === static::$cachedCityId) {
				static::$cachedCityId = (int)City::first('id')->getKey();
			}
			return static::$cachedCityId;
		}

		protected function getCustomerId(): ?int
		{
			if (false === static::$cachedCustomerId) {
				static::$cachedCustomerId = (int)User::whereKey(2)->first('id')->getKey();
			}
			return static::$cachedCustomerId;
		}
	}