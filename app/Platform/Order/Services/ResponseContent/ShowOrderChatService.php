<?php declare(strict_types=1);

namespace App\Platform\Order\Services\ResponseContent;

use App\Models\Chat\Chat;
use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Order\PermissionContainers\OrderPermissionsContainer;
use App\Platform\Order\Voters\Deal\IsUserSelectedAsPerformer;
use App\Repositories\Chat\ChatRepository;

final class ShowOrderChatService
{
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(ChatRepository $chatRepository)
	{
		$this->chatRepository = $chatRepository;
	}

	public function getChat(
		OrderPermissionsContainer $permissionsContainer,
		Order                     $order,
		?User                     $currentUser = null
	): ?Chat
	{
		if (!$order->getContainer()->hasPerformer()) {
			return null;
		}
		if ($permissionsContainer->isOwner() || IsUserSelectedAsPerformer::make()->check($order, $currentUser)) {
			return $this->chatRepository->getChatByUser($order, $order->getContainer()->getPerformer());
		}

		return null;
	}
}