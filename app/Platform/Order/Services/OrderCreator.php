<?php declare(strict_types=1);

namespace App\Platform\Order\Services;

use App\Models\Order\Order;
use App\Platform\Order\DataContainers\OrderCreateContainer;
use App\Repositories\Order\OrderRepository;
use Illuminate\Support\Str;

class OrderCreator
{
	/**
	 * @var OrderCreateContainer
	 */
	private $container;
	/**
	 * @var OrderRepository
	 */
	private $repository;

	public function __construct(OrderCreateContainer $container, OrderRepository $repository)
	{
		$this->container = $container;
		$this->repository = $repository;
	}

	public function create(): Order
	{
		$slug = is_numeric($this->container->getSlug()) ? $this->container->getSlug() . Str::random(2) : $this->container->getSlug();
		$slug = $this->getSlug(Str::limit($slug, 100, ''));
		$this->container->setSlug($slug);

		return $this->repository->create(
			$this->container->toArray()
		);
	}

	protected function getSlug($slug)
	{
		static $c;
		if (null === $c) {
			$c = 0;
		}
		if (!$this->repository->where('slug', $slug)->count()) {
			return $slug;
		}
		if (!$c) {
			$slug .= '-';
		}
		$c++;

		return $this->getSlug($slug . Str::lower(Str::random(1)));
	}
}