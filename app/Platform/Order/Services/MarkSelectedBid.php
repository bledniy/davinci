<?php


namespace App\Platform\Order\Services;


use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use App\Platform\Order\Voters\Deal\IsUserSelectedAsPerformer;
use Illuminate\Support\Collection;

class MarkSelectedBid
{
	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var Collection
	 */
	private $bids;

	public function __construct(Order $order, Collection $bids)
	{
		$this->order = $order;
		$this->bids = $bids;
	}

	/**
	 * @param Order $order
	 * @param Collection | OrderBid[] $bids
	 * @return \Illuminate\Contracts\Foundation\Application|mixed|string
	 */
	public static function make(Order $order, Collection $bids):self
	{
		return app(__CLASS__, compact('order', 'bids'));
	}

	public function apply(): Collection
	{
		return $this->bids->map(function (OrderBid $orderBid) {
			$orderBid->setAttribute('is_selected', IsUserSelectedAsPerformer::make()->check($this->order, $orderBid->getContainer()->getPerformer()));
			return $orderBid;
		});
	}
}