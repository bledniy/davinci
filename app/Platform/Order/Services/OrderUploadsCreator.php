<?php


namespace App\Platform\Order\Services;


use App\Models\Order\Order;
use App\Models\Order\OrderUpload;
use App\Repositories\Order\OrderUploadsRepository;
use Illuminate\Support\Collection;

class OrderUploadsCreator
{

	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var OrderUploadsRepository
	 */
	private $uploadsRepository;
	/**
	 * @var Collection
	 */
	private $uploads;

	public function __construct(Order $order, Collection $uploads, OrderUploadsRepository $uploadsRepository)
	{
		$this->order = $order;
		$this->uploadsRepository = $uploadsRepository;
		$this->uploads = $uploads;
	}

	public static function make(Order $order, Collection $uploads):self
	{
		return app(__CLASS__, compact('order', 'uploads'));
	}

	/**
	 * @return Collection | OrderUpload[]
	 */
	public function create(): Collection
	{
		$orderUploads = collect([]);
		foreach ($this->uploads as $upload) {
			$upload['file'] = $upload['path'];
			$orderUploads->push($this->uploadsRepository->createRelated($upload, $this->order));
		}
		return $orderUploads;
	}


}