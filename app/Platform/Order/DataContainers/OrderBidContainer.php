<?php


	namespace App\Platform\Order\DataContainers;


	use App\Helpers\Date\DaysName\DaysNameHelper;
	use App\Models\Order\OrderBid;
	use Illuminate\Support\Carbon;

	class OrderBidContainer
	{
		/**
		 * @var OrderBid
		 */
		private $bid;
		/**
		 * @var DaysNameHelper
		 */
		private $daysNameHelper;

		public function __construct(OrderBid $bid, DaysNameHelper $daysNameHelper)
		{
			$this->bid = $bid;
			$this->daysNameHelper = $daysNameHelper;
		}

		public function getEntity(): OrderBid
		{
			return $this->bid;
		}

		public function getBid(): int
		{
			return (int)$this->getAttribute('bid');
		}

		public function getBidWithCurrency(): string
		{
			return (int)$this->getAttribute('bid') . ' ' . getCurrencyIcon();
		}

		public function getDeadline()
		{
			return $this->getAttribute('deadline');
		}

		public function getDateCreate()
		{
			return $this->getEntity()->getCreatedAt();
		}

		public function getText(): string
		{
			return (string)$this->getEntity()->getAttribute('text');
		}

		public function isEnabled(): bool
		{
			return (bool)((int)$this->getAttribute('active'));
		}

		public function getLimitEdit(): int
		{
			return (int)$this->getAttribute('available_edits');
		}

		public function getDateEdit(): ?Carbon
		{
			return $this->getAttribute('modified_at');
		}

		protected function getAttribute($attr)
		{
			return $this->getEntity()->getAttribute($attr);
		}

		public function getDateCancel(): ?Carbon
		{
			return $this->getEntity()->getAttribute('canceled_at');
		}

		public function getPerformerKey(): int
		{
			return (int)$this->getAttribute('user_id');
		}

		public function getPerformer(): \App\Models\User
		{
			return $this->getEntity()->getUser();
		}
	}