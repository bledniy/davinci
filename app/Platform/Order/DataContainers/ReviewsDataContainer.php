<?php


namespace App\Platform\Order\DataContainers;


use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use App\Repositories\ReviewRepository;

class ReviewsDataContainer
{

	private $performerReview = false;

	private $customerReview = false;
	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(Order $order, ReviewRepository $reviewRepository)
	{
		$this->order = $order;
		$this->reviewRepository = $reviewRepository;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	public function hasCustomerReview(?User $customer = null): bool
	{
		$customer = $this->getCustomer($customer);
		if (!$customer) {
			return false;
		}
		return !is_null($this->getCustomerReview($customer));
	}

	public function hasPerformerReview(?User $performer = null): bool
	{
		$performer = $this->getPerformer($performer);

		if (!$performer) {
			return false;
		}
		return !is_null($this->getPerformerReview($performer));
	}

	public function getCustomerReview(User $customer): ?Review
	{
		if (false === $this->customerReview) {
			$this->customerReview = $this->reviewRepository->getOneFromCustomer($this->getOrder(), $customer, false);
		}
		return $this->customerReview;
	}

	public function getPerformerReview(User $performer): ?Review
	{
		if (false === $this->performerReview) {
			$this->performerReview = $this->reviewRepository->getOneFromPerformer($this->getOrder(), $performer, false);
		}
		return $this->performerReview;
	}

	private function getPerformer(?User $performer = null): ?User
	{
		return $performer ?? $this->getOrder()->getContainer()->getPerformer();
	}

	private function getCustomer(?User $customer = null): User
	{
		return $customer ?? $this->getOrder()->getContainer()->getCustomer();
	}
}