<?php


namespace App\Platform\Order\DataContainers;


use App\Models\Order\Review;
use App\Models\User;
use App\Platform\Contract\UserTypeContract;

class ReviewDisplayContainer
{

	private $commentOwnerContainer;

	/**
	 * @var Review
	 */
	private $review;

	private $user;

	public function __construct(Review $review, ?User $user = null)
	{
		$this->review = $review;
	}

	/**
	 * @return Review
	 */
	public function getEntity(): Review
	{
		return $this->review;
	}

	public function getOrder()
	{
		return $this->getEntity()->getOrder();
	}


	public function getAbout()
	{
		return $this->getAttribute('about') ?: UserTypeContract::TYPE_PERFORMER;
	}

	public function isAboutCustomer(): bool
	{
		return $this->getAbout() === UserTypeContract::TYPE_CUSTOMER;
	}

	public function isAboutPerformer(): bool
	{
		return $this->getAbout() === UserTypeContract::TYPE_PERFORMER;
	}

	public function getRating(): float
	{
		return $this->review->getRating();
	}

	public function getComment()
	{
		return $this->getEntity()->getComment();
	}

	public function getOrderPrice()
	{
		return $this->getOrder()->price;
	}

	public function getReviewImages()
	{
		return $this->getEntity()->getImages();
	}

	public function isPublished()
	{
		return (bool)(int)$this->getEntity()->getAttribute('is_published');
	}

	public function getSubRatingAdequate(): int
	{
		return (int)$this->getAttribute('adequate_rating');
	}

	public function getSubRatingLegibility(): int
	{
		return (int)$this->getAttribute('legibility_rating');
	}

	public function getSubRatingSociability(): int
	{
		return (int)$this->getAttribute('sociability_rating');
	}

	public function getSubRatingConflict(): int
	{
		return (int)$this->getAttribute('conflict_rating');
	}

	public function getSubRatingRecommend(): int
	{
		return (int)$this->getAttribute('recommend_rating');
	}

	public function getTotalSubRating(): int
	{
		return $this->getSubRatingAdequate() + $this->getSubRatingLegibility() + $this->getSubRatingSociability() + $this->getSubRatingConflict() + $this->getSubRatingRecommend();
	}

	public function getCountSubRatings(): int
	{
		return 5;
	}

	private function getAttribute($attr)
	{
		return $this->getEntity()->getAttribute($attr);
	}

}