<?php declare(strict_types=1);

namespace App\Platform\Order\DataContainers;

use App\Traits\SetterAndGetterMethods;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class OrderCreateContainer
{
	use SetterAndGetterMethods;

	private $category_id;
	private $city_id;
	private $title;
	private $description;
	private $price;
	private $slug;
	private $date_completion;
	private $closes_at;
	private $customer_id;
	private $can_online;
	private $published_at;

	public function __construct($data = [])
	{
		foreach ($data as $key => $value) {
			if ($this->hasSetter($key)) {
				$this->_setProperty($key, $value);
			}
		}
	}

	public function getClosesAt(): Carbon
	{
		return getDateCarbon($this->closes_at);
	}

	public function getCategoryId()
	{
		return $this->category_id;
	}

	public function getCityId()
	{
		return $this->city_id;
	}

	public function getTitle()
	{
		return (string)$this->title;
	}

	public function getDateCompletion(): string
	{
		return (string)$this->date_completion;
	}

	public function getDescription()
	{
		return (string)$this->description;
	}

	public function getPrice()
	{
		return (int)$this->price;
	}

	public function toArray()
	{
		$vars = get_object_vars($this);
		foreach ($vars as $key => $value) {
			if ($this->hasGetter($key)) {
				$vars[$key] = $this->_getProperty($key);
			}
		}

		return $vars;
	}

	/**
	 * @param mixed $category_id
	 * @return OrderCreateContainer
	 */
	public function setCategoryId($category_id)
	{
		$this->category_id = $category_id;
		return $this;
	}

	/**
	 * @param mixed $city_id
	 * @return OrderCreateContainer
	 */
	public function setCityId($city_id)
	{
		$this->city_id = $city_id;
		return $this;
	}

	/**
	 * @param mixed $title
	 * @return OrderCreateContainer
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @param mixed $description
	 * @return OrderCreateContainer
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @param mixed $price
	 * @return OrderCreateContainer
	 */
	public function setPrice($price)
	{
		$this->price = $price;
		return $this;
	}

	/**
	 * @param mixed $date_completion
	 * @return OrderCreateContainer
	 */
	public function setDateCompletion($date_completion)
	{
		$this->date_completion = $date_completion;
		return $this;
	}

	/**
	 * @param mixed $closes_at
	 * @return OrderCreateContainer
	 */
	public function setClosesAt($closes_at)
	{
		$this->closes_at = $closes_at;
		return $this;
	}

	public function getSlug()
	{
		if ($this->slug) {
		    return $this->slug;
		}
		return Str::slug($this->getTitle());
	}

	public function setSlug($slug): self
	{
		$this->slug = $slug;
		return $this;
	}

	public function getCustomerId()
	{
		return $this->customer_id;
	}

	public function setCustomerId($customer_id): self
	{
		$this->customer_id = $customer_id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCanOnline()
	{
		return $this->can_online;
	}

	/**
	 * @param mixed $can_online
	 * @return OrderCreateContainer
	 */
	public function setCanOnline($can_online)
	{
		$this->can_online = $can_online;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPublishedAt()
	{
		return $this->published_at ?? now();
	}

	/**
	 * @param mixed $published_at
	 */
	public function setPublishedAt($published_at): self
	{
		$this->published_at = $published_at;
		return $this;
	}
}