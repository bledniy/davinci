<?php

namespace App\Platform\Order\DataContainers;

use App\Models\Category\Category;
use App\Models\City\City;
use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Order\Voters\Review\IsDateReviewNotExpired;
use Illuminate\Support\Carbon;

class OrderMainDataContainer
{

	/**
	 * @var Order
	 */
	private $order;

	private $performer;
	private $customer;

	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	public function getCategory(): ?Category
	{
		return $this->getOrder()->getCategory();
	}

	public function getLimitEdit(): int
	{
		return (int)$this->getOrder()->getAttribute('available_edits');
	}

	public function getCity(): City
	{
		return $this->getOrder()->getCity();
	}

	public function getTitle()
	{
		return $this->getOrder()->getTitle();
	}

	public function getPrice(): int
	{
		return (int)$this->getOrder()->getAttribute('price');
	}

	public function getPriceWithCurrency(): string
	{
		return $this->getPrice() ? implode(' ', [$this->getPrice(), getCurrencyIcon()]) : '';
	}

	public function getDescription(): string
	{
		return (string)$this->getOrder()->getAttribute('description');
	}

	public function getDateTo(): ?Carbon
	{
		$date = $this->getOrder()->getAttribute('datetime');
		return isDateValid($date) ? getDateCarbon($date) : null;
	}

	public function getDateCreate(): ?Carbon
	{
		return $this->getOrder()->getCreatedAt();
	}

	public function getCustomer(): User
	{
		if (null === $this->customer) {
			$this->customer = $this->getOrder()->getCustomer();
		}
		return $this->customer;
	}

	public function getCustomerId()
	{
		return $this->getOrder()->getAttribute('customer_id');
	}

	public function getPerformer(): ?User
	{
		if (null === $this->performer) {
			$this->performer = $this->getOrder()->getPerformer();
		}
		return $this->performer;
	}

	public function getPerformerId(): ?int
	{
		$id = $this->getOrder()->getAttribute('performer_id');
		if (null === $id) {
			return null;
		}
		return (int)$id;
	}

	public function getBidsCount(): int
	{
		return $this->getOrder()->getAttribute('bids_count');
	}

	public function getViewsCount(): int
	{
		return $this->getOrder()->getAttribute('views');
	}

	public function getSlug(): string
	{
		return (string)$this->getOrder()->getAttribute('slug');
	}

	public function isClosed(): bool
	{
		if ((bool)$this->getOrder()->getAttribute('is_closed')) {
			return true;
		}
		return !app(IsDateReviewNotExpired::class)->check(getDateCarbon($this->getOrder()->getAttribute('closes_at')));
	}

	public function isOpened(): bool
	{
		return !$this->isClosed();
	}

	public function hasPerformer(): bool
	{
		return !is_null($this->getOrder()->getAttribute('performer_id'));
	}

	public function getDateClose(): ?Carbon
	{
		$date = $this->getOrder()->getAttribute('closes_at');
		if (isDateValid($date)) {
			return getDateCarbon($date);
		}
		return null;
	}

	public function getDatePublished(): ?Carbon
	{
		return getDateCarbonIfValid($this->getOrder()->getAttribute('published_at')) ?: $this->getOrder()->getCreatedAt();
	}

	public function getStatusKey()
	{
		if ($this->isClosed()) {
			return 'CLOSED';
		}
		if ($this->hasPerformer()) {
			return 'ACTIVE';
		}
		return 'OPEN';

	}

	public function getStatusName()
	{
		$statuses = [
			'CLOSED' => getTranslate('order.status.closed', 'Сделка закрыта'),
			'ACTIVE' => getTranslate('order.status.active', 'Активная сделка'),
			'OPEN'   => getTranslate('order.status.open', 'Открытая сделка'),
		];
		return $statuses[$this->getStatusKey()] ?? '';
	}

	public function isActive(): bool
	{
		return (bool)$this->order->active;
	}

}