<?php


	namespace App\Platform\Order\PermissionContainers;


	use Illuminate\Contracts\Support\Arrayable;

	class OrderBidPermissionsContainer implements Arrayable
	{

		private $is_owner = false;

		private $can_edit_bid = false;

		private $can_cancel_bid = false;

		public function toArray()
		{
			return get_object_vars($this);
		}

		/**
		 * @return bool
		 */
		public function isOwner(): bool
		{
			return $this->is_owner;
		}

		/**
		 * @param bool $is_owner
		 */
		public function setIsOwner(bool $is_owner): void
		{
			$this->is_owner = $is_owner;
		}

		/**
		 * @return bool
		 */
		public function isCanEditBid(): bool
		{
			return $this->can_edit_bid;
		}

		/**
		 * @param bool $can_edit_bid
		 */
		public function setCanEditBid(bool $can_edit_bid): void
		{
			$this->can_edit_bid = $can_edit_bid;
		}

		/**
		 * @return bool
		 */
		public function isCanCancelBid(): bool
		{
			return $this->can_cancel_bid;
		}

		/**
		 * @param bool $can_cancel_bid
		 */
		public function setCanCancelBid(bool $can_cancel_bid): void
		{
			$this->can_cancel_bid = $can_cancel_bid;
		}

	}