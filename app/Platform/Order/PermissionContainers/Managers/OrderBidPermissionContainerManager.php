<?php


	namespace App\Platform\Order\PermissionContainers\Managers;


	use App\Models\Order\Order;
	use App\Models\Order\OrderBid;
	use App\Models\User;
	use App\Platform\Order\PermissionContainers\OrderBidPermissionsContainer;
	use App\Platform\Order\Voters\Bid\IsUserCanCancelBid;
	use App\Platform\Order\Voters\Bid\IsUserCanEditBid;
	use App\Platform\Order\Voters\Bid\IsUserOwnerBid;
	use App\Platform\Order\Voters\Deal\IsUserSelectedAsPerformer;
	use Illuminate\Support\Collection;

	class OrderBidPermissionContainerManager
	{

		/**
		 * @var IsUserCanEditBid
		 */
		public $isUserCanEditBid;
		/**
		 * @var Order
		 */
		private $order;
		/**
		 * @var User|null
		 */
		private $checkableUser;
		/**
		 * @var OrderBidPermissionsContainer
		 */
		private $container;
		/**
		 * @var OrderBid
		 */
		private $bid;

		public function __construct(Order $order, OrderBidPermissionsContainer $container, ?User $checkableUser = null)
		{
			$this->order = $order;
			$this->checkableUser = $checkableUser;
			$this->container = $container;
		}

		public static function make(Order $order, ?User $checkableUser = null): self
		{
			return app(__CLASS__, compact('order', 'checkableUser'));
		}

		public function getContainer(): OrderBidPermissionsContainer
		{
			return $this->container;
		}


		public function apply()
		{
			$isOwner = IsUserOwnerBid::make()->check($this->bid, $this->checkableUser);
			if (!$isOwner) {
				return $this;
			}
			$this->container->setIsOwner(true);
			//
			$this->isUserCanEditBid = IsUserCanEditBid::make();
			$this->container->setCanEditBid($this->isUserCanEditBid->check($this->bid, $this->order));
			//
			$this->container->setCanCancelBid(
				IsUserCanCancelBid::make()->check($this->bid, $this->order)
				&& IsUserSelectedAsPerformer::make()->check($this->order, $this->checkableUser)
			);

			return $this;
		}

		/**
		 * @param $bids Collection | OrderBid[]
		 */
		public function applyMultiple($bids)
		{
			foreach ($bids as $bid) {
				$this->container = app(OrderBidPermissionsContainer::class);
				$container = $this->setBid($bid)->apply()->getContainer();
				$bid->setAttribute('permissions', $container->toArray());
			}
			return $bids;
		}

		/**
		 * @param OrderBid $bid
		 * @return OrderBidPermissionContainerManager
		 */
		public function setBid(OrderBid $bid): OrderBidPermissionContainerManager
		{
			$this->bid = $bid;
			return $this;
		}
	}