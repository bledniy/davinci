<?php


namespace App\Platform\Order\PermissionContainers\Managers;


use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use App\Platform\Order\PermissionContainers\ReviewPermissionsContainer;
use App\Platform\Order\Voters\Bid\IsUserCanAddBidToDeal;
use App\Platform\Order\Voters\Deal\IsUserCanEditDeal;
use App\Platform\Order\Voters\Review\IsCustomerCanAddReview;
use App\Platform\Order\Voters\Review\IsDateReviewNotExpired;
use App\Platform\Order\Voters\Review\IsUserOwnerReview;

class ReviewCustomerPermissionContainerManager
{
	/** @var IsUserCanEditDeal */
	public $isUserCanEditDeal;

	/**
	 * @var IsUserCanAddBidToDeal
	 */
	public $isUserCanAddBidToDeal;

	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var User|null
	 */
	private $checkableUser;
	/**
	 * @var ReviewPermissionsContainer
	 */
	private $container;
	/**
	 * @var Review|null
	 */
	private $review;

	public function __construct(Order $order, ReviewPermissionsContainer $container, ?User $checkableUser = null, ?Review $review = null)
	{
		$this->order = $order;
		$this->checkableUser = $checkableUser;
		$this->container = $container;
		$this->review = $review;
	}

	public static function make(Order $order, ?User $checkableUser = null, ?Review $review = null): self
	{
		return app(__CLASS__, compact('order', 'checkableUser', 'review'));
	}

	public function getContainer(): ReviewPermissionsContainer
	{
		return $this->container;
	}

	public function apply()
	{
		if ($this->doesntHaveReview()) {
			$this->container->setCanAddReview(IsCustomerCanAddReview::make()->check($this->order, $this->checkableUser));
		} else {
			$this->container->setIsOwner(IsUserOwnerReview::make()->check($this->review, $this->checkableUser));
		}
		$this->container->setIsDateAddExpired(!IsDateReviewNotExpired::make()->check($this->order->getContainer()->getDateClose()));

		return $this;
	}

	protected function doesntHaveReview(): bool
	{
		return null === $this->review;
	}
}