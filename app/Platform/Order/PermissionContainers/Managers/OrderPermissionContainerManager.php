<?php

namespace App\Platform\Order\PermissionContainers\Managers;

use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Order\PermissionContainers\OrderPermissionsContainer;
use App\Platform\Order\Voters\Bid\IsUserCanAddBidToDeal;
use App\Platform\Order\Voters\Deal\IsUserCanCloseDeal;
use App\Platform\Order\Voters\Deal\IsUserCanEditDeal;
use App\Platform\Order\Voters\Deal\IsUserCanSelectPerformer;
use App\Platform\Order\Voters\Deal\IsUserOwnerDeal;

class OrderPermissionContainerManager
{

	/** @var IsUserCanEditDeal */
	public $isUserCanEditDeal;

	/**
	 * @var IsUserCanAddBidToDeal
	 */
	public $isUserCanAddBidToDeal;

	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var User|null
	 */
	private $checkableUser;
	/**
	 * @var OrderPermissionsContainer
	 */
	private $container;

	public function __construct(Order $order, OrderPermissionsContainer $container, ?User $checkableUser = null)
	{
		$this->order = $order;
		$this->checkableUser = $checkableUser;
		$this->container = $container;
	}

	public static function make(Order $order, ?User $checkableUser = null): self
	{
		return app(__CLASS__, compact('order', 'checkableUser'));
	}

	public function getContainer(): OrderPermissionsContainer
	{
		return $this->container;
	}

	public function apply()
	{
		$isOwner = IsUserOwnerDeal::make()->check($this->order->getCustomer(), $this->checkableUser);
		if ($isOwner) {
			$this->container->setIsOwner(true)
				->setCanInitChats(true)
			;
			//
			$this->isUserCanEditDeal = IsUserCanEditDeal::make();
			$this->container->setCanEditDeal($this->isUserCanEditDeal->check($this->order));
			//
			$this->container->setCanSelectPerformer(IsUserCanSelectPerformer::make()->check($this->order));

			$this->container->setCanCloseDeal(IsUserCanCloseDeal::make()->check($this->order));

			return $this;
		}
		$this->isUserCanAddBidToDeal = IsUserCanAddBidToDeal::make();
		$this->container->setCanAddBid($this->isUserCanAddBidToDeal->check($this->order, $this->checkableUser));

		return $this;
	}

}