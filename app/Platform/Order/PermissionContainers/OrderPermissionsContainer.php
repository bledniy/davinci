<?php

namespace App\Platform\Order\PermissionContainers;

use Illuminate\Contracts\Support\Arrayable;

class OrderPermissionsContainer implements Arrayable
{

    private $is_owner = false;

    private $can_add_bid = false;

    private $can_close_deal = false;

    private $can_edit_deal = false;

    private $can_select_performer = false;

    private $can_init_chats = false;


    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * @return bool
     */
    public function isOwner(): bool
    {
        return $this->is_owner;
    }

    public function setIsOwner(bool $is_owner): self
    {
        $this->is_owner = $is_owner;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCanAddBid(): bool
    {
        return $this->can_add_bid;
    }

    /**
     * @param bool $can_add_bid
     */
    public function setCanAddBid(bool $can_add_bid): void
    {
        $this->can_add_bid = $can_add_bid;
    }

    /**
     * @return bool
     */
    public function isCanCloseDeal(): bool
    {
        return $this->can_close_deal;
    }

    /**
     * @param bool $can_close_deal
     */
    public function setCanCloseDeal(bool $can_close_deal): void
    {
        $this->can_close_deal = $can_close_deal;
    }

    /**
     * @return bool
     */
    public function isCanEditDeal(): bool
    {
        return $this->can_edit_deal;
    }

    /**
     * @param bool $can_edit_deal
     */
    public function setCanEditDeal(bool $can_edit_deal): void
    {
        $this->can_edit_deal = $can_edit_deal;
    }

    /**
     * @return bool
     */
    public function isCanSelectPerformer(): bool
    {
        return $this->can_select_performer;
    }

    /**
     * @param bool $can_select_performer
     */
    public function setCanSelectPerformer(bool $can_select_performer): void
    {
        $this->can_select_performer = $can_select_performer;
    }

    public function setCanInitChats(bool $can_init_chats): void
    {
        $this->can_init_chats = $can_init_chats;
    }
}