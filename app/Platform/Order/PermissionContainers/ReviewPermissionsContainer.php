<?php


	namespace App\Platform\Order\PermissionContainers;


	use Illuminate\Contracts\Support\Arrayable;

	class ReviewPermissionsContainer implements Arrayable
	{
		private $is_owner = false;

		private $can_add_review = false;

		private $is_date_add_expired = false;

		public function toArray()
		{
			return get_object_vars($this);
		}

		/**
		 * @return bool
		 */
		public function isOwner(): bool
		{
			return $this->is_owner;
		}

		/**
		 * @param bool $is_owner
		 */
		public function setIsOwner(bool $is_owner): void
		{
			$this->is_owner = $is_owner;
		}

		/**
		 * @return bool
		 */
		public function isCanAddReview(): bool
		{
			return $this->can_add_review;
		}

		/**
		 * @param bool $can_add_review
		 */
		public function setCanAddReview(bool $can_add_review): void
		{
			$this->can_add_review = $can_add_review;
		}

		/**
		 * @return bool
		 */
		public function isIsDateAddExpired(): bool
		{
			return $this->is_date_add_expired;
		}

		/**
		 * @param bool $is_date_add_expired
		 */
		public function setIsDateAddExpired(bool $is_date_add_expired): void
		{
			$this->is_date_add_expired = $is_date_add_expired;
		}


	}