<?php


namespace App\Platform\Order\Model;


use App\Models\Model;

abstract class BaseSetter
{
	/**
	 * @var Model
	 */
	private $entity;

	public function __construct(Model $entity)
	{
		$this->entity = $entity;
	}

	/**
	 * @return Model
	 */
	public function getEntity(): Model
	{
		return $this->entity;
	}

	protected function setAttribute($attr, $value): self
	{
		$this->entity->setAttribute($attr, $value);
		return $this;
	}



}