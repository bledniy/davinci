<?php


namespace App\Platform\Order\Model;


use App\Models\Order\Order;
use Illuminate\Support\Carbon;

class OrderSetter
{
	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	public function setCustomerId($customerId): OrderSetter
	{
		return $this->setAttribute('customer_id', $customerId);
	}

	public function setPerformerId($customerId): OrderSetter
	{
		return $this->setAttribute('performer_id', $customerId);
	}

	public function setCreatedAt($createdAt): OrderSetter
	{
		return $this->setAttribute($this->getOrder()->getCreatedAtColumn(), $createdAt);
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	public function setSlug($slug): OrderSetter
	{
		return $this->setAttribute('slug', $slug);
	}

	public function setImagePoster(?string $path): OrderSetter
	{
		return $this->setAttribute('image', $path);
	}

	public function decrementEdit(): self
	{
		$edits = (int)$this->getOrder()->getAttribute('available_edits');
		if ($edits > 0) {
			--$edits;
			$this->setAttribute('available_edits', $edits);
		}
		return $this;
	}

	public function setPublishDate(Carbon $date): OrderSetter
	{
		return $this->setAttribute('published_at', $date);
	}

	public function setBidsCount(int $count): OrderSetter
	{
		return $this->setAttribute('bids_count', $count);
	}

	private function setAttribute($attr, $value): OrderSetter
	{
		$this->getOrder()->setAttribute($attr, $value);
		return $this;
	}

	private function getAttribute($attr)
	{
		return $this->getOrder()->getAttribute($attr);
	}

	public function setClosesAt($date = null): OrderSetter
	{
		$date = $date ?? now();
		return $this->setAttribute('closes_at', $date);
	}

	public function setClosed(): OrderSetter
	{
		return $this->setAttribute('is_closed', true);
	}

	public function close(): OrderSetter
	{
		return $this->setClosed()->setClosesAt();
	}

	public function incrementViews()
	{
		$count = $this->getOrder()->getContainer()->getViewsCount() + 1;
		return $this->setAttribute('views', $count);
	}

	public function setActive($flag)
	{
		$this->setAttribute('active', (int)$flag);
		return $this;
	}

	public function toggleActive()
	{
		return $this->setActive(!((int)$this->getAttribute('active')));
	}

}