<?php


namespace App\Platform\Order\Model;


use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use Illuminate\Support\Carbon;

class ChatMessageSetter extends BaseSetter
{
	public function setUserId($userId): self
	{
		$this->setAttribute('user_id', $userId);
		return $this;
	}

	public function setType($value): self
	{
		$this->setAttribute('type', $value);
		return $this;
	}

	public function setIsWatched(bool $value): self
	{
		$this->setAttribute('is_watched', $value);
		return $this;
	}

	public function setMessage($value): self
	{
		$this->setAttribute('message', $value);
		return $this;
	}

	public function setChatId($value): self
	{
		$this->setAttribute('chat_id', $value);
		return $this;
	}
}