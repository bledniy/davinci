<?php


namespace App\Platform\Order\Uploads;


use App\Helpers\Debug\LoggerHelper;
use App\Models\Order\Order;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

class OrderCreateUnauthenticatedFilesReStoreJob
{
	/**
	 * @var UploadedFile[]
	 */
	private $files = [];

	private $orderUploads;

	/**
	 * @var OrderCreateUploader
	 */
	private $uploader;
	/**
	 * @var Order
	 */
	private $order;

	public function __construct(
		Order $order,
		OrderCreateUploader $uploader
	)
	{
		$this->orderUploads = collect([]);
		$this->uploader = $uploader;
		$this->order = $order;
	}

	public function run(): self
	{
		$this->uploader->setPath($this->uploader->getPath())->appendToPath($this->order->getKey());
		foreach ($this->getFiles() as $file) {
			try{
				$upload = [
					'path'          => $this->uploader->upload($file),
					'original_name' => $file->getClientOriginalName(),
				];
				$this->orderUploads->push($upload);
			} catch (\Throwable $e){
			    app(LoggerHelper::class)->error($e);
			    continue;
			}

			try{
				unlink($file->path());
			} catch (\Throwable $e){
			    app(LoggerHelper::class)->error($e);
			}
		}
		return $this;
	}


	/**
	 * @return array | UploadedFile[]
	 */
	private function getFiles(): array
	{
		return $this->files;
	}

	public function filesFromArray(array $files)
	{
		foreach ($files as $file) {
			try{
				$this->files[] = new UploadedFile(storageDisk('public')->path($file['path']), $file['original_name'], $file['mime']);
			} catch (\Throwable $e){
			    app(LoggerHelper::class)->error($e);
			}
		}
		return $this;
	}

	/**
	 * @return Collection
	 */
	public function getOrderUploads(): Collection
	{
		return $this->orderUploads;
	}

}