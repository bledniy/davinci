<?php


namespace App\Platform\Order\Uploads;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

class OrderCreateUnauthenticatedFilesStoreJob
{
	/**
	 * @var UploadedFile[]
	 */
	private $files;

	private $orderUploads;

	/**
	 * @var OrderCreateUploader
	 */
	private $uploader;

	public function __construct(
		array $files,
		OrderCreateUploader $uploader
	)
	{
		$this->files = $files;
		$this->orderUploads = collect([]);
		$this->uploader = $uploader;
	}

	public function run(): self
	{
		$this->uploader->setPath($this->uploader->getPath())->appendToPath('temp');
		foreach ($this->getFiles() as $file) {
			$upload = [
				'path'          => $this->uploader->upload($file),
				'original_name' => $file->getClientOriginalName(),
				'mime'          => $file->getClientMimeType(),
			];
			$this->orderUploads->push($upload);
		}
		return $this;
	}


	/**
	 * @return array | UploadedFile[]
	 */
	private function getFiles(): array
	{
		return $this->files;
	}

	/**
	 * @return Collection
	 */
	public function getOrderUploads(): Collection
	{
		return $this->orderUploads;
	}

}