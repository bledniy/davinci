<?php


namespace App\Platform\Order\Uploads;


use App\Helpers\Media\ImageSaver;
use Illuminate\Http\UploadedFile;

class ReviewImagesUploader
{
	/**
	 * @var UploadedFile[]
	 */
	private $uploadedFiles;
	/**
	 * @var ImageSaver
	 */
	private $imageSaver;

	private $folderName = 'deals/reviews';
	/**
	 * PerformerPortfolioImagesUploader constructor.
	 * @param UploadedFile[] $uploadedFiles
	 * @param ImageSaver $imageSaver
	 */
	public function __construct(array $uploadedFiles, ImageSaver $imageSaver)
	{
		$this->uploadedFiles = $uploadedFiles;
		$this->imageSaver = $imageSaver;
	}

	public function upload(): array
	{
		$this->imageSaver->setFolderName($this->folderName)
			->setThumbnailSizes(80, 60)
		;
		$paths = [];
		foreach ($this->uploadedFiles as $uploadedFile) {
			$paths[] = $this->imageSaver->saveFromUploadedFile($uploadedFile);
		}
		return $paths;
	}

	/**
	 * @param string $folderName
	 */
	public function setFolderName(string $folderName): self
	{
		$this->folderName = $folderName;
		return $this;
	}
}