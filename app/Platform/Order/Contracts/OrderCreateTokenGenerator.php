<?php


namespace App\Platform\Order\Contracts;


interface OrderCreateTokenGenerator
{
	public function generate(): string;
}