<?php declare(strict_types=1);

namespace App\Platform\Order\Voters\Deal;

use App\Models\Order\Order;
use App\Models\User;

class IsUserSelectedAsPerformer
{

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(Order $order, ?User $checkableUser): bool
	{
		if (is_null($checkableUser)) {
			return false;
		}

		return (int)$order->getContainer()->getPerformerId() === $checkableUser->getKey();
	}
}