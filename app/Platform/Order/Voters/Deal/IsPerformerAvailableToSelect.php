<?php


	namespace App\Platform\Order\Voters\Deal;


	use App\Models\Order\Order;
	use App\Models\User;
	use App\Repositories\Order\OrderBidRepository;

	class IsPerformerAvailableToSelect
	{

		/**
		 * @var OrderBidRepository
		 */
		private $bidRepository;

		public function __construct(OrderBidRepository $bidRepository)
		{

			$this->bidRepository = $bidRepository;
		}

		public static function make(): self
		{
			return app(__CLASS__);
		}

		public function requires()
		{
			return [
				IsUserOwnerDeal::class,
			];
		}

		public function check(Order $order, User $selectablePerformer)
		{
			$bid = $this->bidRepository->getByOrderAndUser($order, $selectablePerformer);
			if (!$bid) {
				return false;
			}
			if (!$bid->getContainer()->isEnabled()) {
				return false;
			}
			return true;
		}
	}