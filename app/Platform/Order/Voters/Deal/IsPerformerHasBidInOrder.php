<?php


namespace App\Platform\Order\Voters\Deal;


use App\Models\Order\Order;
use App\Models\User;
use App\Repositories\Order\OrderBidRepository;
use Illuminate\Contracts\Auth\Authenticatable;

class IsPerformerHasBidInOrder
{
	/**
	 * @var OrderBidRepository
	 */
	private $bidRepository;

	public function __construct(OrderBidRepository $bidRepository)
	{
		$this->bidRepository = $bidRepository;
	}

	public static function make():self
	{
	    return app(__CLASS__);
	}

	public function check(Order $order, User $performer): bool
	{
		return (bool)$this->bidRepository->getCountByOrderAndUser($order, $performer);
	}
}