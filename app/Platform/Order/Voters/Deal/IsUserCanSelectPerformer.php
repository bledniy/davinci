<?php


namespace App\Platform\Order\Voters\Deal;


use App\Models\Order\Order;

class IsUserCanSelectPerformer
{

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function requires()
	{
		return [
			IsUserOwnerDeal::class,
		];
	}

	public function check(Order $order)
	{
		if ($order->getContainer()->isClosed()) {
			return false;
		}
		if ($order->getContainer()->hasPerformer()) {
			return false;
		}
		return true;
	}
}