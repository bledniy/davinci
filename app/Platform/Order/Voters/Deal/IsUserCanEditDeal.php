<?php


	namespace App\Platform\Order\Voters\Deal;


	use App\Models\Order\Order;

	class IsUserCanEditDeal
	{

		public const REASON_LIMIT_EDIT = 'REASON_LIMIT_EDIT';

		public const REASON_IS_CLOSED = 'REASON_IS_CLOSED';

		public const REASON_HAS_PERFORMER = 'REASON_HAS_PERFORMER';

		private $reason;

		public static function make(): self
		{
			return app(__CLASS__);
		}

		public function requires()
		{
			return [
				IsUserOwnerDeal::class,
			];
		}

		public function check(Order $order)
		{
			if (!$order->getContainer()->getLimitEdit()) {
				$this->setReason(self::REASON_LIMIT_EDIT);
				return false;
			}
			if ($order->getContainer()->isClosed()) {
				$this->setReason(self::REASON_IS_CLOSED);
				return false;
			}
			if ($order->getContainer()->hasPerformer()) {
				$this->setReason(self::REASON_HAS_PERFORMER);
				return false;
			}
			return true;
		}

		/**
		 * @return mixed
		 */
		public function getReason()
		{
			return $this->reason;
		}

		/**
		 * @param mixed $reason
		 */
		public function setReason($reason): void
		{
			$this->reason = $reason;
		}
	}