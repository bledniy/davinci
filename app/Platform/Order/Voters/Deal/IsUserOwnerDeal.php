<?php


	namespace App\Platform\Order\Voters\Deal;


	use App\Models\User;
	use Illuminate\Contracts\Auth\Authenticatable;

	class IsUserOwnerDeal
	{

		public static function make(): self
		{
			return app(__CLASS__);
		}

		public function check(?User $customer, ?User $checkableUser)
		{
			if (is_null($checkableUser) || is_null($customer)) {
				return false;
			}
			return (int)$customer->getKey() === (int)$checkableUser->getKey();
		}
	}