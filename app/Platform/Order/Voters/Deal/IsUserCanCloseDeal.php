<?php


namespace App\Platform\Order\Voters\Deal;


use App\Models\Order\Order;

class IsUserCanCloseDeal
{

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function requires()
	{
		return [
			IsUserOwnerDeal::class,
		];
	}

	public function check(Order $order)
	{
		return !$order->getContainer()->isClosed();
	}
}