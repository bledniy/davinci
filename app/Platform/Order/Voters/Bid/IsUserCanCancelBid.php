<?php declare(strict_types=1);

namespace App\Platform\Order\Voters\Bid;

use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use App\Platform\Order\Voters\Deal\IsUserSelectedAsPerformer;

class IsUserCanCancelBid
{

	public function requires(): array
	{
		return [
			IsUserOwnerBid::class,
			IsUserSelectedAsPerformer::class,
		];
	}

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(OrderBid $bid, Order $order): bool
	{
		if (!$bid->getContainer()->isEnabled()) {
			return false;
		}
		if ($order->getContainer()->isClosed() || $order->getContainer()->hasPerformer()) {
			return false;
		}

		return true;
	}
}