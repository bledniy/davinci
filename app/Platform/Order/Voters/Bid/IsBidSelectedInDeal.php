<?php


	namespace App\Platform\Order\Voters\Bid;


	use App\Models\Order\Order;
	use App\Models\Order\OrderBid;

	class IsBidSelectedInDeal
	{

		public static function make(): self
		{
			return app(__CLASS__);
		}

		public function check(Order $order, ?OrderBid $bid): bool
		{
			if (is_null($bid)) {
				return false;
			}
			if (!$order->getContainer()->hasPerformer()) {
			    return false;
			}
			return $order->getContainer()->getPerformerId() === $bid->getContainer()->getPerformerKey();
		}
	}