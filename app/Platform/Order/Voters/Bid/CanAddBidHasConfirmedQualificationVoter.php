<?php declare(strict_types=1);

namespace App\Platform\Order\Voters\Bid;

use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Cabinet\Certification\UserCertificationConfirmationChecker;
use App\Platform\Contract\Order\Bid\AddBidVoterContract;

final class CanAddBidHasConfirmedQualificationVoter implements AddBidVoterContract
{
	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(Order $order, ?User $checkableUser = null): bool
	{
		if (!$checkableUser) {
			return false;
		}
		$serviceChecker = UserCertificationConfirmationChecker::make($checkableUser);

		return $serviceChecker->check($order->getContainer()->getCategory());
	}

	public function message(): string
	{
		return (string)getTranslate('order.bids.add.fail.confirm-qualification'
			, 'Чтобы добавить заявку, подтвердите квалификацию в личном кабинете');
	}
}