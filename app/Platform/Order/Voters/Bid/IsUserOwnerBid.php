<?php


	namespace App\Platform\Order\Voters\Bid;


	use App\Models\Order\Order;
	use App\Models\Order\OrderBid;
	use App\Models\User;
	use Illuminate\Contracts\Auth\Authenticatable;

	class IsUserOwnerBid
	{

		public static function make(): self
		{
			return app(__CLASS__);
		}

		public function check(OrderBid $bid, ?User $checkableUser): bool
		{
			if (is_null($checkableUser)) {
				return false;
			}
			return $bid->getContainer()->getPerformerKey() === $checkableUser->getKey();
		}
	}