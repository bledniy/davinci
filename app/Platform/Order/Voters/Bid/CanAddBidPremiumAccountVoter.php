<?php


namespace App\Platform\Order\Voters\Bid;


use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Contract\Order\Bid\AddBidVoterContract;

class CanAddBidPremiumAccountVoter implements AddBidVoterContract
{

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(Order $order, ?User $checkableUser = null): bool
	{
		if (null === $checkableUser) {
			return false;
		}
		return $checkableUser->getPremiumAccountService()->isActivePremium();
	}

	public function message(): string
	{
		return (string)getTranslate('order.bids.add.fail.premium-account', 'Чтобы добавить заявку, активируйте премиум аккаунт');
	}
}