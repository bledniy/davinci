<?php


namespace App\Platform\Order\Voters\Bid;


use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Contract\Order\Bid\AddBidVoterContract;
use App\Repositories\Order\OrderBidRepository;

class IsUserCanAddBidToDeal
{

	/** @var AddBidVoterContract[] */
	private static $extraVoters = [];

	private $reason;

	public const REASON_ORDER_CLOSED = 1;

	public const REASON_ORDER_HAS_PERFORMER = 2;

	public const REASON_PERFORMER_HAS_BID = 3;

	/**
	 * @var OrderBidRepository
	 */
	private $repository;

	public function __construct(OrderBidRepository $repository)
	{
		$this->repository = $repository;
	}

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(Order $order, ?User $checkableUser = null)
	{
		if (null === $checkableUser) {
			return false;
		}
		if ($order->getContainer()->isClosed()) {
			$this->reason = static::REASON_ORDER_CLOSED;
			return false;
		}
		if ($order->getContainer()->hasPerformer()) {
			$this->reason = static::REASON_ORDER_HAS_PERFORMER;
			return false;
		}
		if ($this->repository->isUserHasBidInOrder($order, $checkableUser)) {
			$this->reason = static::REASON_PERFORMER_HAS_BID;
			return false;
		}

		foreach (self::$extraVoters as $extraVoter) {
			if (false === $extraVoter->check($order, $checkableUser)) {
				$this->reason = $extraVoter;
				return false;
			}
		}
		return true;
	}

	/**
	 * @return mixed
	 */
	public function getReason()
	{
		return $this->reason;
	}


	public static function registerVoter(AddBidVoterContract $voter)
	{
		self::$extraVoters[] = $voter;
	}
}