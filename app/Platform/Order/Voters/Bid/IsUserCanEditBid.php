<?php


	namespace App\Platform\Order\Voters\Bid;


	use App\Models\Order\Order;
	use App\Models\Order\OrderBid;
	use App\Models\User;
	use Illuminate\Contracts\Auth\Authenticatable;

	class IsUserCanEditBid
	{

		public function requires()
		{
			return [IsUserOwnerBid::class];
		}

		public static function make(): self
		{
			return app(__CLASS__);
		}

		public function check(OrderBid $bid, Order $order): bool
		{
			if (!$bid->getContainer()->isEnabled()) {
				return false;
			}
			if (!$bid->getContainer()->getLimitEdit()) {
				return false;
			}
			if ($order->getContainer()->isClosed() || $order->getContainer()->hasPerformer()) {
				return false;
			}
			return true;
		}
	}