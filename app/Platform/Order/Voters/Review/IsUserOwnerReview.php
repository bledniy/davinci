<?php


namespace App\Platform\Order\Voters\Review;


use App\Models\Order\Review;
use App\Models\User;

class IsUserOwnerReview
{
	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(Review $review, ?User $checkableUser = null)
	{
		if (null === $checkableUser) {
			return false;
		}
		if ((int)$review->getBelongsKey('reviewFrom') !== $checkableUser->getKey()) {
			return false;
		}
		return true;
	}
}