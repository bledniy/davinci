<?php


namespace App\Platform\Order\Voters\Review;


use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Order\Voters\Deal\IsUserOwnerDeal;
use App\Platform\Order\Voters\Deal\IsUserSelectedAsPerformer;

class IsCustomerCanAddReview
{
	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(Order $order, ?User $checkableUser = null)
	{
		if (null === $checkableUser) {
			return false;
		}
		if (!$order->getContainer()->isClosed()) {
			return false;
		}
		if (!IsUserOwnerDeal::make()->check($order->getContainer()->getCustomer(), $checkableUser)) {
			return false;
		}
		if (null === $order->getContainer()->getDateClose()) {
		    return false;
		}
		if (!IsDateReviewNotExpired::make()->check($order->getContainer()->getDateClose())) {
			return false;
		}
		return true;
	}
}