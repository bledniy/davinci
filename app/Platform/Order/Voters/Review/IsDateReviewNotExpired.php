<?php


namespace App\Platform\Order\Voters\Review;


use Illuminate\Support\Carbon;

class IsDateReviewNotExpired
{

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(Carbon $dateClose)
	{
		if ($dateClose > now()) {
		    return true;
		}
		return $dateClose->diffInDays(now()) < (int)config('deals.reviews.days_to_publish');
	}
}