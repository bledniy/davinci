<?php


namespace App\Platform\Order\Voters\Chat;


use App\Models\Order\Order;
use App\Models\User;

class IsUserCanJoinToDealChat
{

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function check(Order $order, ?User $checkableUser): bool
	{
		if (!$order->getContainer()->hasPerformer() && $order->getContainer()->isOpened()) {
			return true;
		}
		return false;
	}

}