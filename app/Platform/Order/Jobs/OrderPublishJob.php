<?php


namespace App\Platform\Order\Jobs;


use App\Models\Order\Order;

class OrderPublishJob
{
	public function handle(Order $order)
	{
		$order->getSetter()->setPublishDate(now());
		return $order;
	}
}