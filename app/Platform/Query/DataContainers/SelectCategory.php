<?php


namespace App\Platform\Query\DataContainers;


use App\Models\Category\Category;
use Illuminate\Support\Collection;

class SelectCategory
{
	/**
	 * @var Category
	 */
	private $category;

	private $isSelected;

	public function __construct(Category $category, bool $isSelected = false)
	{
		$this->category = $category;
		$this->isSelected = $isSelected;
	}

	public function getKey()
	{
		return $this->category->getKey();
	}

	public function getNameDisplay(): ?string
	{
		return $this->category->getNameDisplay();
	}

	/**
	 * @return Collection | self[]
	 */
	public function getSubcategories(): Collection
	{
		return $this->category->getSubcategories();
	}

	public function isSelected(): bool
	{
		return $this->isSelected;
	}

}