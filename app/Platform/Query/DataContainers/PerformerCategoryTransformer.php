<?php


namespace App\Platform\Query\DataContainers;


use App\Models\Category\Category;
use Illuminate\Support\Collection;

class PerformerCategoryTransformer
{

	/**
	 * @var Collection
	 */
	private $categories;
	/**
	 * @var array
	 */
	private $selected;

	public function __construct(Collection $categories, array $selected = [])
	{
		$this->categories = $categories;
		$this->selected = array_flip($selected);
	}

	public function transform()
	{
		$this->categories->transform(function (Category $category) {
			$this->transformSubcategories($category);
			return app(SelectCategory::class, ['category' => $category]);
		});
		return $this->categories;
	}

	private function transformSubcategories(Category $category): void
	{
		if ($category->getSubcategories()) {
			$category->setSubcategories($category->getSubcategories()->transform(function (Category $category) {
				$isSelected = $this->isCategorySelected($category);
				return app(SelectCategory::class, compact('category', 'isSelected'));
			}));
		}
	}

	private function isCategorySelected(Category $category): bool
	{
		return (bool)($this->selected[$category->getKey()] ?? false);
	}
}