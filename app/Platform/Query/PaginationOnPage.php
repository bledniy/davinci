<?php


namespace App\Platform\Query;


class PaginationOnPage
{

	public const DEFAULT = 10;

	public function default(): int
	{
		return $this->getItems()[0] ?? self::DEFAULT;
	}

	public function getItems(): array
	{
		return [
			10,
			20,
			40,
			60,
		];
	}

	public function itemSupports($item): bool
	{
		return 0 < $item && $item <= 60;
	}
}