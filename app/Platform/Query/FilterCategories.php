<?php


namespace App\Platform\Query;


class FilterCategories
{
	public static function filter(array $categories): array
	{
		return array_filter($categories, function($item){
		    return is_numeric($item);
		});
	}

}