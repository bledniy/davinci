<?php


	namespace App\Platform\Notifications\Contracts;


	interface NotificationTypesContract
	{
		public const DEAL_ADDED_REVIEW_FROM_PERFORMER = 'DEAL_ADDED_REVIEW_FROM_PERFORMER';
		public const SELECTED_AS_PERFORMER = 'SELECTED_AS_PERFORMER';
		public const CHAT_MESSAGE = 'CHAT_MESSAGE';
		public const NEW_BLOG_POST = 'NEW_BLOG_POST';
		public const DEAL_ADDED_REVIEW_FROM_CUSTOMER = 'DEAL_ADDED_REVIEW_FROM_CUSTOMER';
		public const DEAL_CLOSED = 'DEAL_CLOSED';
		public const DEAL_ADDED_BID = 'DEAL_ADDED_BID';
		public const PRO_ACCOUNT_EXPIRING = 'PRO_ACCOUNT_EXPIRING';
		public const PRO_ACCOUNT_EXPIRED = 'PRO_ACCOUNT_EXPIRED';
		public const NEW_DEALS = 'NEW_DEALS';
	}