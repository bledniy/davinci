<?php


namespace App\Platform\Cache;


interface CacheKeys
{
	public const CABINET_COUNT_REVIEWS = 'CABINET_COUNT_REVIEWS.';
	public const PLATFORM_USER_CATEGORY_CERTIFIED = 'platform.user.category.';
}