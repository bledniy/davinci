<?php


namespace App\Platform\Cache;


use App\Models\Category\Category;
use App\Models\User;

class PlatformCacheKeysGenerator
{
	public static function userCategoryCertification(User $user, Category $category): string
	{
		return CacheKeys::PLATFORM_USER_CATEGORY_CERTIFIED . $user->getKey() . ':' .$category->getKey();
	}
}