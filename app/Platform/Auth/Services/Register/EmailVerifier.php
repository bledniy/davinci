<?php declare(strict_types=1);

namespace App\Platform\Auth\Services\Register;

use App\Services\Verification\BaseVerificationService;

class EmailVerifier extends BaseVerificationService
{
	/**
	 * @var RegisterStepsDataContainer
	 */
	protected $dataContainer;

	public function __construct(RegisterStepsDataContainer $dataContainer)
	{
		parent::__construct();
		$this->dataContainer = $dataContainer;
	}

	public function notify()
	{
		$this->notifyViaMail();
	}

	protected function getTarget()
	{
		return $this->dataContainer->getEmail();
	}

	protected function getType()
	{
		return 'register';
	}

}