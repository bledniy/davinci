<?php


namespace App\Platform\Auth\Services\Register;


use App\Models\Verification;

class VerificationAvailableService
{
	/**
	 * @var Verification
	 */
	private $verification;

	public function __construct(Verification $verification)
	{
		$this->verification = $verification;
	}

	public function isAvailable(): bool
	{
		return $this->verification->getAvailableAt() <= now();
	}
}