<?php


namespace App\Platform\Auth\Services\Register;


use App\Platform\Auth\Contracts\AuthTokenGenerator;
use Illuminate\Support\Str;

class RegistrationTokenGenerator implements AuthTokenGenerator
{

	public function generate(): string
	{
		return Str::random(64);
	}

}