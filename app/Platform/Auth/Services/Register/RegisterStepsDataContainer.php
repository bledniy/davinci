<?php declare(strict_types=1);

namespace App\Platform\Auth\Services\Register;

use Illuminate\Support\Arr;

class RegisterStepsDataContainer
{
	/**
	 * @var array
	 */
	private $data;

	public function __construct(array $data = [])
	{
		$this->data = $data;
	}

	public function getPassword()
	{
		return $this->getDataItem('password');
	}

	public function getUserType()
	{
		return $this->getDataItem('type');
	}

	public function getEmail()
	{
		return $this->getDataItem('email');
	}

	public function getAvatar()
	{
		return $this->getDataItem('avatar');
	}

	public function getName()
	{
		return $this->getDataItem('name');
	}

	public function getSurname()
	{
		return $this->getDataItem('surname');
	}

	public function getPhone()
	{
		return $this->getDataItem('phone');
	}

	public function getCity()
	{
		return $this->getDataItem('city_id');
	}

	public function getTermsConfirmed()
	{
		return $this->getDataMeta('terms_confirmed');
	}

	public function getCurrentStep()
	{
		return $this->getDataMeta('current_step');
	}

	public function getPhoneVerified()
	{
		return $this->getDataMeta('phone_verified');
	}

	public function getPhoneVerifiedAt()
	{
		return $this->getDataMeta('phone_verified_at');
	}

	public function getEmailVerified()
	{
		return $this->getDataMeta('email_verified');
	}

	public function getEmailVerifiedAt()
	{
		return $this->getDataMeta('email_verified_at');
	}

	public function setPassword($password): RegisterStepsDataContainer
	{
		$this->data['data']['password'] = $password;

		return $this;
	}

	public function setUserType($value): RegisterStepsDataContainer
	{
		return $this->setDataItem('type', $value);
	}

	public function setEmail($value): RegisterStepsDataContainer
	{
		return $this->setDataItem('email', $value);
	}

	public function setName($value): RegisterStepsDataContainer
	{
		return $this->setDataItem('name', $value);
	}

	public function setAvatar($value): RegisterStepsDataContainer
	{
		return $this->setDataItem('avatar', $value);
	}

	public function setSurname($value): RegisterStepsDataContainer
	{
		return $this->setDataItem('surname', $value);
	}

	public function setPhone($value): RegisterStepsDataContainer
	{
		return $this->setDataItem('phone', $value);
	}

	public function setCity($value): RegisterStepsDataContainer
	{
		return $this->setDataItem('city_id', $value);
	}

	public function setTermsConfirmed(): RegisterStepsDataContainer
	{
		return $this->setDataMeta('terms_confirmed', true);
	}

	public function setCurrentStep($step): RegisterStepsDataContainer
	{
		return $this->setDataMeta('current_step', $step);
	}

	public function setPhoneVerified(): RegisterStepsDataContainer
	{
		return $this->setDataMeta('phone_verified', true)->setDataMeta('phone_verified_at', now());
	}

	public function setEmailVerified(): RegisterStepsDataContainer
	{
		return $this->setDataMeta('email_verified', true)->setDataMeta('email_verified_at', now());
	}

	public function addCompletedStep($step): RegisterStepsDataContainer
	{
		if ($this->hasCompletedStep($step)) {
			return $this;
		}
		$this->data['meta']['completed_steps'][] = $step;

		return $this;
	}

	public function getCompletedSteps(): array
	{
		return (array)$this->getDataMeta('completed_steps');
	}

	public function hasCompletedStep($step): bool
	{
		return in_array($step, $this->getCompletedSteps(), true);
	}

	public function pushToCompletedCurrentStep(): RegisterStepsDataContainer
	{
		if ($this->hasCompletedStep($this->getCurrentStep())) {
			return $this;
		}

		return $this->addCompletedStep($this->getCurrentStep());
	}

	/**
	 * google facebook
	 * in lower case
	 * @param string $network
	 * @return RegisterStepsDataContainer
	 */
	public function setSocialNetwork(string $network): RegisterStepsDataContainer
	{
		$this->setDataMeta('social_network', $network);

		return $this;
	}

	public function getSocialNetwork()
	{
		return $this->getDataMeta('social_network');
	}

	public function setSocialNetworkId(string $networkId): RegisterStepsDataContainer
	{
		$this->setDataMeta('social_network_id', $networkId);

		return $this;
	}

	public function getSocialNetworkId()
	{
		return $this->getDataMeta('social_network_id');
	}

	//
	public function getData(): array
	{
		return $this->data;
	}

	protected function getDataItem($key, $default = null)
	{
		return $this->data['data'][$key] ?? $default;
	}

	protected function getDataMeta($key)
	{
		return $this->data['meta'][$key] ?? null;
	}

	protected function setDataItem($key, $value): RegisterStepsDataContainer
	{
		$this->data['data'][$key] = $value;

		return $this;
	}

	protected function setDataMeta($key, $value): RegisterStepsDataContainer
	{
		$this->data['meta'][$key] = $value;

		return $this;
	}

	public function toResponse()
	{
		return array_map(function ($item) {
			return Arr::except($item, ['password']);
		}, $this->getData());
	}


}