<?php


namespace App\Platform\Auth\Services\Register;


use Illuminate\Http\Request;

class RegistrationTokenExtractor
{

	/**
	 * @var Request
	 */
	private $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function getToken()
	{
		if ($token = $this->request->get('__registration_token')) {
			return $token;
		}
		return $this->request->hasHeader('x-registration-token') ? $this->request->headers['x-registration-token'] : null;
	}

	public function hasToken(): bool
	{
		return (bool)$this->getToken();
	}
}