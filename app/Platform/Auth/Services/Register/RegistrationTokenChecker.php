<?php


namespace App\Platform\Auth\Services\Register;


use App\Repositories\TemporaryRepository;

class RegistrationTokenChecker
{

	/**
	 * @var RegistrationTokenExtractor
	 */
	private $extractor;
	/**
	 * @var TemporaryRepository
	 */
	private $temporaryRepository;

	public function __construct(RegistrationTokenExtractor $extractor, TemporaryRepository $temporaryRepository)
	{
		$this->extractor = $extractor;
		$this->temporaryRepository = $temporaryRepository;
	}

	public function check():bool
	{
		if (!$this->extractor->getToken()) {
			return false;
		}
		return !is_null($this->temporaryRepository->findByKey($this->extractor->getToken()));
	}


}