<?php

namespace App\Platform\Auth\Services\Register;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterDataContainerFiller
{
	public function customerData(Request $request, RegisterStepsDataContainer $dataContainer): RegisterStepsDataContainer
	{
		return $this->performerData($request, $dataContainer)
			->setCity($request->get('city'))
			;
	}

	public function performerData(Request $request, RegisterStepsDataContainer $dataContainer): RegisterStepsDataContainer
	{
		return $dataContainer
			->setName($request->get('name'))
			->setSurname($request->get('surname'))
			->setPhone($request->get('phone'))
			->setPassword(Hash::make($request->get('password')))
			;

	}
}