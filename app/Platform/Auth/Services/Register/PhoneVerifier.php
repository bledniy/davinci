<?php


namespace App\Platform\Auth\Services\Register;


class PhoneVerifier extends EmailVerifier
{

	public function notify()
	{
		$this->notifyViaSms();
	}

	protected function getTarget()
	{
		return $this->dataContainer->getPhone();
	}

}