<?php


namespace App\Platform\Auth\Services\Verification;


use App\Models\Verification;

class CodeVerifier
{
	/**
	 * @var Verification
	 */
	private $verification;

	public function __construct(Verification $verification)
	{
		$this->verification = $verification;
	}

	public static function resolve(Verification $verification):self
	{
		return app(self::class, compact('verification'));
	}

	public function compare($code): bool
	{
		return $code === $this->verification->getCode();
	}
}