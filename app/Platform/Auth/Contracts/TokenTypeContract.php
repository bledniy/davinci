<?php


namespace App\Platform\Auth\Contracts;


interface TokenTypeContract
{
	public const TYPE_REGISTER = 'REGISTER';
	public const TYPE_LOGIN = 'LOGIN';

}