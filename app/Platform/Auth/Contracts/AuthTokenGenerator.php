<?php


namespace App\Platform\Auth\Contracts;


interface AuthTokenGenerator
{
	public function generate():string;
}