<?php


namespace App\Platform\Auth\Contracts;


interface RegisterStepContract
{
	public const STEP_CHOOSE_TYPE = 'STEP_CHOOSE_TYPE';
	public const STEP_DATA_CUSTOMER = 'STEP_DATA_CUSTOMER';
	public const STEP_DATA_PERFORMER = 'STEP_DATA_PERFORMER';
	public const STEP_PERFORMER_CONFIRM_EMAIL = 'STEP_PERFORMER_CONFIRM_EMAIL';
	public const STEP_PERFORMER_CONFIRM_PHONE = 'STEP_PERFORMER_CONFIRM_PHONE';
}