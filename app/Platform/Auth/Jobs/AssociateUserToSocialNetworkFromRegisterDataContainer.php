<?php

namespace App\Platform\Auth\Jobs;

use App\Models\User;
use App\Models\User\SocialProvider;
use App\Platform\Auth\Services\Register\RegisterStepsDataContainer;

class AssociateUserToSocialNetworkFromRegisterDataContainer
{
	/**
	 * @var RegisterStepsDataContainer
	 */
	private $dataContainer;

	public function __construct(RegisterStepsDataContainer $dataContainer)
	{
		$this->dataContainer = $dataContainer;
	}

	public function handle(User $user): ?SocialProvider
	{
		$dataCreate = [
			'provider'    => $this->dataContainer->getSocialNetwork(),
			'provider_id' => $this->dataContainer->getSocialNetworkId(),
		];
		/** @var  $provider SocialProvider */
		$provider = $user->socialProviders()->create($dataCreate);
		return $provider;
	}
}