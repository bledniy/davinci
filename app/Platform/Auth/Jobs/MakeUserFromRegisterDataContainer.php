<?php


namespace App\Platform\Auth\Jobs;


use App\Models\User;
use App\Platform\Auth\Services\Register\RegisterStepsDataContainer;
use App\Platform\User\Type\UserTypeHelper;
use App\Repositories\UserRepository;

class MakeUserFromRegisterDataContainer
{
	/**
	 * @var RegisterStepsDataContainer
	 */
	private $dataContainer;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(RegisterStepsDataContainer $dataContainer, UserRepository $userRepository)
	{
		$this->dataContainer = $dataContainer;
		$this->userRepository = $userRepository;
	}

	public function handle(): ?User
	{
		$container = $this->dataContainer;
		$user = [
			'name'     => $container->getName(),
			'surname'  => $container->getSurname(),
			'email'    => $container->getEmail(),
			'phone'    => $container->getPhone(),
			'password' => $container->getPassword(),
			'city_id'  => $container->getCity(),
			'type'     => $container->getUserType(),
		];
		$user[UserTypeHelper::isPerformer($container->getUserType()) ? 'avatar' : 'customer_avatar'] = $container->getAvatar();
		if ($container->getPhoneVerified() && $container->getPhoneVerifiedAt()) {
			$user['phone_verified_at'] = $container->getPhoneVerifiedAt();
		}
		if ($container->getEmailVerified() && $container->getEmailVerifiedAt()) {
			$user['email_verified_at'] = $container->getEmailVerifiedAt();
		}

		return $this->userRepository->create($user);
	}
}