<?php declare(strict_types=1);

namespace App\Mail\Cabinet;

use App\DataContainers\Mail\CertificationData;
use App\DataContainers\Mail\MailAdminConfigDataInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PerformerCertificationUploadedMail extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * @var CertificationData
	 */
	private $data;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(CertificationData $data, MailAdminConfigDataInterface $config, Translator $translator)
	{
		$config->fillFromMailable($this);
		$this->subject(sprintf('%s | %s | %s',
			$translator->get('Новое подтверждение специальности'),
			$data->getCategoryName(),
			$data->getUserFullName()
		));
		$this->data = $data;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->view('mail.cabinet.certification')->with(['data' => $this->data]);
	}
}
