<?php declare(strict_types=1);

namespace App\Mail\Feedback;

use App\DataContainers\Mail\FeedbackReportMailData;
use App\DataContainers\Mail\MailAdminConfigDataInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

final class NotifyAdminReportFeedbackMail extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * @var FeedbackReportMailData
	 */
	private $feedback;

	public function __construct(FeedbackReportMailData $feedback, MailAdminConfigDataInterface $config, Translator $translator)
	{
		$this->feedback = $feedback;
		$config->fillFromMailable($this);
		$this->subject($translator->get('Обратная связь | уведомление об ошибке'));
	}

	public function build()
	{
		return $this->view('mail.feedback.feedback')->with(['feedback' => $this->feedback]);
	}
}
