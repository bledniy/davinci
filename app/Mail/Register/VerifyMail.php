<?php declare(strict_types=1);

namespace App\Mail\Register;

use App\Models\User;
use App\Models\Verification;
use App\Platform\User\UserLanguageExtractor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyMail extends Mailable implements ShouldQueue
{
	use Queueable, SerializesModels;

	/**
	 * @var Verification
	 */
	private $verification;
	/**
	 * @var User | null
	 */
	private $user;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(Verification $verification, ?User $user = null)
	{
		$this->verification = $verification;
		$this->user = $user;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$langId = UserLanguageExtractor::make($this->user)->getLanguageId();
		$textCode = getTranslate('verification.code', null, $langId);
		$verification = $this->verification;
		$with = compact(array_keys(get_defined_vars()));

		return $this->view('mail.register.verify-email')->with($with);
	}
}
