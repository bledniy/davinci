<?php

namespace App\Criteria\Order;

use App\DataContainers\Platform\Order\SearchCabinetOrdersContainer;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria.
 *
 * @package namespace App\Criteria;
 */
class CabinetOrdersFilterCriteria implements CriteriaInterface
{
	/**
	 * @var SearchCabinetOrdersContainer
	 */
	private $container;

	public function __construct(SearchCabinetOrdersContainer $container)
	{
		$this->container = $container;
	}

	public function apply($model, RepositoryInterface $repository)
	{
		/** @var $model Builder */
		if ($this->container) {
			if ($this->container->getCustomerId()) {
				$model = $model->where('customer_id', $this->container->getCustomerId());
			}
			if ($this->container->getPerformerId()) {
				$model = $model->where('performer_id', $this->container->getPerformerId());
			}
			if (!is_null($this->container->isWhereClosed())) {
				$model = $model->where('is_closed', $this->container->isWhereClosed());
			}
			if ($search = $this->container->getSearch()) {
				$model = $model->where(function ($q) use ($search) {
					$q
						->whereLike('title', $search);
				});
			}
		}
		return $model;
	}
}
