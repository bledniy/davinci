<?php

namespace App\Criteria\Order;

use App\DataContainers\Platform\Order\SearchOrdersDataContainer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria.
 *
 * @package namespace App\Criteria;
 */
class OrderFilterCriteria implements CriteriaInterface
{

	private $container;

	public function __construct(SearchOrdersDataContainer $container)
	{
		$this->container = $container;
	}

	/**
	 * Apply criteria in query repository
	 *
	 * @param string | Model | Builder $model
	 * @param RepositoryInterface $repository
	 *
	 * @return mixed
	 */
	public function apply($model, RepositoryInterface $repository)
	{
		if ($this->container) {
			if ($this->container->getCityId()) {
				$model = $model->where('city_id', $this->container->getCityId());
			}
			if ($this->container->getCategoryIds()) {
				$model = $model->whereHas('category', function ($q) {
					$q->whereIn('category_id', $this->container->getCategoryIds());
				});
			}
			if ($this->container->getCanOnline()) {
				$model = $model->where('can_online', true);
			}
			if ($exceptCustomers = $this->container->getExceptCustomers()) {
				$ids = collect($exceptCustomers)->pluck('id')->toArray();
				$model = $model->whereNotIn('customer_id', $ids);
			}
			if ($this->container->isOnlyActive()) {
				$model = $model->where(function ($q) {
					return $q->where('is_closed', false)->whereNull('performer_id');
				});
			}
		}
		return $model;
	}
}
