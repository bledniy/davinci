<?php

namespace App\Events\Chat;

use App\Http\Resources\Chat\ChatMessageResource;
use App\Models\Chat\Chat;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessagesWatchedEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Chat
	 */
	private $chat;
	private $chatMessages;

	public function __construct(Chat $chat, $chatMessages)
    {
		$this->chat = $chat;
		$this->chatMessages = $chatMessages;
		$this->dontBroadcastToCurrentUser();
	}

	public function broadcastWith()
	{
		return ['messages' => ChatMessageResource::collection($this->chatMessages)];
	}

	public function broadcastAs()
	{
		return 'MessagesWatched';
	}

    public function broadcastOn()
    {
        return new PrivateChannel('chats.' . $this->chat->getKey());
    }
}
