<?php

namespace App\Events\Chat;

use App\Http\Resources\Chat\ChatMessageResource;
use App\Http\Resources\Chat\ChatResource;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class MessageToUsersSubmitted implements ShouldBroadcast
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var ChatMessage
	 */
	public $chatMessage;
	/**
	 * @var Collection
	 */
	private $users;

	public function __construct(ChatMessage $chatMessage, Collection $users)
	{
		$this->chatMessage = $chatMessage;
		$this->users = $users;
	}

	public function broadcastWith()
	{
		return ['message' => ChatMessageResource::make($this->chatMessage)];
	}

	public function broadcastAs()
	{
		return 'MessageSubmitted';
	}

	public function broadcastOn()
	{
		return $this->users->map(function (User $user) {
			return new PresenceChannel('chats.user.' . $user->getKey());
		})->toArray();
	}
}
