<?php

namespace App\Events\Chat;

use App\Http\Resources\Chat\ChatMessageResource;
use App\Http\Resources\Chat\ChatResource;
use App\Listeners\Chat\MessageSubmittedNotifyUnread;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessageSubmitted implements ShouldBroadcast
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Chat
	 */
	private $chat;
	/**
	 * @var ChatMessage
	 */
	public $chatMessage;

	public function __construct(Chat $chat, ChatMessage $chatMessage)
	{
		$this->chat = $chat;
		$this->chatMessage = $chatMessage;
		$this->dontBroadcastToCurrentUser();
	}

	public function broadcastWith()
	{
		return ['message' => ChatMessageResource::make($this->chatMessage)];
	}

	public function broadcastAs()
	{
		return 'MessageSubmitted';
	}

	public function broadcastOn()
	{
		return [
			new PresenceChannel('chats.' . $this->chat->getKey()),
		];
	}
	public static function getListeners(): array
	{
	    return [
			MessageSubmittedNotifyUnread::class
	    ];
	}

	/**
	 * @return ChatMessage
	 */
	public function getChatMessage(): ChatMessage
	{
		return $this->chatMessage;
	}

	/**
	 * @return Chat
	 */
	public function getChat(): Chat
	{
		return $this->chat;
	}
}
