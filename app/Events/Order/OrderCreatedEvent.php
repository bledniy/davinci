<?php

namespace App\Events\Order;

use App\Http\Resources\Order\OrderResource;
use App\Models\Order\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderCreatedEvent implements ShouldBroadcastNow
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
		$this->order->load('customer', 'city', 'category');
		$this->dontBroadcastToCurrentUser();
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	public function broadcastAs()
	{
		return 'OrderCreated';
	}

	public function broadcastWith()
	{
		return ['deal' => OrderResource::make($this->order)];
	}

	public function broadcastOn()
	{
		return new Channel('deals.new');
	}
}
