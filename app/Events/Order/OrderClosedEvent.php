<?php

namespace App\Events\Order;

use App\Contracts\Events\Order\EventHasOrder;
use App\Models\Order\Order;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderClosedEvent implements EventHasOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order)
    {
		$this->order = $order;
	}

	public function getOrder(): Order
	{
		return $this->order;
	}
}
