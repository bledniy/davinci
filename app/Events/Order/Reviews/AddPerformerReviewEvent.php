<?php

namespace App\Events\Order\Reviews;

use App\Contracts\Events\Order\EventHasOrder;
use App\Contracts\Events\Order\EventHasReview;
use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AddPerformerReviewEvent implements EventHasReview, EventHasOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Order
	 */
	protected $order;
	/**
	 * @var Review
	 */
	protected $review;
	/**
	 * @var User
	 */
	protected $performer;

	public function __construct(Order $order, Review $review, User $performer)
	{
		$this->order = $order;
		$this->review = $review;
		$this->performer = $performer;
	}

	/**
	 * @return User
	 */
	public function getPerformer(): User
	{
		return $this->performer;
	}

	/**
	 * @return Review
	 */
	public function getReview(): Review
	{
		return $this->review;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}
}
