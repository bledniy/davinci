<?php

namespace App\Events\Order\Reviews;

use App\Contracts\Events\Order\EventHasOrder;
use App\Contracts\Events\Order\EventHasReview;
use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;

class AddCustomerReviewEvent implements EventHasReview, EventHasOrder
{
	/**
	 * @var Order
	 */
	protected $order;
	/**
	 * @var Review
	 */
	protected $review;
	/**
	 * @var User
	 */
	protected $customer;

	public function __construct(Order $order, Review $review, User $customer)
	{
		$this->order = $order;
		$this->review = $review;
		$this->customer = $customer;
	}

	/**
	 * @return User
	 */
	public function getCustomer(): User
	{
		return $this->customer;
	}

	/**
	 * @return Review
	 */
	public function getReview(): Review
	{
		return $this->review;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}
}
