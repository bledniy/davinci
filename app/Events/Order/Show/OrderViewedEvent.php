<?php

namespace App\Events\Order\Show;

use App\Listeners\Order\Show\OnOrderViewedIncrementOrderViewsListener;
use App\Models\Order\Order;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderViewedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var User |null
	 */
	private $currentUser;

	/**
	 * Create a new event instance.
	 *
	 * @param Order $order
	 * @param User |null $currentUser
	 */
    public function __construct(Order $order, ?User $currentUser)
    {
		$this->order = $order;
		$this->currentUser = $currentUser;
	}

	/**
	 * @return User
	 */
	public function getCurrentUser(): ?User
	{
		return $this->currentUser;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}
	public static function getListeners(): array
	{
	    return [
			OnOrderViewedIncrementOrderViewsListener::class,
	    ];
	}
}
