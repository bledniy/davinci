<?php

namespace App\Events\Order\Show\Bids;

use App\Contracts\Events\Order\EventHasOrder;
use App\Http\Resources\Order\OrderBidResource;
use App\Http\Resources\Order\OrderResource;
use App\Listeners\Order\RecalcBidsListener;
use App\Listeners\Order\Show\Bids\BidCreatedListener;
use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BidCreatedEvent implements EventHasOrder, ShouldBroadcastNow
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var OrderBid
	 */
	private $orderBid;
	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order, OrderBid $orderBid)
	{
		$this->orderBid = $orderBid;
		$this->order = $order;
		$this->dontBroadcastToCurrentUser();
	}


	/**
	 * @return OrderBid
	 */
	public function getOrderBid(): OrderBid
	{
		return $this->orderBid;
	}

	public static function getListeners(): array
	{
		return [
			BidCreatedListener::class,
			RecalcBidsListener::class,
		];
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}


	public function broadcastAs()
	{
		return 'BidCreated';
	}

	public function broadcastWith()
	{
		return ['bid' => OrderBidResource::make($this->getOrderBid())];
	}

	public function broadcastOn()
	{
		return new PresenceChannel(sprintf('deals.%d.new-bid', $this->getOrder()->getKey()));
	}
}
