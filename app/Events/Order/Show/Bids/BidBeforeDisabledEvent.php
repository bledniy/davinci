<?php

namespace App\Events\Order\Show\Bids;

use App\Contracts\Events\Order\EventHasOrder;
use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BidBeforeDisabledEvent implements EventHasOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
	/**
	 * @var OrderBid
	 */
	private $orderBid;
	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order, OrderBid $orderBid)
	{
		$this->orderBid = $orderBid;
		$this->order = $order;
	}

	public function getOrderBid(): OrderBid
	{
		return $this->orderBid;
	}

	public function getOrder(): Order
	{
		return $this->order;
	}

}
