<?php

namespace App\Events\Order\Show\Bids;

use App\Listeners\Order\Show\MakeChatWorksOnChoosePerformerListener;
use App\Listeners\Order\Show\NotifyPerformerListener;
use App\Models\Order\Order;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChoosedPerformer
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var User
	 */
	private $performer;

	public function __construct(Order $order, User $performer)
	{
		$this->order = $order;
		$this->performer = $performer;
	}

	/**
	 * @return User
	 */
	public function getPerformer(): User
	{
		return $this->performer;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	public static function getListeners(): array
	{
		return [
			MakeChatWorksOnChoosePerformerListener::class,
			NotifyPerformerListener::class,
		];
	}
}
