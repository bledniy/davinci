<?php

namespace App\Events\Order;

use App\Contracts\Events\Order\EventHasOrder;
use App\Listeners\Order\Update\DecrementLimitEditsListener;
use App\Models\Order\Order;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderBeforeUpdatedEvent implements EventHasOrder
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	public static function getListeners(): array
	{
		return [
			DecrementLimitEditsListener::class
		];
	}
}
