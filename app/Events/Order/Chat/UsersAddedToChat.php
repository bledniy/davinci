<?php

namespace App\Events\Order\Chat;

use App\Models\Chat\Chat;
use App\Models\Order\Order;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UsersAddedToChat
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Chat
	 */
	private $chat;
	/**
	 * @var array | User[]
	 */
	private $users;


	public function __construct(Chat $chat, array $users)
	{
		$this->chat = $chat;
		$this->users = $users;
	}

	/**
	 * @return Chat
	 */
	public function getChat(): Chat
	{
		return $this->chat;
	}

	/**
	 * @return User[]|array
	 */
	public function getUsers(): array
	{
		return $this->users;
	}


}
