<?php

namespace App\Events\Order\Chat;

use App\Models\Chat\Chat;
use App\Models\Order\Order;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatCreated
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var Chat
	 */
	private $chat;
	/**
	 * @var User|null
	 */
	private $user;

	public function __construct(Order $order, Chat $chat, ?User $userInitializedChat= null)
	{
		$this->order = $order;
		$this->chat = $chat;
		$this->user = $userInitializedChat;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	/**
	 * @return Chat
	 */
	public function getChat(): Chat
	{
		return $this->chat;
	}

	/**
	 * @return User|null
	 */
	public function getUser(): ?User
	{
		return $this->user;
	}
}
