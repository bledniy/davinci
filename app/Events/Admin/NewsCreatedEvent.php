<?php

namespace App\Events\Admin;

use App\Models\News\News;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewsCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var News
	 */
	private $news;

	/**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(News $news)
    {
	    $this->news = $news;
    }

	/**
	 * @return News
	 */
	public function getNews(): News
	{
		return $this->news;
	}
}
