<?php declare(strict_types=1);

namespace App\Events\Cabinet;

use App\Services\Payment\Portmone\PaymentResponseDataInterface;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PaymentCallbackEvent
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var PaymentResponseDataInterface
	 */
	private $paymentResponse;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(PaymentResponseDataInterface $paymentResponse)
	{
		$this->paymentResponse = $paymentResponse;
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}

	/**
	 * @return PaymentResponseDataInterface
	 */
	public function getPaymentResponse(): PaymentResponseDataInterface
	{
		return $this->paymentResponse;
	}
}
