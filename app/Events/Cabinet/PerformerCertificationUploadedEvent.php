<?php declare(strict_types=1);

namespace App\Events\Cabinet;

use App\Models\User\UserCertification;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PerformerCertificationUploadedEvent
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var UserCertification
	 */
	private $certification;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(UserCertification $certification)
	{
		$this->certification = $certification;
	}

	/**
	 * @return UserCertification
	 */
	public function getCertification(): UserCertification
	{
		return $this->certification;
	}
}
