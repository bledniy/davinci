<?php

namespace App\Events\Platform\Cabinet;

use App\Listeners\Platform\Cabinet\UserCertificationPerformerCategoryListener;
use App\Models\User;
use App\Models\User\UserCertification;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserCertificationUpdatedEvent
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var UserCertification
	 */
	private $certification;
	/**
	 * @var User
	 */
	private $user;


	public function __construct(UserCertification $certification, User $user)
	{
		$this->certification = $certification;
		$this->user = $user;
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}

	/**
	 * @return UserCertification
	 */
	public function getCertification(): UserCertification
	{
		return $this->certification;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	public static function getListeners(): array
	{
		return [
			UserCertificationPerformerCategoryListener::class,
		];
	}
}
