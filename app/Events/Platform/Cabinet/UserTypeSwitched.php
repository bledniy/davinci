<?php

namespace App\Events\Platform\Cabinet;

use App\Listeners\Platform\Cabinet\UserTypeSwitchedListeners;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserTypeSwitched
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	private $newType;
	/**
	 * @var User
	 */
	private $user;

	/**
	 * Create a new event instance.
	 *
	 * @param $newType
	 * @param User $user
	 */
	public function __construct($newType, User $user)
	{
		$this->newType = $newType;
		$this->user = $user;
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return \Illuminate\Broadcasting\Channel|array
	 */
	public function broadcastOn()
	{
		return new PrivateChannel('channel-name');
	}

	public static function getListeners(): array
	{
		return [
			UserTypeSwitchedListeners::class,
		];
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	/**
	 * @return mixed
	 */
	public function getNewType()
	{
		return $this->newType;
	}
}
