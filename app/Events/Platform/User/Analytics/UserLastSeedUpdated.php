<?php

namespace App\Events\Platform\User\Analytics;

use App\Listeners\Platform\User\Analytics\UserLastSeedUpdatedListener;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class UserLastSeedUpdated
{
	use Dispatchable, InteractsWithSockets, SerializesModels;

	/**
	 * @var Carbon
	 */
	private $dateLastSeen;
	/**
	 * @var User
	 */
	private $user;

	/**
	 * Create a new event instance.
	 *
	 * @param \App\Models\User $user
	 * @param Carbon $dateLastSeen
	 */
	public function __construct(User $user, Carbon $dateLastSeen)
	{
		$this->dateLastSeen = $dateLastSeen;
		$this->user = $user;
	}

	/**
	 * @return Carbon
	 */
	public function getDateLastSeen(): Carbon
	{
		return $this->dateLastSeen;
	}

	/**
	 * @return \App\Models\User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	public static function getListeners(): array
	{
		return [
			UserLastSeedUpdatedListener::class,
		];
	}
}
