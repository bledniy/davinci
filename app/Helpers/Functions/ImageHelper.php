<?php

	use Illuminate\Support\Str;

	function imgPathOriginal($pathToImage)
{
	return Str::replaceLast('_s.', '.', (string)$pathToImage);
}

function imgPathThumbnail($pathToImage)
{
	return Str::replaceLast('.', '_s.', (string)$pathToImage);
}


if (!function_exists('getPathToImage')) {
	function getPathToImage(?string $src = null, $default = 'images/default.svg', $disk = null)
	{
		if (isExternalFile($src)) {
			return $src;
		}
		return storageFileExists($src, $disk) ? getStorageFilePath($src, $disk) : asset($default);
	}
}

function getApiImageUrl($src, $default = 'images/default.svg', $disk = 'api')
{
	return getPathToImage($src, $default, $disk);
}
function getApiFileUrl($src, $disk = 'api'): string
{
	return storageFileExists($src, $disk) ? getStorageFilePath($src, $disk) : '';
}

if (!function_exists('isExternalFile')) {
	function isExternalFile($src)
	{
		return isStringUrl($src);
	}
}

function defaultImage()
{
	return asset('images/staff/default.png');
}