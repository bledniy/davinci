<?php


namespace App\Helpers\Database;


use Illuminate\Database\Eloquent\Relations\HasOne;

class BuilderHelper
{
	public static function removeWhere( $query, $column)
	{
		foreach ( (array)$query->wheres AS $key => $value) {
			if ( $column === $value['column'] ) {
				unset($query->wheres[$key]);
				$query->wheres = array_values($query->wheres);
			}
		}
	}
	public static function hasWhere( $query, $column)
	{
		foreach ( (array)$query->wheres AS $key => $value) {
			if ( $column === $value['column'] ) {
				return true;
			}
		}
		return false;
	}
}