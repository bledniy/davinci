<?php

	namespace App\Traits\Models;


	use App\Models\Order\Order;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	trait BelongsToOrder
	{
		public function order(): BelongsTo
		{
			return $this->belongsTo(Order::class);
		}

		/**
		 * @return Order
		 */
		public function getOrder()
		{
			return $this->order;
		}
	}
