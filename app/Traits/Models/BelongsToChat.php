<?php

namespace App\Traits\Models;


use App\Models\Chat\Chat;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongsToChat
{
	/**
	 * @return BelongsTo
	 */
	public function chat(): BelongsTo
	{
		return $this->belongsTo(Chat::class);
	}

	/**
	 * @return Chat
	 */
	public function getChat()
	{
		return $this->chat;
	}
}
