<?php declare(strict_types=1);

namespace App\Traits\Models\User;

use App\Platform\Cabinet\Display\UserAvatarHelper;
use App\Platform\Cabinet\Display\UserLastActivity;
use App\Platform\ProAccount\UserProAccountService;
use App\Platform\User\UserLanguageExtractor;

trait UserHelpersTrait
{
	protected $userLastActivity;
	protected $userProAccount;
	protected $languageExtractor;

	public function getUserLastActivity(): UserLastActivity
	{
		if (null === $this->userLastActivity) {
			$this->userLastActivity = app(UserLastActivity::class, ['user' => $this]);
		}
		return $this->userLastActivity;
	}

	public function getAvatarHelper(): UserAvatarHelper
	{
		return app(UserAvatarHelper::class, ['user' => $this]);
	}

	public function getPremiumAccountService(): UserProAccountService
	{
		if (null === $this->userProAccount) {
			$this->userProAccount = app(UserProAccountService::class, ['user' => $this]);
		}
		return $this->userProAccount;
	}

	public function getLanguageExtractor(): UserLanguageExtractor
	{
		if (null === $this->languageExtractor) {
			$this->languageExtractor = app(UserLanguageExtractor::class, ['user' => $this]);
		}
		return $this->languageExtractor;
	}
}
