<?php

namespace App\Traits\Models\User;

use App\Helpers\User\Model\UserHasSocialsContainer;
use App\Helpers\User\Model\UserHasSocialsContract;
use App\Models\Category\Category;
use App\Models\City\City;
use Illuminate\Support\Collection;

trait UserAccessorsTrait
{

	public function getUserSocials(): UserHasSocialsContract
	{
		return app(UserHasSocialsContainer::class, ['model' => $this]);
	}

	/**
	 * @return Collection | Category[]
	 */
	public function getServices(): Collection
	{
		return $this->services;
	}

	public function getCity(): ?City
	{
		return $this->city;
	}

}
