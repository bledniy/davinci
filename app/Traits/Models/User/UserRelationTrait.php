<?php

namespace App\Traits\Models\User;

use App\Models\Category\Category;
use App\Models\City\City;
use App\Models\Order\Review;
use App\Models\User\NotificationConfig;
use App\Models\User\SocialProvider;
use App\Models\User\UserMeta;
use App\Models\User\UserPremium;
use App\Platform\Contract\UserTypeContract;
use App\Traits\Models\BelongsToLanguage;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait UserRelationTrait
{
	use BelongsToLanguage;

	public function socialProviders(): HasMany
	{
		return $this->hasMany(SocialProvider::class);
	}

	public function metas(): HasMany
	{
		return $this->hasMany(UserMeta::class);
	}

	public function services(): BelongsToMany
	{
		return $this->belongsToMany(Category::class, 'services', null, 'category_id');
	}

	public function city(): BelongsTo
	{
		return $this->belongsTo(City::class);
	}

	public function notificationConfig()
	{
		return $this->belongsToMany(NotificationConfig::class, 'user_notification_configs');
	}

	public function performerCategories(): BelongsToMany
	{
		return $this->belongsToMany(Category::class, 'performer_categories');
	}

	public function premium(): HasOne
	{
		return $this->hasOne(UserPremium::class);
	}

}
