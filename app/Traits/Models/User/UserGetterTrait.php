<?php

namespace App\Traits\Models\User;


use Illuminate\Support\Carbon;

trait UserGetterTrait
{
	public function getBalance(): int
	{
		return (int)$this->getAttribute('balance');
	}

	public function getName($field = 'name'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getSurname($field = 'surname'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getPatronymic($field = 'patronymic'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getFio(): string
	{
		return $this->getSurname() . ' ' . $this->getName() . ' ' . $this->getPatronymic();
	}

	public function getPhone($field = 'phone'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getEmail($field = 'email'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getAvatar($field = 'avatar'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getPhoneDisplay($field = 'phone'): string
	{
		return extractDigits($this->getAttribute($field));
	}

	public function getDateLastSeen(): ?Carbon
	{
		$date = $this->getAttribute('last_seen_at');
		return isDateValid($date) ? getDateCarbon($date) : null;
	}

	public function getSlug()
	{
		return $this->slug ?? $this->getKey();
	}

	public function getPerformerRating()
	{
		return (float)number_format($this->getAttribute('rating'), 2);
	}

	public function getCustomerRating()
	{
		return (float)number_format($this->getAttribute('customer_rating'), 2);
	}

	public function getCustomerCountReviews():int
	{
		return (int)$this->getAttribute('customer_reviews_count');
	}

	public function getPerformerCountReviews():int
	{
		return (int)$this->getAttribute('reviews_count');
	}

	public function getLanguageId():?int
	{
		return null === $this->getAttribute('language_id') ? null : (int)$this->getAttribute('language_id');
	}
}
