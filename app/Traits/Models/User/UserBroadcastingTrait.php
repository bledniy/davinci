<?php

namespace App\Traits\Models\User;

use App\Helpers\User\Model\UserHasSocialsContainer;
use App\Helpers\User\Model\UserHasSocialsContract;
use App\Models\Category\Category;
use App\Models\City\City;
use Illuminate\Support\Collection;

trait UserBroadcastingTrait
{
	public function receivesBroadcastNotificationsOn()
	{
		return 'users.notifications.' . $this->getKey();
	}

}
