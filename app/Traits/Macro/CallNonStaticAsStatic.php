<?php


namespace App\Traits\Macro;


trait CallNonStaticAsStatic
{
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }

}