<?php


namespace App\Traits\Requests\User;

/**
 * Trait IsPasswordWasSend
 * @package App\Traits\Requests\User
 * applies to Illuminate\Http\Request
 */
trait IsPasswordWasSend
{
	/**
	 * @param array $except
	 * @return bool
	 */
	public function isPasswordsWasSend($except = [])
	{
		$passwords = $this->only(['password', 'password_new', 'password_new_confirmation']);
		$passwords = $this->exceptFields($passwords, $except);
		$passwordWasSend = array_filter(array_filter($passwords, 'strlen'));
		return (boolean)$passwordWasSend;
	}

	public function isOnlyNewPasswordsWasSend()
	{
		return $this->isPasswordsWasSend(['password']);
	}
}
