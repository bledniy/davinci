<?php declare(strict_types=1);

namespace App\Test\Traits;

use App\Models\User;
use Laravel\Sanctum\Sanctum;

trait UsersActingTrait
{
	private $performerId = 1;
	private $customerId = 2;

	protected function switchToCustomer()
	{
		Sanctum::actingAs($this->getCustomer(), ['*']);
	}

	protected function getCustomer():User
	{
		static $customer;
		if (null === $customer) {
			$customer = User::whereKey($this->customerId)->first();
			if (!$customer) {
				throw new \Exception('Customer not found at user_id ' . $this->customerId);
			}
		}
		return $customer;
	}

	protected function switchToPerformer()
	{
		Sanctum::actingAs($this->getPerformer(), ['*']);
	}

	protected function getPerformer():User
	{
		static $performer;
		if (null === $performer) {
			$performer = User::find($this->performerId);
			if (!$performer) {
				throw new \Exception('Performer not found at user_id ' . $this->performerId);
			}
		}
		return $performer;
	}
}