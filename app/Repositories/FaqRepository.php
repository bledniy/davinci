<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\FaqSortCriteria;
use App\Models\Faq\Faq;
use App\Models\Faq\FaqLang;

class FaqRepository extends AbstractRepository
{

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return Faq::class;
	}

	public function modelLang()
	{
		return FaqLang::class;
	}

	public function addAdminCriteriaToQuery()
	{
		$this->pushCriteria(FaqSortCriteria::class);
		return $this;
	}

	public function addPublicCriteriaToQuery()
	{
		$this->pushCriteria(FaqSortCriteria::class);
		$this->pushCriteria(ActiveCriteria::class);
		return $this;
	}

	public function getListAdmin()
	{
		$this->addAdminCriteriaToQuery();
		return $this->get();
	}

	public function getListPublic($type)
	{
		$this->addPublicCriteriaToQuery();
		return $this->with('lang')->where('type', $type)->get();
	}

}
