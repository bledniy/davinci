<?php

	namespace App\Repositories\Order;

	use App\Models\Order\Order;
	use App\Models\Order\OrderUpload;
	use App\Repositories\AbstractRepository;
	use Illuminate\Support\Collection;


	class OrderUploadsRepository extends AbstractRepository
	{
		public function model()
		{
			return OrderUpload::class;
		}

		public function getByOrder($order): Collection
		{
			$order = !is_object($order) ?: $order->getKey();
			return $this->where('order_id', $order)->get();
		}

		public function findOneByOrder(Order $order, $uploadId = null):?OrderUpload
		{
			$this->whereModel($order);
			if ($uploadId){
				$this->where('id', $uploadId);
			}
			return $this->first();
		}

	}

