<?php

namespace App\Repositories\Order;

use App\DataContainers\Platform\Order\SearchCabinetOrdersContainer;
use App\Criteria\ActiveCriteria;
use App\Criteria\Order\CabinetOrdersFilterCriteria;
use App\Criteria\Order\OrderFilterCriteria;
use App\DataContainers\Platform\Order\SearchEditUserOrdersContainer;
use App\DataContainers\Platform\Order\SearchOrdersDataContainer;
use App\Models\Order\Order;
use App\Models\User;
use App\Repositories\AbstractRepository;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class OrderRepository extends AbstractRepository
{
	private $addCriteria = true;

	/**
	 * @param          $id
	 * @param string[] $columns
	 * @return Order
	 */
	public function find($id, $columns = ['*'])
	{
		return parent::find($id, $columns); // TODO: Change the autogenerated stub
	}

	public function model()
	{
		return Order::class;
	}

	/**
	 * @return Order
	 * @throws \Prettus\Repository\Exceptions\RepositoryException
	 */
	public function makeModel()
	{
		return parent::makeModel(); // TODO: Change the autogenerated stub
	}

	public function addPublicCriteriaToQuery(): self
	{
		if ($this->addCriteria) {
			$this->pushCriteria($this->app->make(ActiveCriteria::class));
		}
		$this->applyCriteria()->resetCriteria();
		return $this;
	}

	public function addAdminCriteriaToQuery()
	{
	}

	public function findForEdit($id): ?Order
	{
		return $this->where('id', $id)->first();
	}

	public function getListAdmin(SearchOrdersDataContainer $container)
	{
		$this->addAdminCriteriaToQuery();
		$this->addCriteria = false;
		/** @var  $items */
		$items = $this->orderQuery($container)
			->latest()
			->paginate()
		;
		return $items;
	}

	public function getLastDeals($limit = 5)
	{
		$this->addPublicCriteriaToQuery();
		$this->addOrderBy();
		$this->model->limit($limit);
		return $this->get();
	}

	public function orderQuery(SearchOrdersDataContainer $container)
	{
		$this->addPublicCriteriaToQuery();
		$this->pushCriteria($this->app->make(OrderFilterCriteria::class, ['container' => $container]));
		return $this;
	}

	public function findForShow($key): ?Order
	{
		$this->addPublicCriteriaToQuery();
		if (is_numeric($key)) {
			$this->where('id', $key);
		} else {
			$this->where('slug', $key);
		}
		return $this->first();
	}

	public function getListPublic(SearchOrdersDataContainer $container)
	{
		$perPage = $container->getOnPage();
		$page = $container->getPage();
		$cacheKey = 'orders-paginate.' . md5(serialize($container ? $container->toKey() : []));
		$items = $this->orderQuery($container)
			->forPage($page, $perPage)
			->latest()
			->get()
		;

		$total = Cache::get($cacheKey, function () use ($cacheKey, $container) {
			$q = $this->orderQuery($container);
			$count = $q->count();
			Cache::set($cacheKey, $count, now()->addMinutes(10));
			return $count ?? 0;
		});

		return new LengthAwarePaginator($items, $total, $perPage, $page);
	}

	public function getByUserType(SearchEditUserOrdersContainer $container): Collection
	{
		$this->addPublicCriteriaToQuery();
		$this->addOrderBy();
		if ($container->getPerformerId()) {
			$this->where('performer_id', $container->getPerformerId());
		} else if ($container->getCustomerId()) {
			$this->where('customer_id', $container->getCustomerId());
		}
		return $this->get();
	}

	public function getOrdersCabinet(SearchCabinetOrdersContainer $container)
	{
		$this->addPublicCriteriaToQuery();
		$this->pushCriteria($this->app->make(CabinetOrdersFilterCriteria::class, compact('container')));
		$this->addOrderBy();
		if ($container->isPaginate()) {
			return $this->paginate($container->getOnPage());
		}
		return $this->get();
	}

	public function addOrderBy()
	{
		$this->model = $this->model->latest();
		return $this;
	}

	public function findForActions($orderId): ?Order
	{
		$this->addPublicCriteriaToQuery();
		$this->whereKey($orderId);
		return $this->first();
	}

	public function loadForDeals($deals, $except = [], $additional = [])
	{
		$relations = arrayKeyAsValue(array_merge(['city.lang', 'category.lang', 'customer'], $additional));
		$relations = array_values(Arr::except($relations, $except));
		$deals->load($relations);
		return $deals;
	}

	public function getRecommendDeals(User $user, ?Order $order = null)
	{
		$model = $this->makeModel();
		$this->addPublicCriteriaToQuery();
		if ($order) {
			$this->whereNotIn($model->getKeyName(), [$order->getKey()]);
		}
		$this->where($model->customer()->getForeignKeyName(), '!=', $user->getKey());
		$this->where('is_closed', false);
		$this->whereNull($model->performer()->getForeignKeyName());
		$this->whereNotIn('id', function (Builder $q) use ($user, $model) {
			return $q->select($model->getForeignKey())->from('order_views')->where($user->getForeignKey(), $user->getKey());
		});
		$this->whereIn($model->category()->getForeignKeyName(), function (Builder $q) use ($user) {
			return $q->select('category_id')->from('performer_categories')->where($user->getForeignKey(), $user->getKey());
		});

		return $this->get();
	}

	public function getNewDeals(int $interval)
	{
		$this->addPublicCriteriaToQuery();
		$this->where('is_closed', 0)
			->where('created_at', '>=', \DB::raw(sprintf('NOW() - INTERVAL %d MINUTE', $interval)));

		return $this->get();
	}

}

