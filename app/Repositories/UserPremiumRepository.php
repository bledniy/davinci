<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\User;
use App\Models\User\UserPremium;
use DB;
use Illuminate\Support\Collection;

class UserPremiumRepository extends AbstractRepository
{
	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return UserPremium::class;
	}

	public function findByUser(User $user): ?UserPremium
	{
		return $this->whereModel($user)->first();
	}

	public function getExpiringPremiumUsers(int $days): Collection
	{
		return UserPremium::where('expires_at', '>', DB::raw('NOW()'))
			->where('expires_at', '<', (string)now()->addDays($days))
			->get()
		;
	}

	public function getExpiredPremiumUsers(): Collection
	{
		return UserPremium::where('expires_at', '<=', DB::raw('NOW()'))->get();
	}

}
