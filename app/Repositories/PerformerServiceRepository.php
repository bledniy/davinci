<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\User\Service;


class PerformerServiceRepository extends AbstractRepository
{

	public function model()
	{
		return Service::class;
	}

	public function findByUser(User $user)
	{
		return $this->whereModel($user)->get();
	}

	public function findOneByUser(User $user, $id = null):?Service
	{
		return $this->whereModel($user)
			->when($id, function($q) use ($id){
			    return $q->where('id', $id);
			})->first();
	}

}

