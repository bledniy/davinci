<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\Premium\Premium;
use App\Models\User;
use App\Models\User\UserPremium;

class PremiumRepository extends AbstractRepository
{
	/** @var  Premium | null*/
	protected $model;

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return Premium::class;
	}

	public function getDefaultPremium(): Premium
	{
		return Premium::whereNull('type')->firstOrFail();
	}

	public function isExistsByType($type): bool
	{
		return Premium::where('type', $type)->exists();
	}

}
