<?php declare(strict_types=1);

namespace App\Repositories;

use App\DataContainers\Platform\Payment\PaymentSearchContainer;
use App\Models\Payment;
use App\Models\User\UserPremium;
use Illuminate\Support\Collection;

class PaymentRepository extends AbstractRepository
{

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return Payment::class;
	}

	public function findManyByContainer(PaymentSearchContainer $container): Collection
	{
		$this->applySearchFilter($container);
		return $this->get();
	}

	public function findOneByContainer(PaymentSearchContainer $container): Collection
	{
		$this->applySearchFilter($container);
		return $this->first();
	}

	protected function applySearchFilter(PaymentSearchContainer $container)
	{
		$this->model = $this->model;
		if ($morphedClass = $container->getMorph()) {
			$this->model->whereHasMorph('paymentable', get_class($morphedClass), function ($q) use ($morphedClass) {
				return $q->whereKey($morphedClass->getKey());
			});
		}
		return $this->model;
	}


}
