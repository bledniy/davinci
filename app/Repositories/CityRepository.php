<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\SortNameCriteria;
use App\Models\City\City;
use App\Models\City\CityLang;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Exceptions\RepositoryException;


class CityRepository extends AbstractRepository
{
	public function model()
	{
		return City::class;
	}

	public function modelLang()
	{
		return CityLang::class;
	}

	public function addPublicCriteriaToQuery(): self
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class));
		return $this;
	}

	public function addAdminCriteriaToQuery()
	{
		$this->pushCriteria($this->app->make(SortNameCriteria::class));
		return $this;
	}


	/**
	 * @return LengthAwarePaginator|Collection|array<City>
	 * @throws RepositoryException
	 */
	public function getListAdmin()
	{
		$this->addAdminCriteriaToQuery();
		$this->joinLang($this);
		return $this->paginate();
	}

	public function getListSelect()
	{
		$this->addAdminCriteriaToQuery();
		return $this->get();
	}

	public function getListPublic()
	{
		return Cache::get('api.cities.'.getCurrentLocaleCode(), function () {
			$this->addPublicCriteriaToQuery();
			$this->with('lang');
			$cities = $this->get()->keyBy('id');
			Cache::set('api.cities.'.getCurrentLocaleCode(), $cities);
			return $cities;
		});
	}

}

