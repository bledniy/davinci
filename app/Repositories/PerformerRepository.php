<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\User\PerformerActiveAccountCriteria;
use App\Criteria\User\PerformerFilterCriteria;
use App\DataContainers\Platform\User\SearchPerformerDataContainer;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;


class PerformerRepository extends UserRepository
{


	public function addPublicCriteriaToQuery(): PerformerRepository
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class));

		return $this;
	}

	public function performersQueryPaginate(SearchPerformerDataContainer $container)
	{
		$repo = $this->app->make(self::class);
		$repo->addPublicCriteriaToQuery();
		$select = ['id', 'name', 'surname', 'patronymic', 'avatar', 'about', 'rating', 'last_seen_at', 'slug', 'city_id', 'date_birth', 'reviews_count'];
		$repo->select($select);
		if ($container) {
			$repo->pushCriteria($this->app->make(PerformerFilterCriteria::class, ['container' => $container]));
		}
		return $repo;
	}

	public function getPerformersPaginated(SearchPerformerDataContainer $container): LengthAwarePaginator
	{
		$perPage = $container->getOnPage();
		$page = $container->getPage();
		$cacheKey = 'performers-paginate.' . md5(serialize($container ? $container->toKey() : []));
		$items = $this->performersQueryPaginate($container)
			->forPage($page, $perPage)
			->orderByDesc('rating')
			->orderByDesc('last_seen_at')
			->get()
		;

		$total = Cache::get($cacheKey, function () use ($cacheKey, $container) {
			$q = $this->performersQueryPaginate($container);
			$count = $q->count();
			Cache::set($cacheKey, $count, now()->addMinutes(10));
			return $count ?? 0;
		});

		return new LengthAwarePaginator($items, $total, $perPage, $page);
	}

	public function findForShow($slug): ?User
	{
		$this->addPublicCriteriaToQuery();
		return $this->where('slug', $slug)->first();
	}
}
