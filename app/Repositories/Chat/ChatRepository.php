<?php

namespace App\Repositories\Chat;

use App\Contracts\Models\ModelHasChat;
use App\DataContainers\Platform\Chat\ChatQueryContainer;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Models\Chat\ChatUser;
use App\Models\User;
use App\Platform\Contract\Chat\FavoritesChatContract;
use App\Repositories\AbstractRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;

class ChatRepository extends AbstractRepository
{
	public function model()
	{
		return Chat::class;
	}

	public function getChatUsers(Chat $chat, ?User $exceptUser = null)
	{
		$q = $chat->users();
		if ($exceptUser) {
			$q->where('user_id', '!=', $exceptUser->getKey());
		}

		return $q->get();
	}

	public function createForRelated(ModelHasChat $model, array $attributes = []): Chat
	{
		/** @var  $chat Chat */
		$chat = $model->chat()->create($attributes);

		return $chat;
	}

	public function getForRelated(ModelHasChat $model): Collection
	{
		return $model->chat()->get();
	}

	public function getCompanion(Chat $chat, User $user)
	{
		return $chat->users()->where('user_id', '!=', $user->getKey())->first();
	}

	protected function chatable(ModelHasChat $model)
	{
		$this->model = $model->chat();
	}

	public function show($chatId, User $user): ?Chat
	{
		/** @var  $model Chat */
		$model = app($this->model());
		$this->withCount(['unreadMessages' => function (Builder $q) use ($user) {
			$q->where($user->getForeignKey(), '!=', $user->getKey());
		},
		]);
		$this->model = $this->model->where($model->getKeyName(), $chatId);
		$this->join($model->users()->getTable(), function (JoinClause $q) use ($user) {
			return $q->on('chat_id', 'id')->on($user->getForeignKey(), '=', \DB::raw($user->getKey()));
		});
		$this->joinFavorites($model, $user);
		$this->model->addSelect('chat_favorites.user_id as is_chat_favorite');

		return $this->first();
	}

	public function addUsersToChat(Chat $chat, array $userIds): void
	{
		$chat->users()->attach($userIds);
	}

	public function isUserInChat(User $user, $chatId)
	{
		$model = app($this->model());
		$this->model = $this->model->join($model->users()->getTable(), function (JoinClause $q) use ($user) {
			return $q->on('chat_id', 'id')->on($user->getForeignKey(), '=', \DB::raw($user->getKey()));
		})
			->where('id', $chatId);

		return $this->count();
	}

	public function getChatByUser(ModelHasChat $order, User $user): ?Chat
	{
		/** @var  $model Chat */
		$this->chatable($order);
		$model = app($this->model());
		$this->select($model->getTable() . '.*');
		$this->model->rightJoin($model->users()->getTable(), function (JoinClause $q) use ($user) {
			return $q->on('chat_id', 'id')->on($user->getForeignKey(), '=', \DB::raw($user->getKey()));
		});

		return $this->first();
	}

	public function getCountUnreadMessages(User $user): int
	{
		$chatKey = (new Chat())->getForeignKey();
		$userKey = (new User())->getForeignKey();

		return ChatMessage::query()->where('is_watched', 0)
			->where($userKey, '!=', $user->getKey())
			->whereIn($chatKey, function (\Illuminate\Database\Query\Builder $query) use ($user, $chatKey, $userKey) {
				return $query->select($chatKey)
					->from((new ChatUser())->getTable())
					->where($userKey, $user->getKey())
				;
			})
			->count('id')
		;
	}

	public function getList(ChatQueryContainer $container)
	{
		/** @var $model Chat */
		$user = $container->getUser();
		$this->model = $this->model->newModelQuery();
		$this->applyChatQueryContainer($container);
		$this->withCount(['unreadMessages' => function (Builder $q) use ($user) {
			$q->where($user->getForeignKey(), '!=', $user->getKey());
		},
		]);
		$this->model->addSelect('chat_favorites.user_id as is_chat_favorite');
		$this->orderByDesc('chats.updated_at');

		return $this->get();
	}

	protected function applyChatQueryContainer(ChatQueryContainer $container): self
	{
		$user = $container->getUser();
		/** @var  $model Chat */
		$model = app($this->model());
		$this->whereHas('users', function ($q) use ($user, $container) {
			if ($container->getSearch()) {
				$q
					->where('name', 'like', "%" . $container->getSearch() . "%")
					->orWhere('surname', 'like', "%" . $container->getSearch() . "%")
				;

				return $q;
			}

			return $q->where($user->getForeignKey(), $user->getKey());
		});

		if ($container->getType()) {
			$this->where($model->getTable() . '.type', $container->getType());
		}
		if ($container->getUnread()) {
			$this->whereHas('messages', function ($q) {
				return $q->where('is_watched', 0);
			});
		}
		$this->joinFavorites($model, $user);
		if ($container->getFavorite()) {
			$this->whereNotNull(sprintf('%s.%s', $model->favorites()->getTable(), $model->favorites()->getForeignPivotKeyName()));
		}

		return $this;
	}

	public function getChatsLastMessages($chatsId)
	{
		return ChatMessage::query()->selectRaw('cm.*')->from(\DB::raw('chat_messages cm'))
			->join(\DB::raw('(SELECT MAX(id) as id FROM chat_messages GROUP BY chat_id) cm2'), function (JoinClause $clause) {
				return $clause->on('cm.id', 'cm2.id');
			})
			->whereIn('chat_id', $chatsId)
			->get()
		;
	}

	protected function joinFavorites($model, $user)
	{
		$ft = $model->favorites()->getTable();
		$this->model = $this->model->leftJoin($ft, function (JoinClause $q) use ($user, $ft) {
			return $q->on('chats.id', "$ft.chat_id")->on("$ft.user_id", \DB::raw($user->getKey()));
		});
	}

	public function isChatFavorite($chatId, User $user)
	{
		$this->model = $this->where('id', $chatId);
		$this->whereHas('favorites', function ($q) use ($user) {
			return $q->where('user_id', $user->getKey());
		});

		return $this->model->count();
	}

	public function toggleChatFavorite($chatId, User $user)
	{
		/** @var  $chatModel Chat */
		$chatModel = $this->makeModel()->setAttribute('id', $chatId);

		if ($this->isChatFavorite($chatId, $user)) {
			$chatModel->favorites()->detach($user->getKey());

			return FavoritesChatContract::SUCCESS_DETACHED;
		}
		$chatModel->favorites()->attach($user->getKey());

		return FavoritesChatContract::SUCCESS_ATTACHED;
	}

}
