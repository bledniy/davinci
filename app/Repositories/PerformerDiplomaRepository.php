<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\User\Diploma;


class PerformerDiplomaRepository extends AbstractRepository
{

	public function model()
	{
		return Diploma::class;
	}

	public function findByUser(User $user)
	{
		return $this->whereModel($user)->get();
	}

	public function findOneByUser(User $user, $id = null): ?Diploma
	{
		return $this->whereModel($user)
			->when($id, function ($q) use ($id) {
				return $q->where('id', $id);
			})->first()
			;
	}

}

