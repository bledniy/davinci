<?php

namespace App\Repositories;

use App\Models\Category\Category;
use App\Models\User;
use App\Models\User\UserCertification;
use App\Platform\Cabinet\Certification\CertificationStatuses;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class RedirectRepository.
 */
class UserCertificationRepository extends AbstractRepository
{
	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return UserCertification::class;
	}

	public function findByUser(User $user)
	{
		return $this->whereModel($user)->get();
	}

	/**
	 * @param User $user
	 * @param Category $category
	 * @return null | UserCertification
	 */
	public function findByUserCategory(User $user, Category $category)
	{
		$this->whereModel($user)->whereModel($category)->first();

		return $this->whereModel($user)->whereModel($category)->first();
	}

	public function getListAdmin(array $filters)
	{
		$this->model = $this->latest();
		if ($filters['status'] ?? false) {
			$this->where('status', $filters['status']);
		}

		return $this->paginate();
	}

	public function getUnreadCount()
	{
		return UserCertification::whereIn('status', [CertificationStatuses::STATUS_CHECK])->count();
	}

	public function getCertifiedCategories(User $user): Collection
	{
		return $user->performerCategories()
			->select('categories.*')
			->addSelect('uc.id as user_certifications_id')
			->join(DB::raw('user_certifications uc'), function (JoinClause $q) {
				$q->on(DB::raw('performer_categories.user_id'), DB::raw('uc.user_id'))
					->on(DB::raw('performer_categories.category_id'), DB::raw('uc.category_id'))
				;
			})
			->get()
		;
	}

}
