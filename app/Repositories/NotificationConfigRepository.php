<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\User\NotificationConfig;
use App\Models\User\NotificationConfigLang;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class NotificationConfigRepository extends AbstractRepository
{

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return NotificationConfig::class;
	}

	public function modelLang()
	{
		return NotificationConfigLang::class;
	}

	public function findByUser(User $user)
	{
		$this->joinLang();
		return $this->whereModel($user)->get();
	}

	public function findForIndex(User $user)
	{
		return $this->model->fromRaw('notification_configs nc')
			->leftJoin(DB::raw('notification_config_langs ncl'), function ($join) {
				return $join->on('ncl.notification_config_id', 'nc.id')->on('ncl.language_id', DB::raw(getCurrentLangId()));
			})
			->leftJoin(DB::raw('user_notification_configs unc'), function (JoinClause $join) use ($user) {
				return $join->on('id', 'unc.notification_config_id')
					->on('user_id', DB::raw($user->getKey()))
					;
			})
			->where('type', $user->getType())
			->orWhereNull('type')
			->get()
			;
	}

	public function allOfUser(User $user)
	{
		return $this->model->fromRaw('notification_configs nc')
			->leftJoin(DB::raw('user_notification_configs unc'), function (JoinClause $join) use ($user) {
				return $join->on('id', 'unc.notification_config_id')
					->on('user_id', DB::raw($user->getKey()))
					;
			})
			->get()
			;
	}

	public function syncByUser($notifications, User $user)
	{
		$user->notificationConfig()->sync($notifications);
	}

	public function getUsersIdWithEnabledTypes(array $types): array
	{
		$this->model = $this->model->newQuery();
		$this->model->select('user_id');
		$this->model->join('user_notification_configs', 'id', 'notification_config_id')
			->whereExists(function ($q) use ($types) {
				$q->select('id')->from('notification_configs')->whereIn('code', $types);
			});
		return $this->model->pluck('user_id')->toArray();

	}

}
