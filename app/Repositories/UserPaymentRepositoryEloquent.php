<?php declare(strict_types=1);

namespace App\Repositories;

use App\DataContainers\Platform\Payment\UserPaymentSearchContainerInterface;
use App\Models\User;
use App\Models\User\UserPayment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class UserPaymentRepositoryEloquent extends AbstractRepository
{
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model(): string
	{
		return UserPayment::class;
	}

	public function findUnPayedPayment(User $user): ?UserPayment
	{
		return UserPayment::where($user->getForeignKey(), $user->getKey())
			->where('is_payed', false)
			->first()
		;
	}

	/**
	 * @param UserPaymentSearchContainerInterface $container
	 * @return UserPayment|null|Model
	 */
	public function findOne(UserPaymentSearchContainerInterface $container): ?UserPayment
	{
		$q = $this->getModel()::query();
		$this->applyUserPaymentSearchContainer($q, $container);

		return $q->first();
	}

	/**
	 * @param UserPaymentSearchContainerInterface $container
	 * @return Collection|array<UserPayment>
	 */
	public function findMany(UserPaymentSearchContainerInterface $container): Collection
	{
		$q = $this->getModel()::query();
		$q->latest();
		$this->applyUserPaymentSearchContainer($q, $container);

		return $q->get();
	}

	private function applyUserPaymentSearchContainer(Builder $q, UserPaymentSearchContainerInterface $container): void
	{
		if ($container->hasUser() && $user = $container->getUser()) {
			$q->where($user->getForeignKey(), $user->getKey());
		}
		if ($container->hasIsPayed()) {
			$q->where('is_payed', $container->getIsPayed());
		}
		if ($container->hasReasons()) {
			$q->whereIn('reason', $container->getReasons());
		}
		if ($container->isWithNotNullReason()) {
			$q->whereNotNull('reason');
		}
	}

}
