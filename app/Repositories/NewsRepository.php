<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use App\Criteria\PublishedAtCriteria;
use App\DataContainers\Admin\News\SearchDataContainer;
use App\Models\News\News;
use App\Models\News\NewsLang;
use App\Traits\Repositories\RebuildNextAndPrev;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class NewsRepository.
 */
class NewsRepository extends AbstractRepository
{
	use RebuildNextAndPrev;

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return News::class;
	}

	public function modelLang()
	{
		return NewsLang::class;
	}

	public function addPublicCriteriaToQuery(): self
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class)->setTable('news'));
		$this->pushCriteria($this->app->make(PublishedAtCriteria::class)->setTable('news'));

		return $this;
	}

	public function getListForAdmin(): LengthAwarePaginator
	{
		/** @var  $list */
		$list = News::with('lang')->orderBy('published_at', 'desc')->paginate();
		return $list;
	}

	/**
	 * @param $id
	 * @return \App\Models\News\News
	 */
	public function findForEdit($id): News
	{
		return $this->with('lang')->findOrFail($id);
	}

	public function findByUrl(string $url): News
	{
		return $this->addPublicCriteriaToQuery()
			->where('url',$url)->with(['lang', 'nextNew.lang', 'nextNew.nextNew.lang', 'category.lang'])
			->firstOrFail();
	}

	public function getListPublic(SearchDataContainer $dataContainer): LengthAwarePaginator
	{
		$q = $this->addPublicCriteriaToQuery();
		$q = $this->joinLang($q);
		if ($search = $dataContainer->getSearch()) {
			$q = $q
				->whereLike('url', $search)
				->orWhereLike('name', $search)
				->orWhereLike('title', $search)
			;
		}
		if ($dataContainer->getCategories()) {
			$q = $q->whereIn('news_category_id', $dataContainer->getCategories());
		}

		/** @var  $news */
		$news = $q->orderBy('published_at', 'desc')->paginate($dataContainer->getOnPage());
		return $news;
	}

	public function getAllForNextPrev(): Collection
	{
		$this->pushCriteria($this->app->make(ActiveCriteria::class)->setTable('news'));
		return $this->orderByDesc('published_at')->get();
	}

}
