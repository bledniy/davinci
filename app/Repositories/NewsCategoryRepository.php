<?php

namespace App\Repositories;

use App\Models\News\NewsCategory;
use App\Models\News\NewsCategoryLang;

/**
 * Class NewsCategoryRepository.
 */
class NewsCategoryRepository extends AbstractRepository
{

	/**
	 * @return string
	 *  Return the model
	 */
	public function model()
	{
		return NewsCategory::class;
	}

	public function modelLang()
	{
		return NewsCategoryLang::class;
	}

	public function getListPublic()
	{
		return $this->all();
	}

	public function getListAdmin()
	{
		return $this->all();
	}


}
