<?php

namespace App\Jobs\Order\Review;

use App\Models\User;
use App\Platform\Model\UserFields;
use App\Repositories\ReviewRepository;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecountPerformerRatingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


	/**
	 * @var User
	 */
	private $user;

	public function __construct(User  $user)
	{
		$this->user = $user;
	}

	public function handle(UserRepository $userRepository, ReviewRepository $reviewRepository)
    {
		$rating = $reviewRepository->aboutPerformerQuery($this->user)->avg('rating');
		$fieldRating = 'rating';
		$userRepository->update([$fieldRating => $rating], $this->user);
	}
}
