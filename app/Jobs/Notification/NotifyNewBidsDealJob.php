<?php

namespace App\Jobs\Notification;

use App\Models\Order\Order;
use App\Notifications\Order\OrderNewBidsNotification;
use App\Platform\Chat\Services\UserPresenceOnDealChannel;
use App\Platform\User\Notification\UserNotificationConfigContainer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyNewBidsDealJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	public function handle(UserPresenceOnDealChannel $presenceService)
	{
		$customer = $this->order->getContainer()->getCustomer();
		$config = UserNotificationConfigContainer::make($customer);
//		if (!$config->isEnabledAnyFromGroupNewBids()) {
//			return;
//		}
		if ($presenceService->isUserOnDealPage($customer, $this->order->getKey())) {
			return;
		}
		$notification = new OrderNewBidsNotification($this->order);
		$config->isEnabledMailNewBids() ? $notification->enableViaMail() : $notification->disableViaMail();
		$notification->setBrowserPopup($config->isEnabledBrowserNewBids());
		$customer->notify($notification);
	}
}
