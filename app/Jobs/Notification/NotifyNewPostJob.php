<?php declare(strict_types=1);

namespace App\Jobs\Notification;

use App\Models\News\News;
use App\Models\User;
use App\Notifications\NewPostNotification;
use App\Platform\User\Notification\UserNotificationConfigContainer;
use App\Platform\User\Notification\UserNotificationGroups;
use App\Repositories\NotificationConfigRepository;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyNewPostJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * @var News
	 */
	private $news;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct(News $news)
	{
		$this->news = $news;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(NotificationConfigRepository $notificationConfigRepository, UserRepository $userRepository)
	{
		$userIds = $notificationConfigRepository->getUsersIdWithEnabledTypes(UserNotificationGroups::getTypesOfNewPost());
		$userIdsChunks = array_chunk($userIds, 1000);
		foreach ($userIdsChunks as $userIdsChunk) {
			$users = $userRepository->find($userIdsChunk);
			/** @var  $user User */
			foreach ($users as $user) {
				$configContainer = UserNotificationConfigContainer::make($user);
//					if (!$configContainer->isEnabledAnyFromGroupNewPost()) {
//						continue;
//					}
				$notification = new NewPostNotification($this->news);
				if ($configContainer->isEnabledMailNews()) {
					$notification->enableViaMail();
				}
				$notification->setBrowserPopup($configContainer->isEnabledBrowserNews());
				$user->notify($notification);
			}
		}
	}
}
