<?php

namespace App\Jobs\Notification;

use App\Models\Order\Order;
use App\Models\User;
use App\Notifications\Order\NewDealsNotification;
use App\Notifications\Order\OrderNewBidsNotification;
use App\Platform\Chat\Services\UserPresenceOnDealChannel;
use App\Platform\User\Notification\UserNotificationConfigContainer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class NotifyNewDealsJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * @var Collection
	 */
	private $deals;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(Collection $deals, User $user)
	{
		$this->deals = $deals;
		$this->user = $user;
	}

	public function handle()
	{
		$config = UserNotificationConfigContainer::make($this->user);
		$notification = new NewDealsNotification($this->deals);
		$config->isEnabledMailNewDeals() ? $notification->enableViaMail() : $notification->disableViaMail();
		$notification->setBrowserPopup($config->isEnabledBrowserNewDeals());
		$this->user->notify($notification);
	}
}
