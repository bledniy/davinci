<?php


namespace App\Widgets;


use App\Helpers\Debug\LoggerHelper;
use App\Services\SMS\SMSSenderService;
use Illuminate\Support\Facades\Cache;

class SmsBalanceWidget extends AbstractWidget
{
	/**
	 * @var SMSSenderService
	 */
	private $senderService;

	public function __construct(SMSSenderService $senderService)
	{
		parent::__construct();
		$this->senderService = $senderService;
	}

	public function renderWidget()
	{
		$cacheKey = 'services.sms.balance';
		$warnSum = 100;
		try{
			$balance = Cache::get($cacheKey, function () use ($cacheKey) {
				$value = $this->senderService->getBalance();
				Cache::set($cacheKey, $value, now()->addMinute());
				return $value;
			});
		} catch (\Throwable $e){
		    app(LoggerHelper::class)->error($e);
		}
		$with = compact(array_keys(get_defined_vars()));
		return view('admin.widgets.sms-balance')->with($with);
	}
}