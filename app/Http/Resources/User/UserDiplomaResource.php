<?php

namespace App\Http\Resources\User;

use App\Models\User\Diploma;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDiplomaResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model Diploma */
		$model = $this->resource;

		return [
			'id'         => $model->getKey(),
			'name'       => $model->name,
			'image'      => getApiImageUrl(imgPathOriginal($model->image)),
			'thumbnail'  => getApiImageUrl($model->image),
			'created_at' => (string)$model->getCreatedAt(),
			'updated_at' => (string)$model->getUpdatedAt(),
		];
	}
}
