<?php

namespace App\Http\Resources\User\Platform;

use App\Models\User;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Model\UserFields;

class CustomerResource extends CollectiveUserResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $user User */
		$user = $this->resource;
		$this->addMainInfo(['about' => $user->getAttribute(UserFields::get($user->getType(), 'about')), 'type' => UserTypeContract::TYPE_CUSTOMER]);
		$this->addToProfile([
			'rating'        => $user->getCustomerRating(),
			'avatar'        => $user->getAvatarHelper()->getCustomerAvatar(),
			'reviews_count' => $user->getCustomerCountReviews(),
		]);

		return parent::toArray($request);

	}
}
