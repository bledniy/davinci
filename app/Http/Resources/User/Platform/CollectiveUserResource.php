<?php

namespace App\Http\Resources\User\Platform;

use App\Http\Resources\AbstractJsonResource;
use App\Http\Resources\CityResource;
use App\Models\User;

abstract class CollectiveUserResource extends AbstractJsonResource
{
    protected $userMain = [];
    protected $profile = [];

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var  $user User */
        $user = $this->resource;
        $data = [
            'id' => $user->getKey(),
            'name' => $user->getName(),
            'surname' => $user->getSurname(),
            'patronymic' => $user->getPatronymic(),
            'full_name' => $user->getSurname() . ' ' . $user->getName(),
            'type' => $user->getType(),
        ];
        $data = array_merge($data, $this->userMain);
        $profile = [
            'id' => $user->getKey(),
            'email' => $user->getEmail(),
            'phone' => $user->getPhone(),
            'slug' => $user->getSlug(),
            $user->language()->getForeignKeyName() => $user->getBelongsKey('language'),
            'date_birth' => isDateValid($user->date_birth) ? getDateCarbon($user->date_birth)->toDateString() : null,
            'gender' => $user->gender,
            $user->city()->getForeignKeyName() => $user->getBelongsKey('city'),
            'city' => CityResource::make($this->whenLoaded('city')),
            'last_seen_at' => (string)$user->getUserLastActivity()->getLastSeenAt(),
            'is_online' => $user->getUserLastActivity()->isOnline(),
            'created_at' => (string)$user->getCreatedAt(),
            'is_phone_confirmed' => null !== $user->phone_verified_at,
            'is_email_confirmed' => null !== $user->email_verified_at,
            'has_premium' => $user->getPremiumAccountService()->isActivePremium()
        ];
        if (null !== $user->is_current) {
            $data['is_current'] = (bool)$user->is_current;
        }
        $profile = array_merge($profile, $this->profile);
        $data['profile'] = $profile;
        return $data;
    }


    protected function addMainInfo(array $info)
    {
        $this->userMain = array_merge($this->userMain, $info);
        return $this;
    }

    protected function addToProfile(array $profile)
    {
        $this->profile = array_merge($this->profile, $profile);
        return $this;
    }
}
