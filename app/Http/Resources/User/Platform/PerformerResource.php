<?php

namespace App\Http\Resources\User\Platform;

use App\Models\User;
use App\Platform\Contract\UserTypeContract;

class PerformerResource extends CollectiveUserResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $user User */
		$user = $this->resource;
		$this->addMainInfo(['about' => $user->about, 'type' => UserTypeContract::TYPE_PERFORMER]);
		$this->addToProfile([
			'rating'        => $user->getPerformerRating(),
			'avatar'        => $user->getAvatarHelper()->getPerformerAvatar(),
			'reviews_count' => $user->getPerformerCountReviews(),
			'has_premium'   => $user->getPremiumAccountService()->isActivePremium(),
		]);

		return parent::toArray($request);

	}
}
