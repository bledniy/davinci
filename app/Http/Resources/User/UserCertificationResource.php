<?php

namespace App\Http\Resources\User;

use App\Http\Resources\CategoryResource;
use App\Models\User\UserCertification;
use App\Platform\Cabinet\Certification\CertificationAvailability;
use Illuminate\Http\Resources\Json\JsonResource;

class UserCertificationResource extends JsonResource
{

	public $preserveKeys = true;

	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model UserCertification */
		$model = $this->resource;

		return [
//			'id'  => $model->getKey(),
			'category_id'  => $model->category_id,
//			'reason'       => $model->reason,
			'status'       => $model->getStatus(),
			'status_name'  => $model->getCertificationStatuses()->getStatusName(),
			'certified_at' => $model->certified_at,
			'image'        => getApiImageUrl($model->image),
			'can_upload'   => CertificationAvailability::check($model),
			'category'     => CategoryResource::make($this->whenLoaded('category')),
		];
	}
}
