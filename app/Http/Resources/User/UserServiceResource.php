<?php

namespace App\Http\Resources\User;

use App\Models\User\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class UserServiceResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model Service */
		$model = $this->resource;

		return [
			'id'         => $model->getKey(),
			'name'       => $model->name,
			'price'      => $model->price,
			'currency'   => getCurrencyIcon(),
			'created_at' => (string)$model->getCreatedAt(),
			'updated_at' => (string)$model->getUpdatedAt(),
		];
	}
}
