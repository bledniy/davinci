<?php

namespace App\Http\Resources\User;

use App\Models\User\NotificationConfig;
use Illuminate\Http\Resources\Json\JsonResource;

class UserNotificationConfigResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model NotificationConfig */
		$model = $this->resource;

		return [
			'id'       => $model->getKey(),
			'name'     => $model->name,
			'code'     => $model->code,
			'group'    => $model->group,
			'active'   => (bool)$model->{$model->user()->getForeignPivotKeyName()},
			'children' => UserNotificationConfigResource::collection($this->whenLoaded('child')),
		];
	}
}
