<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class CategoryResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		return [
			'id' => $this->resource->id,
			'name' => $this->resource->name,
			'name_trans' => Str::slug($this->resource->name),
			'language_id' => $this->resource->language_id,
		];
	}

}
