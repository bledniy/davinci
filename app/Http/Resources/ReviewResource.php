<?php

namespace App\Http\Resources;

use App\Models\Order\Review;

class ReviewResource extends AbstractJsonResource
{

	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */

	public function toArray($request)
	{
		/** @var  $model Review */
		$model = $this->resource;
		return [
			'id'          => $model->id,
			'name'        => $model->name,
			'comment'     => $model->description,
			'thumbnail'   => getApiImageUrl($model->image),
			'image'       => getApiImageUrl(imgPathOriginal($model->image)),
			'language_id' => $model->language_id,
		];
	}

}
