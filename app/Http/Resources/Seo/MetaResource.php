<?php declare(strict_types=1);

namespace App\Http\Resources\Seo;

use App\Http\Resources\AbstractJsonResource;

class MetaResource extends AbstractJsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		return parent::toArray($this->resource);
	}
}
