<?php


namespace App\Http\Resources\Traits;


trait ResourceTrait
{
	protected $additionalData = [];

	public function additionalData(array $data)
	{
		$this->additionalData = $data;
		return $this;
	}

	public function appendAdditionalData(array $data)
	{
		$this->additionalData = array_merge($this->additionalData, $data);
		return $this;
	}

	protected function dateToStringIfNotNull($date)
	{
		if (null === $date){
			return null;
		}
		return (string)$date;
	}
}