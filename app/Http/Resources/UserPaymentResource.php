<?php declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\User\UserPayment;

class UserPaymentResource extends AbstractJsonResource
{
	/**
	 * @var UserPayment
	 */
	public $resource;

	public function toArray($request): array
	{
		return [
			'id' => $this->resource->getKey(),
			'payed' => $this->resource->getPayed(),
			'reason' => $this->resource->hasReason() ? trans($this->resource->getReasonEnum()->getTitle()) : null,
			'payDateTime' => $this->dateToStringIfNotNull($this->resource->getUpdatedAt()),
		];
	}
}
