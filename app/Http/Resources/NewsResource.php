<?php

namespace App\Http\Resources;

use App\Models\News\News;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
	public static $wrap = 'news';

	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model News */
		$model = $this->resource;
		$new = [
			'id'           => $model->id,
			'slug'         => $model->url,
			'name'         => $model->name,
			'title'        => $model->title,
			'thumbnail'    => getApiImageUrl($model->image),
			'image'        => getApiImageUrl(imgPathOriginal($model->image)),
			'description'  => $model->description,
			'excerpt'      => $model->excerpt,
			'published_at' => $model->published_at,
			'language_id'  => $model->language_id,
			'category'     => NewsCategoryResource::make($model->category),
			'next'         => [],
		];
		if ($model->relationLoaded('nextNew') && $model->getNext()) {
			$new['next'] = [$model->getNext(),];
			if ($model->getNext()->relationLoaded('nextNew') && $model->getNext()->getNext()) {
				$new['next'][] = $model->getNext()->getNext();
			}
			$new['next'] = array_map(function (News $news) {
				return self::make($news->withoutRelations());
			}, $new['next']);
		}
		return $new;
	}

}
