<?php

namespace App\Http\Resources;

use App\Models\User\UserPremium;

class UserPremiumResource extends AbstractJsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model UserPremium */
		$model = $this->resource;
		return [
			'expires_at' => $this->dateToStringIfNotNull($model->getExpiresAt()),
			'premium'    => PremiumResource::make($this->whenLoaded('premium')),
		];
	}
}
