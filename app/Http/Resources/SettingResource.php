<?php declare(strict_types=1);

namespace App\Http\Resources;

use App\DataContainers\Platform\ApiSettingDataContainer;
use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
	public $preserveKeys = true;

	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model ApiSettingDataContainer */
		$model = $this->resource;

		return [
			'key' => $model->getKey(),
			'name' => $model->getName(),
			'value' => $model->getValue(),
		];
	}

}
