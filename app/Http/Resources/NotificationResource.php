<?php

	namespace App\Http\Resources;

	use \App\Http\Resources\AbstractJsonResource;
	use Illuminate\Notifications\Notification;

	class NotificationResource extends AbstractJsonResource
	{
		/**
		 * Transform the resource into an array.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @return array
		 */
		public function toArray($request)
		{
			/** @var  $resource \Illuminate\Notifications\DatabaseNotification */
			$resource = $this->resource;
			return [
				'id'         => $resource->getKey(),
				'data'       => $resource->data,
				'is_read'    => null !== $resource->read_at,
				'read_at'    => $this->dateToStringIfNotNull($resource->read_at),
				'created_at' => $this->dateToStringIfNotNull($resource->created_at),
			];
		}
	}
