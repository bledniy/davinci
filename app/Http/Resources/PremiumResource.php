<?php

namespace App\Http\Resources;

use App\Models\Premium\Premium;

class PremiumResource extends AbstractJsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model Premium */
		$model = $this->resource;
		return [
//			'name'  => $model->name,
			'price' => $model->getPrice(),
//			'type'  => $model->getType(),
		];
	}
}
