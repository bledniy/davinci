<?php

namespace App\Http\Resources;

use App\Models\Content\Content;
use Illuminate\Http\Resources\Json\JsonResource;

class NeedResource extends JsonResource
{

	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model Content */
		$model = $this->resource;
		return [
			'id'          => $model->id,
			'name'        => $model->name,
			'description' => $model->description,
			'language_id' => $model->language_id,
		];
	}

}
