<?php

namespace App\Http\Resources;

use App\Models\Faq\Faq;
use Illuminate\Http\Resources\Json\JsonResource;

class FaqResource extends JsonResource
{

	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model Faq */
		$model = $this->resource;
		return [
			'id'          => $model->id,
			'question'    => $model->getQuestion(),
			'answer'      => $model->getAnswer(),
			'language_id' => $model->language_id,
		];
	}

}
