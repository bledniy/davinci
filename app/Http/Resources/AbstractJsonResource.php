<?php

namespace App\Http\Resources;

use App\Http\Resources\Traits\ResourceTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class AbstractJsonResource extends JsonResource
{

	protected static $callbacks = [];

	use ResourceTrait;

	public static function addCallback(callable $func)
	{
		self::$callbacks[] = $func;
	}

	public static function getCallbacks(): array
	{
		return self::$callbacks;
	}
}