<?php

namespace App\Http\Resources\Chat;

use \App\Http\Resources\AbstractJsonResource;
use App\Http\Resources\User\Platform\CustomerResource;
use App\Http\Resources\User\Platform\PerformerResource;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Models\User;
use App\Platform\User\Type\UserTypeHelper;
use Illuminate\Pagination\LengthAwarePaginator;

class ChatResource extends AbstractJsonResource
{
	public static $user;

	public function toArray($request)
	{
		/** @var  $model Chat */
		$model = $this->resource;

		$data = [
			'id'                    => $model->getKey(),
			'type'                  => $model->getType(),
			'order_id'              => $model->chatable()->getParentKey(),
			'created_at'            => $this->dateToStringIfNotNull($model->getCreatedAt()),
			'updated_at'            => $this->dateToStringIfNotNull($model->getUpdatedAt()),
			'messages'              => ['data' => []],
			'unread_messages_count' => (int)$model->unread_messages_count,
			'is_favorite'           => (bool)$model->is_chat_favorite,
			'user'                  => null,
			'name'                  => (string)$model->name,
		];
		if ($model->relationLoaded('messages')) {
			$messagesRes = ChatMessageResource::collection($this->whenLoaded('messages'));
			$data['messages']['data'] = $messagesRes;
			if ($model->messages instanceof LengthAwarePaginator) {
				$data['messages']['meta'] = [
					'has_more_pages' => $model->messages->hasMorePages(),
					'next_page_url'  => $model->messages->nextPageUrl(),
				];
			}
		}
		if (($lastMessage = $this->whenLoaded('lastMessage')) instanceof ChatMessage) {
			$data['last_message'] = ChatMessageResource::make($lastMessage);
		}
		if ($model->relationLoaded('userTo') && $model->userTo && $this->getUser()) {
			$data['user'] = UserTypeHelper::isPerformer($this->getUser()->getType())
				? CustomerResource::make($model->userTo)
				: PerformerResource::make($model->userTo);
		}
		return $data;
	}

	protected function getUser(): ?User
	{
		return self::$user;
	}

	public static function setUser(User $user)
	{
		self::$user = $user;
	}

}
