<?php

namespace App\Http\Resources\Chat;

use App\Http\Resources\AbstractJsonResource;
use App\Models\Chat\ChatMessage;
use App\Platform\Contract\Chat\ChatMessageTypesContract;

class ChatMessageResource extends AbstractJsonResource
{

	public $preserveKeys = true;
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model ChatMessage */
		$model = $this->resource;
		$message =  [
			'id'         => $model->getKey(),
			'message'    => $model->getMessage(),
			'user_id'    => (int)$model->user()->getParentKey(),
			'chat_id'    => (int)$model->chat_id,
			'type'       => $model->getType(),
			'is_watched' => $model->isWatched(),
			'created_at' => $this->dateToStringIfNotNull($model->getCreatedAt()),
			'updated_at' => $this->dateToStringIfNotNull($model->getUpdatedAt()),
		];
		if ($model->getType() === ChatMessageTypesContract::TYPE_FILE) {
		    $message['message']['file'] = getApiFileUrl($message['message']['file']);
		}
		return $message;
	}
}
