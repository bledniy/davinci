<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\AbstractJsonResource;
use App\Http\Resources\User\Platform\CustomerResource;
use App\Http\Resources\User\Platform\PerformerResource;
use App\Models\Order\OrderBid;
use App\Models\Order\Review;
use App\Platform\Cabinet\Factory\UserResourceFactory;

class OrderReviewResource extends AbstractJsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model Review */
		$model = $this->resource;
		$data = [
			'comment'            => $model->getContainer()->getComment(),
			'adequate_rating'    => $model->getContainer()->getSubRatingAdequate(),
			'legibility_rating'  => $model->getContainer()->getSubRatingLegibility(),
			'sociability_rating' => $model->getContainer()->getSubRatingSociability(),
			'conflict_rating'    => $model->getContainer()->getSubRatingConflict(),
			'recommend_rating'   => $model->getContainer()->getSubRatingRecommend(),
			'rating'             => $model->getContainer()->getRating(),
			'is_published'       => $model->getContainer()->isPublished(),
			'created_at'         => $this->dateToStringIfNotNull($model->getCreatedAt()),
			'deal'               => OrderResource::make($this->whenLoaded('order')),
		];
		if ($owner = $this->whenLoaded('reviewFrom')) {
			$data['owner'] = $model->getContainer()->isAboutPerformer() ? CustomerResource::make($owner) : PerformerResource::make($owner);
		}

		$data = array_merge($data, $this->additionalData);
		return $data;
	}
}
