<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\AbstractJsonResource;
use App\Http\Resources\User\Platform\PerformerResource;
use App\Models\Order\OrderBid;

class OrderBidResource extends AbstractJsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model OrderBid */
		$model = $this->resource;
		$data = [
			'id'                => $model->getKey(),
			'performer'         => PerformerResource::make($model->getUser()),
			'is_active'         => $model->getContainer()->isEnabled(),
			'is_selected'       => (bool)$model->getAttribute('is_selected'),
			'text'              => $model->getContainer()->getText(),
			'bid'               => $model->getContainer()->getBid(),
			'deadline'          => $model->getContainer()->getDeadline(),
			'bid_with_currency' => $model->getContainer()->getBidWithCurrency(),
			'modified_at'       => $this->dateToStringIfNotNull($model->getContainer()->getDateEdit()),
			'canceled_at'       => $this->dateToStringIfNotNull($model->getContainer()->getDateCancel()),
			'created_at'        => $this->dateToStringIfNotNull($model->getContainer()->getDateCreate()),
			'permissions'       => (array)$model->getAttribute('permissions'),
			'available_edits'   => 0,
		];
		if ($data['permissions']['is_owner'] ?? false) {
			$data['available_edits'] = $model->getContainer()->getLimitEdit();
		}
		$data = array_merge($data, $this->additionalData);
		foreach (self::getCallbacks() as $callback) {
			$data = $callback($data, $model);
		}
		return $data;
	}
}
