<?php

	namespace App\Http\Resources\Order;

	use App\Models\Order\OrderUpload;
	use Illuminate\Http\Resources\Json\JsonResource;

	class OrderUploadResource extends JsonResource
	{
		/**
		 * Transform the resource into an array.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @return array
		 */
		public function toArray($request)
		{
			/** @var  $model OrderUpload */
			$model = $this->resource;
			$data = [
				'id'            => $model->getKey(),
				'file'          => getStorageFilePath($model->getPath(), 'api'),
				'original_name' => $model->getOriginalName(),
			];
			return $data;
		}
	}
