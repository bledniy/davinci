<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\AbstractJsonResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\User\Platform\CustomerResource;
use App\Http\Resources\User\Platform\PerformerResource;
use App\Models\Order\Order;

class OrderResource extends AbstractJsonResource
{

	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $order Order */
		$order = $this->resource;
		$data = [
			'id'                  => $order->getKey(),
			'title'               => $order->getTitle(),
			'description'         => $order->getAttribute('description'),
			'price'               => $order->getPrice(),
			'views'               => $order->getAttribute('views'),
			'slug'                => $order->getAttribute('slug'),
			'can_online'          => (bool)$order->getAttribute('can_online'),
			'bids_count'          => (int)$order->getAttribute('bids_count'),
			'date_completion'     => (string)$order->getAttribute('date_completion'),
			'is_closed'           => (bool)$order->getAttribute('is_closed'),
			'is_active'           => $order->getContainer()->isActive(),
			'closes_at'           => (string)$order->getAttribute('closes_at'),
			'published_at'        => (string)$order->getAttribute('published_at'),
			'price_with_currency' => $order->getContainer()->getPriceWithCurrency(),
			'city'                => CityResource::make($this->whenLoaded('city')),
			'category'            => CategoryResource::make($this->whenLoaded('category')),
			'customer'            => CustomerResource::make($this->whenLoaded('customer')),
			'performer'           => PerformerResource::make($this->whenLoaded('performer')),
			'uploads'             => OrderUploadResource::collection($this->whenLoaded('uploads')),
			'has_performer'       => $order->getContainer()->hasPerformer(),
			'status'              => [
				'key'  => $order->getContainer()->getStatusKey(),
				'name' => $order->getContainer()->getStatusName(),
			],
		];
		if ($this->additionalData['permissions']['is_owner'] ?? false) {
			$data['available_edits'] = $order->getContainer()->getLimitEdit();
		}
		$data = array_merge($data, $this->additionalData);
		foreach (self::getCallbacks() as $callback) {
			$data = $callback($data, $order);
		}

		return $data;
	}


}
