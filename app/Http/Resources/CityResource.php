<?php declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\City\City;
use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
	/**
	 * @var City
	 */
	public $resource;

	public static $wrap = false;

	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		return [
			'id' => $this->resource->id,
			'name' => $this->resource->name,
			'name_trans' => $this->resource->url,
		];
	}

}
