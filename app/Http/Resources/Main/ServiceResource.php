<?php

namespace App\Http\Resources\Main;

use App\Models\Content\Content;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		/** @var  $model Content */
		$model = $this->resource;
		return [
			'id'          => $model->id,
			'name'        => $model->name,
			'link_title'  => $model->title,
			'description' => $model->description,
			'type'        => $model->content_type,
			'thumbnail'   => getApiImageUrl($model->image),
			'image'       => getApiImageUrl(imgPathOriginal($model->image)),
			'language_id' => $model->language_id,
		];
	}

}
