<?php

namespace App\Http\Controllers;

use App\Http\Resources\Chat\ChatResource;
use App\Models\Chat\Chat;
use App\Models\Order\Order;
use Illuminate\Http\Request;

class HomeController extends SiteController
{
	public function index(Request $request)
	{
		if ($request->getHost() === 'admin.dealok.comnd-x.com') {
			return redirect('/admin/login');
		}
		if (isSuperAdmin()) {
		    return nl2br(str_repeat(PHP_EOL, 30));
		}
		return '';
	}
}
