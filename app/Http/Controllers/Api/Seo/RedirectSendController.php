<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Seo;

use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Resources\Seo\RedirectSend;
use App\Repositories\RedirectRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function abort;

class RedirectSendController extends AbstractApiController
{
	public function index(RedirectRepository $redirectRepository, Request $request)
	{
		if (!$url = $request->get('url')) {
			abort(Response::HTTP_NOT_FOUND);
		}
		$url = parse_url(getNonLocaledUrl($url), PHP_URL_PATH);
		$redirect = $redirectRepository->findRedirectByUrl($url);
		if (!$redirect) {
			abort(Response::HTTP_NOT_FOUND);
		}

		return RedirectSend::make($redirect);
	}
}
