<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Seo;

use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Resources\Seo\MetaResource;
use App\Repositories\MetaRepository;
use App\Services\Seo\SeoUrlRewriterInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use function abort;

class SendMetaController extends AbstractApiController
{
	/**
	 * @var SeoUrlRewriterInterface
	 */
	private $requestUrlRewriter;

	public function __construct(SeoUrlRewriterInterface $requestUrlRewriter)
	{
		parent::__construct();
		$this->requestUrlRewriter = $requestUrlRewriter;
	}

	public function index(MetaRepository $metaRepository, Request $request): MetaResource
	{
		if (!$url = $request->get('url')) {
			abort(Response::HTTP_NOT_FOUND);
		}

		if ($this->requestUrlRewriter->supportsRewrite($request->getQueryString())) {
			$url = $this->requestUrlRewriter->rewrite($request->getQueryString());
		} else {
			$url = parse_url(getNonLocaledUrl($url), PHP_URL_PATH);
		}

		$meta = $metaRepository->findOneByUrl($url);
		if (!$meta) {
			abort(Response::HTTP_NOT_FOUND);
		}

		return MetaResource::make($meta);
	}
}
