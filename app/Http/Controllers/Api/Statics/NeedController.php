<?php

namespace App\Http\Controllers\Api\Statics;

use App\Http\Controllers\Controller;
use App\Http\Resources\NeedResource;
use App\Repositories\ContentRepository;

class NeedController extends Controller
{
	/**
	 * @var ContentRepository
	 */
	private $repository;

	public function __construct(ContentRepository $repository)
	{
		$this->repository = $repository;
	}

	public function index()
	{
		$teams = $this->repository->getListPublicByType('need');
		$teams->load('lang');
		$res = NeedResource::collection($teams);
		$with = [
			'section' => [
				'title' => getTranslate('for-performer.need.title', 'Вам понадобится'),
			],
		];
		$res->additional($with);

		return $res;
	}

}
