<?php

namespace App\Http\Controllers\Api\Statics;

use App\Http\Controllers\Controller;
use App\Http\Resources\HowResource;
use App\Repositories\ContentRepository;
use App\Services\ApiData\HowItWorksApiData;
use App\Services\ApiData\PerformerHowItWorksApiData;

class HowController extends Controller
{
	public function index(HowItWorksApiData $howItWorks)
	{
		return $howItWorks->getResponse();
	}

	public function performer(PerformerHowItWorksApiData $apiData)
	{
		return $apiData->getResponse();
	}
}
