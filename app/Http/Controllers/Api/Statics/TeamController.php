<?php

namespace App\Http\Controllers\Api\Statics;

use App\Http\Controllers\Controller;
use App\Http\Resources\TeamResource;
use App\Repositories\ContentRepository;

class TeamController extends Controller
{
	/**
	 * @var ContentRepository
	 */
	private $contentRepository;

	public function __construct(ContentRepository $repository)
	{
		$this->contentRepository = $repository;
	}

	public function index()
	{
		$teams = $this->contentRepository->getListPublicByType('team');
		$teams->load('lang');
		$res = TeamResource::collection($teams);
		$with = [
			'section' => [
				'title'       => getTranslate('team.title', 'Наша команда'),
			],
		];
		$res->additional($with);

		return $res;
	}

}
