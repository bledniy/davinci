<?php

	namespace App\Http\Controllers\Api;

	use App\DataContainers\Admin\News\SearchDataContainer;
	use App\Http\Controllers\Controller;
	use App\Http\Resources\NewsResource;
	use App\Repositories\NewsRepository;
	use Illuminate\Http\Request;

	class NewsController extends Controller
	{
		/**
		 * @var NewsRepository
		 */
		private $repository;

		public function __construct(NewsRepository $repository)
		{
			$this->repository = $repository;
		}

		public function index(Request $request, SearchDataContainer $dataContainer)
		{
			$dataContainer->fillFromRequest($request);
			$news = $this->repository->getListPublic($dataContainer);
			$news->load('category.lang');
			return NewsResource::collection($news);
		}

		public function show($url)
		{
			$new = $this->repository->findByUrl($url);
			return NewsResource::make($new);
		}
	}
