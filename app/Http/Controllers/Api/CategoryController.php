<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Repositories\CategoryRepository;

class CategoryController extends Controller
{
	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;

	public function __construct(CategoryRepository $categoryRepository)
	{
		$this->categoryRepository = $categoryRepository;
	}

	public function index()
	{
		$categories = $this->categoryRepository->getListPublic();

		return CategoryResource::collection($categories);
	}
}
