<?php


namespace App\Http\Controllers\Api\Platform;


use App\DataContainers\Platform\Order\SearchCabinetOrdersContainer;
use App\DataContainers\Platform\User\SearchPerformerDataContainer;
use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Order\OrderReviewResource;
use App\Http\Resources\User\Platform\PerformerResource;
use App\Http\Resources\User\UserDiplomaResource;
use App\Http\Resources\User\UserServiceResource;
use App\Models\User;
use App\Repositories\CityRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\PerformerDiplomaRepository;
use App\Repositories\PerformerRepository;
use App\Repositories\PerformerServiceRepository;
use App\Repositories\ReviewRepository;
use Illuminate\Http\Request;

class PerformersController extends AbstractApiController
{

	/**
	 * @var PerformerRepository
	 */
	private $userRepository;
	/**
	 * @var PerformerServiceRepository
	 */
	private $serviceRepository;
	/**
	 * @var PerformerDiplomaRepository
	 */
	private $diplomaRepository;
	/**
	 * @var CityRepository
	 */
	private $cityRepository;
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(
		PerformerRepository $userRepository,
		PerformerServiceRepository $skillsRepository,
		PerformerDiplomaRepository $portfoliosRepository,
		CityRepository $cityRepository,
		OrderRepository $orderRepository,
		ReviewRepository $reviewRepository
	)
	{
		parent::__construct();
		$this->userRepository = $userRepository;
		$this->serviceRepository = $skillsRepository;
		$this->diplomaRepository = $portfoliosRepository;
		$this->cityRepository = $cityRepository;
		$this->orderRepository = $orderRepository;
		$this->reviewRepository = $reviewRepository;
	}

	public function index(Request $request, SearchPerformerDataContainer $container)
	{
		$container->fillFromRequest($request);
		$performers = $this->userRepository->getPerformersPaginated($container);
		$cities = $this->cityRepository->getListPublic();
		$performers->getCollection()->map(function (User $user) use ($cities) {
			$user->setRelation('city', $cities->get($user->getBelongsKey('city')));
			return $user;
		})
		;
		return PerformerResource::collection($performers);
	}

	public function show($slug)
	{
		$user = $this->userRepository->findForShow($slug);
		if (!$user) {
			abort(404);
		}

		$res = [];
		$res['performer'] = PerformerResource::make($user);

		$skills = $this->serviceRepository->findByUser($user);
		$res['services']['data'] = UserServiceResource::collection($skills);
		$res['services']['section']['title'] = getTranslate('performers.services', 'Услуги');

		$portfolios = $this->diplomaRepository->findByUser($user);
		$res['diploma']['data'] = UserDiplomaResource::collection($portfolios);
		$res['diploma']['section']['title'] = getTranslate('performers.diplomas', 'Сертификаты и дипломы');

		$container = app(SearchCabinetOrdersContainer::class)->setWhereClosed(false)->setPerformerId($user->getKey());
		$deals = $this->orderRepository->getOrdersCabinet($container);
		$this->orderRepository->loadForDeals($deals, ['uploads']);
		$res['active_deals'] = OrderResource::collection($deals);

		$reviews = $this->reviewRepository->getAboutPerformerPaginated($user);
		$reviews->load('order', 'reviewFrom');
		$res['reviews']['data'] = OrderReviewResource::collection($reviews);
		$res['reviews']['meta']['total'] = $reviews->total();

		return ['data' => $res];
	}

	public function allReviews($userId)
	{
		$user = $this->userRepository->find($userId);
		if (!$user) {
			abort(404);
		}

		$reviews = $this->reviewRepository->getAboutPerformer($user);
		$reviews->load('order', 'reviewFrom');
		$res = OrderReviewResource::collection($reviews);
		$res->additional(['user' => PerformerResource::make($user)]);
		return $res;
	}

}