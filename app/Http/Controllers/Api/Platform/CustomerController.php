<?php


namespace App\Http\Controllers\Api\Platform;


use App\DataContainers\Platform\Order\SearchCabinetOrdersContainer;
use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Order\OrderReviewResource;
use App\Http\Resources\User\Platform\CustomerResource;
use App\Http\Resources\User\Platform\PerformerResource;
use App\Repositories\Order\OrderRepository;
use App\Repositories\PerformerRepository;
use App\Repositories\ReviewRepository;

class CustomerController extends AbstractApiController
{

	/**
	 * @var PerformerRepository
	 */
	private $userRepository;
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	public function __construct(PerformerRepository $userRepository, ReviewRepository $reviewRepository, OrderRepository $orderRepository)
	{
		parent::__construct();
		$this->userRepository = $userRepository;
		$this->reviewRepository = $reviewRepository;
		$this->orderRepository = $orderRepository;
	}

	public function show($slug)
	{
		$user = $this->userRepository->findForShow($slug);
		if (!$user) {
			abort(404);
		}
		$user->load('city');
		$data = [
			'customer' => CustomerResource::make($user),
		];

		$container = app(SearchCabinetOrdersContainer::class)->setWhereClosed(false)->setCustomerId($user->getKey());
		$deals = $this->orderRepository->getOrdersCabinet($container);
		$this->orderRepository->loadForDeals($deals, ['uploads']);
		$data['active_deals'] = OrderResource::collection($deals);

		$reviews = $this->reviewRepository->getAboutCustomerPaginated($user);
		$reviews->load('order', 'reviewFrom');
		$data['reviews']['data'] = OrderReviewResource::collection($reviews);
		$data['reviews']['meta']['total'] = $reviews->total();

		return ['data' => $data];
	}

	public function allReviews($userId)
	{
		$user = $this->userRepository->find($userId);
		if (!$user) {
			abort(404);
		}

		$reviews = $this->reviewRepository->getAboutCustomer($user);
		$reviews->load('order', 'reviewFrom');
		$res = OrderReviewResource::collection($reviews);
		$res->additional(['user' => CustomerResource::make($user)]);
		return $res;
	}

}