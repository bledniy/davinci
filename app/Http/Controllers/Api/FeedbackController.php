<?php declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Enum\FeedbackTypeEnum;
use App\Events\Feedback\FeedbackCreated;
use App\Http\Requests\AbstractRequest as Request;
use App\Http\Requests\Feedback\FileFeedbackRequest;
use App\Models\Feedback\Feedback;
use App\Repositories\FeedbackRepository;
use App\Uploaders\Feedback\FeedbackFilesUploader;

class FeedbackController extends AbstractApiController
{
	/**
	 * @var FeedbackRepository
	 */
	private $repository;

	public function __construct(FeedbackRepository $repository)
	{
		parent::__construct();
		$this->repository = $repository;
	}

	public function report(FileFeedbackRequest $request): array
	{
		$request->merge(['type' => new FeedbackTypeEnum(FeedbackTypeEnum::TYPE_REPORT)]);

		return $this->feedback($request);
	}

	protected function feedback(Request $request): array
	{
		$this->setFailMessage(getTranslate('feedback.send-failed'));
		$input = $request->only($request->getFillableFields('files'));

		/** @var  $feedback Feedback */
		if ($feedback = $this->repository->create($input)) {
			if ($request->hasFile('files')) {
				$uploader = app(FeedbackFilesUploader::class)->appendToPath($feedback->getKey());
				foreach ($request->file('files') as $file) {
					$paths[] = $uploader->upload($file);
				}
				$feedback->setFiles($paths ?? []);
				$this->repository->update([], $feedback);
			}
			event(app(FeedbackCreated::class, compact('feedback')));
			$this->setSuccessMessage(getTranslate('feedback.send-success', 'Заявка успешно отправлена'));
		}

		return $this->getResponseMessageForJson();
	}
}
