<?php

namespace App\Http\Controllers\Api;

use App\Enum\PageTypeEnum;
use App\Http\Controllers\Api\Statics\TeamController;
use App\Http\Resources\Main\ServiceResource;
use App\Http\Resources\Order\OrderResource;
use App\Models\Faq\FaqTypes;
use App\Models\Page\Page;
use App\Repositories\ContentRepository;
use App\Repositories\PageRepository;
use App\Services\ApiData\FaqApiData;
use App\Services\ApiData\HowItWorksApiData;
use App\Services\ApiData\LastDealsApiData;
use App\Services\ApiData\PerformerHowItWorksApiData;
use App\Services\ApiData\ReviewsApiData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PageController extends AbstractApiController
{
	/**
	 * @var PageRepository
	 */
	private $repository;
	/**
	 * @var ContentRepository
	 */
	private $contentRepository;

	public function __construct(PageRepository $repository, ContentRepository $contentRepository)
	{
		parent::__construct();
		$this->repository = $repository;
		$this->contentRepository = $contentRepository;
	}

	public function about(Request $request)
	{
		/** @var  $page Page */
		$page = $this->repository->findPageByPageType(PageTypeEnum::ABOUT);
		$this->abortIfNull($page);

		return [
			'data' => [
				'page' => [
					'name' => $page->name,
					'thumbnail' => getApiImageUrl($page->image),
					'image' => getApiImageUrl(imgPathOriginal($page->image)),
					'description' => $page->description,
				],
				'team' => [
					'data' => app(TeamController::class)->index(),
					'section' => [
						'title' => getTranslate('team.title', 'Наша команда'),
					],
				],
			],
		];
	}

	public function offer()
	{
		/** @var  $page Page */
		$page = $this->repository->findPageByPageType(PageTypeEnum::OFFER);
		$this->abortIfNull($page);

		return [
			'data' => [
				'page' => [
					'name' => $page->name,
					'description' => $page->description,
				],
			],
		];
	}

	public function privacy()
	{
		/** @var  $page Page */
		$page = $this->repository->findPageByPageType(PageTypeEnum::PRIVACY);
		$this->abortIfNull($page);

		return [
			'data' => [
				'page' => [
					'name' => $page->name,
					'description' => $page->description,
				],
			],
		];
	}

	public function main(HowItWorksApiData $howItWorks, ReviewsApiData $reviewsApiData, LastDealsApiData $lastDealsApiData)
	{
		/** @var  $page Page */
		$page = $this->repository->findPageByPageType(PageTypeEnum::MAIN);
		$this->abortIfNull($page);
		$cacheKey = 'pages:main.services.' . getCurrentLocaleCode();
		$services = Cache::get($cacheKey, function () use ($cacheKey) {
			$services = $this->contentRepository->getListPublicByType('main-services');
			Cache::set($cacheKey, $services, now()->addMinutes(10));

			return $services;
		});
		$cacheKey = 'pages:main.main-bottom.' . getCurrentLocaleCode();
		$bottom = Cache::get($cacheKey, function () use ($cacheKey) {
			$data = $this->contentRepository->getListPublicByType('main-bottom');
			Cache::set($cacheKey, $data, now()->addMinutes(10));

			return $data;
		});

		return [
			'data' => [
				'page' => [
					'name' => $page->name,
					'description' => $page->description,
				],
				'services' => ServiceResource::collection($services),
				'bottom' => ServiceResource::collection($bottom),
				'how_it_works' => $howItWorks->getResponse(),
				'reviews' => $reviewsApiData->getResponse(),
				'last_deals' => OrderResource::collection($lastDealsApiData->getResponse()),
			],
		];
	}

	public function forPerformer(
		Request                    $request,
		LastDealsApiData           $lastDealsApiData,
		PerformerHowItWorksApiData $howItWorksApiData,
		FaqApiData                 $apiData,
		ReviewsApiData             $reviewsApiData
	)
	{
		/** @var  $page Page */
		$page = $this->repository->findPageByPageType(PageTypeEnum::FOR_PERFORMER);
		$this->abortIfNull($page);
		$services = $this->contentRepository->getListPublicByType('performer-services');

		return [
			'data' => [
				'page' => [
					'name' => $page->name,
					'description' => $page->description,
				],
				'services' => ServiceResource::collection($services)->toArray($request),
				'last_deals' => OrderResource::collection($lastDealsApiData->getResponse()),
				'how_it_works' => $howItWorksApiData->getResponse(),
				'reviews' => $reviewsApiData->getResponse(),
				'faq' => $apiData->getResponse($request->get('type', FaqTypes::DEFAULT)),
			],
		];
	}

	public function paymentAndReturn()
	{
		/** @var  $page Page */
		$page = $this->repository->findPageByPageType(PageTypeEnum::PAYMENT_AND_RETURN);
		$this->abortIfNull($page);
		return [
			'data' => [
				'page' => [
					'name' => $page->name,
					'description' => $page->description,
				],
			]
		];
	}

}
