<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Order\Bid;

use App\Events\Order\Show\Bids\BidCreatedEvent;
use App\Http\Requests\Order\Show\OrderAddBidRequest;
use App\Http\Resources\Order\OrderBidResource;
use App\Models\Order\Order;
use App\Platform\Contract\Order\Bid\AddBidVoterContract;
use App\Platform\Order\PermissionContainers\Managers\OrderBidPermissionContainerManager;
use App\Platform\Order\PermissionContainers\Managers\OrderPermissionContainerManager;
use App\Platform\Order\Voters\Bid\IsUserCanAddBidToDeal;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use Illuminate\Http\Response;

class BidCreateController extends AbstractApiBidController
{
	/**
	 * @var OrderRepository
	 */
	private $repository;
	/**
	 * @var OrderBidRepository
	 */
	private $orderBidRepository;

	public function __construct(
		OrderRepository    $repository,
		OrderBidRepository $orderBidRepository
	)
	{
		parent::__construct();
		$this->repository = $repository;
		$this->orderBidRepository = $orderBidRepository;
	}

	public function store($orderId, OrderAddBidRequest $request)
	{
		/** @var  $order Order */
		$order = $this->repository->findForShow($orderId);
		$this->abortIfNull($order);
		if ($check = $this->checkCan($order)) {
			return $check;
		}
		$currentUser = $this->getUser();
		$manager = OrderPermissionContainerManager::make($order, $currentUser)->apply();
		$permissions = $manager->getContainer();

		if ($permissions->isOwner()) {
			return $this->setFailMessage(getTranslate('order.bids.add.fail.add-to-own', 'Нельзя оставить заявку к своей сделке'))->getResponseMessageForJson();
		}
		if (!$permissions->isCanAddBid()) {
			return $this->setFailMessage($this->getFailMessage($manager))->getResponseMessageForJson();
		}

		$input = $request->only($request->getFillableFields());
		$input[$order->getForeignKey()] = $order->getKey();
		if ($bid = $this->orderBidRepository->createRelated($input, $currentUser)) {
			//for fill defaults
			$bid = $this->orderBidRepository->find($bid->getKey());
			event(app(BidCreatedEvent::class, ['orderBid' => $bid, 'order' => $order]));
			$container = OrderBidPermissionContainerManager::make($order, $currentUser)->setBid($bid)->apply()->getContainer();
			$bid->setAttribute('permissions', $container->toArray());
			$this->setSuccessMessage(getTranslate('order.bids.add.success', 'Заявка успешно добавлена'))->setResponseData(['bid' => OrderBidResource::make($bid)]);
		}

		return new Response($this->getResponseMessageForJson(), Response::HTTP_CREATED);
	}

	protected function getFailMessage(OrderPermissionContainerManager $manager)
	{
		$matches = [
			IsUserCanAddBidToDeal::REASON_ORDER_CLOSED => getTranslate('order.bids.add.fail.deal-is-closed', 'Сделка закрыта'),
			IsUserCanAddBidToDeal::REASON_ORDER_HAS_PERFORMER => getTranslate('order.bids.add.fail.performer-is-selected', 'Исполнитель уже выбран'),
			IsUserCanAddBidToDeal::REASON_PERFORMER_HAS_BID => getTranslate('order.bids.add.fail.already-have', 'Можно оставлять только одну заявку к сделке'),
		];
		$reason = $manager->isUserCanAddBidToDeal->getReason();
		if ($reason instanceof AddBidVoterContract) {
			return $reason->message();
		}

		return (string)($matches[$reason] ?? '');
	}

}