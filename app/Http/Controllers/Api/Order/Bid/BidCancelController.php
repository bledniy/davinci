<?php


	namespace App\Http\Controllers\Api\Order\Bid;


	use App\Events\Order\Show\Bids\BidCanceledEvent;
	use App\Models\Order\Order;
	use App\Platform\Order\PermissionContainers\Managers\OrderBidPermissionContainerManager;
	use App\Repositories\Order\OrderBidRepository;
	use App\Repositories\Order\OrderRepository;
	use Illuminate\Http\Response;

	class BidCancelController extends AbstractApiBidController
	{
		/**
		 * @var OrderRepository
		 */
		private $repository;
		/**
		 * @var OrderBidRepository
		 */
		private $orderBidRepository;

		public function __construct(
			OrderRepository $repository,
			OrderBidRepository $orderBidRepository
		)
		{
			parent::__construct();
			$this->repository = $repository;
			$this->orderBidRepository = $orderBidRepository;
		}

		public function cancel($orderId, $bidId)
		{
			/** @var  $order Order */
			$order = $this->repository->findForShow($orderId);
			$this->abortIfNull($order);
			if ($check = $this->checkCan($order)) {
				return $check;
			}
			$currentUser = $this->getUser();

			if (!$bid = $this->orderBidRepository->findForUpdate($order, $bidId)) {
				abort(Response::HTTP_NOT_FOUND);
			}
			$manager = OrderBidPermissionContainerManager::make($order, $currentUser)->setBid($bid)->apply();
			$permissions = $manager->getContainer();

			if (!$permissions->isOwner()) {
				abort(Response::HTTP_NOT_FOUND);
			}
			if (!$permissions->isCanCancelBid()) {
				return $this->setFailMessage(getTranslate('order.bids.cancel.unavailable', 'Нельзя отозвать ставку'))->getResponseMessageForJson();
			}

			$bid->getSetter()->disable();
			if ($this->orderBidRepository->update([], $bid)) {
				event(new BidCanceledEvent($order, $bid));
				$this->setSuccessMessage(getTranslate('order.bids.cancel.success', 'Вы отозвали ставку'))
				;
			}
			return $this->getResponseMessageForJson();
		}

	}