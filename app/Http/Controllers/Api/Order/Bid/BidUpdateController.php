<?php


	namespace App\Http\Controllers\Api\Order\Bid;


	use App\Events\Order\Show\Bids\BidBeforeEditedEvent;
	use App\Events\Order\Show\Bids\BidCreatedEvent;
	use App\Events\Order\Show\Bids\BidEditedEvent;
	use App\Http\Controllers\Platform\AbstractPlatformController;
	use App\Http\Requests\Order\Show\OrderUpdateBidRequest;
	use App\Http\Resources\Order\OrderBidResource;
	use App\Models\Order\Order;
	use App\Platform\Order\Containers\Show\DealPermissionsContainer;
	use App\Platform\Order\PermissionContainers\Managers\OrderBidPermissionContainerManager;
	use App\Platform\Order\PermissionContainers\Managers\OrderPermissionContainerManager;
	use App\Platform\Order\Services\IsUserOwnerService;
	use App\Platform\Order\Voters\Bid\IsUserCanAddBidToDeal;
	use App\Repositories\Order\OrderBidRepository;
	use App\Repositories\Order\OrderRepository;
	use App\Rules\Order\OrderExistsRule;
	use Illuminate\Http\Response;

	class BidUpdateController extends AbstractApiBidController
	{
		/**
		 * @var OrderRepository
		 */
		private $repository;
		/**
		 * @var OrderBidRepository
		 */
		private $orderBidRepository;

		public function __construct(
			OrderRepository $repository,
			OrderBidRepository $orderBidRepository
		)
		{
			parent::__construct();
			$this->repository = $repository;
			$this->orderBidRepository = $orderBidRepository;
		}

		public function update($orderId, $bidId, OrderUpdateBidRequest $request)
		{
			/** @var  $order Order */
			$order = $this->repository->findForShow($orderId);
			$this->abortIfNull($order);
			if ($check = $this->checkCan($order)) {
				return $check;
			}
			$currentUser = $this->getUser();

			if (!$bid = $this->orderBidRepository->findForUpdate($order, $bidId)) {
				abort(Response::HTTP_NOT_FOUND);
			}
			$manager = OrderBidPermissionContainerManager::make($order, $currentUser)->setBid($bid)->apply();
			$permissions = $manager->getContainer();

			if (!$permissions->isOwner()) {
				abort(Response::HTTP_NOT_FOUND);
			}
			if (!$permissions->isCanEditBid()) {
				return $this->setFailMessage(getTranslate('order.bids.edit.unavailable', 'Редактирование ставки не доступно'))->getResponseMessageForJson();
			}

			$input = $request->only($request->getFillableFields());
			$bid->fillExisting($input);
			event(app(BidBeforeEditedEvent::class, ['orderBid' => $bid, 'order' => $order]));
			if ($this->orderBidRepository->update([], $bid)) {
				event(app(BidEditedEvent::class, ['orderBid' => $bid, 'order' => $order]));
				$container = OrderBidPermissionContainerManager::make($order, $currentUser)->setBid($bid)->apply()->getContainer();
				$bid->setAttribute('permissions', $container->toArray());
				$this->setSuccessMessage(getTranslate('order.bids.edit.success', 'Заявка успешно обновлена'))
					->setResponseData(['bid' => OrderBidResource::make($bid)])
				;
			}
			return $this->getResponseMessageForJson();
		}

	}