<?php


	namespace App\Http\Controllers\Api\Order\Bid;


	use App\Http\Controllers\Api\Order\AbstractApiOrderController;
	use App\Models\Order\Order;

	class AbstractApiBidController extends AbstractApiOrderController
	{
		protected function checkCan(Order $order)
		{
			if (!$order) {
				return $this->setFailMessage('Сделка не найдена')->getResponseMessageForJson();
			}
			return null;
		}

	}