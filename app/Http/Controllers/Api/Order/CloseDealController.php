<?php

	namespace App\Http\Controllers\Api\Order;

	use App\Events\Order\BeforeOrderClosedEvent;
	use App\Events\Order\OrderClosedEvent;
	use App\Platform\Order\PermissionContainers\Managers\OrderPermissionContainerManager;
	use App\Repositories\Order\OrderRepository;
	use App\Repositories\PerformerRepository;
	use Illuminate\Http\Response;

	class CloseDealController extends AbstractApiOrderController
	{
		/**
		 * @var OrderRepository
		 */
		private $orderRepository;
		/**
		 * @var PerformerRepository
		 */
		private $performerRepository;

		public function __construct(OrderRepository $orderRepository, PerformerRepository $performerRepository)
		{
			parent::__construct();
			$this->orderRepository = $orderRepository;
			$this->performerRepository = $performerRepository;
		}

		public function close($orderId)
		{
			$order = $this->orderRepository->findForActions($orderId);
			$this->abortIfNull($order, $order);

			$permissions = (OrderPermissionContainerManager::make($order, $this->getUser()))->apply()->getContainer();
			if (!$permissions->isCanCloseDeal()) {
				return $this->setFailMessage(getTranslate('order.close.fail', 'Нельзя закрыть сделку'))->getResponseMessageForJson();
			}

			event(app(BeforeOrderClosedEvent::class, compact('order')));
			$order->getSetter()->close();
			if ($this->orderRepository->update([], $order)) {
				event(app(OrderClosedEvent::class, compact('order')));
				$this->setSuccessMessage(getTranslate('order.close.success', 'Сделка успешно закрыта'));
			}

			return $this->getResponseMessageForJson();
		}

	}
