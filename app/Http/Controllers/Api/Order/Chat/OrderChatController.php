<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Order\Chat;

use App\Events\Order\Chat\ChatCreated;
use App\Events\Order\Chat\UsersAddedToChat;
use App\Http\Controllers\Api\Order\AbstractApiOrderController;
use App\Http\Resources\Chat\ChatResource;
use App\Platform\Chat\Jobs\AddUsersToChatJob;
use App\Platform\Chat\Jobs\ChatCreatorJob;
use App\Platform\Contract\Chat\ChatTypesContract;
use App\Models\Order\Order;
use App\Platform\Order\Voters\Deal\IsPerformerHasBidInOrder;
use App\Platform\Order\Voters\Deal\IsUserOwnerDeal;
use App\Repositories\Chat\ChatRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\PerformerRepository;

class OrderChatController extends AbstractApiOrderController
{

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;
	/**
	 * @var PerformerRepository
	 */
	private $performerRepository;

	public function __construct(OrderRepository $orderRepository, ChatRepository $chatRepository, PerformerRepository $performerRepository)
	{
		parent::__construct();
		$this->orderRepository = $orderRepository;
		$this->chatRepository = $chatRepository;
		$this->performerRepository = $performerRepository;
	}

	public function chatWithPerformer($orderId, $performerId): ChatResource
	{
		/** @var  $order Order */
		$order = $this->orderRepository->findForActions($orderId);
		$this->abortIfNull($order);
		$customer = $this->getUser();
		if (!IsUserOwnerDeal::make()->check($order->getContainer()->getCustomer(), $customer)) {
			$this->throwNotFound();
		}
		$candidateToPerformer = $this->performerRepository->find($performerId);
		$this->abortIfNull($candidateToPerformer);
		if (!IsPerformerHasBidInOrder::make()->check($order, $candidateToPerformer)) {
			$this->throwNotFound();
		}

		if (!$chat = $this->chatRepository->getChatByUser($order, $candidateToPerformer)) {
			$attr = ['type' => $order->getContainer()->hasPerformer() ? ChatTypesContract::TYPE_WORKS : null];
			$usersToChat = [$customer,$candidateToPerformer];
			$chat = ChatCreatorJob::make($order, $attr)->create();
			event(new ChatCreated($order, $chat, $customer));
			AddUsersToChatJob::make($chat, $usersToChat)->add();
			event(new UsersAddedToChat($chat, $usersToChat));
		}
		return ChatResource::make($chat);
	}

}