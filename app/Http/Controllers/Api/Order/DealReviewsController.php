<?php

	namespace App\Http\Controllers\Api\Order;

	use App\Events\Order\Reviews\AddCustomerReviewEvent;
	use App\Events\Order\Reviews\AddPerformerReviewEvent;
	use App\Events\Order\Reviews\BeforeAddCustomerReviewEvent;
	use App\Events\Order\Reviews\BeforeAddPerformerReviewEvent;
	use App\Http\Requests\Order\ReviewAddRequest;
	use App\Models\Order\Order;
	use App\Platform\Contract\UserTypeContract;
	use App\Platform\Order\DataContainers\ReviewsDataContainer;
	use App\Platform\Order\Voters\Deal\IsUserOwnerDeal;
	use App\Platform\Order\Voters\Deal\IsUserSelectedAsPerformer;
	use App\Platform\Order\Voters\Review\IsDateReviewNotExpired;
	use App\Repositories\Order\OrderRepository;
	use App\Repositories\ReviewRepository;

	class DealReviewsController extends AbstractApiOrderController
	{
		private $orderRepository;
		/**
		 * @var ReviewRepository
		 */
		private $reviewRepository;

		public function __construct(
			OrderRepository $orderRepository,
			ReviewRepository $reviewRepository
		)
		{
			parent::__construct();
			$this->orderRepository = $orderRepository;
			$this->reviewRepository = $reviewRepository;
		}

		public function performer($orderId, ReviewAddRequest $request)
		{
			/** @var  $order Order */
			$order = $this->orderRepository->findForActions($orderId);
			if (!is_null($check = $this->check($order))) {
				return $check;
			}
			$container = $order->getContainer();
			$currentUser = $this->getUser();
			$customer = $container->getCustomer();
			$reviewsContainer = app(ReviewsDataContainer::class, compact('order'));
			if (!app(IsUserSelectedAsPerformer::class)->check($order, $currentUser)) {
				return $this->setFailMessage(getTranslate('order.reviews.fail.performer.not-owner', 'Вы не можете оставить отзыв где вы не выбраны исполнителем'))->getResponseMessageForJson();
			}
			if ($reviewsContainer->hasPerformerReview()) {
				return $this->setFailMessage(getTranslate('order.reviews.fail.duplicate', 'Вы уже оставили отзыв к сделке'))->getResponseMessageForJson();
			}

			$review = $this->reviewRepository->makeModel();
			$review->fill($request->only($request->getFillableFields()));
			$review->getSetter()->setFrom($currentUser)->setAbout(UserTypeContract::TYPE_CUSTOMER)->setTo($customer)->setOrder($order);
			event(app(BeforeAddPerformerReviewEvent::class, compact('order', 'review', 'currentUser')));
			if ($review = $this->reviewRepository->create($review->toArray())) {
				$this->setSuccessMessage(getTranslate('order.reviews.success', 'Ваш отзыв успешно добавлен'));
				event(app(AddPerformerReviewEvent::class, compact('order', 'review', 'currentUser')));
			}

			return $this->getResponseMessageForJson();
		}

		public function customer($orderId, ReviewAddRequest $request)
		{
			/** @var  $order Order */
			$order = $this->orderRepository->findForActions($orderId);
			if (!is_null($check = $this->check($order))) {
				return $check;
			}
			$container = $order->getContainer();
			$currentUser = $this->getUser();
			$performer = $container->getPerformer();
			if (!$performer) {
				abort(404);
			}
			$reviewsContainer = app(ReviewsDataContainer::class, compact('order'));
			if (!app(IsUserOwnerDeal::class)->check($container->getCustomer(), $currentUser)) {
				return $this->setFailMessage(getTranslate('order.reviews.fail.customer.not-owner', 'Отзыв может оставить только автор сделки'))->getResponseMessageForJson();
			}
			if ($reviewsContainer->hasCustomerReview()) {
				return $this->setFailMessage(getTranslate('order.reviews.fail.duplicate', 'Вы уже оставили отзыв к сделке'))->getResponseMessageForJson();
			}

			$review = $this->reviewRepository->makeModel();
			$review->fill($request->only($request->getFillableFields()));
			$review->getSetter()->setFrom($currentUser)->setAbout(UserTypeContract::TYPE_PERFORMER)->setTo($performer)->setOrder($order);
			event(app(BeforeAddCustomerReviewEvent::class, compact('order', 'review', 'currentUser')));
			if ($review = $this->reviewRepository->create($review->toArray())) {
				$this->setSuccessMessage(getTranslate('order.reviews.success', 'Ваш отзыв успешно добавлен'));
				event(app(AddCustomerReviewEvent::class, compact('order', 'review', 'currentUser')));
			}

			return $this->getResponseMessageForJson();
		}

		private function check(?Order $order)
		{
			if (!$order) {
				abort(404);
			}
			$container = $order->getContainer();

			if (!app(IsDateReviewNotExpired::class)->check($container->getDateClose())) {
				return $this->setFailMessage(getTranslate('order.reviews.fail.date-expired', 'Период добавления отзыва истек'))->getResponseMessageForJson();
			}
			if ($container->isOpened()) {
				return $this->setFailMessage(getTranslate('order.reviews.fail.deal-not-closed', 'Добавление отзыва будет доступно после закрытия сделки'))->getResponseMessageForJson();
			}

			return null;
		}
	}
