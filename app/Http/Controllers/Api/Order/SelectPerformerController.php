<?php

	namespace App\Http\Controllers\Api\Order;

	use App\Events\Order\OrderBeforeUpdatedEvent;
	use App\Events\Order\OrderUpdatedEvent;
	use App\Events\Order\Show\Bids\BeforeChoosePerformer;
	use App\Events\Order\Show\Bids\ChoosedPerformer;
	use App\Http\Requests\Order\OrderUpdateRequest;
	use App\Http\Resources\Order\OrderCreatedResource;
	use App\Http\Resources\Order\OrderResource;
	use App\Platform\Order\PermissionContainers\Managers\OrderPermissionContainerManager;
	use App\Platform\Order\Uploads\OrderCreateFilesStoreJob;
	use App\Platform\Order\Voters\Deal\IsPerformerAvailableToSelect;
	use App\Platform\Order\Voters\Deal\IsUserCanEditDeal;
	use App\Repositories\Order\OrderRepository;
	use App\Repositories\PerformerRepository;
	use Illuminate\Support\Facades\DB;

	class SelectPerformerController extends AbstractApiOrderController
	{
		/**
		 * @var OrderRepository
		 */
		private $orderRepository;
		/**
		 * @var PerformerRepository
		 */
		private $performerRepository;

		public function __construct(OrderRepository $orderRepository, PerformerRepository $performerRepository)
		{
			parent::__construct();
			$this->orderRepository = $orderRepository;
			$this->performerRepository = $performerRepository;
		}

		public function select($orderId, $performerId)
		{
			$order = $this->orderRepository->find((int)$orderId);
			$permissions = (OrderPermissionContainerManager::make($order, $this->getUser()))->apply()->getContainer();

			if (!$permissions->isCanSelectPerformer()) {
				return $this->getFailMessage();
			}
			$selectablePerformer = $this->performerRepository->find($performerId);
			if (!IsPerformerAvailableToSelect::make()->check($order, $selectablePerformer)) {
				return $this->getFailMessage();
			}

			$performer = $selectablePerformer;
			$order->getSetter()->setPerformerId($selectablePerformer->getKey());
			event(app(BeforeChoosePerformer::class, compact('order', 'performer')));
			if ($this->orderRepository->update([], $order)) {
				event(app(ChoosedPerformer::class, compact('order', 'performer')));
				$this->setSuccessMessage(translateFormat('order.performer.select.success', ['%name%' => $performer->getFio(), ]));
			}

			return $this->getResponseMessageForJson();
		}

		protected function getFailMessage(): array
		{
			return $this->setFailMessage(getTranslate('order.performer.select.fail', 'Нельзя выбрать исполнителя'))->getResponseMessageForJson();
		}
	}
