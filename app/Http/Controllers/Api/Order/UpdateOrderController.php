<?php

	namespace App\Http\Controllers\Api\Order;

	use App\Events\Order\OrderBeforeUpdatedEvent;
	use App\Events\Order\OrderUpdatedEvent;
	use App\Http\Requests\Order\OrderUpdateRequest;
	use App\Http\Resources\Order\OrderCreatedResource;
	use App\Http\Resources\Order\OrderResource;
	use App\Platform\Order\PermissionContainers\Managers\OrderPermissionContainerManager;
	use App\Platform\Order\Uploads\OrderCreateFilesStoreJob;
	use App\Platform\Order\Voters\Deal\IsUserCanEditDeal;
	use App\Repositories\Order\OrderRepository;
	use Illuminate\Support\Facades\DB;

	class UpdateOrderController extends AbstractApiOrderController
	{
		/**
		 * @var OrderRepository
		 */
		private $orderRepository;

		public function __construct(OrderRepository $orderRepository)
		{
			parent::__construct();
			$this->orderRepository = $orderRepository;
		}

		public function update(OrderUpdateRequest $request, $orderId)
		{
			$order = $this->orderRepository->find((int)$orderId);
			$permissions = ($manager = OrderPermissionContainerManager::make($order, $this->getUser()))->apply()->getContainer();

			if (!$permissions->isCanEditDeal()) {
				return $this->setFailMessage($this->getFailMessage($manager->isUserCanEditDeal->getReason()))->getResponseMessageForJson();
			}
			$input = $request->only($request->getFillableFields('closes_at', 'files'));
			$order->fillExisting($input);
			event(new OrderBeforeUpdatedEvent($order));
			try {
				DB::beginTransaction();
				$this->orderRepository->update($input, $order);
				if ($files = $request->file('files')) {
					app(OrderCreateFilesStoreJob::class, compact('files', 'order'))->run()->getOrderUploads();
				}
				$this->orderRepository->loadForDeals($order);
				$orderResource = OrderResource::make($order);
				$permissions = ($manager = OrderPermissionContainerManager::make($order, $this->getUser()))->apply()->getContainer();
				$orderResource->additionalData(['permissions' => $permissions->toArray()]);
				$this->setSuccessMessage(getTranslate('order.update.success', 'Сделка успешно отредактирована'))
					->setResponseData(['order' => $orderResource])
				;
				DB::commit();
			} catch (\Throwable $e) {
				DB::rollBack();
				app(\App\Helpers\Debug\LoggerHelper::class)->error($e);
				return $this->setFailMessage(getTranslate('order.update.fail', 'Ошибка при редактировании сделки'))->getResponseMessageForJson();
			}
			event(new OrderUpdatedEvent($order));
			return $this->getResponseMessageForJson();
		}

		protected function getFailMessage($reason)
		{
			$matches = [
				IsUserCanEditDeal::REASON_IS_CLOSED     => getTranslate('order.update.fail.deal-is-close', 'Сделка закрыта'),
				IsUserCanEditDeal::REASON_LIMIT_EDIT    => getTranslate('order.update.fail.limit-edit-exceeded', 'Вы исчерпали лимит редактирования'),
				IsUserCanEditDeal::REASON_HAS_PERFORMER => getTranslate('order.update.fail.has-selected-performer', 'Нельзя редактировать сделку после выбора исполнителя'),
			];
			return $matches[ $reason ] ?? '';
		}

	}
