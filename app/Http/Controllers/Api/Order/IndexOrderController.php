<?php

	namespace App\Http\Controllers\Api\Order;

	use App\DataContainers\Platform\Order\SearchOrdersDataContainer;
	use App\Http\Resources\Order\OrderResource;
	use App\Platform\User\AuthenticatedUserResolver;
	use App\Platform\User\Type\UserTypeHelper;
	use App\Repositories\Order\OrderRepository;
	use Illuminate\Http\Request;

	class IndexOrderController extends AbstractApiOrderController
	{
		/**
		 * @var \App\Repositories\Order\OrderRepository
		 */
		private $orderRepository;
		/**
		 * @var AuthenticatedUserResolver
		 */
		private $userResolver;

		public function __construct(OrderRepository $orderRepository, AuthenticatedUserResolver $userResolver)
		{
			parent::__construct();
			$this->orderRepository = $orderRepository;
			$this->userResolver = $userResolver;
		}

		public function index(Request $request, SearchOrdersDataContainer $container)
		{
			$container->fillFromRequest($request);
			if ($user = $this->userResolver->getUser()) {
				$categories = $user->performerCategories()->pluck('id')->toArray();
				if (!$categories) {
					return $this->onDoesntConfirmedCategories();
				}
				$container->setCategoryIds($categories)->setExceptCustomers([$user]);
			}
			$orders = $this->orderRepository->getListPublic($container);
			$this->orderRepository->loadForDeals($orders);
			return OrderResource::collection($orders);
		}

		protected function onDoesntConfirmedCategories(): array
		{
			return $this->setFailMessage(getTranslate('order.need-verification', 'Пожалуйста сначале подтвердите свои навыки в личном кабинете'))
				->setResponseData(['data'              => [],
                                   'need_verification' => true])->getResponseMessageForJson()
				;
		}

	}
