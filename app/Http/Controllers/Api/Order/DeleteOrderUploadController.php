<?php

	namespace App\Http\Controllers\Api\Order;

	use App\Events\Order\OrderBeforeUpdatedEvent;
	use App\Events\Order\OrderUpdatedEvent;
	use App\Http\Requests\Order\OrderUpdateRequest;
	use App\Http\Resources\Order\OrderCreatedResource;
	use App\Http\Resources\Order\OrderResource;
	use App\Platform\Order\PermissionContainers\Managers\OrderPermissionContainerManager;
	use App\Platform\Order\Uploads\OrderCreateFilesStoreJob;
	use App\Platform\Order\Voters\Deal\IsUserCanEditDeal;
	use App\Repositories\Order\OrderRepository;
	use App\Repositories\Order\OrderUploadsRepository;
	use Illuminate\Http\Response;
	use Illuminate\Support\Facades\DB;

	class DeleteOrderUploadController extends AbstractApiOrderController
	{
		/**
		 * @var OrderRepository
		 */
		private $orderRepository;
		/**
		 * @var OrderUploadsRepository
		 */
		private $orderUploadsRepository;

		public function __construct(OrderRepository $orderRepository, OrderUploadsRepository $orderUploadsRepository)
		{
			parent::__construct();
			$this->orderRepository = $orderRepository;
			$this->orderUploadsRepository = $orderUploadsRepository;
		}

		public function delete($orderId, $uploadId)
		{
			$order = $this->orderRepository->find((int)$orderId);
			if (!$order) {
				abort(Response::HTTP_NOT_FOUND);
			}
			$permissions = (OrderPermissionContainerManager::make($order, $this->getUser()))->apply()->getContainer();
			if (!$permissions->isOwner()) {
				abort(Response::HTTP_NOT_FOUND);
			}
			$orderUpload = $this->orderUploadsRepository->findOneByOrder($order, $uploadId);
			if (!$orderUpload) {
				abort(Response::HTTP_NOT_FOUND);
			}

			$this->orderUploadsRepository->delete($orderUpload);
			storageDelete($orderUpload->getPath(), 'api');

			return $this->setSuccessMessage(translateFormat('order.files.delete.success', ['%name%' => $orderUpload->getOriginalName(),]))->getResponseMessageForJson();
		}

	}
