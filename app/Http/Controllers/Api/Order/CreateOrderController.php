<?php

namespace App\Http\Controllers\Api\Order;

use App\Events\Order\OrderBeforePublished;
use App\Events\Order\OrderCreatedEvent;
use App\Events\Order\OrderPublished;
use App\Helpers\Debug\LoggerHelper;
use App\Http\Requests\Order\OrderCreateRequest;
use App\Http\Resources\Order\OrderCreatedResource;
use App\Models\User;
use App\Platform\Order\Contracts\OrderCreateTokenGenerator;
use App\Platform\Order\DataContainers\OrderCreateContainer;
use App\Platform\Order\Services\OrderCreator;
use App\Platform\Order\Services\OrderUploadsCreator;
use App\Platform\Order\Uploads\OrderCreateFilesStoreJob;
use App\Platform\Order\Uploads\OrderCreateUnauthenticatedFilesReStoreJob;
use App\Platform\Order\Uploads\OrderCreateUnauthenticatedFilesStoreJob;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\TemporaryRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CreateOrderController extends AbstractApiOrderController
{
	/**
	 * @var TemporaryRepository
	 */
	private $temporaryRepository;
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;

	public function __construct(TemporaryRepository $temporaryRepository, AuthenticatedUserResolver $userResolver)
	{
		parent::__construct();
		$this->temporaryRepository = $temporaryRepository;
		$this->userResolver = $userResolver;
	}

	public function callAction($method, $parameters)
	{
		if (null === $this->userResolver->getUser()) {
			return parent::callAction('storeUnAuth', $parameters);
		}
		return parent::callAction($method, $parameters);
	}

	public function store(OrderCreateRequest $request)
	{
		$input = $request->only($request->getFillableFields('files'));
		if ($user = $this->userResolver->getUser()) {
			$input['customer_id'] = $user->getKey();
		}
		try {
			DB::beginTransaction();
			$container = app(OrderCreateContainer::class, ['data' => $input]);
			$order = app(OrderCreator::class, ['container' => $container])->create();
			event(app(OrderCreatedEvent::class, compact('order')));
			if ($files = $request->file('files')) {
				app(OrderCreateFilesStoreJob::class, compact('files', 'order'))->run()->getOrderUploads();
			}
			$this->setSuccessMessage(getTranslate('order.create.success', 'Сделка успешно создана'))
				->setResponseData(['order' => OrderCreatedResource::make($order)])
			;
			event(app(OrderBeforePublished::class, compact('order')));
			event(app(OrderPublished::class, compact('order')));
			DB::commit();
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
			DB::rollBack();
			throwIfDev($e);
			$this->setFailMessage(getTranslate('order.create.fail', 'Ошибка при создании сделки'));
		}

		return new Response($this->getResponseMessageForJson(), Response::HTTP_CREATED);
	}

	protected function storeUnAuth(OrderCreateRequest $request)
	{
		$input = $request->only($request->getFillableFields(['files', 'files.*']));
		try {
			if ($files = $request->file('files')) {
				$input['uploads'] = app(OrderCreateUnauthenticatedFilesStoreJob::class, compact('files'))->run()->getOrderUploads()->toArray();
			}
			$token = app(OrderCreateTokenGenerator::class)->generate();
			DB::beginTransaction();
			$this->temporaryRepository->getBuilder($token)->setType('order-create')->notDeletetable()->setData($input)->build();
			DB::commit();
			$this->setResponseData(['needs_auth' => true])
				->setResponseData(['token' => $token])
				->setSuccessMessage(getTranslate('order.create.need-auth'))
			;
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
			throwIfDev($e);
			$this->setFailMessage(getTranslate('order.create.fail', 'Ошибка при создании сделки'));
			DB::rollBack();
		}

		return $this->getResponseMessageForJson();
	}

	public function completeUnAuth(Request $request)
	{
		$temporary = $this->temporaryRepository->findByKey($request->get('token'));
		if (!$temporary) {
			return $this->setFailMessage(getTranslate('order.unauth.expired'))->getResponseMessageForJson();
		}
		/** @var  $user User */
		$user = $this->userResolver->getUser();
		$data = $temporary->getData();
		$data['customer_id'] = $user->getKey();
		$container = app(OrderCreateContainer::class, ['data' => $data]);
		$order = app(OrderCreator::class, ['container' => $container])->create();
		event(app(OrderCreatedEvent::class, compact('order')));
		if (isset($data['uploads'])) {
			$files = app(OrderCreateUnauthenticatedFilesReStoreJob::class, ['order' => $order])->filesFromArray($data['uploads'])->run()->getOrderUploads();
			OrderUploadsCreator::make($order, $files)->create();
		}
		event(app(OrderBeforePublished::class, compact('order')));
		event(app(OrderPublished::class, compact('order')));
		$this->temporaryRepository->delete($temporary);

		return $this->setResponseData(['order' => OrderCreatedResource::make($order)])
			->setSuccessMessage(getTranslate('order.create.success', 'Сделка успешно создана'))
			->getResponseMessageForJson()
			;

	}
}
