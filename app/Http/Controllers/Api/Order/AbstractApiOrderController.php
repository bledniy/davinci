<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Order;

use App\Http\Controllers\Api\AbstractApiController;
use App\Models\User;
use App\Platform\User\AuthenticatedUserResolver;

abstract class AbstractApiOrderController extends AbstractApiController
{
	public function getUser(): User
	{
		return app(AuthenticatedUserResolver::class)->getUser();
	}

	// second method, just for autocomplete
	public function getNullableUser(): ?User
	{
		return app(AuthenticatedUserResolver::class)->getUser();
	}

}