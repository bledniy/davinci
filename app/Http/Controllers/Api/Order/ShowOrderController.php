<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Order;

use App\Events\Order\Show\OrderViewedEvent;
use App\Http\Resources\Chat\ChatResource;
use App\Http\Resources\Order\OrderBidResource;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Order\OrderReviewResource;
use App\Models\Order\Order;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Order\PermissionContainers\Managers\OrderBidPermissionContainerManager;
use App\Platform\Order\PermissionContainers\Managers\OrderPermissionContainerManager;
use App\Platform\Order\PermissionContainers\Managers\ReviewCustomerPermissionContainerManager;
use App\Platform\Order\PermissionContainers\Managers\ReviewPerformerPermissionContainerManager;
use App\Platform\Order\Services\MarkSelectedBid;
use App\Platform\Order\Services\ResponseContent\ShowOrderChatService;
use App\Platform\Order\Voters\Deal\IsUserSelectedAsPerformer;
use App\Repositories\Chat\ChatRepository;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\ReviewRepository;
use Illuminate\Http\Request;

class ShowOrderController extends AbstractApiOrderController
{
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderBidRepository
	 */
	private $orderBidRepository;
	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;
	/**
	 * @var ShowOrderChatService
	 */
	private $showOrderChatService;

	public function __construct(
		OrderRepository    $orderRepository,
		OrderBidRepository $orderBidRepository,
		ReviewRepository   $reviewRepository,
		ShowOrderChatService $showOrderChatService
	)
	{
		parent::__construct();
		$this->orderRepository = $orderRepository;
		$this->orderBidRepository = $orderBidRepository;
		$this->reviewRepository = $reviewRepository;
		$this->showOrderChatService = $showOrderChatService;
	}

	public function show($deal, Request $request): OrderResource
	{
		/** @var  $order Order */
		$order = $this->orderRepository->findForShow($deal);
		$this->abortIfNull($order);
		$currentUser = $this->getNullableUser();
		if ($currentUser) {
			event(app(OrderViewedEvent::class, compact('order', 'currentUser')));
		}
		$this->orderRepository->loadForDeals($order, [], ['performer', 'uploads']);
		$res = OrderResource::make($order);
		$dealPermissions = ($dealPermissionsContainer = OrderPermissionContainerManager::make($order, $currentUser)->apply()->getContainer())->toArray();
		$res->additionalData(['permissions' => $dealPermissions]);
		if ($request->has('only_deal')) {
			return $res;
		}
		//список ставок
		$bids = $this->orderBidRepository->getListPublic($order);
		$bids->load('user');
		$bids = MarkSelectedBid::make($order, $bids)->apply();
		if ($currentUser) {
			$cancelledBidCurrentPerformer = $this->orderBidRepository->findCancelledBid($order, $currentUser);
			if (null !== $cancelledBidCurrentPerformer) {
				$bids->prepend($cancelledBidCurrentPerformer);
			}
		}
		$bids = OrderBidPermissionContainerManager::make($order, $currentUser)->applyMultiple($bids);
		$additional = [
			'bids' => OrderBidResource::collection($bids),
		];

		if ($chat = $this->showOrderChatService->getChat($dealPermissionsContainer, $order, $currentUser)) {
			$additional['chat'] = ChatResource::make($chat);
		}
		$additional['reviews'] = [
			UserTypeContract::TYPE_PERFORMER => null,
			UserTypeContract::TYPE_CUSTOMER => null,
		];
		if ($order->getContainer()->hasPerformer() && $order->getContainer()->isClosed()) {
			$performerReview = $this->reviewRepository->getOneFromPerformer($order, $order->getContainer()->getPerformer(), false);
			if ($performerReview && ($performerReview->getContainer()->isPublished() || IsUserSelectedAsPerformer::make()->check($order, $currentUser))) {
				$performerReview->setRelation('performer', $order->getContainer()->getPerformer());
				$performerReviewRes = OrderReviewResource::make($performerReview);
				$permissions = ReviewPerformerPermissionContainerManager::make($order, $currentUser, $performerReview)->getContainer()->toArray();
				$performerReviewRes->additionalData(compact('permissions'));
				$reviews[UserTypeContract::TYPE_PERFORMER]['review'] = $performerReviewRes;
			} else {
				$performerReview = null;
			}
			$reviews[UserTypeContract::TYPE_PERFORMER]['permissions'] = ReviewPerformerPermissionContainerManager::make($order, $currentUser, $performerReview)->apply()->getContainer()->toArray();

			$customerReview = $this->reviewRepository->getOneFromCustomer($order, $order->getContainer()->getCustomer(), false);
			if ($customerReview && ($customerReview->getContainer()->isPublished() || $dealPermissionsContainer->isOwner())) {
				$customerReviewRes = OrderReviewResource::make($customerReview);
				$permissions = ReviewCustomerPermissionContainerManager::make($order, $currentUser, $customerReview)->getContainer()->toArray();
				$customerReviewRes->additionalData(compact('permissions'));
				$reviews[UserTypeContract::TYPE_CUSTOMER]['review'] = $customerReviewRes;
			} else {
				$customerReview = null;
			}
			$reviews[UserTypeContract::TYPE_CUSTOMER]['permissions'] = ReviewCustomerPermissionContainerManager::make($order, $currentUser, $customerReview)->apply()->getContainer()->toArray();

			$additional['reviews'] = $reviews;
		}
		$additional['recommend'] = $this->recommend($order);
		$res->additional($additional);

		return $res;
	}


	private function recommend(Order $order)
	{
		$user = $this->getNullableUser();
		if (!$user) {
			return [];
		}
		$deals = $this->orderRepository->getRecommendDeals($user, $order);
		$this->orderRepository->loadForDeals($deals);

		return OrderResource::collection($deals);
	}

}
