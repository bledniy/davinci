<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FaqResource;
use App\Models\Faq\FaqTypes;
use App\Repositories\FaqRepository;
use App\Services\ApiData\FaqApiData;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FaqController extends Controller
{
	public function index(Request $request, FaqApiData $apiData)
	{
		return $apiData->getResponse($request->get('type', FaqTypes::DEFAULT));
	}
}
