<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
	public function index()
	{
		$icon = '₴';
//		$langs = [
//			'ru'  => $icon,
//			'uk'  => $icon,
//			'eng' => $icon,
//		];
		return [
			'id'   => 1,
			'name' => $icon,
		];
	}
}
