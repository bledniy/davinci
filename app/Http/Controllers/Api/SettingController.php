<?php

namespace App\Http\Controllers\Api;

use App\DataContainers\Platform\ApiSettingDataContainer;
use App\Http\Controllers\Controller;
use App\Http\Resources\SettingResource;
use App\Models\Setting;
use App\Models\Translate\Translate;
use App\Repositories\Admin\SettingsRepository;

class SettingController extends Controller
{
	/**
	 * @var SettingsRepository
	 */
	private $repository;

	public function __construct(SettingsRepository $repository)
	{
		$this->repository = $repository;
	}

	public function index()
	{
		$res = [];
		$keys = [
			'global.currency',
			'global.sitename',
			'global.schedule.weekdays',
			'global.phone-one',
			'global.phone-two',
			'email.public-email',
			'social.facebook',
			'social.instagram',
		];
		$settings = $this->repository->getListPublic($keys)->keyBy('key');
		foreach ($settings as $setting) {
			$res[$setting->key] = ApiSettingDataContainer::createFromSetting($setting);
		}
		$translates = [
			'contacts.urface.name',
			'contacts.urface.data'
		];
		foreach ($translates as $translateKey) {
			try{
				$translate = Translate::getTranslate($translateKey, true);
				$res[$translate->key] = ApiSettingDataContainer::createFromTranslate($translate);
			} catch (\Throwable $e){
			}
		}

		return SettingResource::collection($res);
	}
}
