<?php


namespace App\Http\Controllers\Api\User;


use App\Http\Controllers\Api\AbstractApiController;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use Illuminate\Support\Facades\Auth;

class ApiUserController extends AbstractApiController
{

	public function user()
	{
		$user = Auth::user();
		if (!$user) {
		    abort(404);
		}
		return UserResourceFactory::make($user);
	}

}