<?php


namespace App\Http\Controllers\Api\User;


use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Requests\User\Cabinet\NotificationReadRequest;
use App\Http\Resources\NotificationResource;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Platform\User\AuthenticatedUserResolver;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Auth;

class UserNotificationsController extends AbstractApiController
{
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;

	public function __construct(AuthenticatedUserResolver $userResolver)
	{
		parent::__construct();
		$this->userResolver = $userResolver;
	}

	public function index()
	{
		/** @var  $user */
		$user = $this->userResolver->getUser();
		$this->abortIfNull($user);
		$notifications = $user->notifications()->latest()->paginate(50);
		return NotificationResource::collection($notifications);
	}

	public function indexUnread()
	{
		/** @var  $user */
		$user = $this->userResolver->getUser();
		$this->abortIfNull($user);
		$notifications = $user->unreadNotifications()->get();
		return NotificationResource::collection($notifications);
	}

	public function markAsRead(NotificationReadRequest $request)
	{
		/** @var  $user */
		$user = $this->userResolver->getUser();
		$user->unreadNotifications()->whereIn('id', array_values($request->get('notifications')))->update(['read_at' => now()]);

		$this->setSuccessMessage(getTranslate('notifications.readed', 'Уведомления прочитаны'));
		return $this->getResponseMessageForJson();
	}

}