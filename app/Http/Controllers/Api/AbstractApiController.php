<?php declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Response;

class AbstractApiController extends BaseController
{
	protected function throwNotFound($message = ''): void
	{
		abort(Response::HTTP_NOT_FOUND, $message);
	}

	protected function throwAccessDenied($message = ''): void
	{
		abort(Response::HTTP_FORBIDDEN, $message);
	}

	protected function abortIfNull($value = null, $message = ''):void
	{
		if (null === $value) {
			$this->throwNotFound($message);
		}
	}
}