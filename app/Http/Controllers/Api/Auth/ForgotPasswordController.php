<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Api\AbstractApiController;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends AbstractApiController
{
	private $linkReset;
	private $token;

	use SendsPasswordResetEmails;

	public function send(Request $request)
	{
		ResetPassword::$createUrlCallback = function ($notifiable, $token) {
			$this->token = $token;
			return $this->linkReset = urlClearSlashes(env('FRONT_APP_URL')) . '/#reset?' . http_build_query([
					'token' => $token,
					'email' => $notifiable->getEmailForPasswordReset(),
				]);
		};

		return $this->sendResetLinkEmail($request);
	}

	protected function sendResetLinkResponse(Request $request, $response)
	{
		return $this->setSuccessMessage(trans($response))
			->setResponseData(isLocalEnv() ? ['link' => $this->linkReset] : [])
			->setResponseData(isLocalEnv() ? ['token' => $this->token] : [])
			->getResponseMessageForJson()
			;
	}
}