<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\AbstractApiController;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class ResetPasswordController extends AbstractApiController
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(UserRepository $userRepository)
	{
		parent::__construct();
		$this->userRepository = $userRepository;
	}

	use ResetsPasswords;


	protected function sendResetResponse(Request $request, $response)
	{
		return $this->setSuccessMessage(trans($response))
			->getResponseMessageForJson()
			;
	}

	protected function sendResetFailedResponse(Request $request, $response)
	{
		if ($request->wantsJson()) {
			throw ValidationException::withMessages([
				'email' => [trans($response)],
			]);
		}

		return redirect()->back()
			->withInput($request->only('email'))
			->withErrors(['email' => trans($response)])
			;
	}

	protected function rules()
	{
		return [
			'token'    => ['required'],
			'email'    => ['required', 'email:rfc,dns'],
			'password' => ['required', 'min:8'],
		];
	}

	protected function resetPassword($user, $password)
	{
		$this->setUserPassword($user, $password);

		$this->userRepository->update([], $user);

		event(new PasswordReset($user));

		Auth::setUser($user);
	}
}
