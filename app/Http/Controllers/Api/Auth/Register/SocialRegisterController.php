<?php

namespace App\Http\Controllers\Api\Auth\Register;

use App\Platform\Auth\Contracts\RegisterStepContract;
use Exception;
use Illuminate\Http\Request;
use Laravel\Socialite\Contracts\User as SocialUser;
use Laravel\Socialite\Facades\Socialite;

class SocialRegisterController extends ApiBaseRegisterController
{
	public function google(Request $request)
	{
		if (!$request->get('token')) {
			return $this->setFailMessage('Token required')->getResponseMessageForJson();
		}
		try {
			$driver = 'google';
			$user = Socialite::driver($driver)->userFromToken($request->get('token'));
			return $this->onUserRetrieved($user, $driver);
		} catch (Exception $e) {
			return $this->getFailResponse($driver);
		}
	}

	public function facebook(Request $request)
	{
		if (!$request->get('token')) {
			return $this->setFailMessage('Token required')->getResponseMessageForJson();
		}
		try {
			$driver = 'facebook';
			$user = Socialite::driver($driver)->userFromToken($request->get('token'));
			return $this->onUserRetrieved($user, $driver);
		} catch (Exception $e) {
			return $this->getFailResponse($driver);
		}
	}

	protected function getFailResponse(string $driver)
	{
		return $this->setFailMessage(translateFormat('auth.social.auth.fail', ['social' => ucfirst($driver)]))->getResponseMessageForJson();
	}

	protected function onUserRetrieved(SocialUser $socialUser, string $provider): array
	{
		$socialId = $socialUser->getId();
		$temporary = $this->getTemporary();
		$dataContainer = $this->getStepsDataContainer();

		$dataContainer->setSocialNetwork($provider)
			->setSocialNetworkId($socialId)
			->setName($socialUser->getName())
			->setAvatar($socialUser->getAvatar())
			->setEmail($socialUser->getEmail())
			->setCurrentStep(RegisterStepContract::STEP_CHOOSE_TYPE)
		;
		$temporary->setData($dataContainer->getData());
		$this->temporaryRepository->update([], $temporary);
		return $this->setSuccessMessage(getTranslate('registration.social.user-found', 'Данные соцсети получены'))
			->setResponseData($dataContainer->toResponse())->getResponseMessageForJson()
			;
	}

}
