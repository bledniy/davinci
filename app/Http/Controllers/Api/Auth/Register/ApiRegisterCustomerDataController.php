<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Auth\Register;

use App\Http\Requests\Auth\Register\RegisterCustomerDataRequest;
use App\Platform\Auth\Contracts\RegisterStepContract;
use App\Platform\Auth\Services\Register\RegisterDataContainerFiller;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;

class ApiRegisterCustomerDataController extends ApiBaseRegisterController
{

	public function index(RegisterCustomerDataRequest $request, UserRepository $userRepository, RegisterDataContainerFiller $filler)
	{
		$this->checkAvailability();
		$temporary = $this->getTemporary();

		$filler->customerData($request, $this->getStepsDataContainer());
		$user = $this->makeUser();

		if (is_null($user)) {
			return $this->setFailMessage(getTranslate('registration.fail'))->getResponseMessageForJson();
		}
		if ($this->getStepsDataContainer()->getSocialNetwork()) {
			$this->associateUser($user);
		}
		// for have all field to response
		$user = $userRepository->find($user->getKey());
		$token = $user->createToken($user->name);
		event(new \Illuminate\Auth\Events\Registered($user));

		$this->temporaryRepository->delete($temporary);
		$this->setSuccessMessage(getTranslate('registration.success', 'Вы успешно зарегистрированы'))
			->setResponseData(['completed' => true])
			->setResponseData(['token' => $token->plainTextToken])
			->setResponseData(['user' => UserResourceFactory::make($user)])
		;
		return new Response($this->getResponseMessageForJson(), Response::HTTP_CREATED);
	}


	protected function checkAvailability($step = ''): void
	{
		parent::checkAvailability(RegisterStepContract::STEP_CHOOSE_TYPE);
	}


}