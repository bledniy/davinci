<?php


namespace App\Http\Controllers\Api\Auth\Register;


use App\Helpers\ResponseHelper;
use App\Http\Controllers\Api\AbstractApiController;
use App\Platform\Auth\Contracts\AuthTokenGenerator;
use App\Platform\Auth\Contracts\RegisterStepContract;
use App\Platform\Auth\Contracts\TokenTypeContract;
use App\Platform\Auth\Services\Register\RegisterStepsDataContainer;
use App\Platform\Auth\Services\Register\RegistrationTokenGenerator;
use App\Repositories\TemporaryRepository;

class InitRegisterTokenController extends AbstractApiController
{
	/**
	 * @var RegistrationTokenGenerator
	 */
	private $tokenGenerator;
	/**
	 * @var TemporaryRepository
	 */
	private $temporaryRepository;

	public function __construct(AuthTokenGenerator $tokenGenerator, TemporaryRepository $temporaryRepository)
	{
		parent::__construct();
		$this->tokenGenerator = $tokenGenerator;
		$this->temporaryRepository = $temporaryRepository;
	}

	public function index()
	{
		$token = $this->tokenGenerator->generate();
		$data = app(RegisterStepsDataContainer::class)->setCurrentStep(RegisterStepContract::STEP_CHOOSE_TYPE)->getData();
		$this->temporaryRepository
			->getBuilder()
			->setKey($token)
			->setData($data)
			->setType(TokenTypeContract::TYPE_REGISTER)
			->build()
		;
		return ['token' => $token, ResponseHelper::STATUS_KEY => ResponseHelper::SUCCESS_KEY];
	}
}