<?php


namespace App\Http\Controllers\Api\Auth\Register;


use App\Http\Requests\Auth\Register\RegisterPerformerDataRequest;
use App\Http\Resources\VerificationResource;
use App\Platform\Auth\Contracts\RegisterStepContract;
use App\Platform\Auth\Services\Register\EmailVerifier;
use App\Platform\Auth\Services\Register\RegisterDataContainerFiller;

class ApiRegisterPerformerDataController extends ApiBaseRegisterController
{

	public function index(RegisterPerformerDataRequest $request, RegisterDataContainerFiller $filler)
	{
		$this->checkAvailability();
		$temporary = $this->getTemporary();
		$dataContainer = $filler->performerData($request, $this->getStepsDataContainer());

		if (!$dataContainer->hasCompletedStep(RegisterStepContract::STEP_DATA_PERFORMER)) {
			$verification = ($verifier = app(EmailVerifier::class, ['dataContainer' => $dataContainer]))->getNewOrRegenerateVerification();
			if ($verifier->isWasCreatedNew() || $verifier->isWasRegenerated()) {
				$verifier->notify();
			}
		} else {
			$verification = (app(EmailVerifier::class, ['dataContainer' => $dataContainer]))->getExistedVerification();
		}

		if ($verification) {
			$this
				->setResponseData(['verification' => VerificationResource::make($verification)])
				->setResponseData(isLocalEnv() ? ['code' => $verification->getCode()] : [])
			;
		}
		$dataContainer->addCompletedStep(RegisterStepContract::STEP_DATA_PERFORMER)->setCurrentStep(RegisterStepContract::STEP_PERFORMER_CONFIRM_EMAIL);

		$temporary->setData($dataContainer->getData());
		$this->temporaryRepository->update([], $temporary);
		$this->successStep()->setResponseData(['data' => $dataContainer->toResponse()]);
		return $this->getResponseMessageForJson();
	}

	protected function checkAvailability($step = ''): void
	{
		parent::checkAvailability(RegisterStepContract::STEP_CHOOSE_TYPE);
	}

}