<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Auth\Register;

use App\Http\Requests\Auth\Register\RegisterStepsRequest;
use App\Platform\Auth\Contracts\RegisterStepContract;
use App\Platform\User\Type\UserTypeHelper;

class ApiRegisterTypeController extends ApiBaseRegisterController
{

	public function index(RegisterStepsRequest $request)
	{
		$temporary = $this->getTemporary();
		$stepsDataContainer = $this->getStepsDataContainer();
		$stepsDataContainer
			->setUserType($request->get('user_type'))
			->setEmail($request->get('email'))
		;

		$nextStep = UserTypeHelper::isCustomer($stepsDataContainer->getUserType()) ? RegisterStepContract::STEP_DATA_CUSTOMER : RegisterStepContract::STEP_DATA_PERFORMER;
		$stepsDataContainer->addCompletedStep(RegisterStepContract::STEP_CHOOSE_TYPE)->setCurrentStep($nextStep);

		$temporary->setData($stepsDataContainer->getData());
		$this->temporaryRepository->update([], $temporary);
		$this->successStep()
			->setResponseData(['data' => $stepsDataContainer->toResponse()])
		;
		return $this->getResponseMessageForJson();
	}
}