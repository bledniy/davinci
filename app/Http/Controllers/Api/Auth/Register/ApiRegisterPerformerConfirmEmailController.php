<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Auth\Register;

use App\Http\Requests\Auth\Register\RegisterPerformerConfirmEmailRequest;
use App\Http\Resources\VerificationResource;
use App\Http\Responses\Verification\VerificationEmailResponse;
use App\Http\Responses\Verification\VerificationFailResponse;
use App\Http\Responses\Verification\VerificationThrottleResponse;
use App\Platform\Auth\Contracts\RegisterStepContract;
use App\Platform\Auth\Services\Register\EmailVerifier;
use App\Platform\Auth\Services\Register\PhoneVerifier;
use App\Platform\Auth\Services\Verification\CodeVerifier;
use Prettus\Validator\Exceptions\ValidatorException;

class ApiRegisterPerformerConfirmEmailController extends ApiBaseRegisterController
{
	/**
	 * @throws ValidatorException
	 */
	public function index(RegisterPerformerConfirmEmailRequest $request): array
	{
		$this->checkAvailability();
		$temporary = $this->getTemporary();

		$dataContainer = $this->getStepsDataContainer();
		if (!$dataContainer->hasCompletedStep(RegisterStepContract::STEP_PERFORMER_CONFIRM_EMAIL)) {
			$verification = ($verifier = app(EmailVerifier::class, ['dataContainer' => $dataContainer]))->getExistedVerification();
			if (!$verification) {
				$verifier->generateVerification();
				$verifier->notify();
				return $this->setFailMessage(translateFormat('verification.email.code-resend-sended', ['%email%' => $dataContainer->getEmail(),]))
					->getResponseMessageForJson()
					;
			}
			if (!CodeVerifier::resolve($verification)->compare($request->get('code'))) {
				return VerificationFailResponse::make()->toResponse();
			}
			$verifier->deleteVerification();

			$dataContainer->addCompletedStep(RegisterStepContract::STEP_PERFORMER_CONFIRM_EMAIL)
				->setCurrentStep(RegisterStepContract::STEP_PERFORMER_CONFIRM_PHONE)
			;
			$dataContainer->setEmailVerified();

			$temporary->setData($dataContainer->getData());
			$this->temporaryRepository->update([], $temporary);

			$verification = ($verifier = app(PhoneVerifier::class, ['dataContainer' => $dataContainer]))->generateVerification();
			$verifier->notify();

		} else {
			$verification = app(PhoneVerifier::class, ['dataContainer' => $dataContainer])->getExistedVerification();
		}
		if ($verification) {
			$this
				->setResponseData(['verification' => VerificationResource::make($verification)])
				->setResponseData(isLocalEnv() ? ['code' => $verification->getCode()] : [])
			;
		}
		$this->successStep()
			->setResponseData(['data' => $dataContainer->toResponse()])
		;
		return $this->getResponseMessageForJson();
	}

	public function resend(): array
	{
		$this->checkAvailability();

		$dataContainer = $this->getStepsDataContainer();

		if ($dataContainer->hasCompletedStep(RegisterStepContract::STEP_PERFORMER_CONFIRM_EMAIL)) {
			return $this->successStep()->getResponseMessageForJson();
		}

		$verifier = app(EmailVerifier::class, ['dataContainer' => $dataContainer]);

		$existedVerification = $verifier->getExistedVerification();
		if ($existedVerification && !$existedVerification->getAvailablityChecker()->isAvailable()) {
			return VerificationThrottleResponse::make($existedVerification)->toResponse();
		}
		$verification = $verifier->getNewOrRegenerateVerification();
		if ($verifier->isWasCreatedNew() || $verifier->isWasRegenerated()) {
			$verifier->notify();
		}

		return VerificationEmailResponse::make($verification)->setResend(true)->setEmail($dataContainer->getEmail())->toResponse();
	}

	protected function checkAvailability($step = ''): void
	{
		parent::checkAvailability(RegisterStepContract::STEP_DATA_PERFORMER);
	}

}