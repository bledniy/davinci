<?php


namespace App\Http\Controllers\Api\Auth\Register;


use App\Http\Requests\Auth\Register\RegisterPerformerConfirmEmailRequest;
use App\Http\Responses\Verification\VerificationFailResponse;
use App\Http\Responses\Verification\VerificationPhoneResponse;
use App\Http\Responses\Verification\VerificationThrottleResponse;
use App\Platform\Auth\Contracts\RegisterStepContract;
use App\Platform\Auth\Services\Register\PhoneVerifier;
use App\Platform\Auth\Services\Verification\CodeVerifier;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;

class ApiRegisterPerformerConfirmPhoneController extends ApiBaseRegisterController
{
	public function index(RegisterPerformerConfirmEmailRequest $request, UserRepository $userRepository)
	{
		$this->checkAvailability();
		$temporary = $this->getTemporary();

		$dataContainer = $this->getStepsDataContainer();
		$verification = ($verifier = app(PhoneVerifier::class, ['dataContainer' => $dataContainer]))->getExistedVerification();
		if (!$verification) {
			$verifier->generateVerification();
			$verifier->notify();
			return $this->setFailMessage(translateFormat('verification.sms.code-resend-sended', ['%phone%' => $dataContainer->getPhone(),]))
				->getResponseMessageForJson()
				;
		}
		if (!CodeVerifier::resolve($verification)->compare($request->get('code'))) {
			return VerificationFailResponse::make()->toResponse();
		}

		$verifier->deleteVerification();
		$dataContainer->pushToCompletedCurrentStep()->setCurrentStep(null);
		$dataContainer->setPhoneVerified();

		$this->temporaryRepository->delete($temporary->getKey());

		$user = $this->makeUser();

		if (is_null($user)) {
			return $this->setFailMessage(getTranslate('registration.fail'))->getResponseMessageForJson();
		}
		if ($this->getStepsDataContainer()->getSocialNetwork()) {
			$this->associateUser($user);
		}
		// for have all field to response
		$user = $userRepository->find($user->getKey());
		$token = $user->createToken($user->name);

		event(new \Illuminate\Auth\Events\Registered($user));
		$this->setSuccessMessage(getTranslate('registration.success'))
			->setResponseData(['completed' => true])
			->setResponseData(['token' => $token->plainTextToken])
			->setResponseData(['user' => UserResourceFactory::make($user)])
		;
		return new Response($this->getResponseMessageForJson(), Response::HTTP_CREATED);
	}

	public function resend()
	{
		$this->checkAvailability();
		$dataContainer = $this->getStepsDataContainer();
		$verifier = app(PhoneVerifier::class, ['dataContainer' => $dataContainer]);

		$existedVerification = $verifier->getExistedVerification();
		if ($existedVerification && !$existedVerification->getAvailablityChecker()->isAvailable()) {
			return VerificationThrottleResponse::make($existedVerification)->toResponse();
		}
		$verification = $verifier->getNewOrRegenerateVerification();
		if ($verifier->isWasCreatedNew() || $verifier->isWasRegenerated()) {
			$verifier->notify();
		}
		return VerificationPhoneResponse::make($verification)->setResend(true)->setPhone($dataContainer->getPhone())->toResponse();
	}

	protected function checkAvailability($step = ''): void
	{
		parent::checkAvailability(RegisterStepContract::STEP_PERFORMER_CONFIRM_EMAIL);
	}


}