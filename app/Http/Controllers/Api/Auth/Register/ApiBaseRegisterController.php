<?php

namespace App\Http\Controllers\Api\Auth\Register;

use App\Helpers\Debug\LoggerHelper;
use App\Http\Controllers\Api\AbstractApiController;
use App\Models\Temporary;
use App\Models\User;
use App\Platform\Auth\Jobs\AssociateUserToSocialNetworkFromRegisterDataContainer;
use App\Platform\Auth\Jobs\MakeUserFromRegisterDataContainer;
use App\Platform\Auth\Services\Register\RegisterStepsDataContainer;
use App\Platform\Auth\Services\Register\RegistrationTokenExtractor;
use App\Repositories\TemporaryRepository;
use Illuminate\Http\Response;

class ApiBaseRegisterController extends AbstractApiController
{
	/**
	 * @var RegistrationTokenExtractor
	 */
	protected $tokenExtractor;
	/**
	 * @var TemporaryRepository
	 */
	protected $temporaryRepository;

	protected $temporary = false;

	protected $stepsDataContainer;

	public function __construct(RegistrationTokenExtractor $tokenExtractor, TemporaryRepository $temporaryRepository)
	{
		parent::__construct();
		$this->tokenExtractor = $tokenExtractor;
		$this->temporaryRepository = $temporaryRepository;
	}

	protected function getTemporary(): Temporary
	{
		if (false === $this->temporary) {
			$this->temporary = $this->temporaryRepository->findByKey($this->tokenExtractor->getToken());
		}
		return $this->temporary;
	}

	protected function getStepsDataContainer(): RegisterStepsDataContainer
	{
		if (is_null($this->stepsDataContainer)) {
			$this->stepsDataContainer = app(RegisterStepsDataContainer::class, ['data' => $this->getTemporary()->getData()]);
		}
		return $this->stepsDataContainer;
	}

	protected function makeUser()
	{
		try {
			return app(MakeUserFromRegisterDataContainer::class, ['dataContainer' => $this->getStepsDataContainer()])->handle();
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
			if (isLocalEnv()) {
				$this->setResponseData(['error' => $e->getMessage()]);
			}
		}
		return null;
	}


	protected function associateUser(User $user)
	{
		//if from social network - associate with network
		return app(AssociateUserToSocialNetworkFromRegisterDataContainer::class, ['dataContainer' => $this->getStepsDataContainer()])
			->handle($user);
	}

	protected function successStep(): ApiBaseRegisterController
	{
		$this->setSuccessMessage(getTranslate('registration.step.success', 'Данные сохранены'));
		return $this;
	}

	protected function checkAvailability($step = ''):void
	{
		if (!$this->getStepsDataContainer()->hasCompletedStep($step)) {
			abort(Response::HTTP_FORBIDDEN);
		}
	}

}