<?php


namespace App\Http\Controllers\Api\Auth\Login;


use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Requests\Auth\Login\CheckLoginRequest;
use App\Http\Requests\User\Login\UserLoginRequest;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Providers\RouteServiceProvider;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;


class LoginController extends AbstractApiController
{
	protected $username = 'email';
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = RouteServiceProvider::HOME;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(UserRepository $userRepository)
	{
		parent::__construct();
		$this->middleware('guest')->except('logout', 'checkLogin');
		$this->userRepository = $userRepository;
	}

	public function checkLogin(CheckLoginRequest $request): array
	{
		$this->setSuccessMessage(getTranslate('validation.login.success', 'Email введен правильно'));
		return $this->getResponseMessageForJson();
	}

	public function username()
	{
		return $this->username;
	}

	public function showLoginForm()
	{
		return '';
	}

	protected function credentials(Request $request)
	{
		return $request->only($this->username());
	}

	public function login(UserLoginRequest $request)
	{
		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if (method_exists($this, 'hasTooManyLoginAttempts') &&
			$this->hasTooManyLoginAttempts($request)) {
			$this->fireLockoutEvent($request);

			$this->sendLockoutResponse($request);
		}
		$this->setUserNameByRequest($request);


		if ($this->attemptLogin($request)) {
			return $this->sendLoginResponse($request);
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}

	protected function sendLoginResponse(Request $request)
	{
		$this->clearLoginAttempts($request);

		if ($response = $this->authenticated($request, $this->guard()->user())) {
			return $response;
		}
		/** @var  $user */
		$user = Auth::user();
		$userData = UserResourceFactory::make($user);
		return [
			'redirect' => $this->redirectPath(),
			'token'    => $user->createToken($user->getFio())->plainTextToken,
			'user'     => $userData,
		];
	}

	protected function attemptLogin(Request $request)
	{
		$credentials = $this->credentials($request);
		$credentials['active'] = 1;
		$user = $this->userRepository->where($credentials)->first();

		if (! $user || ! Hash::check($request->password, $user->password)) {
			throw ValidationException::withMessages([
				'email' => [getTranslate('validation.login.credentials.wrong', 'Не правильный пароль')],
			]);
		}
		Auth::setUser($user);
		return $this->setSuccessMessage(getTranslate('auth.success', 'Вы успешно авторизированы'))->getResponseMessageForJson();
	}

	protected function validateLogin(Request $request)
	{

		//no need validate here, because we have custom request
	}

	protected function setUserNameByRequest(UserLoginRequest $request)
	{
		$this->username = $request->getLoginField();
	}

	protected function guard()
	{
		return Auth::guard('api');
	}

}