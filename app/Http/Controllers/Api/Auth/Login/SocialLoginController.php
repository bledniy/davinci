<?php

namespace App\Http\Controllers\Api\Auth\Login;

use App\Http\Controllers\Api\AbstractApiController;
use App\Models\User;
use App\Models\User\SocialProvider;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Repositories\UserRepository;
use App\Services\Phone\PhoneUkraineFormatter;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Contracts\User as SocialUser;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends AbstractApiController
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(UserRepository $userRepository)
	{
		parent::__construct();
		$this->userRepository = $userRepository;
	}


	public function google(Request $request)
	{
		if (!$request->get('token')) {
			return $this->setFailMessage('Token required')->getResponseMessageForJson();
		}
		try {
			$driver = 'google';
			$user = Socialite::driver($driver)->userFromToken($request->get('token'));
			return $this->onUserRetrieved($user, $driver);
		} catch (Exception $e) {
			return $this->getFailResponse($driver);
		}
	}

	public function facebook(Request $request)
	{
		if (!$request->get('token')) {
			return $this->setFailMessage('Token required')->getResponseMessageForJson();
		}
		try {
			$driver = 'facebook';
			$user = Socialite::driver($driver)->userFromToken($request->get('token'));
			return $this->onUserRetrieved($user, $driver);
		} catch (Exception $e) {
			return $this->getFailResponse($driver);
		}
	}

	protected function getFailResponse(string $driver)
	{
		return $this->setFailMessage(translateFormat('auth.social.auth.fail', ['social' => ucfirst($driver)]))->getResponseMessageForJson();
	}

	protected function onUserRetrieved(SocialUser $socialUser, string $provider)
	{
		if (!$this->getExistedUser($socialUser)) {
			return $this->setFailMessage(getTranslate('auth.social.auth.user-not-found'))->getResponseMessageForJson();
		}

		$socialId = $socialUser->getId();
		$socialProvider = SocialProvider::where('provider_id', $socialId)->where('provider', $provider)->first();

		$user = ($socialProvider) ? $socialProvider->getUser() : $this->onProviderNotFound($socialUser, $provider);
		Auth::setUser($user);
		event(new \Illuminate\Auth\Events\Login('api', $user, false));
		$token = $user->createToken($user->getFio());

		return $this
			->setSuccessMessage(getTranslate('auth.success', 'Вы успешно авторизированы'))
			->setResponseData(['token' => $token->plainTextToken])
			->setResponseData(['user' => UserResourceFactory::make($user)])
			->getResponseMessageForJson()
			;
	}

	protected function getExistedUser(SocialUser $socialUser): ?User
	{
		$email = $socialUser->getEmail();
		$phone = trim($socialUser->phone ?? '');
		if ($phone) {
			$phone = PhoneUkraineFormatter::formatPhone($phone);
		}
		return $this->userRepository->where('email', $email)
			->when($phone, function ($query, $phone) {
				return $query->orWhere('phone', $phone);
			})->first()
			;
	}

	protected function onProviderNotFound(SocialUser $socialUser, string $provider = ''): User
	{
		$socialId = $socialUser->getId();

		$createProviderData = ['provider' => $provider, 'provider_id' => $socialId];
		/** @var  $user User */
		$user = $this->getExistedUser($socialUser);
		$user->socialProviders()->create($createProviderData);
		return $user;
	}

}
