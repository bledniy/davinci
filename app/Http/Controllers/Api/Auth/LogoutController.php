<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Api\AbstractApiController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LogoutController extends AbstractApiController
{
	public function logout()
	{
		$user = Auth::user();
		if (!$user) {
			return new Response($this->setFailMessage(getTranslate('auth.logout.fail', 'Вы не авторизированы'))->getResponseMessageForJson(), Response::HTTP_NOT_FOUND);
		}
		$user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
		return $this->setSuccessMessage(getTranslate('auth.logout.success', 'Вы успешно вышли из аккаунта'))->getResponseMessageForJson();
	}
}