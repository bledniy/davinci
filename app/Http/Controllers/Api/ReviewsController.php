<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReviewResource;
use App\Repositories\ContentRepository;
use App\Services\ApiData\ReviewsApiData;

class ReviewsController extends Controller
{
	public function index(ReviewsApiData $apiData)
	{
		return $apiData->getResponse();
	}
}
