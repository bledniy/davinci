<?php declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\DataContainers\Admin\News\SearchDataContainer;
use App\Http\Controllers\Controller;
use App\Http\Resources\NewsCategoryResource;
use App\Repositories\NewsCategoryRepository;
use Illuminate\Http\Request;

class NewsCategoryController extends Controller
{
	/**
	 * @var NewsCategoryRepository
	 */
	private $repository;

	public function __construct(NewsCategoryRepository $repository)
	{
		$this->repository = $repository;
	}

	public function index(Request $request, SearchDataContainer $dataContainer)
	{
		$dataContainer->fillFromRequest($request);
		$categories = $this->repository->getListPublic();
		$categories->load('lang');

		return NewsCategoryResource::collection($categories);
	}
}
