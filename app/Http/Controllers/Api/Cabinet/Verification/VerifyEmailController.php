<?php

namespace App\Http\Controllers\Api\Cabinet\Verification;

use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Responses\Verification\VerificationChangeCabinetContactsResponse;
use App\Http\Responses\Verification\VerificationEmailResponse;
use App\Http\Responses\Verification\VerificationFailResponse;
use App\Http\Responses\Verification\VerificationThrottleResponse;
use App\Platform\Auth\Services\Verification\CodeVerifier;
use App\Platform\Cabinet\Verification\VerifyEmail;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class VerifyEmailController extends AbstractCabinetController
{
	protected $resend = false;

	protected function sendBody($verifier, $verification)
	{
		$user = $this->getUser();

		if ($verifier->isWasCreatedNew() || $verifier->isWasRegenerated()) {
			$verifier->notify();
		}

		return VerificationEmailResponse::make($verification)->setResend($this->resend)->setEmail($user->getEmail())->toResponse();
	}

	public function send()
	{
		$user = $this->getUser();
		$verifier = app(VerifyEmail::class, compact('user'));
		if ($verifier->getExistedVerification()) {
			return $this->resend();
		}
		$verification = $verifier->generateVerification();

		return $this->sendBody($verifier, $verification);
	}

	public function resend()
	{
		$user = $this->getUser();
		$verifier = app(VerifyEmail::class, compact('user'));
		$verification = $verifier->getExistedVerification();
		if (!$verification) {
			return $this->send();
		}
		if (!$verification->getAvailablityChecker()->isAvailable()) {
			return VerificationThrottleResponse::make($verification)->toResponse();
		}
		$this->resend = true;
		$verification = $verifier->getNewOrRegenerateVerification();

		return $this->sendBody($verifier, $verification);
	}

	public function verify(Request $request, UserRepository $userRepository)
	{
		$user = $this->getUser();
		$verifier = app(VerifyEmail::class, compact('user'));
		$verification = $verifier->getExistedVerification();
		if (!$verification) {
			abort(404, 'First send code');
		}
		if ($verification->isVerified()) {
			return VerificationChangeCabinetContactsResponse::make()->toResponse();
		}
		if (!CodeVerifier::resolve($verification)->compare($request->get('code'))) {
			return VerificationFailResponse::make()->toResponse();
		}
		$verifier->verified();
		$user->getSetter()->setEmailVerifiedAt(now());
		$userRepository->update([], $user);

		return VerificationChangeCabinetContactsResponse::make()->toResponse();
	}

}