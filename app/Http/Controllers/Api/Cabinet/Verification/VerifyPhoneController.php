<?php


namespace App\Http\Controllers\Api\Cabinet\Verification;

use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Responses\Verification\VerificationChangeCabinetContactsResponse;
use App\Http\Responses\Verification\VerificationFailResponse;
use App\Http\Responses\Verification\VerificationPhoneResponse;
use App\Http\Responses\Verification\VerificationThrottleResponse;
use App\Platform\Auth\Services\Verification\CodeVerifier;
use App\Platform\Cabinet\Verification\VerifyPhone;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class VerifyPhoneController extends AbstractCabinetController
{
	protected $resend = false;

	protected function sendBody($verifier, $verification)
	{
		$user = $this->getUser();

		if ($verifier->isWasCreatedNew() || $verifier->isWasRegenerated()) {
			$verifier->notify();
		}

		return VerificationPhoneResponse::make($verification)->setResend($this->resend)->setPhone($user->getPhone())->toResponse();
	}

	public function send()
	{
		$user = $this->getUser();
		$verifier = app(VerifyPhone::class, compact('user'));

		if ($verifier->getExistedVerification()) {
			return $this->resend();
		}
		$verification = $verifier->generateVerification();

		return $this->sendBody($verifier, $verification);
	}

	public function resend()
	{
		$user = $this->getUser();
		$verifier = app(VerifyPhone::class, compact('user'));
		$verification = $verifier->getExistedVerification();
		if (!$verification) {
			return $this->send();
		}
		if (!$verification->getAvailablityChecker()->isAvailable()) {
			return VerificationThrottleResponse::make($verification)->toResponse();
		}
		$this->resend = true;
		$verification = $verifier->getNewOrRegenerateVerification();

//		$verifier->notify();
		return $this->sendBody($verifier, $verification);
	}

	public function verify(Request $request, UserRepository $userRepository)
	{
		$user = $this->getUser();
		$verifier = app(VerifyPhone::class, compact('user'));
		$verification = $verifier->getExistedVerification();
		if (!$verification) {
			abort(404, 'First send code');
		}
		if ($verification->isVerified()) {
			return VerificationChangeCabinetContactsResponse::make()->toResponse();
		}
		if (!CodeVerifier::resolve($verification)->compare($request->get('code'))) {
			return VerificationFailResponse::make()->toResponse();
		}
		$verifier->verified();
		$user->getSetter()->setPhoneVerifiedAt(now());
		$userRepository->update([], $user);

		return VerificationChangeCabinetContactsResponse::make()->toResponse();
	}
}