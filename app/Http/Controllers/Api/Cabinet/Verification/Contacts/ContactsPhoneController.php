<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Cabinet\Verification\Contacts;

use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Requests\User\Cabinet\ChangePhoneRequest;
use App\Http\Responses\Verification\VerificationChangeCabinetContactsResponse;
use App\Http\Responses\Verification\VerificationFailResponse;
use App\Http\Responses\Verification\VerificationPhoneResponse;
use App\Http\Responses\Verification\VerificationThrottleResponse;
use App\Models\Verification;
use App\Platform\Auth\Services\Verification\CodeVerifier;
use App\Platform\Cabinet\Contacts\ChangePhone;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use function abort;
use function app;
use function getTranslate;

class ContactsPhoneController extends AbstractCabinetController
{
	protected $resend = false;

	public function send()
	{
		$user = $this->getUser();
		$verifier = app(ChangePhone::class, compact('user'));
		if ($verifier->getExistedVerification()) {
			return $this->resend();
		}
		$verification = $verifier->generateVerification();

		return $this->sendBody($verifier, $verification);
	}

	public function resend()
	{
		$user = $this->getUser();
		$verifier = app(ChangePhone::class, compact('user'));
		$verification = $verifier->getExistedVerification();
		if (!$verification) {
			return $this->send();
		}
		if (!$verification->getAvailablityChecker()->isAvailable()) {
			return VerificationThrottleResponse::make($verification)->toResponse();
		}
		$this->resend = true;
		$verification = $verifier->getNewOrRegenerateVerification();

		return $this->sendBody($verifier, $verification);
	}

	public function verify(Request $request)
	{
		$user = $this->getUser();
		$verifier = app(ChangePhone::class, compact('user'));
		$verification = $verifier->getExistedVerification();
		if (!$verification) {
			abort(404, 'First send code');
		}
		if ($verification->isVerified()) {
			return VerificationChangeCabinetContactsResponse::make()->toResponse();
		}
		if (!CodeVerifier::resolve($verification)->compare($request->get('code'))) {
			return VerificationFailResponse::make()->toResponse();
		}
		$verifier->verified();

		return VerificationChangeCabinetContactsResponse::make()->toResponse();
	}

	public function change(ChangePhoneRequest $request, UserRepository $userRepository)
	{
		$phone = $request->getPhone();
		$user = $this->getUser();

		$verifier = app(ChangePhone::class, compact('user'));
		if (!($verification = $verifier->getExistedVerification()) || !$verification->isVerified()) {
			return $this->setFailMessage(getTranslate('cabinet.phone.change.fail', 'Сначала подтвердите код из смс'))->getResponseMessageForJson();
		}

		$verifier->deleteVerification();
		$user->getSetter()->setPhone($phone);
		$userRepository->update([], $user);

		return $this->setSuccessMessage(getTranslate('cabinet.phone.changed'))->getResponseMessageForJson();
	}

	protected function sendBody(ChangePhone $verifier, Verification $verification)
	{
		$user = $this->getUser();
		if ($verifier->isWasCreatedNew() || $verifier->isWasRegenerated()) {
			$verifier->notify();
		}

		return VerificationPhoneResponse::make($verification)->setResend($this->resend)->setPhone($user->getPhone())->toResponse();
	}
}