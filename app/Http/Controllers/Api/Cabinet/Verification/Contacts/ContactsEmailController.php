<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Cabinet\Verification\Contacts;

use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Requests\User\Cabinet\ChangeEmailRequest;
use App\Http\Responses\Verification\VerificationChangeCabinetContactsResponse;
use App\Http\Responses\Verification\VerificationEmailResponse;
use App\Http\Responses\Verification\VerificationFailResponse;
use App\Http\Responses\Verification\VerificationThrottleResponse;
use App\Platform\Auth\Services\Verification\CodeVerifier;
use App\Platform\Cabinet\Contacts\ChangeEmail;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use function abort;
use function app;
use function getTranslate;

class ContactsEmailController extends AbstractCabinetController
{
	protected $resend = false;

	protected function sendBody(ChangeEmail $verifier, $verification): array
	{
		$user = $this->getUser();

		if ($verifier->isWasCreatedNew() || $verifier->isWasRegenerated()) {
			$verifier->notify();
		}

		return VerificationEmailResponse::make($verification)->setResend($this->resend)->setEmail($user->getEmail())->toResponse();
	}

	public function send()
	{
		$user = $this->getUser();
		$verifier = app(ChangeEmail::class, compact('user'));
		if ($verifier->getExistedVerification()) {
			return $this->resend();
		}
		$verification = $verifier->generateVerification();

		return $this->sendBody($verifier, $verification);
	}

	public function resend()
	{
		$user = $this->getUser();
		$verifier = app(ChangeEmail::class, compact('user'));
		$verification = $verifier->getExistedVerification();
		if (!$verification) {
			return $this->send();
		}
		if (!$verification->getAvailablityChecker()->isAvailable()) {
			return VerificationThrottleResponse::make($verification)->toResponse();
		}
		$this->resend = true;
		$verification = $verifier->getNewOrRegenerateVerification();

		return $this->sendBody($verifier, $verification);
	}

	public function verify(Request $request)
	{
		$user = $this->getUser();
		$verifier = app(ChangeEmail::class, compact('user'));
		$verification = $verifier->getExistedVerification();
		if (!$verification) {
			abort(404, 'First send code');
		}
		if ($verification->isVerified()) {
			return VerificationChangeCabinetContactsResponse::make()->toResponse();
		}
		if (!CodeVerifier::resolve($verification)->compare($request->get('code'))) {
			return VerificationFailResponse::make()->toResponse();
		}
		$verifier->verified();

		return VerificationChangeCabinetContactsResponse::make()->toResponse();
	}

	public function change(ChangeEmailRequest $request, UserRepository $userRepository)
	{
		$email = $request->get('email');
		$user = $this->getUser();

		$verifier = app(ChangeEmail::class, compact('user'));
		if (!($verification = $verifier->getExistedVerification()) || !$verification->isVerified()) {
			return $this->setFailMessage(getTranslate('cabinet.email.change.fail', 'Сначала подтвердите код из смс'))->getResponseMessageForJson();
		}
		$verifier->deleteVerification();
		$user->getSetter()->setEmail($email);
		$userRepository->update([], $user);

		return $this->setSuccessMessage(getTranslate('cabinet.email.changed', 'E-mail изменен'))->getResponseMessageForJson();
	}


}