<?php

namespace App\Http\Controllers\Api\Cabinet;

use App\Http\Requests\User\Cabinet\DiplomaRequest;
use App\Http\Resources\User\UserDiplomaResource;
use App\Platform\Cabinet\Diploma\CheckDiplomaBelongsToUserService;
use App\Platform\Cabinet\Diploma\DiplomaRemoverService;
use App\Platform\Cabinet\Uploads\PerformerDiplomaImagesUploader;
use App\Repositories\PerformerDiplomaRepository;
use Illuminate\Http\Response;

class PerformerDiplomaController extends AbstractCabinetController
{

	/**
	 * @var PerformerDiplomaRepository
	 */
	private $repository;

	public function __construct(PerformerDiplomaRepository $diplomasRepository)
	{
		parent::__construct();
		$this->repository = $diplomasRepository;
	}

	public function index()
	{
		$user = $this->getUser();
		$diplomas = $this->repository->findByUser($user);

		return UserDiplomaResource::collection($diplomas);
	}

	public function store(DiplomaRequest $request, PerformerDiplomaImagesUploader $uploader)
	{
		$user = $this->getUser();
		$input = $request->only($request->getFillableFields());
		$input['image'] = $uploader->upload($request->file('image'));
		$diploma = $this->repository->createRelated($input, $user);
		$this->setSuccessMessage(getTranslate('cabinet.diploma.create.success', 'Сертификат/диплом успешно добавлен'))
			->setResponseData(['diploma' => UserDiplomaResource::make($diploma)])
		;

		return $this->getResponseMessageForJson();
	}

	public function update(DiplomaRequest $request, $diplomaId, PerformerDiplomaImagesUploader $uploader)
	{
		$user = $this->getUser();
		if (
			($diploma = $this->repository->find($diplomaId))
			&& !CheckDiplomaBelongsToUserService::make($diploma, $user)->check()
		) {
			$this->onNotBelongs();

			return null;
		}

		$input = $request->only($request->getFillableFields('image'));
		if ($request->hasFile('image')) {
			$input['image'] = $uploader->upload($request->file('image'));
		}
		$this->repository->update($input, $diploma);
		$this->setSuccessMessage(getTranslate('cabinet.diploma.update.success', 'Сертификат/диплом успешно обновлен'));
		$this->setResponseData(['diploma' => UserDiplomaResource::make($diploma)]);

		return $this->getResponseMessageForJson();
	}


	public function destroy($diploma)
	{
		$diploma = $this->repository->find($diploma);
		if (!$diploma) {
			abort(404);
		}
		$user = $this->getUser();
		if (!CheckDiplomaBelongsToUserService::make($diploma, $user)->check()) {
			$this->onNotBelongs();

			return null;
		}
		DiplomaRemoverService::make($diploma)->remove();

		return $this->setSuccessMessage(getTranslate('cabinet.diploma.delete.success', 'Сертификат/диплом удален'))->getResponseMessageForJson();
	}

	protected function onNotBelongs()
	{
		abort(Response::HTTP_FORBIDDEN);
	}
}