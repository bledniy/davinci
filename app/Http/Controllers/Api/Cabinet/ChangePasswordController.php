<?php


namespace App\Http\Controllers\Api\Cabinet;

use App\Http\Requests\User\Cabinet\ChangePasswordRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends AbstractCabinetController
{

	public function submit(ChangePasswordRequest $request, UserRepository $userRepository)
	{
		$user = $this->getUser();
		$this->setFailMessage(getTranslate('users.current-password-invalid'));
		$passwordsMatches = Hash::check($request->get('password'), $user->password);
		if ($passwordsMatches) {
			$newPassword = Hash::make($request->get('password_new'));
			$userRepository->update(['password' => $newPassword], $user->getKey());
			$this->setSuccessMessage(getTranslate('cabinet.password.credentials-updated'));
		} else if ($request->expectsJson()) {
			return \Illuminate\Support\Facades\Response::json(
				['errors' => ['password' => [$this->getMessage()]]],
				Response::HTTP_UNPROCESSABLE_ENTITY
			);
		}

		return $this->getResponseMessageForJson();
	}
}