<?php


namespace App\Http\Controllers\Api\Cabinet;


use App\Http\Requests\User\Cabinet\PerformerServiceRequest;
use App\Http\Resources\User\UserServiceResource;
use App\Platform\Cabinet\UserServices\CheckServiceBelongsToUserService;
use App\Platform\Cabinet\UserServices\ServicesRemoverService;
use App\Repositories\PerformerServiceRepository;
use Illuminate\Http\Response;

class PerformerServicesController extends AbstractCabinetController
{

	/**
	 * @var PerformerServiceRepository
	 */
	private $servicesRepository;

	public function __construct(PerformerServiceRepository $servicesRepository)
	{
		parent::__construct();
		$this->servicesRepository = $servicesRepository;
	}

	public function index()
	{
		$user = $this->getUser();
		$services = $this->servicesRepository->findByUser($user);
		return UserServiceResource::collection($services);
	}

	public function store(PerformerServiceRequest $request)
	{
		$user = $this->getUser();
		$input = $request->only($request->getFillableFields());
		$service = $this->servicesRepository->createRelated($input, $user);
		$this->setSuccessMessage(getTranslate('cabinet.services.create.success', 'Услуга успешно создана'));

		return $this->setResponseData(['service' => UserServiceResource::make($service)])->getResponseMessageForJson();
	}

	public function update($id, PerformerServiceRequest $request)
	{
		$user = $this->getUser();
		$service = $this->servicesRepository->find($id);

		$input = $request->only($request->getFillableFields());

		if (!CheckServiceBelongsToUserService::make($service, $user)->check()) {
			$this->onNotBelongs();
		}
		$this->servicesRepository->update($input, $service);
		$this->setSuccessMessage(getTranslate('cabinet.services.update.success', 'Услуга успешно обновлена'));

		return $this->setResponseData(['service' => UserServiceResource::make($service)])->getResponseMessageForJson();
	}

	public function destroy($service)
	{
		$service = $this->servicesRepository->find($service);
		if (!$service) {
			abort(404);
		}
		$user = $this->getUser();
		if (!CheckServiceBelongsToUserService::make($service, $user)->check()) {
			$this->onNotBelongs();
		}
		ServicesRemoverService::make($service)->remove();
		return $this->setSuccessMessage(getTranslate('cabinet.services.delete.success', 'Услуга удалена'))->getResponseMessageForJson();
	}

	protected function onNotBelongs()
	{
		abort(Response::HTTP_FORBIDDEN, getTranslate('cabinet.services.owner.fail', 'Данная услуга вам не принадлежит'));
	}
}