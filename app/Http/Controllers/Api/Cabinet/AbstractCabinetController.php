<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Cabinet;

use App\Http\Controllers\Api\AbstractApiController;
use App\Models\User;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\UserRepository;

class AbstractCabinetController extends AbstractApiController
{
	/**
	 * @var UserRepository
	 */
	protected $userRepository;

	public function __construct()
	{
		parent::__construct();
		$this->userRepository = app(UserRepository::class);
	}

	public function getUser(): User
	{
		return app(AuthenticatedUserResolver::class)->getUser();
	}
}