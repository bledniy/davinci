<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Cabinet\PremiumAccount;

use App\DataContainers\Platform\Payment\UserPaymentSearchContainer;
use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Resources\UserPaymentResource;
use App\Http\Resources\UserPremiumResource;
use App\Platform\ProAccount\UserProAccountPayments;
use App\Repositories\UserPaymentRepositoryEloquent;
use App\Services\Payment\Portmone\CreatePaymentData;
use App\Services\Payment\Portmone\PortmoneConfigData;
use App\Services\User\Payment\PaymentUserPremiumServiceInterface;

class PremiumAccountController extends AbstractCabinetController
{
	/**
	 * @var UserProAccountPayments
	 */
	private $proAccountPayments;
	/**
	 * @var UserPaymentRepositoryEloquent
	 */
	private $userPaymentRepository;

	public function __construct(UserProAccountPayments $proAccountPayments, UserPaymentRepositoryEloquent $userPaymentRepository)
	{
		parent::__construct();
		$this->proAccountPayments = $proAccountPayments;
		$this->userPaymentRepository = $userPaymentRepository;
	}

	public function index(): array
	{
		$user = $this->getUser();
		$userPremiumService = $user->getPremiumAccountService();
		$userProAccount = $userPremiumService->getProAccount();
		if ($userProAccount) {
			$userProAccount->load('premium');
		}

		return [
			'has_premium' => $userPremiumService->isActivePremium(),
			'can_apply_free_pro_account' => $userPremiumService->canApplyFreeProAccount(),
			'user_premium' => $userProAccount ? UserPremiumResource::make($userProAccount) : null,
			'payments_history' => UserPaymentResource::collection(
				$this->userPaymentRepository->findMany(UserPaymentSearchContainer::create()->setUser($user)->setNotNullReason())
			),
		];
	}

	public function applyFreePremium()
	{
		$user = $this->getUser();

		return UserPremiumResource::make(
			$user->getPremiumAccountService()->activateFreeProAccount()
		);
	}

	public function createPaymentForm(
		PaymentUserPremiumServiceInterface $paymentUserPremiumService,
		PortmoneConfigData                 $configData
	): array
	{
		$payment = $paymentUserPremiumService->getBilling($this->getUser());
		($dataCreate = new CreatePaymentData($configData->getPayeeId(), $payment->getNeedPay()))
			->setDescription(getTranslate('payments.premium.description', 'Покупка премиум аккаунта'))
			->setOrderNumber($payment->getKey())
			->setExpiresTime(now()->addMinutes(10)->diffInSeconds(now()))
			->setLang(getCurrentLocaleCode())
			->setSuccessRedirectUrl(route('premium.payment.callback'))
			->setFailureRedirectUrl(route('premium.payment.callback'))
		;

		return $dataCreate->toArray();
	}

}