<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Cabinet\PremiumAccount;

use App\Events\Cabinet\PaymentCallbackEvent;
use App\Http\Controllers\Api\AbstractApiController;
use App\Services\Payment\Portmone\PortmonePaymentServiceInterface;
use Illuminate\Http\Request;

final class PremiumAccountPaymentController extends AbstractApiController
{

	public function paymentCallback(Request $request, PortmonePaymentServiceInterface $paymentService)
	{
		if ($paymentService->isResponseValid($request)) {
			$response = $paymentService->getParsedResponse($request);
			PaymentCallbackEvent::dispatch($response);
			$lang = $request->get('LANG', getCurrentLocaleCode());

			return redirect(
				frontendUrl(
					sprintf(
						'%s/premium?payment=%s',
						$lang,
						$response->getStatus()->isPayed() ? 'success' : 'fail'
					)
				)
			);
		}

		return redirect(frontendUrl('/premium'));
	}
}