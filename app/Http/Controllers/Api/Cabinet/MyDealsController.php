<?php


namespace App\Http\Controllers\Api\Cabinet;


use App\DataContainers\Platform\Order\SearchCabinetOrdersContainer;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order\Order;
use App\Platform\Order\Voters\Review\IsCustomerCanAddReview;
use App\Platform\Order\Voters\Review\IsPerformerCanAddReview;
use App\Platform\User\Type\UserTypeHelper;
use App\Repositories\Order\OrderRepository;
use Illuminate\Http\Request;

class MyDealsController extends AbstractCabinetController
{

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	public function __construct(OrderRepository $orderRepository)
	{
		parent::__construct();
		$this->orderRepository = $orderRepository;
	}

		public function getActiveDeals(Request $request)
		{
			$user = $this->getUser();
			$container = $this->makeSearchContainerFromRequest($request);
			(UserTypeHelper::isPerformer($user->getType())) ? $container->setPerformerId($user->getKey()) : $container->setCustomerId($user->getKey());
			$container->setWhereClosed(false);
			$deals = $this->orderRepository->getOrdersCabinet($container);
			$this->orderRepository->loadForDeals($deals);
			return OrderResource::collection($deals);
		}

	public function getCompletedDeals(Request $request)
	{
		$user = $this->getUser();
		$container = $this->makeSearchContainerFromRequest($request);
		$isPerformer = (UserTypeHelper::isPerformer($user->getType()));
		$isPerformer ? $container->setPerformerId($user->getKey()) : $container->setCustomerId($user->getKey());
		$container->setWhereClosed(true)->setPaginate(true);
		$deals = $this->orderRepository->getOrdersCabinet($container);
		$this->orderRepository->loadForDeals($deals);

		$classPermission = $isPerformer ? IsPerformerCanAddReview::make() : IsCustomerCanAddReview::make();
		$fn = function (array $data, Order $order) use ($classPermission, $user) {
			$data['permissions'] = [
				'can_add_review' => $classPermission->check($order, $user),
			];
			return $data;
		};
		OrderResource::addCallback($fn);
		return OrderResource::collection($deals);
	}

	protected function makeSearchContainerFromRequest(Request $request): SearchCabinetOrdersContainer
	{
		return app(SearchCabinetOrdersContainer::class)->fillFromRequest($request);
	}

}