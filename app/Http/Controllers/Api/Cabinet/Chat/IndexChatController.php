<?php


namespace App\Http\Controllers\Api\Cabinet\Chat;


use App\DataContainers\Platform\Chat\ChatQueryContainer;
use App\Helpers\Dev\QueryDumpFormatter;
use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Resources\Chat\ChatResource;
use App\Models\Chat\Chat;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Request;

class IndexChatController extends AbstractCabinetController
{
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(ChatRepository $chatRepository)
	{
		parent::__construct();
		$this->chatRepository = $chatRepository;
	}

	public function index(Request $request, ChatQueryContainer $container)
	{
		$user = $this->getUser();
		$container->fillFromRequest($request);
		$container->setUser($user);
		$chats = $this->chatRepository->getList($container);
		$lastMessages = $this->chatRepository->getChatsLastMessages($chats->pluck('id')->toArray())->keyBy('chat_id');
		$chats->load(['users' => function (BelongsToMany $q) use ($user) {
			$q->where($user->getForeignKey(), '!=', $user->getKey());
		},
		]);
		$chats->map(function(Chat $chat) use ($lastMessages){
			if ($user = $chat->users->first()) {
				$chat->setRelation('userTo', $user);
			}
			$chat->unsetRelation('users');
			if ($lastMessage = $lastMessages->get($chat->getKey())) {
				$chat->setRelation('lastMessage', $lastMessage);
			}
		    return $chat;
		});
		/** @var  $res ChatResource */
		$res = ChatResource::collection($chats);
		ChatResource::setUser($user);

		return $res;

	}

}