<?php


namespace App\Http\Controllers\Api\Cabinet\Chat;


use App\DataContainers\Platform\Chat\ChatQueryContainer;
use App\Helpers\Dev\QueryDumpFormatter;
use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Resources\Chat\ChatMessageResource;
use App\Http\Resources\Chat\ChatResource;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Models\User;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Platform\Contract\Chat\FavoritesChatContract;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class FavoritesChatController extends AbstractCabinetController
{
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(ChatRepository $chatRepository)
	{
		parent::__construct();
		$this->chatRepository = $chatRepository;
	}


	public function toggle($chatId)
	{
		$currentUser = $this->getUser();
		if (!$this->chatRepository->isUserInChat($currentUser, $chatId)) {
			$this->throwAccessDenied();
		}

		$res = $this->chatRepository->toggleChatFavorite($chatId, $currentUser);
		$this->setSuccessMessage(
			$res === FavoritesChatContract::SUCCESS_ATTACHED
				? getTranslate('chat.favorites.attached')
				: getTranslate('chat.favorites.detached')
		);
		return $this->getResponseMessageForJson();
	}

}