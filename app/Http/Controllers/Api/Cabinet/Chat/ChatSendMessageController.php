<?php


namespace App\Http\Controllers\Api\Cabinet\Chat;


use App\Events\Chat\MessageSubmitted;
use App\Events\Chat\MessageToUsersSubmitted;
use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Requests\User\Cabinet\Chat\SendFileMessageRequest;
use App\Http\Requests\User\Cabinet\Chat\SendMessageRequest;
use App\Http\Resources\Chat\ChatMessageResource;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Platform\Cabinet\Display\UserLastActivity;
use App\Platform\Chat\Jobs\ChatMessageCreatorJob;
use App\Platform\Chat\Uploaders\ChatFileUploader;
use App\Platform\Contract\Chat\ChatMessageTypesContract;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Support\Str;

class ChatSendMessageController extends AbstractCabinetController
{
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(ChatRepository $chatRepository)
	{
		parent::__construct();
		$this->chatRepository = $chatRepository;
	}


	public function store($chatId, SendMessageRequest $request)
	{
		$currentUser = $this->getUser();
		/** @var  $chat Chat */
		$chat = $this->chatRepository->show($chatId, $currentUser);
		$this->abortIfNull($chat);

		$message = ChatMessageCreatorJob::make($chat, $currentUser, (string)$request->get('message'))->create();
		event(new MessageSubmitted($chat, $message));
		$this->notifyOthers($chat, $message);

		return ChatMessageResource::make($message);
	}

	public function files($chatId, SendFileMessageRequest $request, ChatFileUploader $uploader)
	{
		$currentUser = $this->getUser();
		/** @var  $chat Chat */
		$chat = $this->chatRepository->show($chatId, $currentUser);
		$this->abortIfNull($chat);
		$files = $request->file('files');
		$uploader->appendToPath($chat->getKey());
		$messages = [];

		$messageCreator = ChatMessageCreatorJob::make($chat, $currentUser)->withType(ChatMessageTypesContract::TYPE_FILE);
		foreach ($files as $file) {
			$fileInfo = [
				'file'          => $uploader->upload($file),
				'original_name' => $file->getClientOriginalName(),
				'extension'     => $file->getClientOriginalExtension(),
				'mime'          => $file->getClientMimeType(),
				'is_image'      => Str::startsWith($file->getClientMimeType(), 'image'),
			];
			$fileInfo = json_encode($fileInfo, JSON_THROW_ON_ERROR);

			$message = $messageCreator->setMessage($fileInfo)->create();
			event(new MessageSubmitted($chat, $message));
			$this->notifyOthers($chat, $message);
			$messages[] = $message;
		}
		return ChatMessageResource::collection(collect($messages));
	}

	protected function notifyOthers(Chat $chat, ChatMessage $chatMessage):void
	{
		$usersChat = $this->chatRepository->getChatUsers($chat);
		event(new MessageToUsersSubmitted($chatMessage, $usersChat));

	}

}