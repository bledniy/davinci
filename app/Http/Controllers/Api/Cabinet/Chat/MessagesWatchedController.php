<?php

namespace App\Http\Controllers\Api\Cabinet\Chat;


use App\Events\Chat\MessagesWatchedEvent;
use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Requests\User\Cabinet\Chat\MessagesWatchedRequest;
use App\Http\Resources\Chat\ChatMessageResource;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Repositories\Chat\ChatRepository;

class MessagesWatchedController extends AbstractCabinetController
{
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(ChatRepository $chatRepository)
	{
		parent::__construct();
		$this->chatRepository = $chatRepository;
	}

	public function watched($chatId, MessagesWatchedRequest $request)
	{
		$currentUser = $this->getUser();
		/** @var  $chat Chat */
		$chat = $this->chatRepository->show($chatId, $currentUser);
		$this->abortIfNull($chat);
		$ids = $request->getWatchedIds();
		$messages = $chat->messages()->whereIn('id', $ids)->where('is_watched', false)->get();
		if ($messages->isNotEmpty()) {
			$messages->map(function(ChatMessage $message){
				return $message->getSetter()->setIsWatched(true)->getEntity();
			});
			$chat->messages()->whereIn('chat_messages.id', $messages->pluck('id')->toArray())->update(['is_watched' => true]);
			event(new MessagesWatchedEvent($chat, $messages));
		}

		return ChatMessageResource::collection($messages);

	}

}