<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Cabinet\Chat;

use App\DataContainers\Platform\Chat\ChatQueryContainer;
use App\Helpers\Dev\QueryDumpFormatter;
use App\Http\Controllers\Api\Cabinet\AbstractCabinetController;
use App\Http\Resources\Chat\ChatMessageResource;
use App\Http\Resources\Chat\ChatResource;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Models\User;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;

class ShowChatController extends AbstractCabinetController
{
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(ChatRepository $chatRepository)
	{
		parent::__construct();
		$this->chatRepository = $chatRepository;
	}

	public function show($chatId)
	{
		$currentUser = $this->getUser();
		/** @var  $chat Chat */
		$chat = $this->chatRepository->show($chatId, $currentUser);
		$this->abortIfNull($chat);
		$chat->load('users');
		$chat->users->map(function (User $user) use ($currentUser) {
			$user->setAttribute('is_current', $currentUser->getKey() === $user->getKey());
		});
		/** @var  $messages LengthAwarePaginator */
		$messages = $chat->messages()->latest()->paginate();
		$messages->setPath(route('chats.show.load', $chatId));
		$chat->setRelation('messages', $messages);
		$res = ChatResource::make($chat);
		ChatResource::setUser($currentUser);

		$res->additional(['users' => UserResourceFactory::collection($chat->users)]);

		return $res;
	}

	public function loadMessages($chatId): AnonymousResourceCollection
	{
		$currentUser = $this->getUser();
		if (!$this->chatRepository->isUserInChat($currentUser, $chatId)) {
			$this->throwAccessDenied();
		}
		$chat = $this->chatRepository->find($chatId);
		$messages = $chat->messages()->latest()->paginate();

		return ChatMessageResource::collection($messages);
	}

	public function countUnread(): array
	{
		return [
			'count' => $this->chatRepository->getCountUnreadMessages($this->getUser()),
		];
	}

}