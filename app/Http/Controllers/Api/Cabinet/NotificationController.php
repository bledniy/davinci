<?php


namespace App\Http\Controllers\Api\Cabinet;

use App\Http\Requests\User\Cabinet\NotificationConfigRequest;
use App\Http\Resources\User\UserNotificationConfigResource;
use App\Platform\Cabinet\Notification\Config\TreeBuilder;
use App\Repositories\NotificationConfigRepository;

class NotificationController extends AbstractCabinetController
{
	/**
	 * @var NotificationConfigRepository
	 */
	private $repository;

	public function __construct(NotificationConfigRepository $repository)
	{
		parent::__construct();
		$this->repository = $repository;
	}

	public function index()
	{
		// по типу текущего пользователя
		$user = $this->getUser();
		$list = $this->repository->findForIndex($user);

		$tree = TreeBuilder::build($list);

		return UserNotificationConfigResource::collection($tree);
	}

	public function sync(NotificationConfigRequest $request)
	{
		$user = $this->getUser();
		$notifications = (array)$request->get('notifications', []);
		$this->repository->syncByUser($notifications, $user);
		return $this->setSuccessMessage('Настройки сохранены')->getResponseMessageForJson();
	}


}