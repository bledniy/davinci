<?php


namespace App\Http\Controllers\Api\Cabinet;

use App\Http\Requests\User\Cabinet\Personal\PersonalDataRequest;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Platform\Cabinet\Uploads\Avatar\AvatarDeleter;
use App\Platform\Cabinet\Uploads\Avatar\AvatarUploader;
use App\Platform\Model\UserFields;

class PersonalDataController extends AbstractCabinetController
{
	public function submit(PersonalDataRequest $request)
	{
		$user = $this->getUser();
		$this->setSuccessMessage(getTranslate('messages.cabinet.personal.success', 'Данные успешно обновлены'));
		$input = $request->only($request->getFillableFields('about'));

		if ($request->hasFile('avatar')) {
			$this->avatarDelete();
			$avatarUploader = app(AvatarUploader::class, ['file' => $request->file('avatar'), 'user' => $user]);
			$input[UserFields::get($user->getType(), 'avatar')] = $avatarUploader->uploadDefault();
		}
		if ($request->has('about')) {
			$input[UserFields::get($user->getType(), 'about')] = $request->get('about');
		}
		if ($input) {
			$this->userRepository->update($input, $user);
		}
		$this->setResponseData(['user' => UserResourceFactory::make($user)]);
		return $this->getResponseMessageForJson();
	}

	protected function avatarDelete()
	{
		$user = $this->getUser();
		$field = UserFields::get($user->getType(), 'avatar');
		if (!AvatarDeleter::supports($user->getAvatar($field))) {
			return false;
		}
		AvatarDeleter::delete($user->getAvatar($field));
		return true;
	}


}