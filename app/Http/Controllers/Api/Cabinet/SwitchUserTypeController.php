<?php


namespace App\Http\Controllers\Api\Cabinet;


use App\Events\Platform\Cabinet\UserTypeSwitched;
use App\Http\Controllers\Api\AbstractApiController;
use App\Models\User;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Platform\Contract\UserTypeContract;
use App\Platform\User\AuthenticatedUserResolver;
use App\Platform\User\Type\UserTypeHelper;
use Illuminate\Http\Response;

class SwitchUserTypeController extends AbstractApiController
{

	private $userResolver;

	public function __construct(
		AuthenticatedUserResolver $userResolver
	)
	{
		parent::__construct();
		$this->userResolver = $userResolver;
	}

	public function switch()
	{
		/** @var  $user User */
		$user = $this->userResolver->getUser();
		$newType = UserTypeHelper::revert($user->getType());
		if ($newType === UserTypeContract::TYPE_PERFORMER && !$this->isConfirmedContacts($user)) {
		    abort(Response:: HTTP_I_AM_A_TEAPOT, 'Needs verify email (and / or) phone before login as performer');
		}
		event(new UserTypeSwitched($newType, $user));
		return UserResourceFactory::make($user);
	}

	protected function isConfirmedContacts(User $user): bool
	{
		return ($user->phone_verified_at && $user->email_verified_at);
	}
}