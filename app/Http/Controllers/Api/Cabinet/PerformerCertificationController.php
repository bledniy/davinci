<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\Cabinet;

use App\Events\Cabinet\PerformerCertificationUploadedEvent;
use App\Helpers\Media\ImageRemover;
use App\Http\Requests\User\Cabinet\CertificationRequest;
use App\Http\Resources\User\UserCertificationResource;
use App\Models\Category\Category;
use App\Platform\Cabinet\Certification\CertificationAvailability;
use App\Platform\Cabinet\Certification\CertificationStatuses;
use App\Platform\Cabinet\Certification\UserCertificationUploader;
use App\Repositories\CategoryRepository;
use App\Repositories\UserCertificationRepository;

class PerformerCertificationController extends AbstractCabinetController
{
	/**
	 * @var UserCertificationRepository
	 */
	private $userCertificationRepository;
	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;

	public function __construct(UserCertificationRepository $userCertificationRepository, CategoryRepository $categoryRepository)
	{
		parent::__construct();
		$this->userCertificationRepository = $userCertificationRepository;
		$this->categoryRepository = $categoryRepository;
	}

	public function upload(CertificationRequest $request)
	{
		$user = $this->getUser();
		/** @var  $category Category */
		$category = $this->categoryRepository->find($request->get('category_id'));
		if (!CertificationAvailability::make($user)->isCategoryAvailable($category)) {
			return $this->setFailMessage(getTranslate('cabinet.certification.upload.unavailable', 'Загрузка сертификата не доступна, ваша заявка уже рассмотрена'))->getResponseMessageForJson();
		}
		$path = app(UserCertificationUploader::class)->upload($request->file('certificate'));
		$certification = $this->userCertificationRepository->findByUserCategory($user, $category);
		if ($certification) {
			$certification->getCertificationStatuses()->changeStatus(CertificationStatuses::STATUS_CHECK);
			app(ImageRemover::class)->removeImage($certification->image);
			$this->userCertificationRepository->update(['image' => $path], $certification);
		} else {
			$certification = $this->userCertificationRepository->createRelated(['image' => $path, $category->getForeignKey() => $category->getKey()], $user);
		}
		event(new PerformerCertificationUploadedEvent($certification));

		$this->setResponseData(['certification' => UserCertificationResource::make($certification)]);
		return $this->setSuccessMessage(getTranslate('cabinet.certification.upload.success', 'Сертификат загружен'))->getResponseMessageForJson();
	}

	public function index()
	{
		$user = $this->getUser();
		$certifications = $this->userCertificationRepository->findByUser($user)->keyBy('category_id');
		$certifications->load('category.lang');
		return UserCertificationResource::collection($certifications);
	}
}