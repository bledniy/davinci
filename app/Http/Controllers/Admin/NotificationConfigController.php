<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Helpers\Media\ImageRemover;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Resources\User\UserNotificationConfigResource;
use App\Models\News\News;
use App\Models\News\NewsCategory;
use App\Models\User\NotificationConfig;
use App\Repositories\NewsCategoryRepository;
use App\Repositories\NewsRepository;
use App\Repositories\NotificationConfigRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use function __;
use function redirect;
use function view;

class NotificationConfigController extends AdminController
{

	private $name;

	protected $routeKey = 'admin.notifications.config';

	protected $permissionKey = 'news';
	/**
	 * @var NotificationConfigRepository
	 */
	private $repository;

	public function __construct(NotificationConfigRepository $repository)
	{
		parent::__construct();
		$this->name = __('modules.notification_config.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
	}

	public function index(Request $request)
	{
		if ($request->wantsJson()){
			$list = $this->repository->with('lang')->get();
			return UserNotificationConfigResource::collection($list);
		}
		$this->setTitle($this->name);
		$data['content'] = view('admin.notifications.index');
		return $this->main($data);
	}

	public function update(Request $request, NotificationConfig $config)
	{
		$input = $request->only('name');
		if (!$config->exists) {
		    abort(404);
		}
		if ($this->repository->update($input, $config)) {
			$this->setSuccessUpdate();
		}

		return $this->getResponseMessageForJson();
	}

}
