<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Events\Order\Show\Bids\BidDeletedEvent;
use App\Helpers\Debug\LoggerHelper;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Resources\Chat\ChatMessageResource;
use App\Http\Resources\Chat\ChatResource;
use App\Http\Resources\Order\OrderBidResource;
use App\Http\Resources\Order\OrderResource;
use App\Models\Chat\Chat;
use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use App\Platform\Cabinet\Factory\UserResourceFactory;
use App\Platform\Order\Voters\Bid\IsBidBelongsToDeal;
use App\Platform\Order\Voters\Bid\IsBidSelectedInDeal;
use App\Repositories\Chat\ChatRepository;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class OrderChatsController extends AdminController
{
	use Authorizable;

	protected $key = 'orders';

	protected $routeKey = 'admin.orders';

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(ChatRepository $chatRepository, OrderRepository $orderRepository)
	{
		parent::__construct();
		$this->addBreadCrumb(__('modules.orders.title'), $this->resourceRoute('index'));
		$this->chatRepository = $chatRepository;
		$this->orderRepository = $orderRepository;
	}

	public function show(Order $order, Chat $chat, Request $request)
	{
		if ($request->has('ajax')) {
			$chat->load('users', 'messages');
			$res = ChatResource::make($chat);
			$res->additional(['users' => UserResourceFactory::collection($chat->users)]);
			return $res;
		}
		$this->orderRepository->loadForDeals($order);
		$this->addBreadCrumb($order->getTitle(), $this->resourceRoute('edit', $order->getKey()))
			->setTitle('$ ' . __('modules.orders.title'))
			->addBreadCrumb('Чат')
		;
		$orderJson = OrderResource::make($order)->toJson();
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('admin.orders.chat.show')->with($with);
		return $this->main($data);
	}

	public function loadMessages($order, Chat $chat, Request $request)
	{
		\Debugbar::disable();
		$date = getDateCarbonIfValid($request->get('date'));
		if (!$date) {
		    abort(404);
		}
		$messages = $chat->messages()->where('created_at', '>=', $date->toDateTimeString())->get();
		return ChatMessageResource::collection($messages);

	}


}
