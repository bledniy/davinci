<?php

namespace App\Http\Controllers\Admin\Orders;

use App\DataContainers\Platform\Order\SearchOrdersDataContainer;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Resources\Order\OrderBidResource;
use App\Http\Resources\Order\OrderResource;
use App\Models\Category\Category;
use App\Models\City\City;
use App\Models\Order\Order;
use App\Repositories\Chat\ChatRepository;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class OrderController extends AdminController
{
	use Authorizable;

	protected $key = 'orders';

	protected $routeKey = 'admin.orders';

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderBidRepository
	 */
	private $bidRepository;

	public function __construct(OrderRepository $orderRepository, OrderBidRepository $bidRepository)
	{
		parent::__construct();
		$this->addBreadCrumb(__('modules.orders.title'), $this->resourceRoute('index'))->setTitle('$ ' . __('modules.orders.title'));
		$this->shareViewModuleData();
		$this->orderRepository = $orderRepository;
		$this->bidRepository = $bidRepository;
	}

	protected function shareView()
	{
		(function () {
			$categories = Category::all()->load('lang');
			$cities = City::all()->load('lang');
			view()->share(compact(array_keys(get_defined_vars())));
		})();
	}

	public function index(Request $request, SearchOrdersDataContainer $container)
	{
		$container->fillFromRequest($request);
		$list = $this->orderRepository->getListAdmin($container);
		$this->orderRepository->loadForDeals($list, [], ['performer']);
		$this->shareView();
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('admin.orders.index')->with($with);
		return $this->main($data);
	}

	public function edit($orderId, Request $request)
	{
		$order = $this->orderRepository->findForEdit($orderId);
		if (!$order) {
			$this->setFailMessage('Сделка не найдена');
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}
		$title = $this->titleEdit($order, 'title');
		$this->addBreadCrumb($title)->setTitle($title);
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('admin.orders.edit')->with($with);
		return $this->main($data);
	}

	public function show(Order $order)
	{
		\Debugbar::disable();//polling
		$this->orderRepository->loadForDeals($order, [], ['performer']);
		return OrderResource::make($order);
	}

	public function toggleActive(Order $order)
	{
		$order->getSetter()->toggleActive();
		$this->orderRepository->update([], $order);
		return $this->setSuccessMessage($order->getContainer()->isActive() ? 'Сделка включена' : 'Сделка выключена ')->getResponseMessageForJson();
	}

}
