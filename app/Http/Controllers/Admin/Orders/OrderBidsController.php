<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Events\Order\Show\Bids\BidDeletedEvent;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Resources\Chat\ChatResource;
use App\Http\Resources\Order\OrderBidResource;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order\Order;
use App\Models\Order\OrderBid;
use App\Platform\Order\Voters\Bid\IsBidBelongsToDeal;
use App\Platform\Order\Voters\Bid\IsBidSelectedInDeal;
use App\Repositories\Chat\ChatRepository;
use App\Repositories\Order\OrderBidRepository;
use App\Repositories\Order\OrderRepository;
use App\Traits\Authorizable;

class OrderBidsController extends AdminController
{
	use Authorizable;

	protected $key = 'orders';

	protected $routeKey = 'admin.orders';

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderBidRepository
	 */
	private $bidRepository;

	public function __construct(OrderBidRepository $bidRepository)
	{
		parent::__construct();
		$this->bidRepository = $bidRepository;
	}

	public function index(Order $order, ChatRepository $chatRepository)
	{
		\Debugbar::disable();//polling
		$bids = $this->bidRepository->getByOrder($order);
		$chats = $chatRepository->getForRelated($order)->keyBy('chatable_id');
		$chats->load(['users' => function ($q) use ($order) {
			return $q->where('user_id', '!=', $order->getContainer()->getCustomerId());
		},
		]);
		$fn = function (array $data, OrderBid $resource) use ($chats) {
			foreach ($chats as $chat) {
				if (in_array($resource->getContainer()->getPerformerKey(), $chat->users->pluck('id')->toArray(), true)) {
					$data['chat'] = ChatResource::make($chat);
					break;
				}
			}
			return $data;
		};
		OrderBidResource::addCallback($fn);
		return OrderBidResource::collection($bids);
	}

	public function destroy(Order $order, OrderBid $bid)
	{
		if (!IsBidBelongsToDeal::make()->check($order, $bid)) {
			return $this->setFailMessage('Ставка не принадлежит выбранной сделке')->getResponseMessageForJson();
		}
		if (IsBidSelectedInDeal::make()->check($order, $bid)) {
			return $this->setFailMessage('Нельзя удалить ставку выбранного исполнителя')->getResponseMessageForJson();
		}
		if ($this->bidRepository->delete($bid)) {
			event(new BidDeletedEvent($order, $bid));
			$this->setSuccessMessage('Ставка удалена');
		}
		return $this->getResponseMessageForJson();
	}

}
