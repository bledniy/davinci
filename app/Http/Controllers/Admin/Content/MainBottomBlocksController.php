<?php

namespace App\Http\Controllers\Admin\Content;

use App\Traits\Authorizable;

class MainBottomBlocksController extends ContentController
{
	use Authorizable;

	protected $fields = ['name', 'image', 'description'];

	protected $routeKey = 'admin.main-bottom';

	protected $permissionKey = 'main-bottom';

	protected $name = 'Блоки внизу';

	protected $contentType = 'main-bottom';


}
