<?php

namespace App\Http\Controllers\Admin\Content;

use App\Traits\Authorizable;

class MainServicesController extends ContentController
{
	use Authorizable;

	protected $fields = ['name', 'image', 'title', 'description'];

	protected $routeKey = 'admin.main-services';

	protected $permissionKey = 'main-services';

	protected $name = 'Услуги';

	protected $contentType = 'main-services';


}
