<?php

namespace App\Http\Controllers\Admin\Content;

use App\Traits\Authorizable;

class TeamController extends ContentController
{
	use Authorizable;

	protected $fields = ['name', 'image', 'description'];

	protected $routeKey = 'admin.team';

	protected $permissionKey = 'team';

	protected $name = 'Команда';

	protected $contentType = 'team';


}
