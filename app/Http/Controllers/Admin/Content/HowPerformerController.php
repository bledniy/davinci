<?php

namespace App\Http\Controllers\Admin\Content;

use App\Traits\Authorizable;

class HowPerformerController extends ContentController
{
	use Authorizable;

	protected $fields = ['name', 'image', 'description', 'excerpt'];

	protected $permissionKey = 'how-performer';

	protected $routeKey = 'admin.how-performer';

	protected $name = 'Как это работает (исполнитель)';

	protected $contentType = 'how-performer';

}
