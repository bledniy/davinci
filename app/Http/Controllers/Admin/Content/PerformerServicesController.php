<?php

namespace App\Http\Controllers\Admin\Content;

use App\Traits\Authorizable;

class PerformerServicesController extends ContentController
{
	use Authorizable;

	protected $fields = ['name', 'image', 'description'];

	protected $routeKey = 'admin.performer-services';

	protected $permissionKey = 'performer-services';

	protected $name = 'Услуги';

	protected $contentType = 'performer-services';


}
