<?php

namespace App\Http\Controllers\Admin\Content;

use App\Traits\Authorizable;

class NeedController extends ContentController
{
	use Authorizable;

	protected $fields = ['name', 'description'];

	protected $routeKey = 'admin.need';

	protected $permissionKey = 'need';

	protected $name = 'Вам понадобится';

	protected $contentType = 'need';


}
