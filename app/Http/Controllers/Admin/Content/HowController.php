<?php

namespace App\Http\Controllers\Admin\Content;

use App\Traits\Authorizable;

class HowController extends ContentController
{
	use Authorizable;

	protected $fields = ['name', 'image', 'description', 'excerpt'];

	protected $routeKey = 'admin.how';

	protected $permissionKey = 'how';

	protected $name = 'Как это работает';

	protected $contentType = 'how';


}
