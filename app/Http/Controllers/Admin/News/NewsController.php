<?php

namespace App\Http\Controllers\Admin\News;

use App\Events\Admin\NewsChangedEvent;
use App\Events\Admin\NewsCreatedEvent;
use App\Helpers\Media\ImageRemover;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\News\News;
use App\Models\News\NewsCategory;
use App\Repositories\NewsCategoryRepository;
use App\Repositories\NewsRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use function __;
use function event;
use function redirect;
use function view;

class NewsController extends AdminController
{
	use Authorizable;
	use SaveImageTrait;

	public $thumbnailWidth = false;
	public $thumbnailHeight = false;

	private $name;

	protected $key = 'news';

	protected $routeKey = 'admin.news';

	protected $permissionKey = 'news';
	/**
	 * @var NewsRepository
	 */
	private $repository;

	public function __construct(NewsRepository $repository, NewsCategoryRepository $categoryRepository)
	{
		parent::__construct();
		$this->name = __('modules.news.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		view()->share('categories', $categoryRepository->getListAdmin()->load('lang'));
		$this->repository = $repository;
	}

	public function index(NewsRepository $newsRepository)
	{
		$this->setTitle($this->name);
		$vars['list'] = $newsRepository->getListForAdmin();
		$data['content'] = view('admin.news.index', $vars);
		return $this->main($data);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$data['content'] = view('admin.news.create');
		return $this->main($data);
	}


	public function store(NewsRequest $request)
	{
		$input = $request->only($request->getFillableFields('image'));
		if ($news = $this->repository->create($input)) {
			$this->setSuccessStore();
			$this->saveImage($request, $news);
			$this->fireEvents();
			event(new NewsCreatedEvent($news));
		}
		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $news->getKey()))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	public function edit($id)
	{
		$edit = $this->repository->findForEdit($id);

		$photosList = $edit->images;
		$title = $this->titleEdit($edit);
		$this->addBreadCrumb($title)->setTitle($title);

		$data['content'] = view('admin.news.edit', compact(
			'edit',
			'photosList'
		));
		return $this->main($data);
	}

	public function update(NewsRequest $request, News $news)
	{
		$input = $request->only($request->getFillableFields('image'));
		//
		$this->saveImage($request, $news);
		if ($this->repository->update($input, $news)) {
			$this->setSuccessUpdate();
			$this->fireEvents();
		}

		if ($request->hasFile('images')) {
			$this->saveAdditionalImages($news, $request);
		}

		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}

	public function destroy(News $news, ImageRemover $imageRemover)
	{
		if ($this->repository->delete($news->getKey())) {
			$imageRemover->removeImage($news->image);
			foreach ($news->getImages() as $image) {
				$imageRemover->removeImage($image->image);
			}
			$this->setSuccessDestroy();
			$this->fireEvents();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	private function fireEvents()
	{
		event(new NewsChangedEvent());
	}
}
