<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\News;

use App\Helpers\Media\ImageRemover;
use App\Http\Controllers\Admin\AdminController;
use App\Models\News\News;
use App\Models\News\NewsCategory;
use App\Repositories\NewsCategoryRepository;
use App\Repositories\NewsRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use function __;
use function redirect;
use function view;

class NewsCategoryController extends AdminController
{
	use Authorizable;

	private $name;

	protected $key = 'news';

	protected $routeKey = 'admin.news.categories';

	protected $permissionKey = 'news';
	/**
	 * @var NewsRepository
	 */
	private $repository;

	public function __construct(NewsCategoryRepository $repository)
	{
		parent::__construct();
		$this->name = __('modules.news-categories.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		view()->share('categories', $repository->getListAdmin()->load('lang'));
		$this->repository = $repository;
	}

	public function index()
	{
		$this->setTitle($this->name);
		$vars['list'] = $this->repository->with('lang')->get();
		$data['content'] = view('admin.news.categories.index', $vars);
		return $this->main($data);
	}

	public function store(Request $request)
	{
		$input = $request->only('name');
		if ($newsCategory = $this->repository->create($input)) {
			$newsCategory->load('lang');
			$this->setSuccessStore();
		}

		$this->setResponseData(['category' => $newsCategory]);

		return $this->getResponseMessageForJson();
	}

	public function update(Request $request, NewsCategory $category)
	{
		$input = $request->only('name');
		if (!$category->exists) {
		    abort(404);
		}
		if ($this->repository->update($input, $category)) {
			$this->setSuccessUpdate();
		}
		$this->setSuccessUpdate();

		return $this->getResponseMessageForJson();
	}

	public function destroy(NewsCategory $category)
	{
		if ($category->news()->count()) {
		    return $this->setFailMessage('Нельзя удалить категорию, имеющую новости')->getResponseMessageForJson();
		}
		if ($this->repository->delete($category->getKey())) {
			$this->setSuccessDestroy();
		}

		return $this->getResponseMessageForJson();
	}

}
