<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Helpers\Media\ImageRemover;
use App\Http\Requests\Admin\City\CityRequest;
use App\Models\City\City;
use App\Repositories\CityRepository;
use App\Traits\Authorizable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class CityController extends AdminController
{
	use Authorizable;

	private $name;

	protected $key = 'cities';

	protected $routeKey = 'admin.cities';

	protected $permissionKey = 'cities';
	/**
	 * @var CityRepository
	 */
	private $repository;

	public function __construct(CityRepository $repository)
	{
		parent::__construct();
		$this->name = __('modules.cities.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
	}

	public function index()
	{
		$this->setTitle($this->name);
		$vars['list'] = $this->repository->getListAdmin();
		$data['content'] = view('admin.cities.index', $vars);

		return $this->main($data);
	}

	/**
	 * @return Factory|View
	 */
	public function create()
	{
		$data['content'] = view('admin.cities.create');

		return $this->main($data);
	}

	public function store(CityRequest $request)
	{
		$input = $request->except('image');
		if ($city = $this->repository->create($input)) {
			$this->setSuccessStore();
			$this->fireEvents();
		}

		return $this->redirectOnCreated($city);
	}

	public function edit(City $city)
	{
		$edit = $city;
		$title = $this->titleEdit($edit);
		$this->addBreadCrumb($title)->setTitle($title);

		$data['content'] = view('admin.cities.edit', compact(
			'edit'
		));

		return $this->main($data);
	}

	public function update(CityRequest $request, City $city)
	{
		$input = $request->only($request->getFillableFields('image'));

		if ($this->repository->update($input, $city)) {
			$this->setSuccessUpdate();
			$this->fireEvents();
		}

		return $this->redirectOnUpdated($city);
	}

	/**
	 * @param City $city
	 * @param ImageRemover $imageRemover
	 * @return RedirectResponse|Redirector
	 */
	public function destroy(City $city, ImageRemover $imageRemover)
	{
		if (!$city->canDelete()) {
			$this->setFailMessage('Удаление этой записи не доступно');

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}
		if ($this->repository->delete($city->getKey())) {
			$imageRemover->removeImage($city->image);
			$this->setSuccessDestroy();
			$this->fireEvents();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	private function fireEvents()
	{
	}
}
