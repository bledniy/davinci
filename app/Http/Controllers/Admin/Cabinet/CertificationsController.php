<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Cabinet;

use App\Events\Platform\Cabinet\UserCertificationUpdatedEvent;
use App\Helpers\Media\ImageRemover;
use App\Http\Controllers\Admin\AdminController;
use App\Models\User\UserCertification;
use App\Platform\Cabinet\Certification\CertificationStatuses;
use App\Repositories\UserCertificationRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class CertificationsController extends AdminController
{
	use Authorizable;

	private $name;

	protected $key = 'certifications';

	protected $routeKey = 'admin.certifications';

	protected $permissionKey = 'certifications';
	/**
	 * @var UserCertificationRepository
	 */
	private $repository;

	public function __construct(UserCertificationRepository $repository, CertificationStatuses $certificationStatuses)
	{
		parent::__construct();
		$this->name = __('modules.certifications.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
		view()->share(['statuses' => $certificationStatuses->getStatuses()]);
	}

	public function index(Request $request)
	{
		$this->setTitle($this->name);
		$list = $this->repository->getListAdmin(['status' => $request->get('status')]);
		$list->load('user', 'category.lang');
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('admin.certifications.index', $with);

		return $this->main($data);
	}

	public function edit(UserCertification $certification)
	{
		$edit = $certification;
		$title = $this->titleEdit($edit);
		$this->addBreadCrumb($title)->setTitle($title);

		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('admin.certifications.edit', $with);

		return $this->main($data);
	}

	public function update(Request $request, UserCertification $certification)
	{
		$user = $certification->getUser();
		$certification->getCertificationStatuses()->changeStatus($request->get('status'));
		event(new UserCertificationUpdatedEvent($certification, $user));
		if ($this->repository->update([], $certification)) {
			$this->setSuccessUpdate();
		}

		return $this->redirectOnUpdated($certification);
	}

	public function destroy(UserCertification $certification, ImageRemover $imageRemover)
	{
		if (!$certification->canDelete()) {
			$this->setFailMessage('Удаление этой записи не доступно');

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}
		if ($this->repository->delete($certification->getKey())) {
			$imageRemover->removeImage($certification->image);
			$this->setSuccessDestroy();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

}