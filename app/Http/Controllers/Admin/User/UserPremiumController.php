<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Platform\ProAccount\AddPremFromAdmin;
use App\Platform\ProAccount\UserProAccountActivationContainer;
use App\Repositories\PremiumRepository;
use Illuminate\Http\Request;

class UserPremiumController extends AdminController
{
    public function addPremium(
        User $user,
        AddPremFromAdmin $addPremFromAdmin,
        PremiumRepository $premiumRepository,
        UserProAccountActivationContainer $accountActivationContainer
    )
    {
        $premium = $premiumRepository->getDefaultPremium();
        $time = $accountActivationContainer->setMonths(1);
        $addPremFromAdmin->add($user, $premium, $time);

        $this->setSuccessMessage(sprintf('Пользователю  %s присвоен премиум аккаунт', $user->getFio()));
        return back()->with($this->getResponseMessage());
    }
}
