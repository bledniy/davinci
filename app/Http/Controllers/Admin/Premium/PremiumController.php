<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Premium;

use App\Events\Platform\Cabinet\UserCertificationUpdatedEvent;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\Admin\PremiumEditRequest;
use App\Models\Premium\Premium;
use App\Models\User\UserCertification;
use App\Repositories\PremiumRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class PremiumController extends AdminController
{
	use Authorizable;

	private $name;

	protected $key = 'premiums';

	protected $routeKey = 'admin.premiums';

	protected $permissionKey = 'premiums';

	private $repository;

	public function __construct(PremiumRepository $repository)
	{
		parent::__construct();
		$this->name = __('modules.premiums.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
	}

	public function index()
	{
		$this->setTitle($this->name);
		$list = $this->repository->all();
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('admin.premiums.index', $with);
		return $this->main($data);
	}

	public function update(PremiumEditRequest $request, Premium $premium)
	{
		$premium->fillExisting($request->all());
		if ($this->repository->update([], $premium)) {
			$this->setSuccessUpdate();
		}

		return $this->redirectOnUpdated($premium);
	}


}