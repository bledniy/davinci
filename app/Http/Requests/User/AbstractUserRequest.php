<?php

namespace App\Http\Requests\User;

use App\Http\Requests\AbstractRequest;
use App\Services\Phone\PhoneUkraineFormatter;
use Illuminate\Validation\Rule;

abstract class AbstractUserRequest extends AbstractRequest
{

	public function getNameRule(): array
	{
		return ['required', 'string', 'min:3', 'max:255'];
	}

	public function getSurnameRule(): array
	{
		return ['required', 'string', 'min:3', 'max:255'];
	}

	public function getPatronymicRule(): array
	{
		return ['required', 'string', 'min:3', 'max:255'];
	}

	public function getPersonal(array $except = [])
	{
		$fields = [
			'name'       => $this->getNameRule(),
			'surname'    => $this->getSurnameRule(),
			'patronymic' => $this->getPatronymicRule(),
		];
		return $this->exceptFields($fields, $except);
	}

	public function getPasswordCredentials()
	{
		return ['required' => 'required', 'string', 'min' => 'min:8'];
	}

	public function getPasswordConfirmedCredentials(): array
	{
		return array_merge($this->getPasswordCredentials(), [
			'confirmed',
		]);
	}

	public function getChangePasswordRules(array $except = []): array
	{
		$fields = [
				'password_new' => ['required', 'confirmed', 'min:8', 'different:password'],
			] + $this->getCurrentPasswordRule();

		return $this->exceptFields($fields, $except);
	}

	public function getCurrentPasswordRule(): array
	{
		return [
			'password' => $this->getPasswordCredentials(),
		];
	}

	protected function getBasePhoneRule(array $except = []): array
	{
		return $this->exceptFields(['required' => 'required', 'string','max' => 'max:255', 'phone' => 'phone:UA,mobile'], $except);
	}

	public function getLoginPhoneRule(): array
	{
		return array_merge($this->getBasePhoneRule(), ['exists' => 'exists:users,phone']);
	}

	public function getRegisterPhoneRule(): array
	{
		return array_merge($this->getBasePhoneRule(), ['unique' => 'unique:users,phone']);
	}

	protected function getBaseEmailRule(): array
	{
		return [
			'required' => 'required', 'max' => 'max:255', 'email' => 'email:rfc,dns',
		];
	}

	public function getLoginEmailRule(): array
	{
		return array_merge($this->getBaseEmailRule(), ['exists' => 'exists:users,email']);
	}

	public function getRegisterEmailRule(): array
	{
		return array_merge($this->getBaseEmailRule(), ['unique' => 'unique:users,email']);
	}

	public function mergeFormatPhone($field = 'phone')
	{
		$phone = PhoneUkraineFormatter::formatPhone((string)$this->get($field));
		$this->merge([$field => $phone]);
		$all = array_merge($this->all(), [$field => $phone]);
		$this->request->replace($all);
	}

	public function getCityRule(): array
	{
		return ['required', 'exists:cities,id'];
	}

	public function getGenderRule(): array
	{
		return ['required', Rule::in(['male', 'female'])];
	}

	public function getAvatarRule(array $except = []): array
	{
		$fields = ['nullable' => 'nullable', 'image' => 'image', 'mimes' => 'mimes:jpg,jpeg,png', 'max' => 'max:' . (0.5 * 1024)];
		return $this->exceptFields($fields, $except);
	}

}
