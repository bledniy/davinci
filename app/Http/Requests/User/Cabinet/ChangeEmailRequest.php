<?php

namespace App\Http\Requests\User\Cabinet;

use App\Http\Requests\User\AbstractUserRequest;

class ChangeEmailRequest extends AbstractUserRequest
{
	public function rules()
	{
		return [
			'email' => $this->getRegisterEmailRule(),
		];
	}

}
