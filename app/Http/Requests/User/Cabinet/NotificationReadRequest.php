<?php

namespace App\Http\Requests\User\Cabinet;

use App\Http\Requests\AbstractRequest;
use Illuminate\Validation\Rule;

class NotificationReadRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'notifications'   => ['sometimes',],
			'notifications.*' => ['string'],
		];
	}

	protected function mergeRequestValues()
	{
		$notifications = (array)$this->get('notifications', []);
		$notifications=  array_filter($notifications, 'strlen');
		$notifications = arrayValueAsKey(array_flip($notifications));
		$this->merge(['notifications' => $notifications]);
	}
}
