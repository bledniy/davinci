<?php

	namespace App\Http\Requests\User\Cabinet;

	use App\Http\Requests\AbstractRequest;

	class CertificationRequest extends AbstractRequest
	{
		public function rules()
		{
			return [
				'category_id' => ['required', 'exists:categories,id'],
				'certificate' => array_merge(['required'], $this->getImageRule()),
			];
		}
	}
