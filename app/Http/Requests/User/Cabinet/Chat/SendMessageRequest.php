<?php

namespace App\Http\Requests\User\Cabinet\Chat;

use App\Http\Requests\AbstractRequest;

class SendMessageRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'message' => ['required', 'min:1', 'max:3000', 'string'],
		];
	}
}
