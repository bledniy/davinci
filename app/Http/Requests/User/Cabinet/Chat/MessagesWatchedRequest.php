<?php

namespace App\Http\Requests\User\Cabinet\Chat;

use App\Http\Requests\AbstractRequest;

class MessagesWatchedRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'message_id' => ['required',],
		];
	}


	public function getWatchedIds(): array
	{
		$message_id = (array)$this->get('message_id');
		foreach ($message_id as $k => $item) {
			$ids = explode(',', $item);
			if (count($ids) > 1) {
				unset($message_id[$k]);
				$message_id = array_merge($message_id, $ids);
			}
		}

		$message_id = array_filter($message_id, 'is_numeric');
		return $message_id;
	}
}
