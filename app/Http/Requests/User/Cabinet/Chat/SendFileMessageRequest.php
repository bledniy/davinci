<?php

namespace App\Http\Requests\User\Cabinet\Chat;

use App\Http\Requests\AbstractRequest;

class SendFileMessageRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'files' => ['required'],
			'files.*' => $this->getFilesRule(['nullable']),
		];
	}
}
