<?php

namespace App\Http\Requests\User\Cabinet;

use App\Http\Requests\User\AbstractUserRequest;

class ChangePhoneRequest extends AbstractUserRequest
{
	public function rules()
	{
		return [
			'phone' => $this->getRegisterPhoneRule(),
		];
	}

	protected function mergeRequestValues()
	{
		$this->mergeFormatPhone();
	}

	public function getPhone()
	{
		return $this->all()['phone'] ?? null;
	}
}
