<?php

namespace App\Http\Requests\User\Cabinet;

use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;

class PerformerServiceRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'name'  => ['required', 'string', 'max:255'],
			'price' => ['required', 'integer', 'min:0', 'max:' . ValidationMaxLengthHelper::INT_UNSIGNED],
		];
	}
}
