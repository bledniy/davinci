<?php

namespace App\Http\Requests\User\Cabinet;

use App\Http\Requests\AbstractRequest;

class DiplomaRequest extends AbstractRequest
{

	public function rules()
	{
		return $this->nullableIfUpdate([
			'name'  => $this->getRuleRequiredChar(),
			'image' => $this->getImageRule(),
		]);
	}

	protected function nullableIfUpdate(array $rules): array
	{
		if ($this->isActionUpdate()) {
			$rules = $this->addSometimesToRules($rules);
			$rules = $this->removeFromRules($rules, 'required');
		}
		return $rules;
	}
}
