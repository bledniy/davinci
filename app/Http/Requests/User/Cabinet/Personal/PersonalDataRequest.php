<?php

namespace App\Http\Requests\User\Cabinet\Personal;

use App\Http\Requests\User\AbstractUserRequest;

class PersonalDataRequest extends AbstractUserRequest
{
	public function rules()
	{
		return $this->addSometimesToRules([
				'city_id'    => $this->getCityRule(),
				'date_birth' => ['date', 'before:' . now()->toDateString()],
				'gender'     => $this->getGenderRule(),
				'about'      => ['string', 'max:3000'],
				'avatar'       => $this->getImageRule(),
			] +
			$this->getPersonal());
	}

	protected function mergeRequestValues()
	{
		if ($this->has('date_birth')) {
			$this->merge(['date_birth' => getDateCarbon($this->get('date_birth'))->toDateString()]);
		}
		if ($this->has('about')) {
			$this->merge(['about' => strip_tags($this->get('about'))]);
		}
	}

}
