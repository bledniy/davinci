<?php

namespace App\Http\Requests\Auth\Register;

use App\Http\Requests\User\AbstractUserRequest;
use App\Platform\Contract\UserTypeContract;
use Illuminate\Validation\Rule;

class RegisterStepsRequest extends AbstractUserRequest
{
	public function rules()
	{
		return [
			'user_type'  => ['required', Rule::in([UserTypeContract::TYPE_PERFORMER, UserTypeContract::TYPE_CUSTOMER])],
			'email' => $this->getRegisterEmailRule(),
		];
	}

}
