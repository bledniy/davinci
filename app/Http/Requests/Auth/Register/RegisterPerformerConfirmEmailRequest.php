<?php

namespace App\Http\Requests\Auth\Register;

use App\Http\Requests\User\AbstractUserRequest;

class RegisterPerformerConfirmEmailRequest extends AbstractUserRequest
{
	public function rules()
	{
		return [
			'code' => ['required'],
		];
	}

}
