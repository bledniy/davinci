<?php

namespace App\Http\Requests\Auth\Register;

use App\Http\Requests\User\AbstractUserRequest;

class RegisterCustomerDataRequest extends AbstractUserRequest
{
	public function rules()
	{
		return [
			'name'                  => $this->getNameRule(),
			'surname'               => $this->getSurnameRule(),
			'phone'                 => $this->getRegisterPhoneRule(),
			'password'              => $this->getPasswordConfirmedCredentials(),
			'password_confirmation' => ['required'],
			'city'                  => ['required', 'exists:cities,id'],
		];
	}

	public function mergeRequestValues()
	{
		$this->mergeFormatPhone();
	}
}
