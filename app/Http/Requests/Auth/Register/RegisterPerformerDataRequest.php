<?php

namespace App\Http\Requests\Auth\Register;

class RegisterPerformerDataRequest extends RegisterCustomerDataRequest
{
	public function rules()
	{
		return $this->exceptFields(parent::rules(), ['city', 'surname']);
	}

}
