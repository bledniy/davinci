<?php

namespace App\Http\Requests\Auth\Login;

use App\Http\Requests\User\AbstractUserRequest;

class CheckLoginRequest extends AbstractUserRequest
{
	public function rules()
	{
		return [
			'login' => $this->getLoginEmailRule(),
		];
	}

	public function messages()
	{
		return ['login.exists' => getTranslate('validation.login.not-exists', 'Email не найден')];
	}

}
