<?php

namespace App\Http\Requests\Feedback;

use App\Http\Requests\AbstractRequest;
use App\Rules\Feedback\FeedbackThrottle;
use App\Rules\Feedback\UploadLimitRule;

class FileFeedbackRequest extends AbstractRequest
{
	protected $fillableFields = ['type', 'ip'];

	public function rules()
	{
		return [
			'name'    => $this->getRuleRequiredChar(),
			'report'  => array_merge($this->getRuleRequiredChar()),
			'message' => array_merge($this->getRuleRequiredChar(), ['max:2000']),
			'email'   => $this->getBaseEmailRule(),
			'files.*' => $this->getFilesRule(['max']),
			'files'   => [app(UploadLimitRule::class), app(FeedbackThrottle::class)],
		];
	}

	protected function mergeRequestValues()
	{
		try{
		    if ($this->has('files')) {
		    	$files = array_filter($this->file('files'), 'is_file');
		        $this->merge(['files' => $files]);
		    }
		} catch (\Throwable $e){
		}
		$this->merge(['ip' => $this->ip()]);
	}
}
