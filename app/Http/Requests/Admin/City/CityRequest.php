<?php declare(strict_types=1);

namespace App\Http\Requests\Admin\City;

use App\Http\Requests\AbstractRequest;
use Illuminate\Support\Str;

class CityRequest extends AbstractRequest
{
	public function rules(): array
	{
		return $this->patchableRules(
			[
				'name' => ['nullable', 'string', 'max:255'],
				'url' => ['nullable', 'unique:cities,url'],
			]
		);
	}

	protected function mergeRequestValues(): void
	{
		if ($this->has('url')) {
			$this->merge([
				'url' => Str::slug($this->get('url')),
			]);
		}
	}
}
