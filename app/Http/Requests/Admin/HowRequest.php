<?php

namespace App\Http\Requests\Admin;

use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;

class HowRequest extends AbstractRequest
{

	public function rules()
	{
		$rules = [
			'name'        => ['required', 'max:255'],
			'title'        => ['nullable', 'max:255'],
			'description' => ['required', 'max:' . ValidationMaxLengthHelper::TEXT],
			'excerpt'     => ['nullable', 'max:200'],
			'image'       => ['nullable', 'image'],
		];
		return $rules;
	}
}
