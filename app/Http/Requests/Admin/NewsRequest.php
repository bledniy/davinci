<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\News\News;
use App\Traits\Requests\Helpers\GetActionModel;

class NewsRequest extends AbstractRequest implements RequestParameterModelable
{
	use GetActionModel;

	protected $toBooleans = ['active'];

	protected $requestKey = 'news';

	public function rules()
	{
		$rules = [
			'video'            => ['nullable', 'max:255'],
			'name'             => ['required', 'max:255'],
			'url'              => ['required', 'unique' => 'unique:news,url'],
			'news_category_id' => ['required', 'exists:news_categories,id'],
			'published_at'     => ['required', 'date'],
			'excerpt'          => ['max:' . ValidationMaxLengthHelper::TEXT],
			'description'      => ['required', 'max:' . ValidationMaxLengthHelper::TEXT],
			'active'           => ['nullable'],
		];

		if ($this->isActionUpdate() && $news = $this->getActionModel()) {
			/** @var  $news News */
			$rules['url']['unique'] = 'unique:news,url,' . $news->getKey();
		}

		return $rules;
	}


	protected function mergeRequestValues()
	{
		$this->mergeUrlFromName();
		$this->merge([
			'published_at' => getDateCarbon($this->get('published_at')),
			'video'        => $this->get('video'),
		]);
	}
}
