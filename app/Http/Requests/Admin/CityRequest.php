<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\AbstractRequest;

class CityRequest extends AbstractRequest
{

	protected $toBooleans = ['active'];

	public function rules()
	{
		return [
			'name' => ['required', 'string', 'max:255'],
		];
	}
}
