<?php

	namespace App\Http\Requests\Order\Show;

	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;

	class OrderAddBidRequest extends AbstractRequest
	{
		public function rules()
		{
			return [
				'text'     => ['required', 'string', 'max:1000'],
				'deadline' => $this->getRuleRequiredChar(),
				'bid'      => ['required', 'integer', 'min:1', 'max:' . ValidationMaxLengthHelper::INT_UNSIGNED],
			];
		}

		protected function mergeRequestValues()
		{
			$this->merge(['text' => strip_tags($this->get('text'))]);
		}
	}
