<?php declare(strict_types=1);

namespace App\Http\Requests\Order;

use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Rules\Feedback\UploadLimitRule;

class OrderCreateRequest extends AbstractOrderRequest
{
	protected $fillableFields = ['closes_at'];
	protected $toBooleans = ['can_online'];

	public function rules()
	{
		return [
			'category_id' => ['required', 'integer', 'exists:categories,id'],
			'city_id' => ['required', 'integer', 'exists:cities,id'],
			'title' => ['required', 'string', 'max:1000'],
			'description' => ['required', 'string', 'max:3000'],
			'price' => ['nullable', 'numeric', 'max:' . ValidationMaxLengthHelper::INT],
			'date_completion' => ['nullable', 'date'],
			'files.*' => $this->getFilesRule(['max']) + ['max' => 'max:' . 1024 * 20, app(UploadLimitRule::class)->setLimit(20)],
			'can_online' => ['nullable'],
		];
	}

	protected function mergeRequestValues()
	{
		$dateTime = now()->addDays(7);
		$description = strip_tags($this->get('description'), '<b><i><u><ul><ol><li><br><strong>');
		$dateCompletion = null;

		if (isDateValid($this->get('date_completion'))) {
			$dateCompletion = getDateCarbon($this->get('date_completion')) < now() ? $dateTime : getDateCarbon($this->get('date_completion'));
		}
		$this->merge([
			'price' => abs((int)$this->get('price')),
			'description' => $description,
			'closes_at' => (string)$dateTime,
			'date_completion' => getDateCarbonIfValid($dateCompletion),
		]);
	}
}
