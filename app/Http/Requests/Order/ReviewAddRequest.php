<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\AbstractRequest;

class ReviewAddRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'adequate_rating'    => ['required', 'integer', 'min:1', 'max:5'],
			'legibility_rating'  => ['required', 'integer', 'min:1', 'max:5'],
			'sociability_rating' => ['required', 'integer', 'min:1', 'max:5'],
			'conflict_rating'    => ['required', 'integer', 'min:1', 'max:5'],
			'recommend_rating'   => ['required', 'integer', 'min:1', 'max:5'],
			'comment'            => ['required', 'string', 'max:5000'],
		];
	}

	protected function mergeRequestValues()
	{
		$this->merge([
			'comment' => strip_tags($this->get('comment')),
		]);
	}
}
