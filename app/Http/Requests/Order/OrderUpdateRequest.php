<?php

namespace App\Http\Requests\Order;

use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Rules\Feedback\UploadLimitRule;

class OrderUpdateRequest extends OrderCreateRequest
{

	protected function mergeRequestValues()
	{
		$dateTime = now()->addDays(7);
		$description = strip_tags($this->get('description'), '<b><i><u><ul><ol><li><br><strong>');
		$dateCompletion = null;

		if (isDateValid($this->get('date_completion'))) {
			$dateCompletion = getDateCarbon($this->get('date_completion')) < now() ? $dateTime : getDateCarbon($this->get('date_completion'));
		}
		$this->merge([
			'price'           => abs((int)$this->get('price')),
			'description'     => $description,
			'closes_at'       => (string)$dateTime,
			'date_completion' => (string)$dateCompletion,
		]);
	}
}
