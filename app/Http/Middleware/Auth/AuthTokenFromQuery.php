<?php

namespace App\Http\Middleware\Auth;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\PersonalAccessToken;

class AuthTokenFromQuery
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if (isLocalEnv() && !Auth::check() && $request->has('__auth_token')){
		    $token = PersonalAccessToken::where('token', $request->get('__auth_token'))->first();
		    if ($token){
			    $user = User::find($token->tokenable()->getParentKey());
			    if ($user){
				    Auth::guard('sanctum')->setUser($user);
			    }
		    }
	    }
        return $next($request);
    }
}
