<?php

namespace App\Http\Middleware\User;

use App\Helpers\LanguageHelper;
use App\Platform\User\AuthenticatedUserResolver;
use App\Repositories\UserRepository;
use Closure;

class SetLocaleOfUser
{
	/**
	 * @var AuthenticatedUserResolver
	 */
	private $userResolver;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(AuthenticatedUserResolver $userResolver, UserRepository $userRepository)
	{
		$this->userResolver = $userResolver;
		$this->userRepository = $userRepository;
	}

	public function handle($request, Closure $next)
	{
		if ($this->supports()) {
			$this->userResolver->getUser()->getSetter()->setLanguageId(LanguageHelper::getLanguageId());
			$this->userRepository->update([], $this->userResolver->getUser());
		}

		return $next($request);
	}

	protected function supports(): bool
	{
		return
			(null !== LanguageHelper::getLanguageId())
			&& (null !== $this->userResolver->getUser())
			&& $this->userResolver->getUser()->getLanguageId() !== (int)LanguageHelper::getLanguageId();
	}
}
