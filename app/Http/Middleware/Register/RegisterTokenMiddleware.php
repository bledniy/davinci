<?php

namespace App\Http\Middleware\Register;

use App\Platform\Auth\Services\Register\RegistrationTokenChecker;
use Closure;

class RegisterTokenMiddleware
{
	/**
	 * @var RegistrationTokenChecker
	 */
	private $registrationTokenChecker;

	public function __construct(RegistrationTokenChecker $registrationTokenChecker)
	{
		$this->registrationTokenChecker = $registrationTokenChecker;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!$this->check()) {
			abort(401, 'Registration token not found');
		}
		return $next($request);
	}

	protected function check(): bool
	{
		return $this->registrationTokenChecker->check();
	}
}
