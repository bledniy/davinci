<?php


namespace App\Http\Responses;


use App\Traits\Controllers\HasMessages;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Response;

abstract class BaseResponse extends Response implements Arrayable
{
	use HasMessages;

	abstract public function toResponse();

	public function toArray()
	{
		return $this->toResponse();
	}
}