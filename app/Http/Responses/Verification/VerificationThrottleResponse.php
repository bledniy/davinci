<?php


namespace App\Http\Responses\Verification;


use App\Http\Resources\VerificationResource;
use App\Http\Responses\BaseResponse;
use App\Models\Verification;

class VerificationThrottleResponse extends BaseResponse
{

	/**
	 * @var Verification
	 */
	private $verification;

	public function __construct(Verification $verification)
	{
		parent::__construct();
		$this->verification = $verification;
	}

	public static function make(Verification $verification): self
	{
		return app(__CLASS__, compact('verification'));
	}

	public function toResponse()
	{
		return $this->setFailMessage(getTranslate('verification.code-resend-limit'))
			->setResponseData(['verification' => VerificationResource::make($this->verification)])
			->setResponseData(isLocalEnv() ? ['code' => $this->verification->getCode()] : [])
			->getResponseMessageForJson()
			;
	}
}