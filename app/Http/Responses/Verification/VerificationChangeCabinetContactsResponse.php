<?php


namespace App\Http\Responses\Verification;


use App\Http\Responses\BaseResponse;

class VerificationChangeCabinetContactsResponse extends BaseResponse
{
	public static function make():self
	{
		return app(__CLASS__);
	}

	public function toResponse()
	{
		return $this->setSuccessMessage('Код подтвержден')
			->getResponseMessageForJson();
	}

}