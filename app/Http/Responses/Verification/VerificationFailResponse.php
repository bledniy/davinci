<?php


namespace App\Http\Responses\Verification;


use App\Http\Responses\BaseResponse;

class VerificationFailResponse extends BaseResponse
{

	public static function make(): self
	{
		return app(__CLASS__);
	}

	public function toResponse()
	{
		return $this->setFailMessage(getTranslate('verification.code-non-equals', 'Не правильный код верификации'))
			->getResponseMessageForJson()
			;
	}
}