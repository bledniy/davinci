<?php


namespace App\Http\Responses\Verification;


use App\Http\Resources\VerificationResource;
use App\Http\Responses\BaseResponse;
use App\Models\Verification;

class VerificationEmailResponse extends BaseResponse
{

	protected $resend = false;

	protected $email;
	/**
	 * @var Verification
	 */
	private $verification;

	public function __construct(Verification $verification)
	{
		parent::__construct();
		$this->verification = $verification;
	}

	public static function make(Verification $verification): self
	{
		return app(__CLASS__, compact('verification'));
	}

	public function toResponse()
	{
		$message = translateFormat(
			$this->resend ? 'verification.email.code-resend-sended' : 'verification.email.code-send'
			, ['%email%' => $this->email,]);
		return $this->setSuccessMessage($message)
			->setResponseData(['verification' => VerificationResource::make($this->verification)])
			->setResponseData(isLocalEnv() ? ['code' => $this->verification->getCode()] : [])
			->getResponseMessageForJson()
			;
	}

	public function setEmail($data): self
	{
		$this->email = $data;
		return $this;
	}

	public function setResend(bool $resend): self
	{
		$this->resend = $resend;
		return $this;
	}
}