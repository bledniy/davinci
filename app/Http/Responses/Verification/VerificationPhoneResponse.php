<?php


namespace App\Http\Responses\Verification;


use App\Http\Resources\VerificationResource;
use App\Http\Responses\BaseResponse;
use App\Models\Verification;

class VerificationPhoneResponse extends BaseResponse
{
	protected $resend = false;

	protected $phone;
	/**
	 * @var Verification
	 */
	private $verification;

	public function __construct(Verification $verification)
	{
		parent::__construct();
		$this->verification = $verification;
	}

	public static function make(Verification $verification): self
	{
		return app(__CLASS__, compact('verification'));
	}

	public function toResponse()
	{
		$message = translateFormat(
			$this->resend ?
				'verification.sms.code-resend-sended'
				: 'verification.sms.code-send',
			['%phone%' => $this->phone,]);

		return $this->setSuccessMessage($message)
			->setResponseData(['verification' => VerificationResource::make($this->verification)])
			->setResponseData(isLocalEnv() ? ['code' => $this->verification->getCode()] : [])
			->getResponseMessageForJson()
			;
	}

	/**
	 * @param mixed $phone
	 * @return VerificationPhoneResponse
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
		return $this;
	}

	/**
	 * @param bool $resend
	 * @return VerificationPhoneResponse
	 */
	public function setResend(bool $resend): self
	{
		$this->resend = $resend;
		return $this;
	}
}