<?php declare(strict_types=1);

namespace App\Listeners\Admin;

use App\Events\Admin\NewsCreatedEvent;
use App\Jobs\Notification\NotifyNewPostJob;

class NewsCreatedNotifyUsersListener
{
	public function handle(NewsCreatedEvent $event)
	{
		NotifyNewPostJob::dispatch($event->getNews())->onQueue('default');
	}
}
