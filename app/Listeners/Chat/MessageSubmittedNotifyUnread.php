<?php

namespace App\Listeners\Chat;

use App\Events\Chat\MessageSubmitted;
use App\Models\User;
use App\Notifications\Chat\UnreadChatMessageNotification;
use App\Platform\Chat\Services\UserPresenceOnChatChannels;
use App\Platform\User\Notification\UserNotificationConfigContainer;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MessageSubmittedNotifyUnread
{
	/**
	 * @var UserPresenceOnChatChannels
	 */
	private $userPresenceOnChatChannels;
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;

	public function __construct(UserPresenceOnChatChannels $userPresenceOnChatChannels, ChatRepository $chatRepository)
    {
		$this->userPresenceOnChatChannels = $userPresenceOnChatChannels;
		$this->chatRepository = $chatRepository;
	}

    public function handle(MessageSubmitted $event)
    {
    	$chat = $event->getChat();
    	$message = $event->getChatMessage();
    	$userFrom = $message->getUser();
    	/** @var  $usersChat User[] */
		$usersChat = $this->chatRepository->getChatUsers($chat, $userFrom);
		foreach ($usersChat as $userChat) {
			// checkable user notification config
			if ($this->userPresenceOnChatChannels->isUserNeedsNotify($userChat, $chat->getKey())) {
				$notification = new UnreadChatMessageNotification($chat, $message, $userFrom);
				$configContainer = UserNotificationConfigContainer::make($userChat);
				if ($configContainer->isEnabledMailChatMessage()) {
					$notification->enableViaMail();
				}
				$notification->setBrowserPopup($configContainer->isEnabledBrowserChatMessage());
				$userChat->notify($notification);
			}
		}
    }
}
