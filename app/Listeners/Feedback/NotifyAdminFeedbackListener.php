<?php declare(strict_types=1);

namespace App\Listeners\Feedback;

use App\DataContainers\Mail\FeedbackReportMailData;
use App\Events\Feedback\FeedbackCreated;
use App\Mail\Feedback\NotifyAdminReportFeedbackMail;
use Illuminate\Support\Facades\Mail;
use Psr\Log\LoggerInterface;

class NotifyAdminFeedbackListener
{
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	public function handle(FeedbackCreated $event): void
	{
		try {
			Mail::queue(
				app(NotifyAdminReportFeedbackMail::class, [
					'feedback' => FeedbackReportMailData::createFromEntity($event->getFeedback()),
				])
			);
		} catch (\Throwable $e) {
			$this->logger->error($e);
		}
	}
}
