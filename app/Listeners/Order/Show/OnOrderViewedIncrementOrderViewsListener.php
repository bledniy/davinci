<?php

namespace App\Listeners\Order\Show;

use App\Events\Order\Show\OrderViewedEvent;
use App\Platform\Order\Voters\Deal\IsUserOwnerDeal;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderViewsRepository;

class OnOrderViewedIncrementOrderViewsListener
{

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var OrderViewsRepository
	 */
	private $orderViewsRepository;

	public function __construct(OrderRepository $orderRepository, OrderViewsRepository $orderViewsRepository)
    {
		$this->orderRepository = $orderRepository;
		$this->orderViewsRepository = $orderViewsRepository;
	}

    public function handle(OrderViewedEvent $event)
    {
		$order = $event->getOrder();
		$user = $event->getCurrentUser();
		if (is_null($user)) {
		    return;
		}
		if (IsUserOwnerDeal::make()->check($order->getCustomer(), $user)){
			return;
		}
		if ($this->orderViewsRepository->isUserHasView($user, $order)) {
		    return;
		}
		$this->orderViewsRepository->createView($order, $user);
		$order->getSetter()->incrementViews();
		$this->orderRepository->update([], $order);
    }
}
