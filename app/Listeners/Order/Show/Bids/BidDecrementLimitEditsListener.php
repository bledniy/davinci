<?php

	namespace App\Listeners\Order\Show\Bids;

	use App\Contracts\Events\Order\EventHasOrder;
	use App\Repositories\Order\OrderBidRepository;

	class BidDecrementLimitEditsListener
	{
		/**
		 * @var OrderBidRepository
		 */
		private $orderBidRepository;

		/**
		 * Create the event listener.
		 *
		 * @param OrderBidRepository $orderBidRepository
		 */
		public function __construct(OrderBidRepository $orderBidRepository)
		{
			$this->orderBidRepository = $orderBidRepository;
		}

		public function handle(EventHasOrder $event)
		{
			$bid = $event->getOrderBid();
			if (!$bid->getDirty()) {
				return;
			}
			$bid->getSetter()->decrementEdit()->wasModified();
		}
	}
