<?php

namespace App\Listeners\Order\Show;

use App\Events\Chat\MessageSubmitted;
use App\Events\Order\Chat\ChatCreated;
use App\Events\Order\Chat\UsersAddedToChat;
use App\Events\Order\Show\Bids\ChoosedPerformer;
use App\Helpers\Debug\LoggerHelper;
use App\Models\Chat\ChatMessage;
use App\Notifications\Order\UserSelectedAsPerformerNotification;
use App\Platform\Cabinet\Display\UserLastActivity;
use App\Platform\Chat\Jobs\AddUsersToChatJob;
use App\Platform\Chat\Jobs\ChatCreatorJob;
use App\Platform\Chat\Jobs\ChatMessageCreatorJob;
use App\Platform\Contract\Chat\ChatMessageTypesContract;
use App\Platform\Contract\Chat\ChatTypesContract;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyPerformerListener
{

	public function handle(ChoosedPerformer $event)
	{
		try {
			$order = $event->getOrder();
			$notification = new UserSelectedAsPerformerNotification($order);
			$performer = $event->getPerformer();
			$performer->notify($notification);
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
		}
	}
}
