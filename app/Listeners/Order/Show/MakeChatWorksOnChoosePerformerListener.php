<?php

namespace App\Listeners\Order\Show;

use App\Events\Chat\MessageSubmitted;
use App\Events\Order\Chat\ChatCreated;
use App\Events\Order\Chat\UsersAddedToChat;
use App\Events\Order\Show\Bids\ChoosedPerformer;
use App\Helpers\Debug\LoggerHelper;
use App\Models\Chat\ChatMessage;
use App\Platform\Cabinet\Display\UserLastActivity;
use App\Platform\Chat\Jobs\AddUsersToChatJob;
use App\Platform\Chat\Jobs\ChatCreatorJob;
use App\Platform\Chat\Jobs\ChatMessageCreatorJob;
use App\Platform\Contract\Chat\ChatMessageTypesContract;
use App\Platform\Contract\Chat\ChatTypesContract;
use App\Repositories\Chat\ChatRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MakeChatWorksOnChoosePerformerListener
{
	/**
	 * @var ChatRepository
	 */
	private $chatRepository;


	public function __construct(ChatRepository $chatRepository)
	{
		$this->chatRepository = $chatRepository;
	}

	public function handle(ChoosedPerformer $event)
	{
		$order = $event->getOrder();
		$performer = $event->getPerformer();
		$customer = $order->getContainer()->getCustomer();
		try {
			if (!$chat = $this->chatRepository->getChatByUser($order, $performer)) {
				$chat = ChatCreatorJob::make($order, ['type' => ChatTypesContract::TYPE_WORKS])->create();
				event(new ChatCreated($order, $chat, $customer));
				$usersToChat = [$customer, $performer];
				AddUsersToChatJob::make($chat, $usersToChat)->add();
				event(new UsersAddedToChat($chat, $usersToChat));
			} else {
				$chat->setType(ChatTypesContract::TYPE_WORKS);
				$this->chatRepository->update([], $chat);
			}
			$message = getTranslate('chat.deal.on-select-performer', 'Я выбрал вас исполнителем для своего проекта');
			$chatMessage = ChatMessageCreatorJob::make($chat, $customer, $message)->withType(ChatMessageTypesContract::TYPE_SYSTEM_PERFORMER)->create();
			event(new MessageSubmitted($chat, $chatMessage));
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
		}
	}
}
