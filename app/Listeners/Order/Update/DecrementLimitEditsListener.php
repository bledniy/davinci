<?php

	namespace App\Listeners\Order\Update;

	use App\Contracts\Events\Order\EventHasOrder;

	class DecrementLimitEditsListener
	{
		/**
		 * Create the event listener.
		 *
		 * @return void
		 */
		public function __construct()
		{
			//
		}

		/**
		 * Handle the event.
		 *
		 * @param object $event
		 * @return void
		 */
		public function handle(EventHasOrder $event)
		{
			if ($event->getOrder()->getDirty()){
				$event->getOrder()->getSetter()->decrementEdit();
			}
		}
	}
