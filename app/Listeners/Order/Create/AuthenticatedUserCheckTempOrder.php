<?php

namespace App\Listeners\Order\Create;

use App\Events\Order\OrderBeforePublished;
use App\Events\Order\OrderPublished;
use App\Platform\Order\Helpers\TempOrderHelper;
use App\Platform\Route\PlatformRoutes;
use App\Repositories\Order\OrderRepository;
use App\Services\User\Login\UserLoginService;
use Illuminate\Auth\Events\Login;
use Throwable;

class AuthenticatedUserCheckTempOrder
{
	/**
	 * @var TempOrderHelper
	 */
	private $tempOrderHelper;
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var UserLoginService
	 */
	private $userLoginService;

	/**
	 * Create the event listener.
	 *
	 * @param TempOrderHelper $tempOrderHelper
	 * @param OrderRepository $orderRepository
	 * @param UserLoginService $userLoginService
	 */
	public function __construct(TempOrderHelper $tempOrderHelper, OrderRepository $orderRepository, UserLoginService $userLoginService)
	{
		$this->tempOrderHelper = $tempOrderHelper;
		$this->orderRepository = $orderRepository;
		$this->userLoginService = $userLoginService;
	}

	public function handle(Login $event)
	{
		try {
			if (!$user = $event->user) {
				return;
			}
			if (!$orderId = $this->tempOrderHelper->getOrderId()) {
				return;
			}
			if (!$order = $this->orderRepository->find($orderId)) {
				return;
			}

			$redirectTo = route(PlatformRoutes::ORDER_SHOW, $order->getSlug());
			$this->userLoginService->setRedirectTo($redirectTo);

			$order->getSetter()->setCustomerId($user->getAuthIdentifier());
			$this->orderRepository->update([], $order);

			event(app(OrderBeforePublished::class, compact('order')));
			event(app(OrderPublished::class, compact('order')));

		} catch (Throwable $e) {
			logger($e);
		}
	}
}
