<?php

namespace App\Listeners\Order\Review;

use App\Contracts\Events\Order\EventHasReview;
use App\Helpers\Debug\LoggerHelper;
use App\Jobs\Order\Review\RecountCustomerCountReviewsJob;
use App\Jobs\Order\Review\RecountCustomerRatingJob;
use App\Jobs\Order\Review\RecountPerformerCountReviewsJob;
use App\Jobs\Order\Review\RecountPerformerRatingJob;
use App\Platform\Contract\UserTypeContract;
use App\Platform\Model\UserFields;
use App\Repositories\ReviewRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Collection;

class UserRatingChangedListener
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	public function handle(EventHasReview $event): void
	{
		try {
			$review = $event->getReview();
			$user = $this->userRepository->addPublicCriteriaToQuery()->find($review->getAttribute('to_id'));
			if (!$user) {
				return;
			}
			if (!$order = $review->getOrder()) {
				return;
			}
			if ($review->getReviewAbout() === UserTypeContract::TYPE_PERFORMER) {
				RecountPerformerCountReviewsJob::dispatch($user);
				RecountPerformerRatingJob::dispatch($user);
				return;
			}
			RecountCustomerCountReviewsJob::dispatch($user);
			RecountCustomerRatingJob::dispatch($user);
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
			throwIfDev($e);
		}
	}
}
