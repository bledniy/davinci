<?php

namespace App\Listeners\Order\Review;

use App\Contracts\Events\Order\EventHasReview;

class CalculateAVGRatingReviewListener
{
	public function handle(EventHasReview $event)
	{
		$review = $event->getReview();
		$container = $review->getContainer();
		$rating = $container->getTotalSubRating() / $container->getCountSubRatings();
		$review->getSetter()->setRating($rating);
	}
}
