<?php

namespace App\Listeners\Order\Review;

use App\Contracts\Events\Order\EventHasOrder;
use App\Models\Order\Review;
use App\Notifications\Order\CustomerAddedReviewAboutPerformerNotification;
use App\Notifications\Order\DealCompletedPerformerNotification;
use App\Platform\Contract\UserTypeContract;
use App\Repositories\ReviewRepository;

class NotifyPerformerAboutCustomerComment
{

	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(ReviewRepository $reviewRepository)
	{
		$this->reviewRepository = $reviewRepository;
	}

	public function handle(EventHasOrder $event)
	{
		$order = $event->getOrder();
		$reviews = $this->reviewRepository->whereModel($order)->get();
		$performerReview = $reviews->where('about', UserTypeContract::TYPE_CUSTOMER)->first();
		$customerReview = $reviews->where('about', UserTypeContract::TYPE_PERFORMER)->first();
		$performer = $order->getContainer()->getPerformer();
		if (!$performerReview  && $customerReview && $performer) {
			$notification = new CustomerAddedReviewAboutPerformerNotification($order, $customerReview);
			$performer->notify($notification);
		}
	}
}
