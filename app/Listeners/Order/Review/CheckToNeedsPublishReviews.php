<?php

namespace App\Listeners\Order\Review;

use App\Contracts\Events\Order\EventHasOrder;
use App\Models\Order\Review;
use App\Notifications\Order\DealCompletedCustomerNotification;
use App\Notifications\Order\DealCompletedPerformerNotification;
use App\Platform\Contract\UserTypeContract;
use App\Repositories\ReviewRepository;

class CheckToNeedsPublishReviews
{

	/**
	 * @var ReviewRepository
	 */
	private $reviewRepository;

	public function __construct(ReviewRepository $reviewRepository)
	{
		$this->reviewRepository = $reviewRepository;
	}

	public function handle(EventHasOrder $event)
	{
		$order = $event->getOrder();
		$reviews = $this->reviewRepository->whereModel($order)->get();
		if ($reviews->count() === 2) {
			/** @var  $customerReview Review */
			foreach ($reviews as $review) {
				$review->getSetter()->publish();
				$this->reviewRepository->update([], $review);
			}
			$performer = $order->getContainer()->getPerformer();
			$customer = $order->getContainer()->getCustomer();

			$customerReview = $this->reviewRepository->getOneFromCustomer($order, $customer);

			if ($performer && $customerReview) {
				$notification = new DealCompletedPerformerNotification($order, $customerReview, $customer);
				$performer->notify($notification);
			}

			$performerReview = $this->reviewRepository->getOneFromPerformer($order, $performer);
			if ($performerReview) {
				$notification = new DealCompletedCustomerNotification($order, $performerReview, $performer);
				$customer->notify($notification);
			}
		}
	}
}
