<?php declare(strict_types=1);

namespace App\Listeners\Cabinet;

use App\Events\Cabinet\PaymentCallbackEvent;
use App\Services\User\Payment\PaymentUserPremiumServiceInterface;
use Psr\Log\LoggerInterface;
use Throwable;

class PaymentCallbackAddPremiumListener
{
	/**
	 * @var PaymentUserPremiumServiceInterface
	 */
	private $paymentUserPremiumService;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	public function __construct(PaymentUserPremiumServiceInterface $paymentUserPremiumService, LoggerInterface $logger)
	{
		$this->paymentUserPremiumService = $paymentUserPremiumService;
		$this->logger = $logger;
	}

	public function handle(PaymentCallbackEvent $event): void
	{
		$response = $event->getPaymentResponse();
		if (!$response->getStatus()->isPayed()) {
			return;
		}
		try{
			$this->paymentUserPremiumService->dispatchFromPaymentSystem($response);
		} catch (Throwable $e){
			$this->logger->error($e);
		}
	}
}
