<?php declare(strict_types=1);

namespace App\Listeners\Cabinet;

use App\DataContainers\Mail\CertificationData;
use App\Events\Cabinet\PerformerCertificationUploadedEvent;
use App\Mail\Cabinet\PerformerCertificationUploadedMail;
use Illuminate\Support\Facades\Mail;
use Psr\Log\LoggerInterface;

class PerformerCertificationNotifyAdminListener
{
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	public function __construct(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	public function handle(PerformerCertificationUploadedEvent $event)
	{
		try {
			Mail::queue(
				app(PerformerCertificationUploadedMail::class, [
					'data' => CertificationData::createFromEntity($event->getCertification()),
				])
			);
		} catch (\Throwable $e) {
			$this->logger->error($e);
		}
	}
}
