<?php

namespace App\Listeners\User\Registration;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Auth\Events\Registered;

class OnUserRegistered
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
		$this->userRepository = $userRepository;
	}

    public function handle(Registered $event)
    {
    	/** @var  $user User*/
		$user = $event->user;
		$user->getSetter()->setLastSeen()->setAuthenticatedAt();
		$this->userRepository->update([], $user);
    }
}
