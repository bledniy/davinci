<?php

namespace App\Listeners\User\Registration;

use App\Models\User;
use App\Services\User\Registration\UserRegistrationStepsService;
use Illuminate\Auth\Events\Registered;

class UserRegisteredFirstStepListener
{
	/**
	 * @var UserRegistrationStepsService
	 */
	private $registrationStepsService;

	/**
	 * Create the event listener.
	 *
	 * @param UserRegistrationStepsService $registrationStepsService
	 */
	public function __construct(UserRegistrationStepsService $registrationStepsService)
	{
		$this->registrationStepsService = $registrationStepsService;
	}

	/**
	 * Handle the event.
	 *
	 * @param Registered $event
	 * @return void
	 */
	public function handle(Registered $event)
	{
		$user = $event->user;
		if ($user instanceof User) {
			$this->registrationStepsService->setUserId($user->id);
		}
	}
}
