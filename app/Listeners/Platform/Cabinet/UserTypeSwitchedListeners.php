<?php

namespace App\Listeners\Platform\Cabinet;

use App\Events\Platform\Cabinet\UserTypeSwitched;
use App\Repositories\UserRepository;

class UserTypeSwitchedListeners
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
		$this->userRepository = $userRepository;
	}

	/**
	 * Handle the event.
	 *
	 * @param UserTypeSwitched $event
	 * @return void
	 */
    public function handle(UserTypeSwitched $event)
    {
        $user = $event->getUser();
		$newType = $event->getNewType();
		$user->getSetter()->setType($newType);

		try{
			$this->userRepository->update([], $user);
		} catch (\Throwable $e){
		}
    }
}
