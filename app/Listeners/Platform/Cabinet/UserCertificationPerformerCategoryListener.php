<?php

namespace App\Listeners\Platform\Cabinet;

use App\Events\Platform\Cabinet\UserCertificationUpdatedEvent;
use App\Helpers\Debug\LoggerHelper;
use App\Platform\Cache\PlatformCacheKeysGenerator;
use Illuminate\Support\Facades\Cache;

class UserCertificationPerformerCategoryListener
{
	public function handle(UserCertificationUpdatedEvent $event)
	{
		$user = $event->getUser();
		$certification = $event->getCertification();
		$categoryExists = $user->performerCategories()
			->where($certification->category()->getForeignKeyName(), $certification->getBelongsKey('category'))
			->count();

		if ($categoryExists) {
			if (!$certification->getCertificationStatuses()->isSuccess()) {
				$user->performerCategories()->detach($certification->getBelongsKey('category'));
				return;
			}
		} else {
			$user->performerCategories()->attach($certification->getBelongsKey('category'));
		}

		try{
			$cacheKey = PlatformCacheKeysGenerator::userCategoryCertification($user, $certification->getCategory());
			Cache::forget($cacheKey);
		} catch (\Throwable $e){
		    app(LoggerHelper::class)->error($e);
		}
		
	}
}
