<?php

namespace App\Listeners\Platform\User\Analytics;

use App\Events\Platform\User\Analytics\UserLastSeedUpdated;
use App\Repositories\UserRepository;
use Prettus\Validator\Exceptions\ValidatorException;

class UserLastSeedUpdatedListener
{
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * Create the event listener.
	 *
	 * @param UserRepository $userRepository
	 */
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	/**
	 * Handle the event.
	 *
	 * @param UserLastSeedUpdated $event
	 * @return void
	 * @throws ValidatorException
	 */
	public function handle(UserLastSeedUpdated $event): void
	{
		$user = $event->getUser();
		$date = $event->getDateLastSeen();
		$user->setDateLastSeen($date);
		$this->userRepository->update([], $user);
	}
}
