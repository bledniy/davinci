<?php

namespace App\Broadcasting;

use App\Models\User;
use App\Platform\Cabinet\Factory\UserResourceFactory;

class NewDealBidChannel
{
	public function join(User $user, $dealId): bool
	{
		return $user->getKey();
	}
}
