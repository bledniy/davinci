<?php

	namespace App\Broadcasting;

	use App\Models\User;
	use App\Platform\Cabinet\Factory\UserResourceFactory;

	class UsersNotificationsChannel
	{
		public function join($userConnected, $user)
		{
			try {
				return $userConnected->getKey() === (int)$user;
			} catch (\Throwable $e) {
				return false;
			}
		}
	}
