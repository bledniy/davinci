<?php

namespace App\Broadcasting;

use App\Models\User;

class UserChatsChannel
{
	/**
	 * Create a new channel instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	public function join(User $user, $userId)
	{
		if ($user->getKey() === (int)$userId) {
			return (int)$userId;
		}
		return false;
	}
}
