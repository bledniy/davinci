<?php

namespace App\Broadcasting;

use App\Models\User;
use App\Repositories\Chat\ChatRepository;

class UserChatChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function join(User $user, $chatId)
    {
    	if (app(ChatRepository::class)->isUserInChat($user, $chatId)) {
    	    return $user->getKey();
    	}
		return false;
    }

}
