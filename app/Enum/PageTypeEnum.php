<?php declare(strict_types=1);

namespace App\Enum;

final class PageTypeEnum extends AbstractStrEnum
{
	public const ABOUT = 'about';
	public const MAIN = 'main';
	public const OFFER = 'offer';
	public const PRIVACY = 'privacy';
	public const FOR_PERFORMER = 'for-performer';
	public const PAYMENT_AND_RETURN = 'payment-and-return';

	public static $enums = [
		'О нас' => self::ABOUT,
		'Главная' => self::MAIN,
		'Оферта' => self::OFFER,
		'Политика конфиденциальности' => self::PRIVACY,
		'Для исполнителя' => self::FOR_PERFORMER,
		'Оплата и возврат' => self::PAYMENT_AND_RETURN,
	];

	public function isAbout(): bool
	{
		return $this->getKey() === self::ABOUT;
	}

	public function isMain(): bool
	{
		return $this->getKey() === self::MAIN;
	}

	public function isOffer(): bool
	{
		return $this->getKey() === self::OFFER;
	}

	public function isPrivacy(): bool
	{
		return $this->getKey() === self::PRIVACY;
	}

	public function isForPerformer(): bool
	{
		return $this->getKey() === self::FOR_PERFORMER;
	}
}