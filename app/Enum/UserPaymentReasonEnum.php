<?php declare(strict_types=1);

namespace App\Enum;

final class UserPaymentReasonEnum extends AbstractStrEnum
{
	public const PREMIUM_PAYED = 'PREMIUM_PAYED';
	public const FREE_PREMIUM = 'FREE_PREMIUM';

	public static $enums = [
		'cabinet.payment.reasons.premium' => self::PREMIUM_PAYED,
		'cabinet.payment.reasons.free-premium' => self::FREE_PREMIUM,
	];
}