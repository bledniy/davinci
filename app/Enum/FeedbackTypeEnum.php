<?php declare(strict_types=1);

namespace App\Enum;

final class FeedbackTypeEnum extends AbstractStrEnum
{
	public const TYPE_REPORT = 'report';

	public const TYPE_DEFAULT = 'default';

	public static $enums = [
		self::TYPE_REPORT,
		self::TYPE_DEFAULT,
	];

	public function isReport(): bool
	{
		return $this->isEq(self::TYPE_REPORT);
	}

	public function isDefault(): bool
	{
		return $this->isEq(self::TYPE_DEFAULT);
	}
}