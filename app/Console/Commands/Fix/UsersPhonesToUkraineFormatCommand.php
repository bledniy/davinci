<?php declare(strict_types=1);

namespace App\Console\Commands\Fix;

use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\Phone\PhoneUkraineFormatter;
use Illuminate\Console\Command;

class UsersPhonesToUkraineFormatCommand extends Command
{
    protected $signature = 'fix:users.phone-reformat';

    protected $description = 'Make users phone to ukraine format +38099... (means remove symbols like " ( ) ")';
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
		$this->userRepository = $userRepository;
	}

    public function handle()
    {
		$this->userRepository->all()->map(function(User $user){
			$user->getSetter()->setPhone(PhoneUkraineFormatter::formatPhone($user->getPhone()));
			try{
				$this->userRepository->update([], $user);
			} catch (\Throwable $e){
			    $this->warn($e->getMessage());
			}
		});
        return 1;
    }
}
