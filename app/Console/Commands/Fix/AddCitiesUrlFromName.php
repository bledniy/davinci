<?php declare(strict_types=1);

namespace App\Console\Commands\Fix;

use App\Models\City\CityLang;
use App\Repositories\CityRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class AddCitiesUrlFromName extends Command
{
	protected $signature = 'fix:cities.add.name';

	protected $description = 'Command description';
	/**
	 * @var CityRepository
	 */
	private $repository;

	public function __construct(
		CityRepository $repository
	)
	{
		parent::__construct();
		$this->repository = $repository;
	}

	public function handle()
	{
		$cities = $this->repository->all();
		$cities->load('lang');
		/** @var  $city CityLang */
		foreach ($cities as $city) {
			if ($city->getUrl()) {
				continue;
			}
			$city->setAttribute('url', Str::slug($city->getName()));
			$city->save();
		}

		return 1;
	}
}
