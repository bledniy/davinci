<?php

namespace App\Console\Commands\Premium;

use App\Models\User;
use App\Notifications\Premium\PremiumExpiredNotification;
use App\Repositories\PerformerRepository;
use App\Repositories\UserPremiumRepository;
use Illuminate\Console\Command;

class NotifyExpiredPremium extends Command
{
	protected $signature = 'premium:notify.expired';

	protected $description = 'Notify users about expired premium account';

	/**
	 * @var UserPremiumRepository
	 */
	private $userPremiumRepository;

	/**
	 * @var PerformerRepository
	 */
	private $performerRepository;

	public function __construct(UserPremiumRepository $userPremiumRepository, PerformerRepository $performerRepository)
	{
		parent::__construct();
		$this->userPremiumRepository = $userPremiumRepository;
		$this->performerRepository = $performerRepository;
	}

	public function handle()
	{
		$userPremiums = $this->userPremiumRepository->getExpiredPremiumUsers();
		if ($userPremiums->isEmpty()) {
			return 1;
		}
		//todo add check if is notified
		$userPremiums = $userPremiums->keyBy('user_id');
		$performers = $this->performerRepository->find($userPremiums->pluck('user_id')->toArray());
		$performers->each(function (User $user) use ($userPremiums) {
			$premium = $userPremiums->get($user->getKey());
			if ($premium) {
				$notification = new PremiumExpiredNotification($userPremiums->get($user->getKey()));
				$notification->enableViaMail();
				$user->notify($notification);
			}
		});

		return 1;
	}
}
