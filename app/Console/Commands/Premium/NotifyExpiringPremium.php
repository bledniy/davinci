<?php

namespace App\Console\Commands\Premium;

use App\Models\User;
use App\Notifications\Premium\PremiumExpiringNotification;
use App\Repositories\PerformerRepository;
use App\Repositories\UserPremiumRepository;
use Illuminate\Console\Command;

class NotifyExpiringPremium extends Command
{
	protected $signature = 'premium:notify.expiring';

	protected $description = 'Notify users about expiring premium account';

	/**
	 * @var UserPremiumRepository
	 */
	private $userPremiumRepository;
	/**
	 * @var PerformerRepository
	 */
	private $performerRepository;

	public function __construct(UserPremiumRepository $userPremiumRepository, PerformerRepository $performerRepository)
	{
		parent::__construct();
		$this->userPremiumRepository = $userPremiumRepository;
		$this->performerRepository = $performerRepository;
	}

	public function handle()
	{
		$userPremiums = $this->userPremiumRepository->getExpiringPremiumUsers(5);
		if ($userPremiums->isEmpty()) {
			return 1;
		}
		$userPremiums = $userPremiums->keyBy('user_id');
		$performers = $this->performerRepository->find($userPremiums->pluck('user_id')->toArray());
		$performers->each(function (User $user) use ($userPremiums) {
			$premium = $userPremiums->get($user->getKey());
			if ($premium) {
				$notification = new PremiumExpiringNotification($premium);
				$notification->enableViaMail();
				$user->notify($notification);
			}
		});

		return 1;
	}


}
