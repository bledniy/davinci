<?php

namespace App\Console\Commands\Order;

use App\Jobs\Notification\NotifyNewBidsDealJob;
use App\Models\Order\Order;
use App\Repositories\Order\OrderRepository;
use Illuminate\Console\Command;

class NotifyNewBidsCommand extends Command
{
	private $interval = 10;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:new-bids.notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	public function __construct(OrderRepository $orderRepository)
	{
		parent::__construct();
		$this->interval = (int)config('notifications.deals.new-bids.interval', 10);
		$this->orderRepository = $orderRepository;
	}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$deals = $this->orderRepository->where('active', 1)->where('is_closed', 0)->whereHas('bids', function ($q) {
			$q->where('created_at', '>=', \DB::raw(sprintf('NOW() - INTERVAL %d MINUTE', $this->interval)));
		})
			->with(['customer','bids' => function($q){
				$q->where('created_at', '>=', \DB::raw(sprintf('NOW() - INTERVAL %d MINUTE', $this->interval)))
				->whereNull('canceled_at');
			}, 'bids.user'])->get();
		$deals->map(function(Order $order){
			NotifyNewBidsDealJob::dispatch($order);
		});
		return  1;
    }
}
