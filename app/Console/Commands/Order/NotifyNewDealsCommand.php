<?php

namespace App\Console\Commands\Order;

use App\Jobs\Notification\NotifyNewDealsJob;
use App\Models\Order\Order;
use App\Models\User;
use App\Platform\Notifications\Contracts\NotificationTypesContract;
use App\Platform\User\Notification\NotificationConfigTypes;
use App\Platform\User\Notification\UserNotificationConfigContainer;
use App\Repositories\NotificationConfigRepository;
use App\Repositories\Order\OrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Console\Command;

class NotifyNewDealsCommand extends Command
{
	private $interval;
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'orders:new-deals.notify';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;
	/**
	 * @var NotificationConfigRepository
	 */
	private $notificationConfigRepository;
	/**
	 * @var UserRepository
	 */
	private $userRepository;

	public function __construct(OrderRepository $orderRepository, NotificationConfigRepository $notificationConfigRepository, UserRepository $userRepository)
	{
		parent::__construct();
		$this->interval = (int)config('notifications.deals.new-deals.interval', 60);
		$this->orderRepository = $orderRepository;
		$this->notificationConfigRepository = $notificationConfigRepository;
		$this->userRepository = $userRepository;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$types = [NotificationConfigTypes::BROWSER_NEW_ORDERS, NotificationConfigTypes::MAIL_NEW_ORDERS];
		$usersToNotify = $this->userRepository->find($this->notificationConfigRepository->getUsersIdWithEnabledTypes($types));
		$deals = $this->orderRepository->getNewDeals($this->interval);

		$usersToNotify->map(function (User $user) use ($deals) {
			NotifyNewDealsJob::dispatch($deals, $user);
		});
		return 1;
	}
}
