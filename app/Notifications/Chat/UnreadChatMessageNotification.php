<?php

namespace App\Notifications\Chat;

use App\Http\Resources\Chat\ChatMessageResource;
use App\Models\Chat\Chat;
use App\Models\Chat\ChatMessage;
use App\Models\User;
use App\Notifications\AbstractNotification;
use App\Platform\Notifications\Contracts\NotificationTypesContract;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use function translateFormat;
use function url;

class UnreadChatMessageNotification extends AbstractNotification
{
	use Queueable;

	/**
	 * @var Chat
	 */
	private $chat;
	/**
	 * @var ChatMessage
	 */
	private $chatMessage;
	/**
	 * @var User|null
	 */
	private $messageOwner;

	protected $database = false;
	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Chat $chat, ChatMessage $chatMessage, ?User $messageOwner = null)
	{
		$this->chat = $chat;
		$this->chatMessage = $chatMessage;
		$this->messageOwner = $messageOwner;
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		return (new MailMessage)
			->greeting('')
			->line('The introduction to the notification.')
			->action('Notification Action', url('/'))
			->line('Thank you for using our application!')
			;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		$langId =  \App\Platform\User\UserLanguageExtractor::make($notifiable)->getLanguageId();
		$userName = $this->messageOwner ? $this->messageOwner->getFio() : '';
		return [
			'id'      => $this->id,
			'type'    => NotificationTypesContract::CHAT_MESSAGE,
			'title'   => translateFormat('notifications.chat.unread-message', ['%chat%' => $this->chat->name, '%username%' => $userName,], $langId),
			'message' => $this->chatMessage->getMessage(),
			'config' => [
				'html5_notification' => $this->browserPopup
			],
			'data' => [
				'message' => ChatMessageResource::make($this->chatMessage)
			]
		];
	}

}
