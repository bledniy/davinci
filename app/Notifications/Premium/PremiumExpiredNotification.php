<?php

namespace App\Notifications\Premium;

use App\Models\User\UserPremium;
use App\Notifications\AbstractNotification;
use App\Platform\Notifications\Contracts\NotificationTypesContract;
use App\Platform\ProAccount\ExpiringPremiumMessageGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;

class PremiumExpiredNotification extends AbstractNotification
{
	use Queueable;

	private $userPremium;

	public function __construct(UserPremium $userPremium)
	{
		$this->userPremium = $userPremium;
	}

	public function toMail($notifiable)
	{
		$messageExpires = ExpiringPremiumMessageGenerator::expired($this->userPremium);
		return (new MailMessage)
			->line($messageExpires)
			->action('Notification Action', url('/'))
			;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		$langId =  \App\Platform\User\UserLanguageExtractor::make($notifiable)->getLanguageId();
		$messageExpires = ExpiringPremiumMessageGenerator::expired($this->userPremium);
		return [
			'id'      => $this->id,
			'type'    => NotificationTypesContract::PRO_ACCOUNT_EXPIRED,
			'title'   => $messageExpires,
			'message' => getTranslate('notifications.pro-account.expiring.message', null , $langId),
			'config'  => [
				'html5_notification' => $this->browserPopup,
			],
			'data'    => [
			],
		];
	}
}
