<?php

namespace App\Notifications\Order;

use App\Models\Order\Order;
use App\Notifications\AbstractNotification;
use App\Platform\Notifications\Contracts\NotificationTypesContract;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use function getTranslate;

class OrderNewBidsNotification extends AbstractNotification
{
	use Queueable;

	/**
	 * @var Order
	 */
	private $order;

	private $bidsCount;

	public function __construct(Order $order)
	{
		$this->order = $order;
		$this->bidsCount = $this->order->bids->count();
	}

	public function toMail($notifiable)
	{
		return (new MailMessage)
			->line(sprintf('У вас %d новых ставок к сделке %s', $this->bidsCount, $this->order->getTitle()))
			->action('Перейти', frontendUrl('/orders/' . $this->order->getContainer()->getSlug()))
			;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		$langId =  \App\Platform\User\UserLanguageExtractor::make($notifiable)->getLanguageId();
		return [
			'id'      => $this->id,
			'type'    => NotificationTypesContract::DEAL_ADDED_BID,
			'title'   => getTranslate('notifications.deals.bids.new', null, $langId),
			'message' => translateFormat('notifications.deals.bids.new.message', ['%count%' => $this->bidsCount, ], $langId),
			'config'  => [
				'html5_notification' => $this->browserPopup,
			],
			'data'    => [
				'deal' => [
					'id'    => $this->order->getKey(),
					'slug'  => $this->order->getContainer()->getSlug(),
				],
			],
		];
	}
}
