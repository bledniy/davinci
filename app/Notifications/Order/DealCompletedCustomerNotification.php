<?php declare(strict_types=1);

namespace App\Notifications\Order;

use App\Models\Order\Order;
use App\Models\Order\Review;
use App\Models\User;
use App\Notifications\AbstractNotification;
use App\Platform\Notifications\Contracts\NotificationTypesContract;
use App\Platform\User\UserLanguageExtractor;
use Illuminate\Bus\Queueable;
use function getTranslate;

class DealCompletedCustomerNotification extends AbstractNotification
{
	use Queueable;

	/**
	 * @var Order
	 */
	private $order;
	/**
	 * @var Review
	 */
	private $review;
	/**
	 * @var User
	 */
	private $performer;

	public function __construct(Order $order, Review $review, User $performer)
	{
		$this->order = $order;
		$this->review = $review;
		$this->performer = $performer;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param mixed $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		$langId = UserLanguageExtractor::make($notifiable)->getLanguageId();

		return [
			'id' => $this->id,
			'type' => NotificationTypesContract::DEAL_CLOSED,
			'title' => getTranslate('notifications.deals.closed', null, $langId),
			'message' => sprintf('%s: %s', $this->performer->getFio(), $this->review->getContainer()->getComment()),
			'config' => [
				'html5_notification' => $this->browserPopup,
			],
			'data' => [
				'rating' => $this->review->getContainer()->getRating(),
				'deal' => [
					'id' => $this->order->getKey(),
					'slug' => $this->order->getContainer()->getSlug(),
				],

			],
		];
	}

}
