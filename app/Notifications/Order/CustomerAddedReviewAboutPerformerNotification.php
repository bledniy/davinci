<?php

	namespace App\Notifications\Order;

	use App\Models\Order\Order;
	use App\Models\Order\Review;
	use App\Notifications\AbstractNotification;
	use App\Platform\Notifications\Contracts\NotificationTypesContract;
	use Illuminate\Bus\Queueable;
	use function getTranslate;

	class CustomerAddedReviewAboutPerformerNotification extends AbstractNotification
	{
		use Queueable;

		/**
		 * @var Order
		 */
		private $order;
		/**
		 * @var Review
		 */
		private $review;

		public function __construct(Order $order, Review $review)
		{
			$this->order = $order;
			$this->review = $review;
		}

		/**
		 * Get the array representation of the notification.
		 *
		 * @param mixed $notifiable
		 * @return array
		 */
		public function toArray($notifiable)
		{
			$langId =  \App\Platform\User\UserLanguageExtractor::make($notifiable)->getLanguageId();
			return [
				'id'      => $this->id,
				'type'    => NotificationTypesContract::DEAL_ADDED_REVIEW_FROM_CUSTOMER,
				'title'   => getTranslate('notifications.deals.review.from-customer', null, $langId),
				'message' => getTranslate('notifications.deals.review.from-customer.opposite', null, $langId),
				'config'  => [
					'html5_notification' => $this->browserPopup,
				],
				'data'    => [
					'deal' => [
						'id'    => $this->order->getKey(),
						'slug'  => $this->order->getContainer()->getSlug(),
					],

				],
			];
		}

	}
