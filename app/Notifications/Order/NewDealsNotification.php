<?php

namespace App\Notifications\Order;

use App\Models\Order\Order;
use App\Notifications\AbstractNotification;
use App\Platform\Notifications\Contracts\NotificationTypesContract;
use App\Platform\User\UserLanguageExtractor;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Collection;
use function getTranslate;

class NewDealsNotification extends AbstractNotification
{
	use Queueable;

	/**
	 * @var Collection | Order[]
	 */
	private $deals;
	private $dealsCount;

	public function __construct(Collection $deals)
	{
		$this->deals = $deals;
		$this->dealsCount = $deals->count();
	}

	public function toMail($notifiable)
	{
		return (new MailMessage)
			->line(sprintf('Найдено %d новых сделок', $this->dealsCount))
			->action('Перейти', frontendUrl('/orders'))
			;
	}

	public function toArray($notifiable)
	{
		$langId = \App\Platform\User\UserLanguageExtractor::make($notifiable)->getLanguageId();
		return [
			'id'      => $this->id,
			'type'    => NotificationTypesContract::NEW_DEALS,
			'title'   => getTranslate('notifications.deals.new', null, $langId),
			'message' => translateFormat('notifications.deals.new.message', ['%count%' => $this->dealsCount,], $langId),
			'config'  => [
				'html5_notification' => $this->browserPopup,
			],
			'data'    => [
				'deals' => [
					'count' => $this->dealsCount,
				],
			],
		];
	}
}
