<?php

	namespace App\Notifications\Order;

	use App\Models\Order\Order;
	use App\Notifications\AbstractNotification;
	use App\Platform\Notifications\Contracts\NotificationTypesContract;
	use Illuminate\Bus\Queueable;
	use Illuminate\Notifications\Messages\MailMessage;
	use function getTranslate;
	use function url;

	class UserSelectedAsPerformerNotification extends AbstractNotification
	{
		use Queueable;

		/**
		 * @var Order
		 */
		private $order;

		public function __construct(Order $order)
		{
			$this->order = $order;
		}


		/**
		 * Get the array representation of the notification.
		 *
		 * @param mixed $notifiable
		 * @return array
		 */
		public function toArray($notifiable)
		{
			$langId =  \App\Platform\User\UserLanguageExtractor::make($notifiable)->getLanguageId();
			return [
				'id'      => $this->id,
				'type'    => NotificationTypesContract::SELECTED_AS_PERFORMER,
				'title'   => getTranslate('notifications.deals.selected-as-performer', null, $langId),
				'message' => getTranslate('notifications.deals.selected-as-performer.message', null, $langId),
				'config'  => [
					'html5_notification' => $this->browserPopup,
				],
				'data'    => [
					'deal' => [
						'id'    => $this->order->getKey(),
						'slug'  => $this->order->getContainer()->getSlug(),
						'title' => $this->order->getContainer()->getTitle(),
					],
				],
			];
		}

	}
