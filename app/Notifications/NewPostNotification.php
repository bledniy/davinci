<?php

	namespace App\Notifications;

	use App\Http\Resources\Chat\ChatMessageResource;
	use App\Models\Chat\Chat;
	use App\Models\Chat\ChatMessage;
	use App\Models\News\News;
	use App\Models\User;
	use App\Notifications\AbstractNotification;
	use App\Platform\Notifications\Contracts\NotificationTypesContract;
	use Illuminate\Bus\Queueable;
	use Illuminate\Contracts\Queue\ShouldQueue;
	use Illuminate\Notifications\Messages\BroadcastMessage;
	use Illuminate\Notifications\Messages\MailMessage;
	use Illuminate\Notifications\Notification;
	use function getTranslate;
	use function translateFormat;
	use function url;

	class NewPostNotification extends AbstractNotification
	{
		use Queueable;

		/**
		 * @var News
		 */
		private $news;

		/**
		 * NewPostNotification constructor.
		 * @param News $news
		 */
		public function __construct(News $news)
		{
			$this->news = $news;
		}

		/**
		 * Get the mail representation of the notification.
		 *
		 * @param mixed $notifiable
		 * @return \Illuminate\Notifications\Messages\MailMessage
		 */
		public function toMail($notifiable)
		{
			return (new MailMessage)
				->line(__METHOD__)
				->action('Notification Action', url('/'))
				->line('Thank you for using our application!')
				;
		}

		/**
		 * Get the array representation of the notification.
		 *
		 * @param mixed $notifiable
		 * @return array
		 */
		public function toArray($notifiable)
		{
			$langId =  \App\Platform\User\UserLanguageExtractor::make($notifiable)->getLanguageId();
			return [
				'id'      => $this->id,
				'type'    => NotificationTypesContract::NEW_BLOG_POST,
				'title'   => getTranslate('notifications.news.new', null, $langId),
				'message' => $this->news->name,
				'config'  => [
					'html5_notification' => $this->browserPopup,
				],
				'data'    => [
					'post' => ['slug' => $this->news->getSlug()],
				],
			];
		}

		public function toBroadcast($notifiable)
		{
			return parent::toBroadcast($notifiable);
		}

	}
