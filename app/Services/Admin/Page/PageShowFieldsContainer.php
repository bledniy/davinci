<?php declare(strict_types=1);

namespace App\Services\Admin\Page;

final class PageShowFieldsContainer
{
	private $withImage = false;

	public function isWithImage(): bool
	{
		return $this->withImage;
	}

	public function setWithImage(bool $withImage): self
	{
		$this->withImage = $withImage;

		return $this;
	}
}