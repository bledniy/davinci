<?php


namespace App\Services\Verification;


use App\Helpers\Debug\LoggerHelper;
use App\Mail\Register\VerifyMail;
use App\Models\User;
use App\Models\Verification;
use App\Repositories\VerificationRepository;
use App\Services\SMS\SMSSenderService;
use Illuminate\Support\Facades\Mail;

abstract class BaseVerificationService
{
	protected $wasCreatedNew = false;
	protected $wasRegenerated = false;

	// cache for
	protected $existed = false;

	/** @var Verification */
	protected $verification;

	/**
	 * @var VerificationRepository
	 */
	protected $verificationRepository;

	public function __construct()
	{
		$this->verificationRepository = app(VerificationRepository::class);
	}

	abstract protected function getTarget();

	abstract protected function getType();

	public function deleteVerification(): void
	{
		if (!$verification = $this->getExistedVerification()) {
			return;
		}
		$this->verificationRepository->delete($verification->getKey());
	}

	protected function getCodeData(): array
	{
		return [
			'code' => random_int(1000, 9999),
			'type' => $this->getType(),
			'target' => $this->getTarget(),
			'available_at' => now()->addMinute(),
		];
	}

	public function getExistedVerification(): ?Verification
	{
		if (false === $this->existed) {
			$this->existed = $this->verificationRepository->where('target', $this->getTarget())->where('type', $this->getType())->first();
		}

		return $this->existed;
	}

	public function getNewOrRegenerateVerification(): Verification
	{
		if ($this->getExistedVerification()) {
			return $this->regenerateCode();
		}
		$this->wasCreatedNew = true;

		return $this->generateVerification();
	}

	public function generateVerification(?array $data = null): Verification
	{
		$data = $data ?? $this->getCodeData();
		$this->existed = $this->verification = $this->verificationRepository->create($data);
		$this->wasCreatedNew = true;

		return $this->existed;
	}

	public function regenerateCode(?Verification $verification = null): Verification
	{
		$verification = $verification ?: $this->getExistedVerification();
		$this->verification = $verification;
		$this->verificationRepository->update($this->getCodeData(), $verification);
		$this->wasRegenerated = true;

		return $verification;
	}

	abstract public function notify();

	/**
	 * @return bool
	 */
	public function isWasCreatedNew(): bool
	{
		return $this->wasCreatedNew;
	}

	/**
	 * @return bool
	 */
	public function isWasRegenerated(): bool
	{
		return $this->wasRegenerated;
	}

	public function verified(): self
	{
		$this->verificationRepository->update([], $this->getNewOrRegenerateVerification()->setIsVerified());

		return $this;
	}

	protected function notifyViaSms()
	{
		try {
			app(SMSSenderService::class)->send($this->getTarget(), $this->getNewOrRegenerateVerification()->getCode());
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
		}
	}

	protected function notifyViaMail(?User $user = null)
	{
		try {
			$mail = app(VerifyMail::class, ['verification' => $this->getNewOrRegenerateVerification(), 'user' => $user]);
			$mail->to($this->getTarget());
			Mail::queue($mail);
		} catch (\Throwable $e) {
			app(LoggerHelper::class)->error($e);
		}
	}


}