<?php


namespace App\Services\ApiData;


use App\Http\Resources\HowResource;
use App\Repositories\ContentRepository;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class HowItWorksApiData
{

	/**
	 * @var ContentRepository
	 */
	private $repository;

	public function __construct(ContentRepository $repository)
	{
		$this->repository = $repository;
	}

	public function getResponse()
	{
		$hows = $this->repository->getListPublicByType('how');
		$hows->load('lang');
		$res = HowResource::collection($hows);
		return [
			'data'    => $res,
			'section' => [
				'title'       => getTranslate('for-performer.how-it-works.title', 'Как начать сотрудничество?'),
				'description' => getTranslate('for-performer.how-it-works.description', ''),
			],
		];
	}
}