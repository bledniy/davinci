<?php


namespace App\Services\ApiData;


use App\Http\Resources\FaqResource;
use App\Repositories\FaqRepository;
use Illuminate\Support\Str;

class FaqApiData
{

	/**
	 * @var FaqRepository
	 */
	private $repository;

	public function __construct(FaqRepository $repository)
	{
		$this->repository = $repository;
	}

	public function getResponse($type)
	{
		$faqs = $this->repository->getListPublic(Str::upper($type));
		$faqs->load('lang');
		$res = FaqResource::collection($faqs);
		return [
			'data'    => $res,
			'section' => [
				'title' => getTranslate('faq.title', 'Вопросы и ответы'),
			],
		];
	}
}