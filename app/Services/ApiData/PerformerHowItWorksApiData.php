<?php


namespace App\Services\ApiData;


use App\Http\Resources\HowResource;
use App\Repositories\ContentRepository;

class PerformerHowItWorksApiData
{

	/**
	 * @var ContentRepository
	 */
	private $repository;

	public function __construct(ContentRepository $repository)
	{
		$this->repository = $repository;
	}

	public function getResponse()
	{
		$hows = $this->repository->getListPublicByType('how-performer');
		$hows->load('lang');
		$res = HowResource::collection($hows);
		return [
			'data'    => $res,
			'section' => [
				'title'       => getTranslate('how-it-works.performer.title', 'Как начать сотрудничество?'),
				'description' => getTranslate('how-it-works.performer.description', ''),
			],
		];
	}
}