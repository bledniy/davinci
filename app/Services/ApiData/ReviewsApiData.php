<?php


namespace App\Services\ApiData;


use App\Http\Resources\ReviewResource;
use App\Repositories\ContentRepository;

class ReviewsApiData
{

	/**
	 * @var ContentRepository
	 */
	private $repository;

	public function __construct(ContentRepository $repository)
	{
		$this->repository = $repository;
	}

	public function getResponse()
	{
		$reviews = $this->repository->getListPublicByType('reviews');
		$reviews->load('lang');
		$res = ReviewResource::collection($reviews);
		return [
			'data'    => $res,
			'section' => [
				'title' => getTranslate('reviews.title'),
			],
		];
	}
}