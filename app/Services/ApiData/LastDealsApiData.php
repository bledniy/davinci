<?php


namespace App\Services\ApiData;


use App\Repositories\Order\OrderRepository;
use Illuminate\Support\Facades\Cache;

class LastDealsApiData
{

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	public function __construct(OrderRepository $orderRepository)
	{
		$this->orderRepository = $orderRepository;
	}

	public function getResponse()
	{
		return  Cache::get('pages:main.last-deals', function () {
			$data = $this->orderRepository->getLastDeals();
			$this->orderRepository->loadForDeals($data);
			Cache::set('pages:main.last-deals', $data, now()->addSeconds(10));
			return $data;
		});
	}
}