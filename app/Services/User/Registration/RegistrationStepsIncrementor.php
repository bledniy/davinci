<?php


namespace App\Services\User\Registration;


class RegistrationStepsIncrementor
{
	public function incrementStep($step)
	{
		return ($step === $this->getLastStep()) ? $step : ++$step;
	}

	private function getLastStep()
	{
		return UserRegistrationSteps::LAST_STEP;
	}
}