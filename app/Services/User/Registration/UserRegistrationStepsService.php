<?php


namespace App\Services\User\Registration;


use App\Traits\ResolveSelf;

class UserRegistrationStepsService
{
	/**
	 * @var RegistrationStepsIncrementor
	 */
	private $stepsIncrementor;

	public function __construct(RegistrationStepsIncrementor $stepsIncrementor)
	{
		$this->stepsIncrementor = $stepsIncrementor;
	}

	private $prefix = 'registration';

	use ResolveSelf;

	protected function setStep($step)
	{
		$this->storeData($this->getStepKey(), $step);
		return $this;
	}

	private function incrementStep(): self
	{
		$this->setStep($this->stepsIncrementor->incrementStep($this->getStep()));
		return $this;
	}


	public function hasSteps(): bool
	{
		return (bool)$this->getStep();
	}

	public function finishRegistration(): void
	{
		session()->remove($this->prefix);
	}

	public function setUserId(int $id): self
	{
		$this->storeData('user_id', $id);
		return $this;
	}

	public function getUserId(): ?int
	{
		return $this->getData('user_id');
	}

	public function personalDataSubmitted(): self
	{
		$this->setStep(UserRegistrationSteps::STEP_PERSONAL_DATA);
		return $this->incrementStep();
	}

	public function phoneFormatValidated(): self
	{
		return $this->incrementStep();
	}

	public function phoneVerified(): self
	{
		return $this->incrementStep();
	}

	public function typeUserSelected(): self
	{
		$this->incrementStep();
		return $this;
	}

	//
	public function isStepPersonalData(): bool
	{
		return (!$this->hasSteps()) ? false : ($this->getStep() === UserRegistrationSteps::STEP_PERSONAL_DATA);
	}

	public function isStepConfirmPhone(): bool
	{
		return (!$this->hasSteps()) ? false : ($this->getStep() === UserRegistrationSteps::STEP_CONFIRM_PHONE);
	}

	public function isStepCheckPhone(): bool
	{
		return (!$this->hasSteps()) ? false : ($this->getStep() === UserRegistrationSteps::STEP_CHECK_PHONE);
	}

	public function isStepTypeUser(): bool
	{
		return (!$this->hasSteps()) ? false : ($this->getStep() === UserRegistrationSteps::STEP_TYPE_USER);
	}

	//

	private function getStepKey(): string
	{
		return 'step';
	}

	public function getStep()
	{
		return (int)($this->getData($this->getStepKey()) ?? UserRegistrationSteps::STEP_PERSONAL_DATA);
	}

	private function storeData(string $key, $data): void
	{
		$prefix = $this->getPrefix();
		session()->put($prefix . $key, $data);
	}

	private function getData(string $key)
	{
		$prefix = $this->getPrefix();
		return session($prefix . $key);
	}

	private function getPrefix(): string
	{
		return $this->prefix . '.';
	}

	/**
	 * @return bool
	 */

	private function getSocialKey()
	{
		return 'is-socials';
	}

	public function isSocialRegister(): bool
	{
		return (int)$this->getData($this->getSocialKey());
	}

	public function setSocialRegister(bool $socialRegister): self
	{
		$this->storeData($this->getSocialKey(), (int)$socialRegister);
		return $this;
	}
}