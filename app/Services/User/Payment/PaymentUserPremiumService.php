<?php declare(strict_types=1);

namespace App\Services\User\Payment;

use App\Enum\UserPaymentReasonEnum;
use App\Models\User;
use App\Models\User\UserPayment;
use App\Platform\ProAccount\UserProAccountActivationContainer;
use App\Platform\ProAccount\UserProAccountActivator;
use App\Repositories\PaymentRepository;
use App\Repositories\PremiumRepository;
use App\Repositories\UserPaymentRepositoryEloquent;
use App\Services\Payment\Portmone\PaymentResponseDataInterface;
use RuntimeException;
use Throwable;

final class PaymentUserPremiumService implements PaymentUserPremiumServiceInterface
{
	/**
	 * @var PaymentRepository
	 */
	private $paymentRepository;
	/**
	 * @var UserProAccountActivator
	 */
	private $proAccountActivator;
	/**
	 * @var PremiumRepository
	 */
	private $premiumRepository;

	public function __construct(
		UserPaymentRepositoryEloquent $paymentRepository,
		UserProAccountActivator       $proAccountActivator,
		PremiumRepository             $premiumRepository
	)
	{
		$this->paymentRepository = $paymentRepository;
		$this->proAccountActivator = $proAccountActivator;
		$this->premiumRepository = $premiumRepository;
	}

	/**
	 * @throws Throwable
	 */
	public function dispatchFromPaymentSystem(PaymentResponseDataInterface $data): void
	{
		/** @var  $payment UserPayment */
		$payment = $this->paymentRepository->find($data->getOrderId());
		if ($payment->isPayed()) {
			return;
		}

		$payment->fillFromPaymentResponseData($data)
			->setReason(new UserPaymentReasonEnum(UserPaymentReasonEnum::PREMIUM_PAYED))
			->setIsPayed(true)
			->save()
		;
		$user = $payment->getUser();
		if (null === $user) {
			throw new RuntimeException('User cannot be null');
		}

		//todo переделать по нормальному, пока оплата только на 1 месяц
		$premium = $this->premiumRepository->getDefaultPremium();
		$months = ($data->getPayedAmount() / $payment->getNeedPay());
		$proAccActivationContainer = new UserProAccountActivationContainer;
		if ((int)$months > 0) {
			$proAccActivationContainer->setMonths((int)$months);
		} else {
			$proAccActivationContainer->setDays((int)( $months * 30));
		}
		$this->proAccountActivator->extendDateExpirationProAccount(
			$premium,
			$user->getPremiumAccountService()->getProAccount(),
			$proAccActivationContainer
		);
	}

	public function getBilling(User $user): UserPayment
	{
		$payment = $this->paymentRepository->findUnPayedPayment($user);
		if (null !== $payment) {
			return $payment;
		}
		$premium = $this->premiumRepository->getDefaultPremium();
		$payment = new UserPayment();
		$payment->setNeedPay($premium->getPrice());
		$payment->user()->associate($user);
		$payment->save();

		return $payment;
	}
}