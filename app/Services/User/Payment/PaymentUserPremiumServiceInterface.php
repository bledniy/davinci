<?php declare(strict_types=1);

namespace App\Services\User\Payment;

use App\Models\User;
use App\Models\User\UserPayment;
use App\Services\Payment\Portmone\PaymentResponseDataInterface;

interface PaymentUserPremiumServiceInterface
{
	public function dispatchFromPaymentSystem(PaymentResponseDataInterface $data): void;

	public function getBilling(User $user): UserPayment;
}