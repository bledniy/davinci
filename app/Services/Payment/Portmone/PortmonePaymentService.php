<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

use App\Services\Payment\Portmone\Exception\PaymentRequestFailedException;
use App\Services\Payment\Portmone\Exception\PaymentResponseParsingXmlFailedException;
use Illuminate\Http\Request;

final class PortmonePaymentService implements PortmonePaymentServiceInterface
{

	/**
	 * @var PaymentAuthRequestServiceInterface
	 */
	private $paymentAuthRequestService;

	public function __construct(PaymentAuthRequestServiceInterface $paymentAuthRequestService)
	{
		$this->paymentAuthRequestService = $paymentAuthRequestService;
	}

	public function isResponseValid(Request $request): bool
	{
		if (!$request->has('SHOPORDERNUMBER')) {
			return false;
		}

		try {
			$this->paymentAuthRequestService->getPaymentAuth((int)$request->get('SHOPORDERNUMBER'));
		} catch (PaymentRequestFailedException | PaymentResponseParsingXmlFailedException $e) {
			return false;
		}

		return true;
	}

	public function getParsedResponse(Request $request): PaymentResponseDataInterface
	{
		return $this->paymentAuthRequestService->getPaymentAuth((int)$request->get('SHOPORDERNUMBER'));
	}

}