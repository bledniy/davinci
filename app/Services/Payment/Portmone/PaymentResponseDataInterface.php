<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

use DateTimeInterface;

interface PaymentResponseDataInterface
{
	/**
	 * @return int
	 */
	public function getOrderId(): int;

	/**
	 * @return int
	 */
	public function getBillingId(): int;

	/**
	 * @return PaymentStatusEnum
	 */
	public function getStatus(): PaymentStatusEnum;

	/**
	 * @return int
	 */
	public function getPayedAmount(): int;

	/**
	 * @return DateTimeInterface
	 */
	public function getPayDateTime(): DateTimeInterface;
}