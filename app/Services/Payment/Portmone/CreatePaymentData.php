<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

final class CreatePaymentData implements PortmonePaymentDataInterface
{

	/**
	 * @var int
	 */
	private $payeeId;

	/**
	 * @var float
	 */
	private $amount;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var int
	 */
	private $expiresTime;

	/**
	 * @var int
	 */
	private $orderNumber;


	/**
	 * @var string
	 */
	private $successRedirectUrl;

	/**
	 * @var string
	 */
	private $failureRedirectUrl;

	/**
	 * @var string
	 */
	private $lang;

	private $apiUrl = 'https://www.portmone.com.ua/gateway/';

	public function __construct(int $payeeId, float $amount)
	{
		$this->amount = $amount;
		$this->payeeId = $payeeId;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @return int
	 */
	public function getExpiresTimeSeconds(): int
	{
		return $this->expiresTime;
	}

	/**
	 * @return int
	 */
	public function getOrderNumber(): int
	{
		return $this->orderNumber;
	}

	/**
	 * @return float
	 */
	public function getAmount(): float
	{
		return $this->amount;
	}

	/**
	 * @return string
	 */
	public function getSuccessRedirectUrl(): string
	{
		return $this->successRedirectUrl;
	}

	/**
	 * @return string
	 */
	public function getFailureRedirectUrl(): string
	{
		return $this->failureRedirectUrl;
	}

	/**
	 * @return string
	 */
	public function getLang(): string
	{
		return $this->lang;
	}

	/**
	 * @param string $description
	 * @return CreatePaymentData
	 */
	public function setDescription(string $description): CreatePaymentData
	{
		$this->description = $description;

		return $this;
	}

	/**
	 * @param int $expiresTime
	 * @return CreatePaymentData
	 */
	public function setExpiresTime(int $expiresTime): CreatePaymentData
	{
		$this->expiresTime = $expiresTime;

		return $this;
	}

	/**
	 * @param int $orderNumber
	 * @return CreatePaymentData
	 */
	public function setOrderNumber(int $orderNumber): CreatePaymentData
	{
		$this->orderNumber = $orderNumber;

		return $this;
	}

	/**
	 * @param string $successRedirectUrl
	 * @return CreatePaymentData
	 */
	public function setSuccessRedirectUrl(string $successRedirectUrl): CreatePaymentData
	{
		$this->successRedirectUrl = $successRedirectUrl;

		return $this;
	}

	/**
	 * @param string $failureRedirectUrl
	 * @return CreatePaymentData
	 */
	public function setFailureRedirectUrl(string $failureRedirectUrl): CreatePaymentData
	{
		$this->failureRedirectUrl = $failureRedirectUrl;

		return $this;
	}

	/**
	 * @param string $lang
	 * @return CreatePaymentData
	 */
	public function setLang(string $lang): CreatePaymentData
	{
		$this->lang = $lang;

		return $this;
	}

	/**
	 * @param float $amount
	 * @return CreatePaymentData
	 */
	public function setAmount(float $amount): CreatePaymentData
	{
		$this->amount = $amount;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getPayeeId(): int
	{
		return $this->payeeId;
	}

	/**
	 * @return string
	 */
	public function getApiUrl(): string
	{
		return $this->apiUrl;
	}

	/**
	 * @param string $apiUrl
	 */
	public function setApiUrl(string $apiUrl): void
	{
		$this->apiUrl = $apiUrl;
	}

	public function toArray(): array
	{
		return get_object_vars($this);
	}
}