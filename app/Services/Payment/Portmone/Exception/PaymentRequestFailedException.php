<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone\Exception;

use RuntimeException;

final class PaymentRequestFailedException extends RuntimeException
{

}