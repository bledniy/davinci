<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

use App\Services\Payment\Portmone\Exception\PaymentRequestFailedException;

final class PortmoneGatewayRequest
{
	private $gatewayUrl = 'https://www.portmone.com.ua/gateway/';

	/**
	 * @param $url
	 * @param $data
	 * @return string
	 * @throws PaymentRequestFailedException
	 */
	public function request($data): string
	{
		$ch = curl_init($this->gatewayUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$response = curl_exec($ch);
		$httpCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if (200 !== $httpCode) {
			throw  new PaymentRequestFailedException();
		}

		return $response;
	}
}