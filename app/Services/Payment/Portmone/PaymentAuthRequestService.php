<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

use App\Services\Payment\Portmone\Exception\PaymentRequestFailedException;
use App\Services\Payment\Portmone\Exception\PaymentResponseParsingXmlFailedException;
use Illuminate\Support\Carbon;
use SimpleXMLElement;

final class PaymentAuthRequestService implements PaymentAuthRequestServiceInterface
{

	/**
	 * @var PortmoneConfigData
	 */
	private $config;

	/**
	 * @var PortmoneGatewayRequest
	 */
	private $gatewayRequest;

	public function __construct(PortmoneConfigData $config, PortmoneGatewayRequest $gatewayRequest) {
		$this->config = $config;
		$this->gatewayRequest = $gatewayRequest;
	}
	/**
	 * @param int $orderId
	 * @return PaymentResponseDataInterface
	 * @throws PaymentRequestFailedException|PaymentResponseParsingXmlFailedException
	 */
	public function getPaymentAuth(int $orderId): PaymentResponseDataInterface
	{
		$data = [
			"method" => "result",
			"payee_id" => $this->config->getPayeeId(),
			"login" => $this->config->getLogin(),
			"password" => $this->config->getPassword(),
			"shop_order_number" => $orderId,
		];

		$xml = $this->parseXml($this->gatewayRequest->request($data));

		return PaymentAuthData::create(
			(int)$xml->request->shop_order_number,
			(new PaymentStatusEnum((string)$xml->orders->order->status)),
			(int)$xml->orders->order->bill_amount,
			Carbon::parse((string)$xml->orders->order->pay_date),
			(int)$xml->orders->order->shop_bill_id
		);
	}


	/**
	 * @param string $string
	 * @return SimpleXMLElement
	 * @throws PaymentResponseParsingXmlFailedException
	 */
	private function parseXml(string $string): SimpleXMLElement
	{
		libxml_use_internal_errors(true);
		$xml = simplexml_load_string($string, 'SimpleXMLElement', LIBXML_NOCDATA);
		if (false === $xml) {
			throw new PaymentResponseParsingXmlFailedException();
		}

		return $xml;
	}
}