<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

use App\Services\Payment\Portmone\Exception\PaymentRequestFailedException;
use App\Services\Payment\Portmone\Exception\PaymentResponseParsingXmlFailedException;

interface PaymentAuthRequestServiceInterface
{
	/**
	 * @param int $orderId
	 * @return PaymentResponseDataInterface
	 * @throws PaymentRequestFailedException|PaymentResponseParsingXmlFailedException
	 */
	public function getPaymentAuth(int $orderId): PaymentResponseDataInterface;
}