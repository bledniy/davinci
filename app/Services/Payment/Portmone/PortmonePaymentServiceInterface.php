<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

use Illuminate\Http\Request;

interface PortmonePaymentServiceInterface
{
	public function isResponseValid(Request $request): bool;

	public function getParsedResponse(Request $request): PaymentResponseDataInterface;

}