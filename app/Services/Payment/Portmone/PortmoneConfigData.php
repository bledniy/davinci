<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

final class PortmoneConfigData
{
	/**
	 * @var int
	 */
	private $payeeId;

	/**
	 * @var string
	 */
	private $login;

	/**
	 * @var string
	 */
	private $password;

	public function __construct(int $payeeId, string $login, string $password)
	{
		$this->payeeId = $payeeId;
		$this->login = $login;
		$this->password = $password;
	}

	/**
	 * @return int
	 */
	public function getPayeeId(): int
	{
		return $this->payeeId;
	}

	/**
	 * @return string
	 */
	public function getLogin(): string
	{
		return $this->login;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}
}