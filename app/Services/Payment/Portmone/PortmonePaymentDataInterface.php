<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

interface PortmonePaymentDataInterface
{
	public function getDescription(): string;

	public function getExpiresTimeSeconds(): int;

	public function getOrderNumber(): int;

	public function getAmount(): float;

	public function getSuccessRedirectUrl(): string;

	public function getFailureRedirectUrl(): string;

	public function getLang(): string;
}