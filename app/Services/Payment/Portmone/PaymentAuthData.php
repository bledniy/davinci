<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

use DateTimeInterface;

final class PaymentAuthData implements PaymentResponseDataInterface
{
	/**
	 * @var int
	 */
	private $orderId;

	/**
	 * @var PaymentStatusEnum
	 */
	private $status;

	/**
	 * @var int
	 */
	private $payedAmount = 0;

	/**
	 * @var DateTimeInterface
	 */
	private $payDateTime;

	/**
	 * @var int
	 */
	private $billingId;

	private function __construct() {}

	/**
	 * @return int
	 */
	public function getOrderId(): int
	{
		return $this->orderId;
	}

	/**
	 * @return PaymentStatusEnum
	 */
	public function getStatus(): PaymentStatusEnum
	{
		return $this->status;
	}

	/**
	 * @return int
	 */
	public function getPayedAmount(): int
	{
		return $this->payedAmount;
	}

	/**
	 * @return DateTimeInterface
	 */
	public function getPayDateTime(): DateTimeInterface
	{
		return $this->payDateTime;
	}

	public function getBillingId(): int
	{
		return $this->billingId;
	}

	public static function create(int $orderId, PaymentStatusEnum $status, int $payedAmount, DateTimeInterface $payDate, int $billingId): self
	{
		$self = new self();
		$self->orderId = $orderId;
		$self->status = $status;
		$self->payedAmount = $payedAmount;
		$self->payDateTime = $payDate;
		$self->billingId = $billingId;

		return $self;
	}

}