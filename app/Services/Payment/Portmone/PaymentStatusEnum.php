<?php declare(strict_types=1);

namespace App\Services\Payment\Portmone;

use App\Enum\AbstractStrEnum;

final class PaymentStatusEnum extends AbstractStrEnum
{
	public const ORDER_PAYED = 'PAYED';
	public const ORDER_CREATED = 'CREATED';
	public const ORDER_REJECTED = 'REJECTED';

	public static $enums = [
		self::ORDER_PAYED,
		self::ORDER_CREATED,
		self::ORDER_REJECTED,
	];

	public function isPayed():bool
	{
		return $this->isEq(self::ORDER_PAYED);
	}

	public function isRejected():bool
	{
		return $this->isEq(self::ORDER_REJECTED);
	}
}