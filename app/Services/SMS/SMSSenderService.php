<?php


namespace App\Services\SMS;


class SMSSenderService
{
	/**
	 * @var SendDriverSMSFly
	 */
	private $driver;

	public function __construct(SendDriverSMSFly $driver)
	{
		$this->driver = $driver;
	}

	public function send(string $phone, $message): self
	{
		if (isLocalEnv()) {
			logger(sprintf('SMS CODE EMIT SUBMIT %s %s', $phone, (string)$message));
			return $this;
		}
		$this->driver->send($phone, $message);
		return $this;
	}

	public function getBalance(): float
	{
		return $this->driver->getBalance();
	}

}