<?php


namespace App\Services\SMS;


use App\Helpers\Debug\LoggerHelper;
use Exception;

class SendDriverSMSFly
{
	public const API_URL = 'https://sms-fly.com/api/api.php';
	public const API_URL_NO_ALFANAME = 'https://sms-fly.com/api/api.noai.php';
	/**
	 * @var SendConfig
	 */
	private $config;

	public function __construct(SendConfig $config)
	{
		$this->config = $config;
	}

	public function send($phone, $message)
	{
		try {
			return $this->sendSMS($phone, $message);
		} catch (Exception $e) {
			app(LoggerHelper::class)->error($e);
		}
		return false;
	}

	public function sendSMS($to, $text, $description = '')
	{
		$to = $this->processPhone($to);
		$text = htmlspecialchars($text, ENT_QUOTES);
		$description = htmlspecialchars($description, ENT_QUOTES);
		$source = (!$this->config->getAlphaName()) ? '' : 'source="' . $this->config->getAlphaName() . '"';

		$data = '<message start_time="AUTO" end_time="AUTO" lifetime="4" rate="1" desc="' . $description . '" ' . $source . '>';
		$data .= "<body>" . $text . "</body>";
		$data .= "<recipient>" . $to . "</recipient>";
		$data .= "</message>";

		$response = $this->sendPOST('SENDSMS', $data);
		$code = $response->state->attributes()->code . '';

		switch ($code) {

			case 'ACCEPT':
				return [
					'campaignID' => $response->state->attributes()->campaignID . '',
					'status'     => $response->to[0]->attributes()->status . '',
				];

			default:
				throw new Exception(self::getErrorText($code));
		}
	}

	public static function getErrorText($code)
	{
		switch ($code) {
			case 'XMLERROR':
				return 'Некорректный XML';
			case 'ERRPHONES':
				return 'Неверно задан номер получателя';
			case 'ERRSTARTTIME':
				return 'Не корректное время начала отправки';
			case 'ERRENDTIME':
				return 'Не корректное время окончания рассылки';
			case 'ERRLIVETIME':
				return 'Не корректное время жизни сообщения';
			case 'ERRSPEED':
				return 'Не корректная скорость отправки сообщений';
			case 'ERRALFANAME':
				return 'Данное альфанумерическое имя использовать запрещено, либо ошибка';
			case 'ERRTEXT':
				return 'Некорректный текст сообщения';

			default:
				return 'Неизвестный код ошибки ' . $code;
		}
	}

	protected function sendPOST($operation, $data = '')
	{
		if (!extension_loaded('curl')) {
			throw new Exception('cURL extension missing');
		}

		$data = '<?xml version="1.0" encoding="utf-8"?><request>'
			. '<operation>' . $operation . '</operation>'
			. $data . '</request>';

		$ch = curl_init($this->getApiUrl());
		$options = [
			CURLOPT_USERPWD        => $this->config->getLogin() . ':' . $this->config->getPassword(),
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_POST           => 1,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_TIMEOUT        => 15,
			CURLOPT_POSTFIELDS     => $data,
			CURLOPT_HTTPHEADER     => [
				"Content-Type: text/xml",
				"Accept: text/xml",
			],
		];

		curl_setopt_array($ch, $options);
		$response = curl_exec($ch);

		if ($response === false) {
			$error = curl_error($ch);
			curl_close($ch);
			throw new Exception($error);
		}
		curl_close($ch);

		$response = @simplexml_load_string($response);
		if (($response === false) || ($response === null)) {
			throw new Exception('String could not be parsed as XML');
		}

		return $response;
	}

	public function getBalance(): float
	{
		$response = $this->sendPOST('GETBALANCE');
		return (float)($response->balance . '');
	}

	protected function getApiUrl(): string
	{
		if (!$this->config->getAlphaName()) {
			return self::API_URL_NO_ALFANAME;
		}
		return self::API_URL;
	}

	public function processPhone($international_phone)
	{
		return str_replace('+', '', $international_phone);
	}


}