<?php declare(strict_types=1);

namespace App\Services\SMS;

class SendConfig
{
	public function __construct()
	{
		$this->setLogin((string)config('sms.login'));
		$this->setPassword((string)config('sms.password'));
		$this->setAlphaName(config('sms.alphaName'));
	}

	private $login = '';

	private $password = '';

	private $alphaName;


	public function getLogin(): string
	{
		return $this->login;
	}

	public function setLogin(string $login): void
	{
		$this->login = $login;
	}

	public function getPassword(): string
	{
		return $this->password;
	}

	public function setPassword(?string $password): void
	{
		$this->password = $password;
	}

	/**
	 * @return mixed
	 */
	public function getAlphaName()
	{
		return $this->alphaName;
	}

	/**
	 * @param mixed $alphaName
	 */
	public function setAlphaName($alphaName): void
	{
		$this->alphaName = $alphaName;
	}

}