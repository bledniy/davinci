<?php

namespace App\Providers;

use App\Platform\Order\Voters\Bid\CanAddBidPremiumAccountVoter;
use App\Platform\Order\Voters\Bid\IsUserCanAddBidToDeal;
use Illuminate\Support\ServiceProvider;

class PremiumAccountServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		IsUserCanAddBidToDeal::registerVoter(CanAddBidPremiumAccountVoter::make());
    }
}
