<?php declare(strict_types=1);

namespace App\Providers;

use App\Platform\Auth\Contracts\AuthTokenGenerator;
use App\Platform\Auth\Services\Register\RegistrationTokenGenerator;
use App\Platform\Order\Contracts\OrderCreateTokenGenerator;
use App\Platform\Order\Voters\Bid\CanAddBidHasConfirmedQualificationVoter;
use App\Platform\Order\Voters\Bid\IsUserCanAddBidToDeal;
use App\Services\Seo\ApiMetaRequestUrlRewriter;
use App\Services\Seo\SeoUrlRewriterInterface;
use Illuminate\Support\ServiceProvider;

class PlatformBindingsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthTokenGenerator::class, RegistrationTokenGenerator::class);
        $this->app->bind(OrderCreateTokenGenerator::class, RegistrationTokenGenerator::class);
        $this->app->bind(SeoUrlRewriterInterface::class, ApiMetaRequestUrlRewriter::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		IsUserCanAddBidToDeal::registerVoter(CanAddBidHasConfirmedQualificationVoter::make());
    }
}
