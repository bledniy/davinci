<?php declare(strict_types=1);

namespace App\Providers;

use App\Services\Payment\Portmone\PaymentAuthRequestService;
use App\Services\Payment\Portmone\PaymentAuthRequestServiceInterface;
use App\Services\Payment\Portmone\PortmoneConfigData;
use App\Services\Payment\Portmone\PortmonePaymentService;
use App\Services\Payment\Portmone\PortmonePaymentServiceInterface;
use App\Services\User\Payment\PaymentUserPremiumService;
use App\Services\User\Payment\PaymentUserPremiumServiceInterface;
use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->app->bind(PortmonePaymentServiceInterface::class, PortmonePaymentService::class);
		$this->app->bind(PaymentAuthRequestServiceInterface::class, PaymentAuthRequestService::class);
		$this->app->bind(PortmoneConfigData::class, function () {
			return new PortmoneConfigData(
				(int)config('services.portmone.payeeId'),
				(string)config('services.portmone.login'),
				(string)config('services.portmone.password')
			);
		});
		$this->app->bind(PaymentUserPremiumServiceInterface::class, PaymentUserPremiumService::class);
	}
}
