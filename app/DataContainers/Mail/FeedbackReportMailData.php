<?php declare(strict_types=1);

namespace App\DataContainers\Mail;

use App\Models\Feedback\Feedback;

final class FeedbackReportMailData
{
	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $report;

	public static function createFromEntity(Feedback $feedback): self
	{
		$self = new self();
		$self->name = (string)$feedback->getAttribute('name');
		$self->message = (string)$feedback->getAttribute('message');
		$self->email = (string)$feedback->getAttribute('email');
		$self->report = (string)$feedback->getAttribute('report');

		return $self;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getReport(): string
	{
		return $this->report;
	}

}