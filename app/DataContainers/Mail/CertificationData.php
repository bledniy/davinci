<?php declare(strict_types=1);

namespace App\DataContainers\Mail;

use App\Models\User\UserCertification;

final class CertificationData
{
	/**
	 * @var string
	 */
	private $categoryName;

	/**
	 * @var int
	 */
	private $certificationId;

	/**
	 * @var ?string
	 */
	private $imagePath;

	/**
	 * @var ?string
	 */
	private $userFullName;

	/**
	 * @var int
	 */
	private $userId;

	public static function createFromEntity(UserCertification $entity): self
	{
		$self = new self();
		$self->categoryName = $entity->getCategory()->getNameDisplay();
		$self->certificationId = $entity->getKey();
		$self->imagePath = $entity->getImagePath();
		$self->userFullName = $entity->getUser()->getFio();
		$self->userId = $entity->getUser()->getKey();

		return $self;
	}

	/**
	 * @return string
	 */
	public function getCategoryName(): string
	{
		return $this->categoryName;
	}

	/**
	 * @return int
	 */
	public function getCertificationId(): int
	{
		return $this->certificationId;
	}

	/**
	 * @return string|null
	 */
	public function getImagePath(): ?string
	{
		return $this->imagePath;
	}

	public function hasImage(): bool
	{
		return null !== $this->imagePath;
	}

	/**
	 * @return string|null
	 */
	public function getUserFullName(): ?string
	{
		return $this->userFullName;
	}

	/**
	 * @return int
	 */
	public function getUserId(): int
	{
		return $this->userId;
	}
}