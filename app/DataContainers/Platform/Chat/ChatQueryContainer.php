<?php


namespace App\DataContainers\Platform\Chat;


use App\Models\User;
use Illuminate\Http\Request;

class ChatQueryContainer
{
	private $type;

	private $favorite;

	private $unread;

	private $search = '';
	/**
	 * @var User
	 */
	private $user;

	public function fillFromRequest(Request $request): self
	{
		if ($type = $request->get('type')) {
			$this->setType($type);
		}
		if ($request->get('favorite')) {
			$this->setFavorite(true);
		}
		if ($request->get('unread')) {
			$this->setUnread(true);
		}
		if ($search = $request->get('search')) {
			$this->setSearch($search);
		}
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	public function setType($type): self
	{
		$this->type = $type;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFavorite()
	{
		return $this->favorite;
	}

	public function setFavorite($favorite): self
	{
		$this->favorite = $favorite;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUnread()
	{
		return $this->unread;
	}


	public function setUnread(bool $unread): self
	{
		$this->unread = $unread;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSearch(): string
	{
		return $this->search;
	}

	public function setSearch(string $search): self
	{
		$this->search = $search;
		return $this;
	}

	/**
	 * @return User
	 */
	public function getUser(): User
	{
		return $this->user;
	}

	public function setUser(User $user): self
	{
		$this->user = $user;
		return $this;
	}

}