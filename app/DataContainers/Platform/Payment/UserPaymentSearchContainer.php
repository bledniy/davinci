<?php declare(strict_types=1);

namespace App\DataContainers\Platform\Payment;

use App\Enum\UserPaymentReasonEnum;
use App\Models\User;

class UserPaymentSearchContainer implements UserPaymentSearchContainerInterface
{
	/**
	 * @var User|null
	 */
	private $user;

	/**
	 * @var bool|null
	 */
	private $isPayed;

	/**
	 * @var array<UserPaymentReasonEnum>
	 */
	private $reasons = [];

	private $notNullReason = true;

	public static function create(): self
	{
		return new self;
	}

	public function hasUser(): bool
	{
		return null !== $this->user;
	}

	/**
	 * @return ?User
	 */
	public function getUser(): ?User
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 * @return UserPaymentSearchContainer
	 */
	public function setUser(User $user): UserPaymentSearchContainer
	{
		$this->user = $user;

		return $this;
	}

	public function hasIsPayed(): bool
	{
		return null !== $this->isPayed;
	}

	/**
	 * @return bool|null
	 */
	public function getIsPayed(): ?bool
	{
		return $this->isPayed;
	}

	/**
	 * @param bool|null $isPayed
	 * @return UserPaymentSearchContainer
	 */
	public function setIsPayed(?bool $isPayed): UserPaymentSearchContainer
	{
		$this->isPayed = $isPayed;

		return $this;
	}

	/**
	 * @param array<UserPaymentReasonEnum> $reasons
	 * @return $this
	 */
	public function setReasons(array $reasons): self
	{
		$this->reasons = $reasons;

		return $this;
	}

	/**
	 * @return array<UserPaymentReasonEnum>
	 */
	public function getReasons(): array
	{
		return $this->reasons;
	}

	public function hasReasons(): bool
	{
		return count($this->reasons) > 0;
	}

	public function isWithNotNullReason(): bool
	{
		return $this->notNullReason;
	}

	/**
	 * @param bool $notNullReason
	 * @return UserPaymentSearchContainer
	 */
	public function setNotNullReason(bool $notNullReason = true): UserPaymentSearchContainer
	{
		$this->notNullReason = $notNullReason;

		return $this;
	}
}