<?php declare(strict_types=1);

namespace App\DataContainers\Platform\Payment;

use App\Enum\UserPaymentReasonEnum;
use App\Models\User;

interface UserPaymentSearchContainerInterface
{
	public function getUser(): ?User;

	public function hasUser(): bool;

	public function hasIsPayed(): bool;

	public function getIsPayed(): ?bool;

	/**
	 * @return array<UserPaymentReasonEnum>
	 */
	public function getReasons(): array;

	public function hasReasons(): bool;

	public function isWithNotNullReason(): bool;

}