<?php declare(strict_types=1);

namespace App\DataContainers\Platform\Payment;

use App\Contracts\Models\ModelHasMorphPayment;

class PaymentSearchContainer
{
	/**
	 * @var ModelHasMorphPayment
	 */
	private $morph;

	/**
	 * @return ModelHasMorphPayment
	 */
	public function getMorph(): ModelHasMorphPayment
	{
		return $this->morph;
	}

	/**
	 * @param ModelHasMorphPayment $morph
	 */
	public function setMorph(ModelHasMorphPayment $morph): void
	{
		$this->morph = $morph;
	}

}