<?php


	namespace App\DataContainers\Platform\Order;


	use App\DataContainers\Platform\Order\Traits\UserTypeKeys;
	use App\Platform\Query\PaginationOnPage;
	use Illuminate\Http\Request;

	class SearchEditUserOrdersContainer
	{
		use UserTypeKeys;

		public function reset()
		{
			return app(__CLASS__);
		}
	}