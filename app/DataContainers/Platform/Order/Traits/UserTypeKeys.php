<?php


namespace App\DataContainers\Platform\Order\Traits;


trait UserTypeKeys
{
	protected $customerId;

	protected $performerId;

	/**
	 * @return mixed
	 */
	public function getCustomerId()
	{
		return $this->customerId;
	}

	/**
	 * @param mixed $customerId
	 */
	public function setCustomerId($customerId): self
	{
		$this->customerId = $customerId;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPerformerId()
	{
		return $this->performerId;
	}

	/**
	 * @param mixed $performerId
	 */
	public function setPerformerId($performerId): self
	{
		$this->performerId = $performerId;
		return $this;
	}

}