<?php


namespace App\DataContainers\Platform\Order;


use App\Models\User;
use App\Platform\Query\FilterCategories;
use App\Platform\Query\PaginationOnPage;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class SearchOrdersDataContainer
{
	/**
	 * @var PaginationOnPage
	 */
	private $paginationOnPage;

	private $canOnline = false;

	private $onlyActive = false;

	private $cityId;

	private $onPage;

	private $categoryIds = [];

	private $page;

	private $exceptCustomers = [];

	public function __construct(PaginationOnPage $paginationOnPage)
	{
		$this->paginationOnPage = $paginationOnPage;
	}

	/**
	 * @return mixed
	 */
	public function getCityId()
	{
		return $this->cityId;
	}

	public function setCityId($cityId): self
	{
		$this->cityId = $cityId;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getOnPage(): int
	{
		return $this->onPage;
	}

	public function setOnPage(int $onPage): self
	{
		$this->onPage = $onPage;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCategoryIds(): array
	{
		return $this->categoryIds;
	}

	public function setCategoryIds($categoryIds): self
	{
		$this->categoryIds = array_map(function($int){
		    return (int)$int;
		}, Arr::sort((array)$categoryIds));
		return $this;
	}

	/**
	 * @return int
	 */
	public function getPage(): int
	{
		return $this->page;
	}

	public function setPage(int $page): self
	{
		$this->page = $page;
		return $this;
	}

	public function fillFromRequest(Request $request): self
	{
		$onPage = $this->paginationOnPage->itemSupports(($onPage = abs((int)$request->get('limit'))))
			? $onPage
			: $this->paginationOnPage->default();
		$this
			->setCategoryIds(FilterCategories::filter((array)$request->get('category')))
			->setCityId((int)$request->get('city'))
			->setPage(abs((int)$request->get('page') ?: 1))
			->setOnPage($onPage)
			->setCanOnline($request->has('online'))
			->setOnlyActive($request->has('only_active'))
		;
		return $this;
	}

	public function toKey(): array
	{
		return [
			'categoryId' => $this->getCategoryIds(),
			'cityId'     => $this->getCityId(),
			'online'     => $this->getCanOnline(),
			'active'     => $this->isOnlyActive(),
		];
	}

	public function getCanOnline(): bool
	{
		return $this->canOnline;
	}

	public function setCanOnline($canOnline): self
	{
		$this->canOnline = $canOnline;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isOnlyActive(): bool
	{
		return $this->onlyActive;
	}

	/**
	 * @param bool $onlyActive
	 */
	public function setOnlyActive(bool $onlyActive): void
	{
		$this->onlyActive = $onlyActive;
	}

	/**
	 * @return array | User[]
	 */
	public function getExceptCustomers(): array
	{
		return $this->exceptCustomers;
	}

	/**
	 * @param array $exceptCustomers
	 */
	public function setExceptCustomers(array $exceptCustomers): void
	{
		$this->exceptCustomers = $exceptCustomers;
	}

}