<?php


	namespace App\DataContainers\Platform\Order;


	use App\DataContainers\Platform\Order\Traits\UserTypeKeys;
	use App\Platform\Query\PaginationOnPage;
	use Illuminate\Http\Request;

	class SearchCabinetOrdersContainer
	{
		use UserTypeKeys;
		/**
		 * @var PaginationOnPage
		 */
		protected $paginationOnPage;

		protected $search = '';

		protected $onPage;

		protected $page;

		protected $paginate = false;

		protected $whereClosed;

		public function __construct(PaginationOnPage $paginationOnPage)
		{
			$this->paginationOnPage = $paginationOnPage;
		}

		/**
		 * @return int
		 */
		public function getOnPage(): int
		{
			return $this->onPage;
		}

		public function setOnPage(int $onPage): self
		{
			$this->onPage = $onPage;
			return $this;
		}

		/**
		 * @return int
		 */
		public function getPage(): int
		{
			return $this->page;
		}

		public function setPage(int $page): self
		{
			$this->page = $page;
			return $this;
		}

		public function setSearch(string $search): self
		{
			$this->search = $search;
			return $this;
		}

		/**
		 * @return string
		 */
		public function getSearch(): string
		{
			return $this->search;
		}

		public function fillFromRequest(Request $request): self
		{
			$onPage = $this->paginationOnPage->itemSupports(($onPage = (int)$request->get('limit')))
				? $onPage
				: $this->paginationOnPage->default();
			$this
				->setPage(abs((int)$request->get('page') ?: 1))
				->setOnPage($onPage)
				->setSearch((string)$request->get('search'))
			;
			return $this;
		}

		public function toKey(): array
		{
			return [
				'search' => $this->getSearch(),
			];
		}


		/**
		 * @return bool
		 */
		public function isPaginate(): bool
		{
			return $this->paginate;
		}

		/**
		 * @param bool $paginate
		 */
		public function setPaginate(bool $paginate): self
		{
			$this->paginate = $paginate;
			return $this;
		}

		/**
		 * @return bool
		 */
		public function isWhereClosed(): ?bool
		{
			return $this->whereClosed;
		}

		/**
		 * @param bool $whereClosed
		 */
		public function setWhereClosed(bool $whereClosed): self
		{
			$this->whereClosed = $whereClosed;
			return $this;
		}

	}