<?php declare(strict_types=1);

namespace App\DataContainers\Platform;

use App\Models\Setting;
use App\Models\Translate\Translate;

final class ApiSettingDataContainer
{
	/**
	 * @var string|null
	 */
	private $key;

	/**
	 * @var string|null
	 */
	private $name;

	/**
	 * @var string|null
	 */
	private $value;

	public static function createFromSetting(Setting $setting): self
	{
		$self = new self();
		$self->key = $setting->key;
		$self->value = $setting->getValue();
		$self->name = $setting->display_name;

		return $self;
	}

	public static function createFromTranslate(Translate $translate): self
	{
		$self = new self();
		$self->key = $translate->key;
		$self->value = $translate->getValue();
		$self->name = $translate->comment;

		return $self;
	}

	/**
	 * @return string|null
	 */
	public function getKey(): ?string
	{
		return $this->key;
	}

	/**
	 * @return string|null
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @return string|null
	 */
	public function getValue(): ?string
	{
		return $this->value;
	}

}