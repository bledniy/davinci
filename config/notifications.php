<?php
return [
	'deals' => [
		'new-bids' => [
			'interval' => 10 //minutes
		],
		'new-deals' => [
			'interval' => 60 //minutes
		],
	],
];