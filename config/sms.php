<?php
return [
	'login' => env('SMS_LOGIN'),
	'password' => env('SMS_PASSWORD'),
	'alphaName' => env('SMS_ALPHA_NAME', 'InfoCenter'),
];
