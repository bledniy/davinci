<?php /** @var $item \App\Models\Premium\Premium */ ?>
<?php /** @var $permissionKey string */ ?>

<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="th-description">@lang('form.price')</th>
            <th class="text-right">
                @can('create_' . $permissionKey)
{{--                    <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>--}}
                @endcan
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>
                    <input type="text" data-url-update="{{ route($routeKey . '.update', $item->getKey()) }}"
                           value="{{ $item->getPrice() }}" name="price"
                           placeholder="Название" class="form-control">
                </td>
                <td class="text-primary text-right">
{{--                    @include('admin.partials.action.index_actions')--}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
