<?php /** @var $statuses \App\Models\News\NewsCategory[] */ ?>
@isset($statuses)
    @if (!($withoutLabel ?? false))
        <label for="status">
            Статус
        </label>
    @endif
    <select name="status" id="status" class="form-control selectpicker"
            @isset($edit)
            data-url-update="{{ route($routeKey . '.update', $edit->getKey()) }}"
            @endisset
            autocomplete="off">
        @foreach($statuses as $status => $name)
            <option value="{{ $status }}" {!! selectedIfTrue(($edit->status ?? null) === $status) !!}
            >{{ $name }}</option>
        @endforeach
    </select>
@endisset
