<?php /** @var $item \App\Models\User\UserCertification */ ?>
<form action="">
    <div class="form-group">
        <div class="row">
            <div class="col-3">
                <select name="status" id="status" class="form-control selectpicker"
                        autocomplete="off">
                    <option value="">Выберите статус</option>
                    @foreach($statuses as $status => $name)
                        <option value="{{ $status }}" {!! selectedIfTrue($request->get('status') === $status) !!}
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2">
                <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i>
                    @lang('form.search')
                </button>
            </div>
        </div>
    </div>
</form>
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="">ID</th>
            <th class="">@lang('form.image.image')</th>
            <th class="th-description">Дата загрузки</th>
            <th class="th-description">Пользователь</th>
            <th class="th-description">Категория</th>
            <th class="th-description">Статус</th>
            <th class="text-right"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{ $item->getKey() }}</td>
                <td>
                    <div class="img-container">
                        <a href="{{ imgPathOriginal(getPathToImage($item->image)) }}" class="fancy" data-fancybox="news-image">
                            <img src="{{ getPathToImage($item->image) }}" alt=""/>
                        </a>
                    </div>
                </td>
                <td>
                    {{ $item->created_at }}
                </td>
                <td>
                    @if ($item->user)
                        <a href="{{ urlEntityEdit($item->user) }}">{{ $item->getUser()->getFio() }}</a>
                    @endif
                </td>
                <td>
                    <span class="badge {{ $item->isUnread() ? 'bg-danger ' : '' }}">
                        @if ($item->getCategory())
                            {{ $item->getCategory()->getNameDisplay() }}
                        @endif
                    </span>
                </td>
                <td>
                    @includeIf('admin.certifications.partials.status-select', ['edit' => $item, 'withoutLabel' => true])
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}