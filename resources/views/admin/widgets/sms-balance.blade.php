<div class="d-inline-block">
    <div class="card">
        <div class="card-header">Баланс смс сервиса</div>
        <hr class="m-0">
        <div class="card-body">
            @if ($balance ?? false)
                {{ $balance }} грн.
                <div><small>данные обновляются раз в минуту</small></div>
                @if ($balance < $warnSum)
                    <div class="alert alert-danger">На счету осталось меньше {{ $warnSum }} грн.</div>
                @endif
            @else
                <p>Информация временно отсутствует</p>
            @endif
        </div>
    </div>
</div>