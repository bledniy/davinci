<?php /** @var $edit \App\Models\User\UserCertification*/ ?>
<table class="table">
    <tr>
        <td> Пользователь:</td>
        <td><a href="{{ urlEntityEdit($edit->user) }}" class="badge badge-secondary">{{ $edit->getUser()->getFio() }}</a></td>
    </tr>
    <tr>
        <td> Категория:</td>
        <td>{{ $edit->getCategory()->getNameDisplay() }}</td>
    </tr>
</table>

<div class="row my-5">
    <div class="col-md-4">
        @includeIf('admin.certifications.partials.status-select')
    </div>
</div>
<div class="row">
    <div class="col-4">
        <a href="{{ getPathToImage($edit->image) }}">
            <img src="{{ getPathToImage($edit->image) }}" class="img-fluid" alt="">
        </a>
    </div>
</div>


