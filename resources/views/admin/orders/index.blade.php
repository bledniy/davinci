<?php /** @var $item \App\Models\Order\Order */ ?>
<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $city \App\Models\City\City */ ?>
<?php /** @var $container \App\DataContainers\Platform\Order\SearchOrdersDataContainer */ ?>
@php
    $canDelete = false;
@endphp
<form action="">
    <div class="form-group">
        <div class="row">
            <div class="col-2">
                <select name="category" id="category" class="form-control selectpicker"
                        autocomplete="off">
                    <option value="">Выберите категорию</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->getKey() }}"
                                {!! selectedIfTrue( in_array($category->getKey(),$container->getCategoryIds(), true)) !!}
                        >{{ $category->getNameDisplay() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2">
                <select name="city" id="city" class="form-control selectpicker"
                        autocomplete="off">
                    <option value="">Выберите город</option>
                    @foreach($cities as $city)
                        <option value="{{ $city->getKey() }}"
                                {!! selectedIfTrue( $city->getKey() === $container->getCityId()) !!}
                        >{{ $city->getName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2">
                <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i>
                    @lang('form.search')
                </button>
            </div>
        </div>
    </div>
</form>

{{$list->render()}}
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="">ID</th>
            <th class="th-description">Сделка</th>
            <th class="th-description">Заказчик</th>
            <th class="th-description">Детали</th>
            <th class="text-right"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{ $item->getKey() }}</td>
                <td>
                    <div class="">
                        <h4><a href="{{ urlEntityEdit($item) }}" class="text-decorated-hover">{{ $item->getContainer()->getTitle() }}</a>
                        </h4>
                        <a href="{{ urlEntityEdit($item->getContainer()->getCity()) }}"
                           class="text-decorated-hover">{{ $item->getContainer()->getCity()->getName() }}</a>
                        <br>
                        <span>{{ $item->getContainer()->getCategory()->getNameDisplay() }}</span>
                    </div>
                </td>
                <td>
                    @if ($customer = $item->getContainer()->getCustomer())
                        <div class="customer-avatar mb-3" style="background-image: url({{ $customer->getAvatarHelper()->getCustomerAvatar() }});">
                        </div>
                        <a href="{{ urlEntityEdit($customer) }}" class="fio">{{ $customer->getFio() }}</a>
                    @endif
                </td>
                <td>
                    <table class="table table-sm table-order-details m-0">
                        <tr>
                            <td>Статус:</td>
                            <td>{{ $item->getContainer()->getStatusName() }}</td>
                        </tr>
                        <tr>
                            <td>Кол-во ставок:</td>
                            <td>{{ $item->getContainer()->getBidsCount() }}</td>
                        </tr>
                        <tr>
                            <td>Кол-во просмотров:</td>
                            <td>{{ $item->getContainer()->getViewsCount() }}</td>
                        </tr>
                        <tr>
                            <td><b>Исполнитель</b></td>
                            <td>
                                @if ($item->getContainer()->hasPerformer())
                                    <a href="{{ urlEntityEdit($item->getContainer()->getPerformer()) }}" class="text-secondary"
                                    >✅ {{ $item->getContainer()->getPerformer()->getFio() }}</a>
                                @else
                                    Исполнитель не выбран
                                @endif
                            </td>
                        </tr>
                    </table>
                </td>
                <td>{{ getDateFormatted($item->getContainer()->getDatePublished()) }}</td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}