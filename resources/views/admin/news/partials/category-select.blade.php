<?php /** @var $categories \App\Models\News\NewsCategory[] */ ?>
<label for="newsCategory">
    Категория
</label>
<select name="news_category_id" id="newsCategory" class="form-control selectpicker">
    @foreach($categories as $category)
        <option value="{{ $category->getKey() }}" onautocomplete="off"
                {!! selectedIfTrue(($edit->news_category_id ?? null) === $category->getKey()) !!}
        >{{ $category->name }}</option>
    @endforeach
</select>
