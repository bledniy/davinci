<news-categories
        :url="'{{ langUrl('/admin/news/categories') }}'"
        :list="{{ json_encode($list->toArray()) }}"
></news-categories>