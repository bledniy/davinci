<?php /** @var $fields array */ ?>
<div class="row">
    <div class="col-12">
        @if ($fields['name'] ?? false)
            @includeIf('admin.partials.crud.elements.name')
        @endif
    </div>
    <div class="col-12">
        @if ($fields['title'] ?? false)
            @includeIf('admin.partials.crud.elements.title')
        @endif
    </div>
    <div class="col-6 offset-1">
        @if ($fields['image'] ?? false)
            @includeIf('admin.partials.crud.elements.image-upload-group')
        @endif
    </div>
    <div class="col-6">
        @if ($fields['active'] ?? false)
            @include('admin.partials.crud.elements.active')
        @endif
    </div>
    <div class="col-12">
        @if ($fields['url'] ?? false)
            @include('admin.partials.crud.elements.url')
        @endif
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if ($fields['description'] ?? false)
            @include('admin.partials.crud.textarea.description')
        @endif
    </div>
    <div class="col-12">
        @if ($fields['excerpt'] ?? false)
            @include('admin.partials.crud.textarea.excerpt')
        @endif
    </div>
</div>

