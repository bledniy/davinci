@isset($groups)
    <?php /** @var $group \App\Models\MenuGroup */ ?>
	<label for="menu_group_id">{{ __('modules.menu.group') }}</label>
	<select name="menu_group_id" id="menu_group_id" class="form-control selectpicker">
		@foreach($groups as $group)
			@php
				$selected = (isset($edit)) ? $group->id == $edit->menu_group_id : false;
			@endphp
			<option value="{{ $group->id }}"
				{!! selectedIfTrue($selected) !!}>{{ $group->name }}
			</option>
		@endforeach
	</select>
@endisset
