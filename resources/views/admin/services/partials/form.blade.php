@include('admin.partials.crud.elements.name')

<div class="row">
    <div class="col-6">
        @includeIf('admin.partials.crud.elements.int-price')
    </div>
    <div class="col-6">
        @include('admin.partials.crud.elements.active')
    </div>
</div>

{{--<div class="w-75">@include('admin.partials.crud.elements.image-upload-group')</div>--}}

@include('admin.partials.crud.textarea.description')
