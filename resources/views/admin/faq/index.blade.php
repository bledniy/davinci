<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="">{{ __('form.sorting') }}</th>
            <th class="th-description">@lang('form.title')</th>
            <th>{{ __('form.active') }}</th>
            <th>Тип</th>
            <th class="text-right">
                <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
            </th>
        </tr>
        </thead>
        @if ($list->isNotEmpty())
            <tbody data-sortable-container="true" data-table="{{ $list->first()->getTable() }}">
            @foreach($list as $item)
				<?php /** @var $item \App\Models\Faq\Faq */ ?>
                <tr class="draggable" data-sort="" data-id="{{ $item->getKey() }}">
                    <td>
                        @include('admin.partials.sort_handle')
                    </td>
                    <td>
                        <a href="{{ route($routeKey.'.edit',  $item->id) }}">
                            {{ $item->getQuestion() }}
                        </a>
                    </td>
                    <td>
                        {{ translateYesNo($item->getAttribute('active')) }}
                    </td>
                    <td>
                        {{ $types[$item->type] ?? '' }}
                    </td>
                    <td class="text-primary text-right">
                        @include('admin.partials.action.index_actions')
                    </td>
                </tr>
            @endforeach
            </tbody>
        @endif
    </table>
</div>
