<label for="type">Тип</label>
<select name="type" id="type" class="form-control selectpicker" autocomplete="off">
    @foreach($types ?? [] as $type => $name)
        <option value="{{ $type }}"
                {!! selectedIfTrue(old('type', $edit->type ?? null) === $type) !!}
        >{{ $name }}</option>
    @endforeach
</select>