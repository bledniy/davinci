<div class="row">
    <div class="col-2">
        @includeIf('admin.faq.partials.type-select')
    </div>
    <div class="col-6 offset-1">
        @include('admin.partials.crud.elements.active')
    </div>
    <div class="col-6">
        @include('admin.partials.crud.default', ['name'=>'question','title'=>'Вопрос'])
    </div>
</div>
<div class="row">
    <div class="col-12">
        @include('admin.partials.crud.textarea',['name'=>'answer','title'=>'Ответ'])
    </div>
</div>

{{--@includeIf('admin.partials.crud.js.init-editor', ['name' => 'answer'])--}}
