<?php /** @var $data \App\DataContainers\Mail\CertificationData */ ?>
@extends('mail.layout.layout')

@section('content')
    <table class="table">
        <tr>
            <td>Имя</td>
            <td>
                <a href="{{ route('admin.users.edit', $data->getUserId()) }}">{{ $data->getUserFullName() }}</a>
            </td>
        </tr>
        <tr>
            <td>Категория</td>
            <td>{{ $data->getCategoryName() }}</td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: left">
                <a style="padding: 10px; background: #284921; color:white; text-decoration:none"
                   href="{{ route('admin.certifications.edit', $data->getCertificationId()) }}">Смотреть</a>
            </td>
        </tr>
        @if ($data->hasImage())
            <tr>
                <td>Сертификат</td>
                <td colspan="2" style="text-align: center">
                    <a href="{{ route('admin.certifications.edit', $data->getCertificationId()) }}">
                        <img src="{{ getPathToImage($data->getImagePath()) }}" alt="Сертификат">
                    </a>
                </td>
            </tr>
        @endif
    </table>
@stop