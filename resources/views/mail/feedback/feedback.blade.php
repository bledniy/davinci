<?php /** @var $feedback \App\DataContainers\Mail\FeedbackReportMailData */ ?>
@extends('mail.layout.layout')

@section('content')
    <table class="table">
        <tr>
            <td>Имя</td>
            <td>{{ $feedback->getName() }}</td>
        </tr>
        <tr>
            <td>Короткое описание</td>
            <td>{{ $feedback->getMessage() }}</td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>{{ $feedback->getEmail() }}</td>
        </tr>
        <tr>
            <td>Описание ошибки</td>
            <td>{{ $feedback->getReport() }}</td>
        </tr>
    </table>
@stop