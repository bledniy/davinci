import Bids from "./components/Deal/Bids";
import Deal from "./components/Deal/Deal";
import Chat from "./components/Deal/Chat/Chat";

require('./bootstrap');
import Vue from 'vue';
// import App from './App.vue';

import Notifications from 'vue-notification'
import 'es6-promise/auto'
import Vuex from "vuex";
import VueChatScroll from 'vue-chat-scroll'

Vue.use(VueChatScroll)
Vue.use(Notifications)
Vue.use(Vuex)

window.Vue = Vue;
Vue.config.productionTip = false

import newsCategories from './components/News/NewsCategories'
import notificationConfig from './components/NotificationConfig/NotificationConfig'

new Vue({
    el: "#app",
    // render: h => h(App),
    components: {
        Bids,
        Deal,
        Chat,
        newsCategories,
        notificationConfig
    }
})
// .$mount('#app')