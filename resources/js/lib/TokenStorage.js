class TokenStorage {
    constructor() {
        this.setToken(this._get());
    }

    getToken() {
        return this.token
    }

    setToken(token) {
        this._store(this.token = token);
        return this
    }

    _store(token) {
        window.localStorage.setItem('token', token)
    }

    _get() {
        return window.localStorage.getItem('token')
    }
}

export default TokenStorage