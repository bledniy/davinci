export default class Hosts {
    static isLocalFrontCheck;
    static frontLocalhost = '//10.0.1.10:3000';
    static frontRemote = '//dealok.com.ua';

    static frontHost() {
        return this.frontRemote;
    }

    static isLocalhost() {
        return location.host === 'dealok'
    }

    static  isLocalFrontAvailable() {
        if (typeof this.isLocalFrontCheck !== 'undefined') {
            return this.isLocalFrontCheck;
        }
        try {
            const request = new XMLHttpRequest();
            request.timeout = 500;
            request.open('GET', this.frontLocalhost, false);
            request.send(null);
            this.isLocalFrontCheck = (request.status === 200)
        } catch (e){
            this.isLocalFrontCheck = false
        }
        return this.isLocalFrontCheck;
    }
}