<?php

use App\Http\Controllers\Api\Auth\ForgotPasswordController;
use App\Http\Controllers\Api\Auth\Login\LoginController;
use App\Http\Controllers\Api\Auth\Login\SocialLoginController;
use App\Http\Controllers\Api\Auth\LogoutController;
use App\Http\Controllers\Api\Auth\Register\ApiRegisterCustomerDataController;
use App\Http\Controllers\Api\Auth\Register\ApiRegisterPerformerConfirmEmailController;
use App\Http\Controllers\Api\Auth\Register\ApiRegisterPerformerConfirmPhoneController;
use App\Http\Controllers\Api\Auth\Register\ApiRegisterPerformerDataController;
use App\Http\Controllers\Api\Auth\Register\ApiRegisterTypeController;
use App\Http\Controllers\Api\Auth\Register\InitRegisterTokenController;
use App\Http\Controllers\Api\Auth\Register\SocialRegisterController;
use App\Http\Controllers\Api\Auth\ResetPasswordController;
use App\Http\Controllers\Api\Cabinet\ChangePasswordController;
use App\Http\Controllers\Api\Cabinet\Chat\ChatSendMessageController;
use App\Http\Controllers\Api\Cabinet\Chat\FavoritesChatController;
use App\Http\Controllers\Api\Cabinet\Chat\IndexChatController;
use App\Http\Controllers\Api\Cabinet\Chat\MessagesWatchedController;
use App\Http\Controllers\Api\Cabinet\Chat\ShowChatController;
use App\Http\Controllers\Api\Cabinet\MyDealsController;
use App\Http\Controllers\Api\Cabinet\NotificationController;
use App\Http\Controllers\Api\Cabinet\PerformerCertificationController;
use App\Http\Controllers\Api\Cabinet\PerformerDiplomaController;
use App\Http\Controllers\Api\Cabinet\PerformerServicesController;
use App\Http\Controllers\Api\Cabinet\PersonalDataController;
use App\Http\Controllers\Api\Cabinet\PremiumAccount\PremiumAccountController;
use App\Http\Controllers\Api\Cabinet\PremiumAccount\PremiumAccountPaymentController;
use App\Http\Controllers\Api\Cabinet\SwitchUserTypeController;
use App\Http\Controllers\Api\Cabinet\Verification\Contacts\ContactsEmailController;
use App\Http\Controllers\Api\Cabinet\Verification\Contacts\ContactsPhoneController;
use App\Http\Controllers\Api\Cabinet\Verification\VerifyEmailController;
use App\Http\Controllers\Api\Cabinet\Verification\VerifyPhoneController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CityController;
use App\Http\Controllers\Api\CurrencyController;
use App\Http\Controllers\Api\FaqController;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\NewsCategoryController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\Order\Bid\BidCancelController;
use App\Http\Controllers\Api\Order\Bid\BidCreateController;
use App\Http\Controllers\Api\Order\Bid\BidUpdateController;
use App\Http\Controllers\Api\Order\Chat\OrderChatController;
use App\Http\Controllers\Api\Order\CloseDealController;
use App\Http\Controllers\Api\Order\CreateOrderController;
use App\Http\Controllers\Api\Order\DealReviewsController;
use App\Http\Controllers\Api\Order\DeleteOrderUploadController;
use App\Http\Controllers\Api\Order\IndexOrderController;
use App\Http\Controllers\Api\Order\SelectPerformerController;
use App\Http\Controllers\Api\Order\ShowOrderController;
use App\Http\Controllers\Api\Order\UpdateOrderController;
use App\Http\Controllers\Api\PageController;
use App\Http\Controllers\Api\Platform\CustomerController;
use App\Http\Controllers\Api\Platform\PerformersController;
use App\Http\Controllers\Api\ReviewsController;
use App\Http\Controllers\Api\Seo\RedirectSendController;
use App\Http\Controllers\Api\Seo\SendMetaController;
use App\Http\Controllers\Api\SettingController;
use App\Http\Controllers\Api\Statics\HowController;
use App\Http\Controllers\Api\Statics\NeedController;
use App\Http\Controllers\Api\Statics\TeamController;
use App\Http\Controllers\Api\User\ApiUserController;
use App\Http\Controllers\Api\User\UserNotificationsController;
use App\Platform\Route\RegisterStepsUrls;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//

Route::prefix('')->group(function () {
	Route::post('login/check', [LoginController::class, 'checkLogin'])->name('login.check');
	Route::post('login', [LoginController::class, 'login'])->name('login');
	Route::post('login/social/google', [SocialLoginController::class, 'google'])->name('login.google');
	Route::post('login/social/facebook', [SocialLoginController::class, 'facebook'])->name('login.facebook');
//
	Route::post('register/init', [InitRegisterTokenController::class, 'index'])->name('register.init');
	Route::middleware(['registerToken',])
		->group(function () {
			Route::post('register/social/google', [SocialRegisterController::class, 'google'])->name('register.google');
			Route::post('register/social/facebook', [SocialRegisterController::class, 'facebook'])->name('register.facebook');
			Route::post(RegisterStepsUrls::STEP_CHOOSE_TYPE, [ApiRegisterTypeController::class, 'index'])->name('register.type');
			Route::post(RegisterStepsUrls::STEP_DATA_CUSTOMER, [ApiRegisterCustomerDataController::class, 'index'])->name('register.customer.personal');
			Route::post(RegisterStepsUrls::STEP_DATA_PERFORMER, [ApiRegisterPerformerDataController::class, 'index'])->name('register.performer.personal');
			Route::post(RegisterStepsUrls::STEP_PERFORMER_CONFIRM_EMAIL, [ApiRegisterPerformerConfirmEmailController::class, 'index'])->name('register.performer.email.confirm');
			Route::post(RegisterStepsUrls::STEP_PERFORMER_CONFIRM_EMAIL_RESEND, [ApiRegisterPerformerConfirmEmailController::class, 'resend'])->name('register.performer.email.resend');
			Route::post(RegisterStepsUrls::STEP_PERFORMER_CONFIRM_PHONE, [ApiRegisterPerformerConfirmPhoneController::class, 'index'])->name('register.performer.phone.confirm');
			Route::post(RegisterStepsUrls::STEP_PERFORMER_CONFIRM_PHONE_RESEND, [ApiRegisterPerformerConfirmPhoneController::class, 'resend'])->name('register.performer.phone.resend');
		})
	;
	Route::middleware(['guest'])
		->group(function () {
			Route::post('password/email', [ForgotPasswordController::class, 'send'])->name('password.email');
			Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.reset');
		})
	;
	Route::middleware('auth:sanctum')->group(function () {
		Route::get('/user/info', [ApiUserController::class, 'user'])->name('user.info');
		Route::get('/user/notifications', [UserNotificationsController::class, 'index']);
		Route::get('/user/notifications/unread', [UserNotificationsController::class, 'indexUnread']);
		Route::post('/user/notifications/read', [UserNotificationsController::class, 'markAsRead']);
		Route::post('logout', [LogoutController::class, 'logout'])->name('logout');
	});
	Route::get('currency', [CurrencyController::class, 'index']);
	Route::get('settings', [SettingController::class, 'index']);
	Route::get('cities', [CityController::class, 'cities']);
	Route::get('categories', [CategoryController::class, 'index']);
	Route::get('news/categories', [NewsCategoryController::class, 'index']);
	Route::get('news', [NewsController::class, 'index']);
	Route::get('news/{url}', [NewsController::class, 'show']);
	Route::get('faq', [FaqController::class, 'index']);
	Route::get('reviews', [ReviewsController::class, 'index']);
	Route::get('how-it-works', [HowController::class, 'index']);
	Route::prefix('for-performer')->group(function () {
		Route::get('how-it-works', [HowController::class, 'performer']);
		Route::get('need', [NeedController::class, 'index']);
	});
	Route::get('teams', [TeamController::class, 'index']);
	Route::prefix('pages')->group(function () {
		Route::get('about', [PageController::class, 'about']);
		Route::get('offer', [PageController::class, 'offer']);
		Route::get('privacy', [PageController::class, 'privacy']);
		Route::get('main', [PageController::class, 'main']);
		Route::get('for-performer', [PageController::class, 'forPerformer']);
		Route::get('payment-and-return', [PageController::class, 'paymentAndReturn']);
	});

	Route::post('feedback/report', [FeedbackController::class, 'report'])->name('feedback.report');

	Route::middleware('auth:sanctum')->prefix('account/cabinet')->group(function () {
		Route::post('personal', [PersonalDataController::class, 'submit']);
		Route::post('switch-type', [SwitchUserTypeController::class, 'switch']);
		//
		Route::post('certification', [PerformerCertificationController::class, 'upload']);
		Route::get('certification', [PerformerCertificationController::class, 'index']);

		Route::apiResource('services', PerformerServicesController::class)->except('show');
		Route::apiResource('diplomas', PerformerDiplomaController::class)->except('show');

		Route::post('password', [ChangePasswordController::class, 'submit']);

		Route::get('notifications/config', [NotificationController::class, 'index']);
		Route::put('/notifications/config/sync', [NotificationController::class, 'sync']);
		//CHANGE contacts
		Route::post('phone', [ContactsPhoneController::class, 'send']);
		Route::post('phone/verify', [ContactsPhoneController::class, 'verify']);
		Route::post('phone/change', [ContactsPhoneController::class, 'change']);
		Route::post('email', [ContactsEmailController::class, 'send']);
		Route::post('email/verify', [ContactsEmailController::class, 'verify']);
		Route::post('email/change', [ContactsEmailController::class, 'change']);
		// VERIFICATION
		Route::prefix('verification')->group(function () {
			Route::post('/phone', [VerifyPhoneController::class, 'send']);
			Route::post('/phone/verify', [VerifyPhoneController::class, 'verify']);
			Route::post('/email', [VerifyEmailController::class, 'send']);
			Route::post('/email/verify', [VerifyEmailController::class, 'verify']);
		});
		//

		Route::get('deals/active', [MyDealsController::class, 'getActiveDeals']);
		Route::get('deals/completed', [MyDealsController::class, 'getCompletedDeals']);
		Route::get('chats', [IndexChatController::class, 'index']);
		Route::get('chats/count/unread', [ShowChatController::class, 'countUnread']);
		Route::get('chats/{chatId}', [ShowChatController::class, 'show']);
		Route::get('chats/{chatId}/load', [ShowChatController::class, 'loadMessages'])->name('chats.show.load');
		Route::post('chats/{chatId}', [ChatSendMessageController::class, 'store']);
		Route::post('chats/{chatId}/files', [ChatSendMessageController::class, 'files']);
		Route::post('chats/{chatId}/watched', [MessagesWatchedController::class, 'watched']);
		Route::post('chats/{chatId}/favorites', [FavoritesChatController::class, 'toggle']);
		Route::prefix('premium')->group(function () {
			Route::get('index', [PremiumAccountController::class, 'index'])->name('premium.index');
			Route::post('apply-free', [PremiumAccountController::class, 'applyFreePremium'])->name('premium.free.apply');
			Route::get('create-payment', [PremiumAccountController::class, 'createPaymentForm'])->name('premium.payment.create');
		});
	});
	Route::any('premium/payment/callback', [PremiumAccountPaymentController::class, 'paymentCallback'])->name('premium.payment.callback');

	Route::get('seo/meta', [SendMetaController::class, 'index']);
	Route::get('seo/redirects', [RedirectSendController::class, 'index']);


	Route::apiResource('performers', PerformersController::class)->only('index', 'show');
	Route::get('performers/{performerId}/reviews', [PerformersController::class, 'allReviews']);
	Route::apiResource('customers', CustomerController::class)->only('show');
	Route::get('customers/{customerId}/reviews', [CustomerController::class, 'allReviews']);

	Route::post('deals', [CreateOrderController::class, 'store'])->name('deals.create');
	Route::get('deals', [IndexOrderController::class, 'index']);
	Route::get('/deals/{deal}', [ShowOrderController::class, 'show']);
	Route::middleware('auth:sanctum')->prefix('deals')->group(function () {
		Route::post('complete', [CreateOrderController::class, 'completeUnAuth']);
		Route::put('/{deal}', [UpdateOrderController::class, 'update'])->name('deals.update');
		Route::post('/{deal}/close', [CloseDealController::class, 'close'])->name('deals.close');
		Route::delete('/{deal}/uploads/{uploadId}', [DeleteOrderUploadController::class, 'delete']);
		Route::post('{deal}/bid', [BidCreateController::class, 'store'])->name('deals.bids.create');
		Route::put('{deal}/bid/{bid}', [BidUpdateController::class, 'update'])->name('deals.bids.update');
		Route::post('{deal}/bid/{bid}/cancel', [BidCancelController::class, 'cancel']);
		Route::post('{deal}/select/{performerId}', [SelectPerformerController::class, 'select'])->name('deals.select-performer');
		Route::post('/{deal}/review/customer', [DealReviewsController::class, 'customer'])->name('deals.review.customer');
		Route::post('/{deal}/review/performer', [DealReviewsController::class, 'performer'])->name('deals.review.performer');
		Route::get('/{deal}/performer/{performerId}/chat', [OrderChatController::class, 'chatWithPerformer']);
	});

});
