<?php

use App\Http\Controllers\Admin\Admin\AdminController;
use App\Http\Controllers\Admin\AdminMenuController;
use App\Http\Controllers\Admin\AjaxController;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\Cabinet\CertificationsController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\Content\HowController;
use App\Http\Controllers\Admin\Content\HowPerformerController;
use App\Http\Controllers\Admin\Content\MainBottomBlocksController;
use App\Http\Controllers\Admin\Content\MainServicesController;
use App\Http\Controllers\Admin\Content\NeedController;
use App\Http\Controllers\Admin\Content\PerformerServicesController;
use App\Http\Controllers\Admin\Content\ReviewsController;
use App\Http\Controllers\Admin\Content\TeamController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\Feedback\FeedbackController;
use App\Http\Controllers\Admin\IndexController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\Meta\MetaController;
use App\Http\Controllers\Admin\Meta\RedirectController;
use App\Http\Controllers\Admin\Meta\RobotController;
use App\Http\Controllers\Admin\Meta\SitemapController;
use App\Http\Controllers\Admin\News\NewsCategoryController;
use App\Http\Controllers\Admin\News\NewsController;
use App\Http\Controllers\Admin\NotificationConfigController;
use App\Http\Controllers\Admin\Orders\OrderBidsController;
use App\Http\Controllers\Admin\Orders\OrderChatsController;
use App\Http\Controllers\Admin\Orders\OrderController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\PhotosController;
use App\Http\Controllers\Admin\Premium\PremiumController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\Staff\LogViewController;
use App\Http\Controllers\Admin\TranslateController;
use App\Http\Controllers\Admin\User\UserController;
use App\Http\Controllers\Admin\User\UserPremiumController;

Route::group(['prefix' => \Config::get('app.admin-url')], function () {
	Route::get('/login', [LoginController::class, 'showLoginForm'])->name('admin.login');
	Route::post('/login', [LoginController::class, 'login']);
	Route::post('/logout', [LoginController::class, 'logout'])->name('admin.logout');
});
Route::group(
	[
		'prefix' => LaravelLocalization::setLocale(),
		'middleware' => ['localizationRedirect'],
	], function () {

	Route::middleware(['auth:admin', 'bindings'])
		->prefix(\Config::get('app.admin-url'))
		->group(function () {
			Route::post('addpremium/{user}', [UserPremiumController::class, 'addPremium'])->name('addprem');
			Route::get('/', [IndexController::class, 'index'])->name('admin.index');
			/** @see UserController */
			Route::resource('users', UserController::class, ['as' => 'admin']);
			Route::post('/users/signsuperadmin', [UserController::class, 'signSuperAdmin'])->name('signsuperadmin');
			Route::group(['prefix' => 'profile',], function () {
				Route::get('/', [AdminController::class, 'profile'])->name('admin.profile');
				Route::post('/', [AdminController::class, 'profileUpdate'])->name('admin.profile.update');
			});

			Route::resources([
				'translate' => TranslateController::class,
				'settings' => SettingController::class,
			]);
			Route::resources([
				'how' => HowController::class,
				'how-performer' => HowPerformerController::class,
				'team' => TeamController::class,
				'need' => NeedController::class,
				'main-services' => MainServicesController::class,
				'performer-services' => PerformerServicesController::class,
				'main-bottom' => MainBottomBlocksController::class,
				'reviews' => ReviewsController::class,
			], ['as' => 'admin', 'except' => ['show'],]);

			Route::resources([
				'roles' => RoleController::class,
				'menu' => MenuController::class,
				'admin-menus' => AdminMenuController::class,
				'cities' => CityController::class,
				'news' => NewsController::class,
				'faq' => FaqController::class,
				'pages' => PageController::class,
				'premiums' => PremiumController::class,
			], ['as' => 'admin', 'except' => ['show']]
			);
			Route::resource('news/categories', NewsCategoryController::class, ['as' => 'admin.news']);

			Route::resource('certifications', CertificationsController::class, ['as' => 'admin', 'except' => ['show', 'create']]);
			Route::resource('orders', OrderController::class, ['as' => 'admin', 'only' => ['index', 'edit', 'show']]);
			Route::patch('/orders/{order}/active', [OrderController::class, 'toggleActive']);
			Route::apiResource('/orders/{order}/chats', OrderChatsController::class)->only(['index', 'show']);
			Route::get('/orders/{order}/chats/{chat}/load', [OrderChatsController::class, 'loadMessages']);
			Route::prefix('orders')->group(function () {
				Route::apiResource('{order}/bids', OrderBidsController::class, ['only' => ['index', 'destroy']]);
			});
			Route::apiResource('notifications/config', NotificationConfigController::class, ['as' => 'admin.notifications'])->only(['index', 'update']);

			Route::post('/admin-menus/nesting', [AdminMenuController::class, 'nesting'])->name('admin.admin-menus.nesting');
			Route::patch('/admin-menus/save/all', [AdminMenuController::class, 'updateAll'])->name('admin-menus.updateAll');

			Route::get('/feedback/{type?}', [FeedbackController::class, 'index'])->name('admin.feedback.index');
			Route::match(['DELETE', 'POST'], 'feedback/{feedback}', [FeedbackController::class, 'destroy'])->name('admin.feedback.destroy');

			Route::group([], function () {
				/** @see \App\Http\Controllers\Admin\Meta\MetaController */
				Route::resource('meta', MetaController::class, ['parameters' => ['meta' => 'meta'], 'as' => 'admin', 'except' => ['show']]);
				Route::resource('redirects', RedirectController::class, ['as' => 'admin', 'except' => ['show']]);

				Route::get('/robots', [RobotController::class, 'index'])->name('admin.robots.index');
				Route::put('/robots', [RobotController::class, 'update'])->name('admin.robots.update');

				Route::resource('/sitemap', SitemapController::class, ['only' => ['index', 'store'], 'as' => 'admin']);
			});

			Route::group(['prefix' => 'photos'], function () {
				Route::post('/edit', [PhotosController::class, 'edit']);
				Route::post('/delete', [PhotosController::class, 'delete']);
				Route::match(['get', 'post'], '/get-cropper', [PhotosController::class, 'getPhotoCropper']);
			});
			Route::group(['as' => 'settings.', 'prefix' => 'settings',], function () {
				Route::get('{id}/delete_value', [
					'uses' => [SettingController::class, '@delete_value'],
					'as' => 'delete_value',
				]);
			});

			Route::group(['prefix' => 'ajax'], function () {
				Route::post('/sort', [AjaxController::class, 'sort'])->name('sort');
				Route::post('/delete', [AjaxController::class, 'delete'])->name('delete');
			});


			Route::prefix('dashboard')
				->group(function () {
					Route::post('/cache/clear', [IndexController::class, 'clearCache'])->name('cache.clear');
					Route::post('/cache/clear/view', [IndexController::class, 'clearView'])->name('cache.view');
					Route::post('/artisan/storage-link', [IndexController::class, 'storageLink'])->name('artisan.storage.link');
					Route::post('/artisan/refresh-db', [IndexController::class, 'refreshDb'])->name('artisan.db.refresh');
					Route::get('/counters', [IndexController::class, 'getCounters'])->name('dashboard.counters');
				});

			Route::get('logs', [LogViewController::class, 'index']);

		});
});