<?php
Broadcast::channel('chats.{chatId}', \App\Broadcasting\UserChatChannel::class);
Broadcast::channel('chats.user.{userId}', \App\Broadcasting\UserChatsChannel::class);
Broadcast::channel('deals.new', \App\Broadcasting\NewDealsChannel::class);
Broadcast::channel('deals.{dealId}.new-bid', \App\Broadcasting\NewDealBidChannel::class);
Broadcast::channel('users.notifications.{user}',\App\Broadcasting\UsersNotificationsChannel::class);
