<?php


use App\Http\Controllers\HomeController;

Route::group(
	[
		'prefix'     => LaravelLocalization::setLocale(),
		'middleware' => ['localizationRedirect'],
	], function () {

	Route::get('/', [HomeController::class, 'index'])->name('home');
});
